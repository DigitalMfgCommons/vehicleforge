package com.ge.research.ceed.seleniumuitests;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;


/**
 * Unit test for testing logins for Forge.
 */
public abstract class BaseTest {


    static HtmlUnitDriver driver;
    static String baseUrl;
    static StringBuffer verificationErrors = new StringBuffer();

   
    
    @BeforeClass
    public static void setUp() throws Exception {
    	String browserName = System.getProperty("browser", "none").toLowerCase();
    	
    	
    	if(browserName.equals("none")) {
    		driver = new HtmlUnitDriver(TestUtils.ENABLE_JAVASCRIPT);
    	} else {
    		BrowserVersion version = null;
        	if (browserName.equals("chrome")) {
        		version = BrowserVersion.CHROME;
        	} else if (browserName.equals("firefox")) {
        		version = BrowserVersion.FIREFOX_24;
        	} else if (browserName.equals("ie")) {
        		version = BrowserVersion.INTERNET_EXPLORER_11;
        	} else {
        		fail("Unknown browser " + browserName);
        	}
    		driver = new HtmlUnitDriver(version);//org.openqa.selenium.chrome.ChromeDriver);//System.getProperty("browser"); //TestUtils.ENABLE_JAVASCRIPT);
    	}

    	
    	baseUrl = TestUtils.BASE_URL;
        driver.manage().timeouts().implicitlyWait(TestUtils.DEFAULT_IMPLICIT_TIMEOUT_SECONDS, TimeUnit.SECONDS); 
        RegisterSelenium();
        
    }   
    
    public static void RegisterSelenium() throws Exception {
    	try {
    		driver.manage().deleteAllCookies();
   		
            driver.get(baseUrl);
          
            if (!driver.getCurrentUrl().endsWith("index2.php")) {
                fail(" Public logging into the system has failed. " + driver.getCurrentUrl());
            }          
           

        } catch (Exception e) {
            System.out.println("*** TEST Failure ***");
            System.out.println("URL : " + driver.getCurrentUrl());
            System.out.println("Title : " + driver.getTitle());

            fail(e.getLocalizedMessage());
        }	
    	assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
    	
    	//Register Selenium Tester Account
    	driver.findElement(By.linkText("Register")).click();
        assertTrue(driver.getCurrentUrl().endsWith("account/register2.php"));
        
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys(TestUtils.USER_EMAIL);
		
		driver.findElement(By.id("unix_name")).clear();
		driver.findElement(By.id("unix_name")).sendKeys(TestUtils.CREDENTIAL_FORGE_USER);
		
		driver.findElement(By.id("password1")).clear();
		driver.findElement(By.id("password1")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
		
		driver.findElement(By.id("password2")).clear();
		driver.findElement(By.id("password2")).sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
		
		driver.findElement(By.id("firstname")).clear();
		driver.findElement(By.id("firstname")).sendKeys("Selenium");
		
		driver.findElement(By.id("lastname")).clear();
		driver.findElement(By.id("lastname")).sendKeys("Tester");
		
		driver.findElement(By.id("accept_conditions")).click();
		
		driver.findElement(By.name("submit")).click();
		
		assertTrue(driver.getCurrentUrl().endsWith("/account/register2.php"));
		/*System.out.print("Found Index :" );
		System.out.println(driver.getPageSource().indexOf("That username already exists."));*/
		
		//If created new user successfully, then run the below to approve the Selenium Tester.
		if (driver.getPageSource().indexOf("That username already exists.") == -1) {
			AdminLogin();
			ApproveSelenium();	
			Logout();			
		}
		
		
  	    SeleniumLogin();  	      
  	    CreateTestDome();
  	    
  	  /*  System.out.print("Found Index :" );
		System.out.println(driver.getPageSource().indexOf("Unix name already taken"));
		*/
		if (driver.getPageSource().indexOf("Unix name already taken") == -1) {
  	    Logout();
  	    AdminLogin();
  	    ApproveProject();	  	    
		Logout();
  	    SeleniumLogin(); 
  	 
  	  driver.get(baseUrl + "projects/testdemo0526");
      assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
      driver.findElement(By.linkText("Components")).click();
      assertTrue(driver.getCurrentUrl().indexOf("components/?group_id=") != -1);
		
      //Test Add Component button
      driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
      driver.findElement(By.id("addComponent")).click();
		driver.findElement(By.id("cname")).clear();
		driver.findElement(By.id("cname")).sendKeys("ttt");
		driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
  	    
		}					
		Logout();  	
	}

	private static void Logout() {
		driver.findElement(By.xpath("//div[4]/ul/li[4]/a")).click();
	    driver.findElement(By.xpath("//li[4]/ul/li[3]/a")).click();
	    assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
		
	}

	private static void CreateTestDome() {
		String currentURL = baseUrl + "register/";
        driver.get(currentURL);
    
        WebElement element = driver.findElement(By.name("full_name"));
        element.click();

      
        element.sendKeys("Test Demo");
        
        element = driver.findElement(By.name("purpose"));
        element.click();
        element.sendKeys("To test Test Demo project creation.");

        element = driver.findElement(By.name("description"));
        element.click();
        element.sendKeys("Test Demo Project - To be used by selenium test for testing project admin");

        element = driver.findElement(By.name("unix_name"));
        element.click();
        element.sendKeys("testdemo0526" );


        element = driver.findElement(By.name("submit"));
        element.submit();	      		
	}

	
	private static void ApproveProject() {
		 driver.findElement(By.linkText("Pending Projects")).click();
	        //assertTrue(driver.getCurrentUrl().endsWith("admin/quickpass_pending_user.php"));
	        driver.findElement(By.xpath("//form-right/button[1]")).click();
	        driver.findElement(By.name("approve-all")).click(); 
	}
	
	private static void ApproveSelenium() {
		     	
        driver.findElement(By.linkText("Pending Users")).click();
        //assertTrue(driver.getCurrentUrl().endsWith("admin/quickpass_pending_user.php"));
        driver.findElement(By.xpath("//form-right/button[1]")).click();
        driver.findElement(By.name("approve-all")).click();    
	   /* driver.findElement(By.xpath("//div[4]/ul/li[4]/a")).click();
	    driver.findElement(By.xpath("//li[4]/ul/li[3]/a")).click();
	    assertTrue(driver.getCurrentUrl().endsWith("index2.php"));*/
		
	}
	
	private static void AdminLogin() {
		WebElement element = driver.findElement(By.id("site_login"));
        element.click();   
        
        element = driver.findElement(By.name("form_loginname"));
        element.click();
        element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_USER);
        
        element = driver.findElement(By.name("form_pw"));
        element.click();
        element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_PASS);
        
        element = driver.findElement(By.className("btn"));
        element.submit();
       
        assertTrue(driver.getCurrentUrl().endsWith("my/"));	
		
	}

	private static void SeleniumLogin() {
		 WebElement element = driver.findElement(By.id("site_login"));
         element.click();   
         
         element = driver.findElement(By.name("form_loginname"));
         element.click();
         element.sendKeys(TestUtils.CREDENTIAL_FORGE_USER);
         
         element = driver.findElement(By.name("form_pw"));
         element.click();
         element.sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
         
         element = driver.findElement(By.className("btn"));
         element.submit();
        
         assertTrue(driver.getCurrentUrl().endsWith("my/"));	
 	      
		
	}
	
	

	@AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }
   
    


 /**
     * Test the login page that protects the overall site from public access.
     */
    @Test
    public final void testPublicLoginProtection() throws Exception {

    	try {
    		driver.manage().deleteAllCookies();
   		
            driver.get(baseUrl);
            /**
            WebElement element = driver.findElement(By.name("user"));
            element.click();
            element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_USER);

            element = driver.findElement(By.name("pass"));
            element.click();
            element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_PASS);

            element = driver.findElement(By.name("login"));
            element.submit();
            **/
            if (!driver.getCurrentUrl().endsWith("index2.php")) {
                fail(" Public logging into the system has failed.");
            }          
           

        } catch (Exception e) {
            System.out.println("*** TEST Failure ***");
            System.out.println("URL : " + driver.getCurrentUrl());
            System.out.println("Title : " + driver.getTitle());

            fail(e.getLocalizedMessage());
        }
    }
    
    

    
    /**
     * Test logging into the actual Forge system and verifying the request is redirected to IP
     * and that user gets directed to the homepage.
     * Login as Selenium Tester
     */
    @Test
    public void testForgeLogin() throws Exception {

       // Login into the public section of the site. 
       
	   if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
           testPublicLoginProtection();
       } 
	   else {
       
   		driver.manage().deleteAllCookies();
	   }
            
       // Click on Log In in the page.
       WebElement element = driver.findElement(By.id("site_login"));
       element.click();   
       
       element = driver.findElement(By.name("form_loginname"));
       element.click();
       element.sendKeys(TestUtils.CREDENTIAL_FORGE_USER);
       
       element = driver.findElement(By.name("form_pw"));
       element.click();
       element.sendKeys(TestUtils.CREDENTIAL_FORGE_PASS);
       
       element = driver.findElement(By.className("btn"));
       element.submit();
       
//       assertTrue(driver.getPageSource().indexOf("Your account does not exist.") == -1);
//       assertTrue(driver.getPageSource().indexOf("Invalid Password Or User Name") == -1);
       assertTrue(driver.getCurrentUrl().endsWith("my/"));
       //driver.findElement(By.xpath("//li[4]/ul/li[3]/a")).click();
       
      
    }
    
    
    
    
   /**
     * Test logging into the actual Forge system and verifying the request is redirected to IP
     * and that user gets directed to the homepage.
     * Login as Forge Admin
     */ 
    @Test
   public void testForgeAdminLogin() throws Exception{
      
      if (TestUtils.CREDENTIAL_GATEWAY_REQUIRED) {
            testPublicLoginProtection();
        }
        else{
           driver.manage().deleteAllCookies();
        }
        
        
      // Click on Log In in the page.
       WebElement element = driver.findElement(By.id("site_login"));
       element.click();   
       
       element = driver.findElement(By.name("form_loginname"));
       element.click();
       element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_USER);
       
       element = driver.findElement(By.name("form_pw"));
       element.click();
       element.sendKeys(TestUtils.CREDENTIAL_GATEWAY_PASS);
       
       element = driver.findElement(By.className("btn"));
       element.submit();
       
       
//       assertTrue(driver.getPageSource().indexOf("Your account does not exist.") == -1);
//       assertTrue(driver.getPageSource().indexOf("Invalid Password Or User Name") == -1);
       assertTrue(driver.getCurrentUrl().endsWith("my/"));
       //driver.findElement(By.xpath("//li[4]/ul/li[3]/a")).click();
        
   
   }
    
    
    
   
    
    
    
}
