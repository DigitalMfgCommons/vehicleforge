package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.fail;
import org.junit.Test;

/**
 * Unit test for testing different URL scenarios while a user is logged in to the Forge.
 * Test to see if someone can access direct pages directly through guessing the URL.
 */
public class SecurityUrlParametersTest extends BaseTest {

    /**
     * If the page does not state the error "No User Name Provided" or 
     * "That user does not exist" then the test should fail.
     */
    @Test
    public void testInvalidParameterUserProfile01() throws Exception {

        testForgeLogin();       
        
        // supplying no username
        driver.get(baseUrl + "users/");
        String pageSource = driver.getPageSource();
        
        if (pageSource.indexOf("No User Name Provided") == -1) {
            fail("Page should have stated \"No User Name Provided\" due to intentional invalid URL access.");
        }
        
        // supplying a fake username
        driver.get(baseUrl + "users/fakeusername");
        pageSource = driver.getPageSource();
        
        if (pageSource.indexOf("That user does not exist") == -1) {
            fail("Page should have stated \"That user does not exist\" due to intentional invalid URL access.");
        }        
    }

    /**
     * Test omitting parameter group_id value from the URL to see
     * if someone can access a project or a list of projects.
     */
    @Test
    public void testInvalidParameterTracker01() throws Exception {

        testForgeLogin();
        driver.get(baseUrl + "tracker/?group_id=");
        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional missing parameter.");
        }
    }

    /**
     *
     */
    @Test
    public void testInvalidParameterTracker02() throws Exception {

        testForgeLogin();
        
        // use NEGATIVE group_id value
        driver.get(baseUrl + "tracker/?group_id=-28348384");

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     *
     */
    @Test
    public void testInvalidParameterTracker03() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id=ABC123");

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using special characters.
     */
    @Test
    public void testInvalidParameterTracker04() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id=987654321");  // this test will fail when we are wildly successful

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using code.
     */
    @Test
    public void testInvalidParameterTracker05() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id=" + TestUtils.encodeUrl("<% System.exit(1); %>"));

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using no parameter in URL.
     */
    @Test
    public void testInvalidParameterTracker06() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?");

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using special characters.
     */
    @Test
    public void testInvalidParameterTracker07() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id='");

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using special characters.
     */
    @Test
    public void testInvalidParameterTracker08() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id=\"");

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using special characters.
     */
    @Test
    public void testInvalidParameterTracker09() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id=" + TestUtils.encodeUrl("<%"));

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }

    /**
     * Test invalid parameter using special characters.
     */
    @Test
    public void testInvalidParameterTracker10() throws Exception {

        testForgeLogin();
        
        // use invalid group_id value
        driver.get(baseUrl + "tracker/?group_id=" + TestUtils.encodeUrl("%>"));

        String pageSource = driver.getPageSource();

        if (pageSource.indexOf("Permission denied") == -1) {
            fail("Page should have stated \"Permission denied\" due to intentional parameter corruption.");
        }
    }
}
