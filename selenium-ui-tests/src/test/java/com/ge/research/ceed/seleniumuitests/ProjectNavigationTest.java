package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.concurrent.TimeUnit;

/**
 * Unit test for Forge projects.
 * To make sure the test cases for project page could run successfully, 
 * you must make sure you have loged in as Selenium Tester, 
 * and created one project named as "testing1", 
 * and add one components named as "ttt" under the Template Project!
 */
public class ProjectNavigationTest extends BaseTest {
    /**
     * Test submitting a project request without an SCM.
     */
    @Test
    public void testCreateProjectNoSCM() throws Exception {

        testForgeLogin();
 
        String currentURL = baseUrl + "register/";
        driver.get(currentURL);
    
        WebElement element = driver.findElement(By.name("full_name"));
        element.click();

        element.sendKeys("Selenium Automated Tests - " + TestUtils.ran);
        
        element = driver.findElement(By.name("purpose"));
        element.click();
        element.sendKeys("To test automated project creation.");

        element = driver.findElement(By.name("description"));
        element.click();
        element.sendKeys("Test Project - To Be Deleted. Do not use.");

        element = driver.findElement(By.name("unix_name"));
        element.click();
        element.sendKeys("test" + TestUtils.ran);


        element = driver.findElement(By.name("submit"));
        element.submit();
        
        //assertTrue(driver.getTitle().equals("VehicleForge: Registration complete"));
        //assertTrue(driver.getPageSource().indexOf("Approving Project:" + driver.findElement(By.name("unix_name")))!= -1);
        assertTrue(driver.getPageSource().indexOf("Approval ERROR: Permission denied.") != -1);
    }
    
  
    
    
    /**
     * After logging in, test the Projects Tab within main menu.
    */
    @Test
    public void testProjectListNavigation() throws Exception{
     
        testForgeAdminLogin();
        driver.findElement(By.linkText("Projects")).click();
        assertTrue(driver.getCurrentUrl().endsWith("softwaremap/full_list.php"));
        
        driver.findElement(By.linkText("Test Demo")).click();
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Projects")).click();
        
       
    
    }
    
    /**
     * After logging in and selecting a project, test sub-menu items.
     * You must make sure you load a particular project page before running this test.
     */
    @Test
    public void testProjectNavigationMenu() throws Exception {

    	testForgeLogin();
           
        // Load a particular project page before you test this links
        driver.get(baseUrl + "projects/testdemo0526");
        
        driver.findElement(By.linkText("Project Home")).click();
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        
        driver.findElement(By.linkText("Components")).click();
        assertTrue(driver.getCurrentUrl().indexOf("components/?group_id=") != -1);
        
        driver.findElement(By.linkText("Services")).click();
        assertTrue(driver.getCurrentUrl().indexOf("services/?group_id=") != -1);
        
        driver.findElement(By.linkText("Activity")).click();
        assertTrue(driver.getCurrentUrl().indexOf("activity/?group_id=") != -1);
        
    }
    
    /**
     * Project Home Section: Add and delete an activity.
     */
    @Test
    public void testProjectHome() throws Exception {

    	testForgeLogin();
           
        // Load a particular project page before you test this links
        driver.get(baseUrl + "projects/testdemo0526");
        driver.findElement(By.linkText("Project Home")).click();
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        
        // Add and then delete an activity (twice)
		driver.findElement(By.id("home_comment_text")).clear();
		driver.findElement(By.id("home_comment_text")).sendKeys("This activity is created true as a selenium test.");
		driver.findElement(By.id("home_comment_submit")).click();
		driver.findElement(By.linkText("Delete")).click();
		
		driver.findElement(By.id("home_comment_text")).clear();
		driver.findElement(By.id("home_comment_text")).sendKeys("This is another activity created after the first one was deleted.");
		driver.findElement(By.id("home_comment_submit")).click();
		driver.findElement(By.linkText("Delete")).click();
    }
    
    /**
     * Project Components Section: Add a component, tag it, and navigate
     * each sub-menu: File, Sub-Components, and Services.
     */
    @Test
    public void testProjectComponents() throws Exception {

    	testForgeLogin();
           
        // Load a particular project page before you test this links
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Components")).click();
        assertTrue(driver.getCurrentUrl().indexOf("components/?group_id=") != -1);
		
        //Test Add Component button
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.findElement(By.id("addComponent")).click();
		driver.findElement(By.id("cname")).clear();
		driver.findElement(By.id("cname")).sendKeys("Selenium added component for test 1");
		driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		//driver.findElement(By.linkText("Selenium added component for test 1")).click();
		driver.findElement(By.id("addTagField")).clear();
		driver.findElement(By.id("addTagField")).sendKeys("Selenium added tag for test 1");
		driver.findElement(By.cssSelector("input.btn.btn-small")).click();
		driver.findElement(By.linkText("Files")).click();
		driver.findElement(By.linkText("Sub-Components")).click();
		driver.findElement(By.linkText("Details")).click();
		driver.findElement(By.linkText("Services")).click();	
    }
    
    
   /* 
     @Test
    public void testProjectSearchComponents() throws Exception {

        testForgeLogin();
           
        // Load a particular project page before you test this links
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Components")).click();
        assertTrue(driver.getCurrentUrl().indexOf("/components/?group_id=") != -1);
        
        WebElement element = driver.findElement(By.xpath(".//*[@id='component-search']/input"));
        element.click();
        element.sendKeys("ttt" + "\n" + "\n");
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        assertTrue(driver.getCurrentUrl().indexOf("components/?group_id=&cid=") != -1);
    }
  */  
    
    
    @Test
    public void testProjectServices() throws Exception {
    	
    	testForgeLogin();
    	
    	// Load a particular project page before you test this links
        //driver.get(baseUrl + "projects/testproject");
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Services")).click();
        assertTrue(driver.getCurrentUrl().indexOf("services/?group_id=") != -1);
    	
        driver.findElement(By.xpath(".//*[@id='service_nav']/li[1]/a")).click();
        assertTrue(driver.getCurrentUrl().indexOf("services/index.php?group_id=") != -1);
        		
		driver.findElement(By.xpath(".//*[@id='service_nav']/li[2]/a")).click();
		assertTrue(driver.getCurrentUrl().indexOf("services/register.php?group_id=") != -1);
		
		driver.findElement(By.xpath(".//*[@id='service_nav']/li[3]/a")).click();
		assertTrue(driver.getCurrentUrl().indexOf("services/publish.php?group_id") != -1);
		
		driver.findElement(By.xpath(".//*[@id='service_nav']/li[4]/a")).click();
		assertTrue(driver.getCurrentUrl().indexOf("services/integrate.php?group_id") != -1);
		
        //test for "1 Connect Services"
		driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div[2]/ul/li[1]/a")).click();
        //test for "2 Build Runtime Interface"
		driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div[2]/ul/li[2]/a")).click();
        //test for "3 Test and Save"
		driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div[2]/ul/li[3]/a")).click();
    }
    
    
    
     @Test
     public void testProjectActivity() throws Exception {
    	
    	 testForgeLogin();
        
        
        // Load a particular project page before you test this links
        //driver.get(baseUrl + "projects/testproject");
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Activity")).click();
        assertTrue(driver.getCurrentUrl().indexOf("activity/?group_id=") != -1);
        
        driver.findElement(By.xpath(".//*[@id='body']/div[1]/div[2]/div/div/form/table/tbody/tr[2]/td[4]/input")).click();
        assertTrue(driver.getCurrentUrl().indexOf("activity/index.php?group_id") != -1);
        
     }
       
  
    
     /**
     * Project Admin Section: test the Admin feature for project
     * each sub-menu: Project Information, Users and Permissions, Tools, Project History
     */
   
     @Test
     public void testProjectAdmin() throws Exception {
    	
    	testForgeAdminLogin();
        
        
        // Load a particular project page before you test this links
        //driver.get(baseUrl + "projects/testdemo0526");
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Admin")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/?group_id=") != -1);
        
        driver.findElement(By.linkText("Project Information")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/?group_id=") != -1);
        
        driver.findElement(By.linkText("Users and Permissions")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/users.php?group_id=") != -1);
        
        driver.findElement(By.linkText("Tools")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/tools.php?group_id=") != -1);
        
        driver.findElement(By.linkText("Project History")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/history.php?group_id=") != -1);
        
        
     }
     
     
     /**
     * Project Admin Section: Project Information
     * update project information
     */
   
     @Test
     public void testProjectAdminProjectInformation() throws Exception {
    	
    	testForgeAdminLogin();
        
        
        // Load a particular project page before you test this links
        //driver.get(baseUrl + "projects/testdemo");
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Admin")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/?group_id=") != -1);
        
        driver.findElement(By.linkText("[Edit Trove]")).click();
        new Select(driver.findElement(By.cssSelector("select[name=\"root1[1]\"]"))).selectByVisibleText("Intended Audience :: Developers");
        driver.findElement(By.cssSelector("input[name=\"submit\"]")).click();
        driver.findElement(By.cssSelector("input[name=\"submit\"]")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Project information updated[\\s\\S]*$"));
        
     }
    
    
    
     /**
     * Project Admin Section: Project Tools
     * Add Use Documents Tool to project
     */
   
     @Test
     public void testProjectAdminProjectTools() throws Exception {
    	
    	testForgeAdminLogin();
        
        
        // Load a particular project page before you test this links
        driver.get(baseUrl + "projects/testdemo0526");
        assertTrue(driver.getCurrentUrl().endsWith("projects/testdemo0526"));
        driver.findElement(By.linkText("Admin")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/?group_id=") != -1);
        
        //Test User and Permission sub-tab
        driver.findElement(By.linkText("Users and Permissions")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/users.php?group_id=") != -1);
        
        //Test Tools sub-tab
        driver.findElement(By.linkText("Tools")).click();
        driver.findElement(By.cssSelector("input[name=\"use_docman\"]")).click();
        driver.findElement(By.cssSelector("input[name=\"submit\"]")).click();
        assertTrue(driver.findElement(By.cssSelector("BODY")).getText().matches("^[\\s\\S]*Project information updated[\\s\\S]*$"));
        
        //Test Project History sub-tab
        driver.findElement(By.linkText("Project History")).click();
        assertTrue(driver.getCurrentUrl().indexOf("project/admin/history.php?group_id=") != -1);
        
     }
    
    
    
    
}
