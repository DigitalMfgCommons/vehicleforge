package com.ge.research.ceed.seleniumuitests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.openqa.selenium.By;

/**
 * @author Amine Chigani
 * 
 * Unit tests for navigating through the Community section of the site
 * clicking each sub-tab and checking whether the correct page is loaded. 
 */
public class CommunityNavigationTest extends BaseTest {
 
	/** Community Tab: Click on the Community tab after login.
     * Click on the sub-tabs and check whether the correct page is loaded.
     */
	@Test
    public void testCommunitySubMenuNavigation() throws Exception {

        testForgeLogin();
        
		//Test Community tab after login
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Community sub-tab
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Members sub-tab
        driver.findElement(By.linkText("Members")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/members.php"));
        
		//Test Discussion Board sub-tab
        driver.findElement(By.linkText("Discussion Board")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/boards.php"));
        
		//Test Resources sub-tab
        driver.findElement(By.linkText("Resources")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/resources.php"));
	}
	
	/** Community Home: Click on Popular buttons.  
     * Then, click on "view all" links for all sections within the page
     * Order: Popular Challenges, Popular Projects, Popular Components,
     * Hot Discussions, Popular Resources, and Active Members.
     */
	@Test
    public void testCommunityHomeTab() throws Exception {
		
		testForgeLogin();
		
		//Test Community tab
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Popular button
        driver.findElement(By.linkText("Popular")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/index.php"));
        
		//Test Latest button
        driver.findElement(By.linkText("Latest")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/latest.php"));
        driver.findElement(By.linkText("Community")).click();
        
		//Test view all link text within Popular Challenges
		driver.findElement(By.linkText("view all")).click();
		assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
		driver.findElement(By.linkText("Community")).click();
		
		//Test view all link text within Popular Projects
		driver.findElement(By.xpath("(//a[contains(text(),'view all')])[2]")).click();
		assertTrue(driver.getCurrentUrl().endsWith("softwaremap/full_list.php"));
		driver.findElement(By.linkText("Community")).click();
		
		//Test view all link text within Popular Components
		driver.findElement(By.xpath("(//a[contains(text(),'view all')])[3]")).click();
		assertTrue(driver.getCurrentUrl().endsWith("marketplace/components.php"));
		driver.findElement(By.linkText("Community")).click();
		
		//Test view all link text within Hot Discussions
		driver.findElement(By.xpath("(//a[contains(text(),'view all')])[4]")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/boards.php"));
		driver.findElement(By.linkText("Community")).click();
		
		//Test view all link text within Popular Resources
		driver.findElement(By.xpath("(//a[contains(text(),'view all')])[5]")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/resources.php"));
		driver.findElement(By.linkText("Community")).click();
		
		//Test view all link text within Active Members
		driver.findElement(By.xpath("(//a[contains(text(),'view all')])[6]")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/members.php"));
	}
	
	
/**
     * Community Home: Click on Popular and Latest buttons.  
     * Then, click on "Read More" links for all sections within the page
     * Order: Popular Challenges, Popular Projects, Popular Components,
**/
	@Test
	public void testCommunityReadMoreButton() throws Exception{
	
	    testForgeLogin();
		
		//Test Community tab
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));	
		
		//Test Read More[1] within Popular Challenges
	    driver.findElement(By.xpath("//div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
		
		//Test Read More[2] within Popular Challenges
		driver.findElement(By.xpath("//div[2]/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
		
		//Test Read More[3] within Popular Projects
		driver.findElement(By.xpath("//div[3]/div/div[2]/div/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
		
		//Test Read More[4] within Popular Projects
		driver.findElement(By.xpath("//div[3]/div/div[2]/div[2]/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
	
	}
	
	
	/** Community Home: Click on Latest buttons.  
     * Then, click on "view all" links for all sections within the page
     * Order: Latest Challenges, Latest Projects, Latest Components,
     * Latest Discussions, New Resources, and New Members.
     */
	@Test
    public void testCommunityHomeLatestTab() throws Exception {
		
		testForgeLogin();
		
		//Test Community tab
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Latest button
        driver.findElement(By.linkText("Latest")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/latest.php"));
        
		//Test view all link text within Latest Challenges
		driver.findElement(By.xpath(".//*[@id='body']/div/div[2]/div/div[2]/div[1]/div[1]/div/div[1]/div/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("index2.php"));
		driver.findElement(By.linkText("Community")).click();
		driver.findElement(By.linkText("Latest")).click();
		
		//Test view all link text within Latest Projects
		driver.findElement(By.xpath("//div[2]/div/div/div/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("softwaremap/full_list.php"));
		driver.findElement(By.linkText("Community")).click();
		driver.findElement(By.linkText("Latest")).click();
		
		//Test view all link text within Latest Components
		driver.findElement(By.xpath("//div[3]/div/div/div/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("marketplace/components.php"));
		driver.findElement(By.linkText("Community")).click();
		driver.findElement(By.linkText("Latest")).click();
		
		//Test view all link text within Latest Discussions
		driver.findElement(By.xpath(".//*[@id='body']/div/div[2]/div/div[2]/div[2]/div[1]/div/div[1]/div/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/boards.php"));
		driver.findElement(By.linkText("Community")).click();
		driver.findElement(By.linkText("Latest")).click();
		
		//Test view all link text within New Resources
		driver.findElement(By.xpath("//div[2]/div[2]/div[2]/div/div/div/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/resources.php"));
		driver.findElement(By.linkText("Community")).click();
		driver.findElement(By.linkText("Latest")).click();
		
		//Test view all link text within New Members
		driver.findElement(By.xpath("//div[2]/div[3]/div/div/div/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/members.php"));
	}
	
	
/**
     * Community Home: Click on Popular and Latest buttons.  
     * Then, click on "Read More" links for all sections within the page
     * Order: Latest Challenges and Latest Components,
**/
	@Test
	public void testCommunityLatestReadMoreButton() throws Exception{
	
	    testForgeLogin();
		
		//Test Community tab
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));	
		
		//Test Latest button
        driver.findElement(By.linkText("Latest")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/latest.php"));
		
		//Test Read More[1] within Latest Challenges
	    driver.findElement(By.xpath("//div[1]/div/div[2]/div[1]/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
		
		//Test Read More[2] within Latest Challenges
		driver.findElement(By.xpath("//div[1]/div/div[2]/div[2]/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
		
		//Test Read More[3] within Latest Projects
		driver.findElement(By.xpath("//div[3]/div/div[2]/div/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
		
		//Test Read More[4] within Latest Projects
		driver.findElement(By.xpath("//div[3]/div/div[2]/div[2]/div[2]/a")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/"));
	
	}
	
	
	/** 
	 * Members: Type a search phrase and click Search button. 
	 * Select by Names, Type a search phrase, and then click Search.
	 * Same for skills.
     */
	@Test
    public void testCommunityMembersTab() throws Exception {
		
		testForgeLogin();
		
		//Test Community tab
        driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Members tab
        driver.findElement(By.linkText("Members")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/members.php"));
		//select the text input field
        driver.findElement(By.id("search_query")).click();
		driver.findElement(By.id("search_query")).clear();
		//type "selenium"
		driver.findElement(By.id("search_query")).sendKeys("angela");
		// click the search button
		driver.findElement(By.xpath("//form/div/button")).click(); 
		assertTrue(driver.getCurrentUrl().endsWith("community/members.php?q=angela"));
		driver.findElement(By.xpath(".//*[@id='user_link']")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/members.php?q=angela#"));
		driver.findElement(By.id("profile_link")).click();
		assertTrue(driver.getCurrentUrl().endsWith("users/angela"));
		
		
	}   
	

	/** Discussion Board: Click on various Forum Navigation menu items
	 *  sub-menu items.
	 *  Click on Newest Topics and Popular Topics tabs.
     */
	@Test
    public void testCommunityDiscussionBoardTab() throws Exception {
		
		testForgeLogin();
		
		//Test Community tab
		driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Discussion Board
        driver.findElement(By.linkText("Discussion Board")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/boards.php"));
        
        // Click General, Forum name, and then back up to the main page. 
        driver.findElement(By.linkText("General")).click();
		driver.findElement(By.linkText("Test Forum")).click();
		assertTrue(driver.getCurrentUrl().indexOf("boards.php?forum=") != -1);
		driver.findElement(By.linkText("General")).click();
		assertTrue(driver.getCurrentUrl().indexOf("boards.php?section=") != -1);
		driver.findElement(By.linkText("Message Boards Home")).click();
		driver.findElement(By.linkText("Software")).click();
		driver.findElement(By.linkText("Other")).click();
		driver.findElement(By.linkText("Popular Topics")).click();
		driver.findElement(By.linkText("Newest Topics")).click();
		driver.findElement(By.linkText("New Topic")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/boards.php?cmd=topic"));
	}
	
	/** Discussion Board: Create a new topic.
     */
	@Test
    public void testCommunityDiscussionBoardNewTopic() throws Exception {
		
		testForgeLogin();
		
		//Test Community tab
		driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Discussion Board sub-tab
        driver.findElement(By.linkText("Discussion Board")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/boards.php"));
        
        //click New Topic button to add new topic and then back up to the main page. 
        driver.get(baseUrl + "/community/boards.php");
		driver.findElement(By.linkText("New Topic")).click();
		driver.findElement(By.name("title")).click();
		driver.findElement(By.name("title")).clear();
		driver.findElement(By.name("title")).sendKeys("Test Topic");
		driver.findElement(By.name("forum_id")).click();
		driver.findElement(By.cssSelector("option[value=\"1\"]")).click();
		driver.findElement(By.name("body")).click();
		driver.findElement(By.name("body")).clear();
		driver.findElement(By.name("body")).sendKeys("This is a test topic.");
		driver.findElement(By.cssSelector("button.btn.btn-primary")).click();
		driver.findElement(By.linkText("Discussion Board")).click();
		driver.findElement(By.linkText("Test Topic")).click();
		driver.findElement(By.linkText("Discussion Board")).click();
	}
	
	/** Resources: Search for a resource. Browse resources through the left menu items. 
	 *  Click on the New Page button. Click on Recent Changes and Popular Resources tab. 
     */
	@Test
    public void testCommunityResourcesTab() throws Exception {
		
		testForgeLogin();
		
		//Test Community tab
		driver.findElement(By.linkText("Community")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/"));
        
		//Test Resources sub-tab
        driver.findElement(By.linkText("Resources")).click();
        assertTrue(driver.getCurrentUrl().endsWith("community/resources.php"));
        
		//Test Search feature within Resources page
        driver.findElement(By.id("searchQuery")).clear();
		driver.findElement(By.id("searchQuery")).sendKeys("component");
		driver.findElement(By.id("searchBtn")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/resources.php?q=" + "component"));
		
		//Test Recent Changes and Popular Resources
		driver.findElement(By.linkText("Community")).click();
		driver.findElement(By.linkText("Resources")).click();
		driver.findElement(By.linkText("Recent Changes")).click();
		driver.findElement(By.linkText("Popular Resources")).click();
		
		//Test Resources Home and Getting Started
		driver.findElement(By.linkText("Resources Home")).click();
		driver.findElement(By.linkText("Getting Started")).click();
		
		//Test Technical Manuals and FAQ
		driver.findElement(By.linkText("Technical Manuals")).click();
		driver.findElement(By.linkText("FAQ")).click();
		
		//Test View All Resources
		driver.findElement(By.linkText("View All Resources")).click();
		assertTrue(driver.getCurrentUrl().endsWith("community/resources.php?title=all"));
		
		//Test New Page
		driver.findElement(By.linkText("New Page")).click();
		driver.findElement(By.linkText("Cancel")).click();
	}
}
