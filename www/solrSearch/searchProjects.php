<?php
require_once 'env.inc.php';
require_once '../../common/include/pre.php';
require_once APP_PATH.'www/community/community_util.php';
require_once '../../common/SolrPhpClient/Apache/Solr/Service.php';
require_once SOLR_CONFIG_FILE;

global $url;
global $port;
global $path;
$solrClient = new SolrProjectClient($url,$port,$path);
if(isset($_GET['q'])) {
  $query = $_GET['q'];
} else {
  $query = '';
}

if(isset($_GET['start'])) {
  $start = $_GET['start'];
} else {
  $start = 0;
}

if(isset($_GET['resultCount'])) {
  $resultCount = $_GET['resultCount'];
} else {
  $resultCount = 100;
}

$results = $solrClient->search($query,$start,$resultCount);
echo $results;

?>