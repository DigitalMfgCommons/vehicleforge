<?php

  /**
   * @file
   * Implements a Solr proxy.
   *
   * Currently requires json_decode which is bundled with PHP >= 5.2.0.
   *
   * You must download the SolrPhpClient and store it in the same directory as this file.
   *
   *   http://code.google.com/p/solr-php-client/
   */

require_once( '../../common/SolrPhpClient/Apache/Solr/Service.php');

/* Vehicle Forge specific includes */
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once SOLR_CONFIG_FILE;


solr_proxy_main();

/**
 * Executes the Solr query and returns the JSON response.
 */
function solr_proxy_main() {

//  $url = 'ec2-50-19-6-166.compute-1.amazonaws.com';
//  $port = 8080;
//  $path = '/solr/';
//  $core = "components";
  
  global $url;
  global $port;
  global $path;
  global $core;
	$user = session_get_user();  

  if ( isset($_GET['q'])) {

    $params = array();
    
    $params['q.alt'] = '*' . $_GET['q'] . '*';

    // The names of Solr parameters that may be specified multiple times.
    $multivalue_keys = array('bf', 'bq', 'facet.date', 'facet.date.other', 'facet.field', 'facet.query', 'fq', 'pf', 'qf');

    foreach ($_GET as $key=>$value) {
      $value = urldecode($value);
      if (in_array($key, $multivalue_keys)) {
/*       	 if( $key != 'fq'){ */
             $params[$key][] = $value;
/* 				 } */
      }
      elseif ($key == 'q') {
        $keys = $value;
      }
      elseif( $key == 'core'){
	$core = $value;
      }
      else {
        $params[$key] = $value;
      }
    }
    if( isset( $core)){
      $path .= $core . '/';
      if( $core != 'users'){

			
		    // loop over groups, add fq parameter for each group
  	    $fqString = 'is_public:true';
		    if ($user) {
			    foreach( $user->getGroups() AS $g ){
			    	$fqString .= ' || unix_group_name:' . $g->getUnixName();
			    }
		    }
		    
		    // add group restriction to fq parameter
		    $params['fq'][] = $fqString;
	    
    	}
    }

    // Replace these arguments as needed.
    $solr = new Apache_Solr_Service( $url, $port, $path);

    try {
      $response = $solr->search($keys, $params['start'], $params['rows'], $params);
    }
    catch (Exception $e) {
      die($e->__toString());
    }

    print $response->getRawResponse();
  }
}

