
/* Author: Jake Berlier (210054368) */
/* The super-class that defines the contract for core-specific solr
   search result displaying functionality */

function SolrClient(resultSummaryDiv, resultListDiv) {
    this.resultSummaryDiv = resultSummaryDiv;
    this.resultListDiv = resultListDiv;
}

SolrClient.prototype.getType = function() {
	return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
    };
