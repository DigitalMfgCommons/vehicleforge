		

	<div class="hero-unit" style="margin-left: -20px;">

	   <h1 id="about" helptip-text="Welcome! Clicking on the 'help' button on each page will reveal helpful tips on how to use and navigate the site." helptip-position="left"> <? echo forge_get_config('forge_name'); ?> <span style="font-weight: 300; font-size:0.6em;">a digital manufacturing and design innovation platform</span></h1>
		<br/><h3 style="font-weight:400;">Join our community of engineers and designers and help solve some of the toughest challenges in manufacturing.  </h3>	

		
		<br/><div class="row-fluid">
			<div class="span4" style="margin-top: 7px"><div style="width: 200px; margin: 0 auto; margin-top: 25px;"><a href="community/"><img src="/themes/gforge/images/community-icon.png"></a></div></div>
			<div class="span4" style="margin-top: 7px"><div style="width: 200px; margin: 0 auto; margin-top: 25px;"><a href="softwaremap/"><img src="/themes/gforge/images/project-icon.png"></a></div></div>
			<div class="span4" style="margin-top: 7px"><div style="width: 200px; margin: 0 auto; margin-top: 25px;"><a href="marketplace/services.php"><img src="/themes/gforge/images/marketplace-icon.png"></a></div></div>
		</div>
		
		<div class="row-fluid" style="text-align: center;">
			<div class="span4"><a href="community/"><h3>Community</h3><p>Meet people.</p></a></div>
			<div class="span4"><a href="softwaremap/"><h3>Projects</h3><p>Design stuff.</p></a></div>
			<div class="span4"><a href="marketplace/services.php"><h3>Marketplace</h3><p>Find stuff.</p></a></div>
		</div>
		

	</div>


	
	
<?php
$Layout->col(12, true);
?>
	<h2>About GE Forge</h2>
	
	
	<p>GE Forge was funded by DARPA as part of the Adaptive Vehicle Make (AVM) research portfolio for building the digital infrastructure to radically transform the systems engineering/design/verification, manufacturing, and innovation of the overall “make” process of delivering new defense systems or variants.</p>

<?
// news
$Layout->endcol()->col(8,true);
$HTML->heading(_('Latest News'),2);
echo news_show_latest(forge_get_config('news_group'),10,true,false,false,-1);
$Layout->endcol()->col(4);

// stats
$HTML->heading(_('Site Stats'),2);
echo show_features_boxes();
$Layout->endcol();

include $gfwww.'help/help.php';
?>
