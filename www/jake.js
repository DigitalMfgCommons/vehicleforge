// <!--BEGIN: Component Modal-->
// <div class="modal hide fade" id="componentModal">
	
// 	<div class="modal-header">
// 	  <a class="close" data-dismiss="modal">×</a>
// 	  <h2 id="component_name"></h2>
// 	  <h3 style="font-weight: 500;"><span style="font-weight: 300">in</span> <a id="group_name" href="#"></a></h3>
// 	</div>
	
// 	<div class="modal-body">
// 		<div class="row">
// 			<div style="margin:10px;">
// 				<img src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">
// 			  <p id="component_desc"></p>
// 			</div>
// 	  </div>
//   </div>
  
// 	<div class="modal-footer">
// 		<?php if (session_loggedin()) { ?>      
//       <a id="component_url" href="#" class="btn">View Component</a>
			
// 			<div class="btn-group pull-right" style="margin-left: 10px;">
// 			  <a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" style="width:120px;">
// 			    <span id="add_to_project_status">Add to Project</span>
// 			    <span class="caret"></span>
// 			  </a>
// 			  <ul class="dropdown-menu">
// 			  	<?php
// 			  		foreach (session_get_user()->getOwnedProjects() as $p) {
// 			  			echo '<li><a href="#" class="add_to_project" group_id="'.$p->getID().'">'.$p->getPublicName().'</a></li>';
// 			  		}
// 			  	?>				
// 			  </ul>
// 			</div>			
//     <?php } else { ?>
//     	<p>You must <a href="/account/login">login</a> to use this component.</p>
//     <?php } ?>
//   </div>
// </div>
// <!--END: Component Modal--> 

//writeComponentModal('componentModalContainer');

function writeComponentModal(componentModalContainer, sessionLoggedIn, projectListHtml) {
    var html;
    if(sessionLoggedIn) {
	html = $('<div class="modal hide fade" id="componentModal">'+
		 '<div class="modal-header">'+
		 '<a class="close" data-dismiss="modal">×</a>'+
		 '<h2 id="component_name"></h2>'+
		 '<h3 style="font-weight: 500;"><span style="font-weight: 300">in</span> <a id="group_name" href="#"></a></h3>'+
		 '</div>'+
		 '<div class="modal-body">'+
		 '<div class="row">'+
		 '<div style="margin:10px;">'+
		 '<img src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">'+
		 '<p id="component_desc"></p>'+
		 '</div>'+
		 '</div>'+
		 '</div>'+
		 '<div class="modal-footer">'+
		 '<a id="component_url" href="#" class="btn">View Component</a>'+ 
		 '<div class="btn-group pull-right" style="margin-left: 10px;">'+
		 '<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown" href="#" style="width:120px;">'+
		 '<span id="add_to_project_status">Add to Project</span>'+
		 '<span class="caret"></span>'+
		 '</a>'+
		 '<ul class="dropdown-menu">'+
		 projectListHtml +
		 '</ul>'+
		 '</div>'+
		 '</div>'+
		 '</div>');
    } else {
	html = $('<div class="modal hide fade" id="componentModal">'+
		 '<div class="modal-header">'+
		 '<a class="close" data-dismiss="modal">×</a>'+
		 '<h2 id="component_name"></h2>'+
		 '<h3 style="font-weight: 500;"><span style="font-weight: 300">in</span> <a id="group_name" href="#"></a></h3>'+
		 '</div>'+
		 '<div class="modal-body">'+
		 '<div class="row">'+
		 '<div style="margin:10px;">'+
		 '<img src="http://placehold.it/160x120&text=image" alt="" style="float:left; margin-right: 20px;">'+
		 '<p id="component_desc"></p>'+
		 '</div>'+
		 '</div>'+
		 '</div>'+
		 '<div class="modal-footer">'+
		 '<p>You must <a href="/account/login">login</a> to use this component.</p>'+
		 '</div>'+
		 '</div>');	
    }
    $('#'+componentModalContainer).html(html);
    alert("jake.js:writeComponentModal");
}
