$(function(){
	function convertToTextarea(Element){
		var text=Element.text();
		Element.replaceWith('<div class="editor"><textarea cols="40">'+text+'</textarea><input type="submit" value="Save Changes" class="a_edit_submit btn btn-primary" /><input type="submit" value="Cancel" class="a_edit_cancel btn" /></div>');
	}

	function convertFromTextarea(Element,Text){
		Element.replaceWith('<p>'+Text+'</p>');
	}

	$(document).on('click', ".a_edit", function(){
		convertToTextarea($(this).parent().siblings(".message").children('p'));
	});

	$(document).on('click', ".a_edit_cancel", function(){
		convertFromTextarea($(this).parent(), $(this).siblings("textarea").val());
	});

	$(document).on('click', ".a_edit_submit", function(){
		var	text=$(this).siblings("textarea").val(),
		aid=$(this).parent().parent().parent().parent().parent().attr("id").substr(1),
		$me=$(this);
		$.getJSON("/ajax/home_post_edit.php",{"aid":aid,"content":text},function(msg){
			if (!msg.error){
				convertFromTextarea($me.parent(),text);
			}
		});
	});

	var $home_comment_submit=$("#home_comment_submit"),
	$home_comment_text=$("#home_comment_text"),
	$a_delete=$(".a_delete");
	
	$home_comment_submit.click(function(){
		$.getJSON('/ajax/home_post.php',{'rid':$home_comment_submit.data('rid'),'tid':$home_comment_submit.data('tid'),'comment':$home_comment_text.val()},function(msg){
			if ($("#no_comments").length)$("#no_comments").remove();

			var $comment=$('<div class="hidden" id="a'+msg.aid+'">'+msg.comment+'</div>');
			$("#activity").prepend($comment);
			$comment.slideDown(function(){
				
			}).removeClass('hidden');
			$home_comment_text.val('');
		});
	});

	$(document).on("click", "a_delete", function(){
		var $activity=$(this).parent().parent().parent().parent(),
            aid=$activity.attr('id').substr(1);
		$.getJSON("/ajax/home_delete.php",{aid:aid},function(msg){
			if (msg.error==false){
				$activity.slideUp(function(){
					$activity.remove();
				});
			}
		});
	});

	function reply(activity_object,callback){
		var textarea_object=activity_object.children('.comment_panel').children('.message').children('textarea'),
		comment=textarea_object.val(),
		aid=activity_object.attr('id').substr(1);
		if (comment!=""){
			$.getJSON("/ajax/activity_post_comment.php",{"aid":aid,"comment":comment},function(msg){
				callback(msg);
				if (msg.error==false){
					textarea_object.val("");
					var $reply=$('<div class="hidden reply">'+msg.msg+'</div>');
					activity_object.children('.comment_panel').before($reply);
					$(".hidden.reply").slideDown(function(){
						$(this).children('.feed-nested').unwrap();
					});
				}
			});
		}
	}

	$(document).on('click', ".reply_button", function(){
		var $me=$(this);
		if ($me.hasClass('primary')){
			reply($me.parent().parent(),function(msg){
				if (msg.error==false){
					$me.parent().siblings('.comment_panel').slideUp();
					$me.siblings('.cancel_button').slideUp();
				}
			});
		}else{
			$me.parent().siblings('.comment_panel').slideDown();
			$me.siblings('.cancel_button').slideDown();
		}

		$me.toggleClass('primary');
	});

	$(document).on('keydown', ".reply_textarea", function(ev){
		$me=$(this);
		if (ev.keyCode==13){
			reply($(this).parent().parent().parent(),function(msg){
				$me.parent().parent().slideUp().siblings('.feed-comment').children('.cancel_button').slideUp().siblings('.reply_button').toggleClass('primary');
			});
		}
	});

	$(document).on('click', ".cancel_button", function(){
		var $me=$(this);
		$me.siblings('.reply_button').removeClass('primary');
		$me.parent().siblings('.comment_panel').slideUp();
		$me.slideUp();
	});
});