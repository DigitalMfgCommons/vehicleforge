$(function(){
	var one_hour=3600,
		one_day=one_hour*24,
		one_week=one_day*7;

    function get_time(time){
        var d=new Date(time),
            h=d.getHours(),
            x=h<13?'am':'pm',
            hs=h<13?h:h-12;

        return hs+":"+d.getMinutes()+x;
    }

    function update_times(){
        $("time").each(function(){
            var start=$(this).data("init"),
                now=Math.ceil((new Date).getTime()/1000),
                diff=now-start;

            if (diff<60){
                //<1 minute
                $(this).text(diff+" seconds ago");
            }else{
	            //TODO: over 60seconds goes to 2 minutes
                if (diff<one_hour){
                    $(this).text(Math.ceil((diff/60))+" minutes ago");
                }else{
                    //>1 hour
                    if (diff<one_day-one_hour){
                        var hours=Math.round(diff/one_hour);

                        if (hours==1){
                            $(this).text("1 hour ago");
                        }else{
                            $(this).text(hours+" hours ago");
                        }
                    }else{
                        //>1 day
                        var d=new Date(start);
	                    //TODO: Use PHP to generate this

                        if (diff<one_week){
                            var days=Math.round(diff/one_day),
                                text=days==1?"1 day ago":days+" days ago";

                            text+=" at "+get_time(start-(3600*5));
                            $(this).text(text);
                        }else{
                            //>1 week
                            //keep as PHP time
                        }
                    }
                }
            }
        });
    }

    update_times();
    setInterval(update_times,5000);
});
