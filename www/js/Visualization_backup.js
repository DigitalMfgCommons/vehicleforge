//<script>

/* Graph Visualization - 
 * ** These variables will typically be the same across any graph **
 * margin: Defines the dimensions surrounding a graph
 * width:  Sets the width of the graph in dimensions
 * height:  Sets the height of the graph dimensions
 * x:  Sets the width of the x axis
 * y:  Sets the height of the y axis
 * xAxis:  Sets the Scale for the xAxis
 * yAxis:  Sets the Scale for the yAxis
 * type: Input the Type of Graph.  This variable will set the 
 * 		 graph to instantiate.
 * legend:  Sets the Legend for a Graph
 * div:  
 * 
 *  Member Functions
 *  makeLineGraph() - Creates a Line Graph
 *  makeBarGraph() - Creates a Bar Graph
 *  makeScatterGraph() - Creates a Scatter Graph
 *  getType() - Returns the type of Graph
 *  
 */

/**
 * Graph - Creates a D3 Graph Object
 */

(function(){
	
	modelViz = {
			version: "0.1.0"
	}
	modelViz.graph = function () {
		var graph = graph();
		return graph;
	}
	modelViz.lineGraph = function() {
		var graph = runLineVisualization();
		return graph;
	};
	modelViz.scatterGraph = function() {
		var graph = runScatterVisualization();
		return graph;
	};
	modelViz.populateSelectors = function() {
		var selectors = populateSelectors();
		return selectors;
	}
	
	modelViz.createJSONData = function() {
		var jsonData = createJSONData();
		return jsonData;
	}
	function Graph (type) { 
		this.margin = {top: 20, right: 20, bottom: 30, left: 50};
		this.width = 500 - margin.left - margin.right;
	    this.height = 250 - margin.top - margin.bottom;
	}

	function sortFunction(a, b){
		return (a.x - b.x);
		}

	function runLineVisualization() {

	var margin = {top:20, right: 20, bottom: 30, left: 50},
	    width = 500 - margin.left - margin.right,
	    height = 250 - margin.top - margin.bottom;

	var x,y;
	//.range([0, width]);
	//.range([height, 0]);

	var axisType =  $("#axisType").val();
	if (axisType == "log") {
		x = d3.scale.log().range([0, width]);
		y = d3.scale.log().range([height, 0]);
	} else {
		x = d3.scale.linear().range([0, width]);
		y = d3.scale.linear().range([height, 0]);
	}


	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("bottom");

	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left");

	var line = d3.svg.line()
	    .x(function(d) { return x(d.x); })
	    .y(function(d) { return y(d.y); });

	    
	var svg = d3.select("#visual").append("svg:svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    .attr( "id", "svgGraph")
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	var xName = $("#selectX").val();
	var yName = $("#selectY").val();
	var xArray = getArrayFromSelectorID("#selectX");
	var yArray = getArrayFromSelectorID("#selectY");

	if (xArray.length != yArray.length) {
		alert("X and Y Array Values need to be the same length.")
		return;
	}

	// Formats the data to output to D3
	var plotData = [];

	for (var i = 0, xLength = xArray.length; i < xLength; i++) {
		 plotData.push( { x: xArray[i], y: yArray[i]})
		}

	plotData.sort(sortFunction)

	 x.domain(d3.extent(xArray, function(d) { return d; })).nice();
	 y.domain(d3.extent(yArray, function(d) { return d; })).nice();

	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	      .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .text(xName);

	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .text(yName);

	  svg.append("path")
	      .datum(plotData)
	      .attr("class", "line")
	      .attr("d", line);
	}

	function createJSONData() {
		
		var xName = $("#selectX").val(),
		yName = $("#selectY").val(),
		xArray = getArrayFromSelectorID("#selectX"),
		yArray = getArrayFromSelectorID("#selectY"),
		jsonData = [];
		
		for (var i = 0, xLength = xArray.length; i < xLength; i++) {
			var dataRow = {};
			dataRow[xName] = xArray[i].toString();
			dataRow[yName] = yArray[i].toString();
			// This line was added to demonstrate that each point should have a URL
			dataRow["URL"] = "https://www.ge.com";
			dataRow["ID"] = i;
			jsonData.push(dataRow);
		}
		return JSON.stringify(jsonData);
	}

	function populateSelectors() {
		
		// Get the Label names 
		var numLabels = [];
		$("#outputs label").each( function(index, v) {
			if ($(this).attr("title")=="Matrix" || $(this).attr("title")=="Vector" || $(this).attr("title")=="Array") {
				numLabels.push($(this).html());
			}
		});
		
		// Get the X and Y Selectors and set them to the values in numLabels array
		$.each(numLabels, function(key, value) {   
		     $('#selectY')
		         .append($("<option></option>")
		         .attr("value",value)
		         .text(value)); 
		     $('#selectX')
		     	.append($("<option></option>")
	         	.attr("value",value)
	         	.text(value)); 
		});
	}

	function getArrayFromSelectorID(id) {
		var unflattenedData = [],
		selectedValue = $(id).val(),
		flattenedData = [];
		// This method is causing an offending error
		$("input:text").each(function(){
			if ($(this).attr("id") == selectedValue){
				unflattenedData = eval($(this).val());
			}
		});
		
		flattenedData = unflattenedData;
		if (unflattenedData[0] instanceof Array) {
			flattenedData = unflattenedData.reduce(function(a, b) {return a.concat(b);});
		} 
		return flattenedData;
	}

	function runScatterVisualization() {

		var margin = {top: 20, right: 20, bottom: 30, left: 40},
	    width = 500 - margin.left - margin.right,
	    height = 250 - margin.top - margin.bottom;

		// Need to programatically change the scale to the 
		// log, linear, etc. scales
		
		var x,y;
	    //.range([0, width]);
	    //.range([height, 0]);

		var axisType =  $("#axisType").val();
		if (axisType == "log") {
			x = d3.scale.log().range([0, width]);
			y = d3.scale.log().range([height, 0]);
		} else {
			x = d3.scale.linear().range([0, width]);
			y = d3.scale.linear().range([height, 0]);
		}
		
		//x.range([0, width]);
		//y.range([height, 0]);
		

	var color = d3.scale.category10();

	var xAxis = d3.svg.axis()
	    .scale(x)
	    .orient("bottom");

	var yAxis = d3.svg.axis()
	    .scale(y)
	    .orient("left");

	var svg = d3.select("#visual").append("svg:svg")
	    .attr("width", width + margin.left + margin.right)
	    .attr("height", height + margin.top + margin.bottom)
	    .attr("id", "svgGraph")
	  .append("g")
	    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	var xName = $("#selectX").val();
	var yName = $("#selectY").val();
	var xArray = getArrayFromSelectorID("#selectX");
	var yArray = getArrayFromSelectorID("#selectY");

	if (xArray.length != yArray.length) {
		alert("X and Y Array Values need to be the same length.")
		return;
	}

	// Formats the data to output to D3
	var plotData = [];

	for (var i = 0, xLength = xArray.length; i < xLength; i++) {
		 plotData.push( { x: xArray[i], y: yArray[i]})
		}

	 x.domain(d3.extent(xArray, function(d) { return d; })).nice();
	 y.domain(d3.extent(yArray, function(d) { return d; })).nice();
	 
	 
	  svg.append("g")
	      .attr("class", "x axis")
	      .attr("transform", "translate(0," + height + ")")
	      .call(xAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("x", width)
	      .attr("y", -6)
	      .style("text-anchor", "end")
	      .text(xName);

	  svg.append("g")
	      .attr("class", "y axis")
	      .call(yAxis)
	    .append("text")
	      .attr("class", "label")
	      .attr("transform", "rotate(-90)")
	      .attr("y", 6)
	      .attr("dy", ".71em")
	      .style("text-anchor", "end")
	      .text(yName)
	// Add the key name to the text
	      // Also, programmatically, replace www.ge.com with a link
	      // to something in the database.
	  svg.selectAll(".dot")
	      .data(plotData)
	    .enter().append("svg:circle")
	      .attr("class", "dot")
	      .attr("r", 3.5)
	      .attr("cx", function(d){return x(d.x);})
	      .attr("cy", function(d){return y(d.y);})
	      .style("fill", "blue")
	      .on('mouseover', function() {
	    	  d3.select(this).style("fill","red")
	    	  })
	      .on('mouseout', function() {
	    	  d3.select(this).style("fill","blue")
	    	  })
	    	 .on("click", function() {
	    		 window.open("http://www.ge.com");
	    	 })
	  	  .append("svg:title")
	  	  .text(function(d) { return "(" + d.x + "," + d.y + ")"; });
	  	 
	  var legend = svg.selectAll(".legend")
	      .data(color.domain())
	    .enter().append("g")
	      .attr("class", "legend")
	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

	  legend.append("rect")
	      .attr("x", width - 18)
	      .attr("width", 18)
	      .attr("height", 18)
	      .style("fill", "white");

	  legend.append("text")
	      .attr("x", width - 24)
	      .attr("y", 9)
	      .attr("dy", ".35em")
	      .style("text-anchor", "end")
	      .text("Test");
	}
})();

