<?php
/**
 * FusionForge Documentation Manager
 *
 * Copyright 2000, Quentin Cregan/Sourceforge
 * Copyright 2002-2003, Tim Perdue/GForge, LLC
 * Copyright 2010, Franck Villaume - Capgemini
 * Copyright (C) 2010-2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'docman/DocumentManager.class.php';
require_once $gfcommon.'docman/Document.class.php';
require_once $gfcommon.'docman/DocumentFactory.class.php';
require_once $gfcommon.'docman/DocumentGroup.class.php';
require_once $gfcommon.'docman/DocumentGroupFactory.class.php';
require_once $gfcommon.'docman/include/DocumentGroupHTML.class.php';
require_once $gfcommon.'docman/include/utils.php';
require_once $gfcommon.'include/TextSanitizer.class.php'; // to make the HTML input by the user safe to store


/* are we using docman ? */
if (!forge_get_config('use_docman'))
	exit_disabled('home');

/* get informations from request or $_POST */
$group_id = getIntFromRequest('group_id');

/* validate group */
if (!$group_id)
	exit_no_group();

session_require_perm ('project_read', $group_id) ;

$g = group_get_object($group_id);
if (!$g || !is_object($g))
	exit_no_group();

/* is this group using docman ? */
if (!$g->usesDocman())
	exit_disabled();

if ($g->isError())
	exit_error($g->getErrorMessage(),'docman');

use_javascript('tablesorter/jquery.tablesorter.min.js');
use_stylesheet('/js/tablesorter/style.css');

$JS='$(function(){
	$("#files").tablesorter({
		debug:false,
		headers:    {
		    4:  {
                sorter:false
		    }
		}
	}).addClass("tablesorter");

	$("#addfolder_button").click(function(){
		$("#addfolder_panel").slideDown();
	});

	$("#addfolder_name").keyup(function(){
		if ($(this).val()!=""){
			$("#addfolder_submit").removeClass("disabled");
		}else{
			$("#addfolder_submit").addClass("disabled");
		}
	});

	$("#addfolder_cancel").click(function(){
		$("#addfolder_panel").slideUp();
	});

	$("#addfolder_submit").click(function(){
		if ($("#addfolder_name").val()!=""){
			$.getJSON("ajax/new_folder.php",{"name":$("#addfolder_name").val(),"group_id":'.$group_id.'},function(msg){
				if (msg.error==false){
					$("#addfolder_name").val("");
					$("#addfolder_panel").slideUp();
					$(".folder_contents").append(\'<a href="?group_id='.$group_id.'&dgroup_id=\'+msg.fid+\'">\'+msg.fname+\'</a>\');
				}else{
					alert("error:"+msg.msg);
				}
			});
		}
	});

	$("#adddoc_button").click(function(){
		//$("#adddoc_panel").slideDown();
	});

	$("#adddoc_cancel").click(function(){
		$("#adddoc_panel").slideUp();
	});

	$("#doc_file").change(function(){
		console.log($(this).val());
		if ($(this).val()!=""){
			$("#upload_doc").attr("disabled",false).removeClass("disabled");
		}else{
			$("#upload_doc").attr("disabled",true).addClass("disabled");
		}
	});
});';
add_js($JS);

$DocGroupID=isset($_GET['dgroup_id'])?$_GET['dgroup_id']:0;

if (isset($_GET['action'])){
	switch($_GET['action']){
		case 'addfolder':
			if (isset($_POST['folder_name'])){
				$Folder=$_POST['folder_name'];
				$DocGroup=new DocumentGroup(null,true);
				if (!$DocGroup->create($Folder,$group_id,'',user_getid()))
					$error_msg=$DocGroup->getErrorMessage();
			}
			break;

		case 'adddoc':
			if (isset($_FILES['doc'])){
				$Doc=new Document();
				$Desc=getStringFromRequest('desc');
				if (!$Doc->create(user_getid(),'doc',$Desc,$DocGroupID,1,$group_id))
					$error_msg=$Doc->getErrorMessage();
				else
					$feedback='Document uploaded';
			}
			break;

		case 'deldoc':
			$Doc=new Document(getIntFromRequest('docid'));
			if (!$Doc->delete())
				$error_msg=$Doc->getErrorMessage();
			else
				$feedback='Document deleted';
			break;
	}
}

site_project_header(array('group'=>$group_id,'toptab'=>'docman'), 4);
$Layout->col(12,true);


if (!is_dir(GF_IMAGE_STORE.'project_files/'.$g->getUnixName())){
	mkdir(GF_IMAGE_STORE.'project_files/'.$g->getUnixName());
}


?>
<style>
.folder_contents{margin-left:20px}
#files td{padding:5px}
</style>

<div style="clear:both">
<?php
if ($DocGroupID==0)
	echo util_image_link(null,'/images/apps/folder--plus.png',array('id'=>'addfolder_button'),array('alt'=>'Add Folder'));

echo util_image_link('?group_id='.$group_id.'&action=adddoc&dgroup_id='.$DocGroupID,'/images/apps/document--plus.png',array('id'=>'adddoc_button'),array('alt'=>'Upload Document'));

	?>
	<div id="addfolder_panel" class="hidden">
		<strong>Add a Folder</strong><br />
		<input type="text" id="addfolder_name" />
		<input type="submit" value="Add Folder" id="addfolder_submit" class="btn primary disabled" />
		<input type="button" value="Cancel" id="addfolder_cancel" class="btn" />
	</div>
	<div id="adddoc_panel" class="hidden">
		<strong>Add a Document</strong><br />
		<input type="file" id="adddoc_file" />
		<input value="Upload Doc" type="button" id="adddoc_upload" class="btn primary disabled">
		<input value="Cancel" type="button" id="adddoc_cancel" class="btn">
	</div>
</div>
<div style="float:left;width:150px">
<?php
if ($DocGroupID==0){
	echo '<strong>';
}else{
	echo '<a href="?group_id='.$group_id.'">';
}
echo 'Documents Home';
if ($DocGroupID==0){
	echo '</strong>';
}else{
	echo '</a>';
}
?>
<div class="folder_contents">
<?php
$Result=db_query_params("SELECT * FROM doc2_groups WHERE group_id=$1",array($group_id));
while ($DocGroup=db_fetch_array($Result)){
	if ($DocGroup['doc_group_id']==$DocGroupID){
		echo '<strong>';
	}else{
		echo '<a href="?group_id='.$group_id.'&dgroup_id='.$DocGroup['doc_group_id'].'">';
	}
	echo $DocGroup['group_name'];
	if ($DocGroup['doc_group_id']==$DocGroupID){
		echo '</strong>';
	}else{
		echo '</a>';
	}
	echo '<br />';
}
?>
</div>
</div><div style="margin-left:150px">
<?php
$ThisGroup=new DocumentGroup();
$Docs=$ThisGroup->getContents($group_id,$DocGroupID);
?>
<table class="zebra-striped" id="files">
	<thead>
	<tr>
		<th>Filename</th>
		<th>Owner</th>
		<th>Description</th>
		<th>Date Modified</th>
		<th>Actions</th>
	</tr>
	</thead><tbody>
<?php
if ($Docs){
	foreach($Docs as $Doc){
		$D=new Document($Doc['file_id']);
		echo '<tr>
		<td><a href="'.$D->getPublicPath().'">'.$D->getName().'</a></td>
		<td>'.$D->getOwnerName().'</td>
		<td>'.$D->getDescription().'</td>
		<td>'.date("M d, Y",$D->getDateModified()).' at '.date("g:ia",$D->getDateModified()).'</td>
		<td>'.util_image_link('?group_id='.$group_id.'&action=deldoc&docid='.$Doc['file_id'],'/images/apps/document--minus.png',array(),array('alt'=>'Delete Document')).'</td>
		</tr>';
	}
}else{
	echo '<strong>There are no documents in this directory</strong>';
}
?>
</tbody></table>
</div>
<?php
if (isset($_GET['action'])){
	switch($_GET['action']){
		case 'addfolder':
			echo '<form action="?group_id='.$group_id.'&action=addfolder" method="post">
			<input type="text" name="folder_name" />
			<input type="submit" value="Add Folder" />
			</form>';
			break;

		case 'adddoc':
			echo '<form action="?group_id='.$group_id.'&action=adddoc&dgroup_id='.$DocGroupID.'" enctype="multipart/form-data" method="post">
			<input type="file" name="doc" id="doc_file" autocomplete="off" /><br />
			<textarea name="desc"></textarea>
			<input type="submit" id="upload_doc" disabled value="Upload Doc" class="btn primary disabled" />
			</form>';
			break;
	}
}

$Layout->endcol();
site_project_footer();

?>
