'use strict';
/**
* dmc.dashboard Module
*
* Dashboard
*/
angular.module('dmc.home', ['dmc.configs.ngmaterial', 'ngMdIcons', 'ui.router', 'md.data.table', 'dmc.common.header', 'dmc.common.footer'])
.config(function($stateProvider, $urlRouterProvider){
  $stateProvider
    .state('home', {
      url: '',
      abstract: true
    });
  $urlRouterProvider.otherwise('/');
});
