'use strict';

angular.module('dmc.widgets.submissions',[
        'dmc.ajax',
        'dmc.data',
        'dmc.socket'
    ]).
    directive('uiWidgetSubmissions', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            templateUrl: 'templates/components/ui-widgets/submissions.html',
            scope: {
                widgetTitle: "=",
                projectId : "="
            },
            link: function (scope, iElement, iAttrs) {
                iElement.addClass('submissions-widget');
            },
            controller: function($scope, $element, $attrs, $mdDialog, socketFactory, dataFactory, ajax) {
                $scope.submissions = [{
                    id : 1,
                    date : moment(new Date()).format("MM/DD/YY HH:mm A"),
                    success : 95
                },{
                    id : 2,
                    date : moment(new Date()).format("MM/DD/YY HH:mm A"),
                    success : 55
                },{
                    id : 3,
                    date : moment(new Date()).format("MM/DD/YY HH:mm A"),
                    success : 88
                },{
                    id : 4,
                    date : moment(new Date()).format("MM/DD/YY HH:mm A"),
                    success : 35
                },{
                    id : 5,
                    date : moment(new Date()).format("MM/DD/YY HH:mm A"),
                    success : 77
                }];
                $scope.submissions = [];

                $scope.onOrderChange = function(){

                };
            }
        };
    }]);