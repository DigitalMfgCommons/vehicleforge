'use strict';

angular.module('dmc.my_projects', [
        'dmc.widgets.services',
        'dmc.widgets.tasks',
        'dmc.widgets.discussions',
        'dmc.widgets.projects',
        'dmc.configs.ngmaterial',
        'ngMdIcons',
        'ui.router',
        'md.data.table',
        'dmc.common.header',
        'dmc.common.footer'
])
.config(function($stateProvider, $urlRouterProvider, $httpProvider){
    $stateProvider.state('project', {
        url: '',
        abstract: true
    });
    $urlRouterProvider.otherwise('/');
})
.controller('DMCMyProjectsController', function ($scope) {
    $scope.sortList = [{
            tag : "id", name : "ID Project"
        },{
            tag : "name", name : "Name"
        },{
            tag : "date", name : "Date created"
    }];
    $scope.filterList = [{
        tag : "id1", name : "ID Project"
    },{
        tag : "id2", name : "ID Project"
    },{
        tag : "id3", name : "ID Project"
    }];
});

angular.module('dmc.create_project', [
    'dmc.configs.ngmaterial',
    'dmc.rfpInvite',
    'ngMdIcons',
    'ui.router',
    'md.data.table',
    'dmc.common.header',
    'dmc.common.footer'
]).config(function($stateProvider, $urlRouterProvider, $httpProvider){
    $stateProvider.state('create_project', {
        url: '',
        abstract: true
    });
    $urlRouterProvider.otherwise('/');
}).controller('DMCCreateProjectController', function ($scope) {

});