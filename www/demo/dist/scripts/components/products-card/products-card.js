'use strict';
/**
* dmc.component.productscard Module
*
* DMC Tree Menu
*/

angular.module('dmc.component.productscard', [
    'dmc.component.productcard',
    'angularUtils.directives.dirPagination',
    'ngCookies'
])
.directive('dmcProductsCard', function(){
     return {
      restrict: 'E',
      transclude: true,
      scope: {
          cardSource : "=",
          totalSize : "=",
          pageSize : "=",
          currentPage : "=",
          titleCard: "=",
          cardLoading: "=",
          searchCard: "="
      },
      templateUrl: 'templates/components/products-card/products-card-tpl.html',
      controller: function($scope,$cookies){
          if(parseInt($scope.currentPage) <= 0) $scope.currentPage = 1;

          $scope.itemsArray = [];

          $scope.sortArray = [{
              val : 'popular', name: 'Most Popular'
          }];
          $scope.pageSort = 'popular';

          $scope.showArray = [
              {
                  val:10, name: '10 items'
              },
              {
                  val:25, name: '25 items'
              },
              {
                  val:50, name: '50 items'
              },
              {
                  val:100, name: '100 items'
              },
              {
                  val:$scope.totalSize, name: 'All items'
              }
          ];

          $scope.updateData = function(){
              $scope.itemsArray = new Array($scope.totalSize);
              for(var i=0;i<$scope.itemsArray.length;i++) $scope.itemsArray[i] = i;
              var j = ($scope.currentPage-1)*$scope.pageSize;
              for(var i=0;i<$scope.cardSource.arr.length;i++){
                  $scope.itemsArray[j] = $scope.cardSource.arr[i];
                  j++;
              }
          };
          $scope.updateData();

          $scope.$watch(function() { return $cookies.updateProductCard; }, function(newValue) {
              $scope.updateData();
          });

          $scope.$watch('cardSource', function(){
              $scope.updateData();
          });

          $scope.pageChangeHandler = function(num) {
              $scope.currentPage = num;
              $cookies.changedPage = num;
          };

          $scope.clearFilter = function(){

          };

          $scope.filterItems = [{
              id : 1,
              name : "Analytical Services"
          },{
              id : 2,
              name : "4 Stars & Up"
          }];
      }
    };
});
