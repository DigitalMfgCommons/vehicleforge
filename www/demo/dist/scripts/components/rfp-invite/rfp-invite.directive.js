'use strict';
angular.module('dmc.rfpInvite', [
        'ngMaterial',
        'dmc.ajax',
        'dmc.data',
        'ngMdIcons',
        'dmc.widgets.documents',
        'dmc.compare'
    ]).directive('rfpTabs', ['$parse', function () {
        return {
            restrict: 'A',
            templateUrl: 'templates/components/rfp-invite/rfp-invite.html',
            scope: {
                columns: "=",
                widgetTitle: "=",
                widgetStyle: "=",
                projectId: "="
            },
            controller: function ($scope) {
                // Specify a list of font-icons with ligatures and color overrides
                var iconData = [
                    {name: 'accessibility', color: "#777"},
                    {name: 'question_answer', color: "rgb(89, 226, 168)"}
                ];
                $scope.fonts = [].concat(iconData);
                // Create a set of sizes...
                $scope.sizes = [
                    {size: "md-18", padding: 0}
                ];

                $scope.data = {
                    secondLocked : true,
                    thirdLocked : true,
                    fourthLocked : true
                };
                $scope.selectedIndex = 0;
                $scope.goToNextTab = function(number){
                    $scope.selectedIndex = number-1;
                    if(number == 2){
                        $scope.data.thirdLocked = false;
                    }else if(number == 3){
                        $scope.data.fourthLocked = false;
                    }
                };

                $scope.disableEnable = function(number,val){
                    var v = (val ? false : true);
                    switch(number){
                        case 2 :
                            $scope.data.secondLocked = v;
                            break;
                        case 3 :
                            $scope.data.thirdLocked = v;
                            break;
                        case 4 :
                            $scope.data.fourthLocked = v;
                            break;
                    }
                };

                $("md-tabs").on("click","md-tab-item",function(){
                    $scope.enableNext($(this).index()+2);
                });

                $scope.enableNext = function(number){
                    switch(number){
                        case 3 :
                            $scope.data.thirdLocked = false;
                            break;
                        case 4 :
                            $scope.data.fourthLocked = false;
                            break;
                    }
                };

                // Invitees
                $scope.invitees = [{
                    id : 1,
                    name : "Member 1",
                    avatar : "/images/avatar-fpo.jpg",
                    company : "Company A",
                    rating : 3.8,
                    description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    skills : ['skill', 'skill', 'skill'],
                    projects : {
                        total : 30,
                        completed : 20
                    },
                    memberFor : '2 years',
                    isCompare : false,
                    isInvite : true
                },{
                    id : 2,
                    name : "Member 2",
                    avatar : "/images/avatar-fpo.jpg",
                    company : "Company B",
                    rating : 4.8,
                    description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    skills : ['skill', 'skill', 'skill', 'skill'],
                    projects : {
                        total : 45,
                        completed : 30
                    },
                    memberFor : '5 years',
                    isCompare : false,
                    isInvite : true
                }];

            }
        };
    }])
    .directive('rfpTabOne', function () {
        return {
            templateUrl: 'templates/components/rfp-invite/rfp-tab-one.html',
            scope : {
              goToTab : '=',
              disableEnableTab : '='
            },
            controller: function ($scope) {
                $scope.$watch('form.$valid', function(current, old){
                    $scope.disableEnableTab(2,current);
                });
            }
        }
    })
    .directive('rfpTabTwo', function () {
        return {
            templateUrl: 'templates/components/rfp-invite/rfp-tab-two.html',
            scope : {
                goToTab : '=',
                disableEnableTab : '='
            },
            controller: function ($scope,$compile,ajax,dataFactory) {
                $scope.isSelected = false;
                $scope.services = [];
                $scope.getServices = function(){
                    ajax.on(dataFactory.getUrlAllServices(null),{
                        sort : $scope.sort,
                        order : $scope.order,
                        limit : 5,
                        offset : 0
                    },function(data){
                        $scope.services = data.result;
                        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') $scope.$apply();
                    },function(){
                        alert("Ajax faild: getServices");
                    });
                };
                $scope.getServices();

                $scope.changeItem = function(item){

                };

                $scope.onOrderChange = function(){

                };

                $scope.selectModel = function(ev,item,select){
                    for(var i in $scope.services){
                        if($scope.services[i].select && $scope.services[i].id !== item.id) $scope.services[i].select = false;
                    }
                    $(ev.target).parents(".tableServices").find(".opened").removeClass('opened');
                    $("#inputs-outputs").remove();
                    if(!select) {
                        $scope.isSelected = true;
                        var tr = $(ev.target).parents(".table-line");
                        tr.addClass('opened');
                        $($compile('<tr id="inputs-outputs" inputs-outputs total-outputs="'+item.specificationsData.output+'" total-inputs="'+item.specificationsData.input+'" service-name="\'' + item.title + '\'"></tr>')($scope)).insertAfter(tr);
                    }else{
                        $scope.isSelected = false;
                    }
                };
            }
        }
    })
    .directive('rfpTabThree', function () {
        return {
            templateUrl: 'templates/components/rfp-invite/rfp-tab-three.html',
            scope : {
                goToTab : '=',
                disableEnableTab : '=',
                invitees : '='
            },
            controller: function ($scope) {
                $scope.compare = [];

                $scope.members = [];
                $scope.members.push({
                    val : 1,
                    name : 'Member 1'
                });
                $scope.members.push({
                    val : 2,
                    name : 'Member 2'
                });

                $scope.$watchCollection('invitees',function(newArray,oldArray){
                    for(var i in $scope.foundMembers){
                        var found = false;
                        for(var j in newArray){
                            if($scope.foundMembers[i].id == newArray[j].id){
                                found = true;
                                break;
                            }
                        }
                        $scope.foundMembers[i].isInvite = found;
                    }
                });

                $scope.$watchCollection('compare',function(newArray,oldArray){
                    for(var i in $scope.foundMembers){
                        var found = false;
                        for(var j in newArray){
                            if($scope.foundMembers[i].id == newArray[j].id){
                                found = true;
                                break;
                            }
                        }
                        $scope.foundMembers[i].isCompare = found;
                    }
                });

                $scope.compareMember = function(item){
                    var found = false;
                    for(var i in $scope.compare){
                        if($scope.compare[i].id === item.id) {
                            $scope.compare.splice(i, 1);
                            found = true;
                            break;
                        }
                    }
                    if(!found) $scope.compare.push(item);
                    if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') $scope.$apply();
                };

                $scope.foundMembers = [{
                    id : 1,
                    name : "Member 1",
                    avatar : "/images/avatar-fpo.jpg",
                    company : "Company A",
                    rating : 3.8,
                    description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    skills : ['skill', 'skill', 'skill'],
                    projects : {
                        total : 30,
                        completed : 20
                    },
                    memberFor : '2 years',
                    isCompare : false,
                    isInvite : true
                },{
                    id : 2,
                    name : "Member 2",
                    avatar : "/images/avatar-fpo.jpg",
                    company : "Company B",
                    rating : 4.8,
                    description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    skills : ['skill', 'skill', 'skill', 'skill'],
                    projects : {
                        total : 45,
                        completed : 30
                    },
                    memberFor : '5 years',
                    isCompare : false,
                    isInvite : true
                },{
                    id : 3,
                    name : "Member 3",
                    avatar : "/images/avatar-fpo.jpg",
                    company : "Company C",
                    rating : 7.8,
                    description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                    skills : ['skill'],
                    projects : {
                        total : 10,
                        completed : 8
                    },
                    memberFor : '1 year',
                    isCompare : false,
                    isInvite : false
                }];

                $scope.addToInvitation = function(item){
                    var found = false;
                    for(var i in $scope.invitees){
                        if($scope.invitees[i].id === item.id) {
                            $scope.invitees.splice(i, 1);
                            found = true;
                            break;
                        }
                    }
                    if(!found) $scope.invitees.push(item);
                    if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') $scope.$apply();
                };
            }
        }
    })
    .directive('rfpTabFour', function () {
        return {
            templateUrl: 'templates/components/rfp-invite/rfp-tab-four.html',
            scope : {
                goToTab : '=',
                disableEnableTab : '=',
                invitees : '='
            },
            controller: function ($scope) {
                $scope.$watchCollection(
                    "invitees",
                    function( newValue, oldValue ) {

                    }
                );
            }
        }
    })
    .directive('inputsOutputs', function () {
        return {
            restrict: 'A',
            templateUrl: 'templates/components/rfp-invite/inputs-outputs-tpl.html',
            scope : {
                serviceName: '=',
                totalInputs: '=',
                totalOutputs: '='
            },
            controller: function ($scope) {
                $scope.inputs = new Array($scope.totalInputs);
                $scope.outputs = new Array($scope.totalOutputs);
            }
        }
    }).directive('dmcSelectedInvitees', function () {
        return {
            restrict: 'A',
            templateUrl: 'templates/components/rfp-invite/selected-invitees-tpl.html',
            scope:{
                invitees: '='
            },
            controller: function ($scope) {

                $scope.removeInvite = function(item){
                    for(var i in $scope.invitees){
                        if($scope.invitees[i].id === item.id){
                            $scope.invitees.splice(i,1);
                            if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') $scope.$apply();
                            break;
                        }
                    }
                };



                $scope.clear = function(){
                    $scope.invitees = [];
                    if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') $scope.$apply();
                };
            }
        }
    }).directive('dmcMemberCard', function () {
        return {
            restrict: 'A',
            templateUrl: 'templates/components/rfp-invite/member-card-tpl.html',
            scope:{
                compareMember: '=',
                cardSource: '=',
                inviteMember: '='
            },
            controller: function ($scope) {
                $scope.addToInvitation = function(){
                    $scope.inviteMember($scope.cardSource);
                    $scope.cardSource.isInvite = ($scope.cardSource.isInvite ? false : true);
                };

                $scope.addToCompare = function(){
                    $scope.compareMember($scope.cardSource);
                    $scope.cardSource.isCompare = ($scope.cardSource.isCompare ? false : true);
                };
            }
        }
    });

