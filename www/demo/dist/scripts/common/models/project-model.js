'use strict';

angular.module('dmc.model.project', ['dmc.data'])
 .service('DMCProjectModel', ['$http', 'dataFactory', function($http, dataFactory) {

    this.getModel = function(id) {
        return $http.get(dataFactory.getApiUrl() + '/projects/' + id).then(
            function(response){
              return response.data;
            },
            function(response){
              return response;
            }
          );
    };
}]);