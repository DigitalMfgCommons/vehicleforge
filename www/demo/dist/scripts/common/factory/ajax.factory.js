'use strict';

angular.module('dmc.ajax',[]).factory('ajax', function () {
        return {
            on: function(urlAddress,dataObject,successFunction,errorFunction){
                $.ajax({
                    type: "POST",
                    url: urlAddress,
                    dataType: "json",
                    encoding: "UTF-8",
                    data: $.param(dataObject),
                    success: function (data, status) {
                        successFunction(data, status);
                    },
                    error: function (data, status) {
                        errorFunction()
                    }
                });
            }
        };
    }
);