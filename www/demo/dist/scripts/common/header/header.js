'use strict';
/**
* dmc.common.header Module
*
* Global Header
*/
angular.module('dmc.common.header', [])
.directive('dmcTopHeader', [ function(){
  return {
    restrict: 'E',
    transclude: true,
    scope: {
      showNotification: '='
    },
    templateUrl: 'templates/common/header/header-tpl.html'
  };
}]);
