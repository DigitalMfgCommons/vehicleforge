'use strict';

angular.module('dmc.project', [
        'dmc.configs.ngmaterial',
        'ngMdIcons',
        'ngtimeago',
        'dmc.widgets.services',
        'dmc.widgets.tasks',
        'dmc.widgets.discussions',
        'dmc.widgets.documents',
        'dmc.widgets.components',
        'ui.router',
        'md.data.table',
        'dmc.common.header',
        'dmc.common.footer',
        'dmc.model.project',
        'ui.autocomplete'
])
.config(function($stateProvider, $urlRouterProvider, $httpProvider){
    $stateProvider.state('project', {
        url: '/:projectId',
        templateUrl: 'templates/project/project.html',
        controller: 'DMCProjectController as projectCtrl',
        resolve: {
            projectData: ['DMCProjectModel', '$stateParams',
                function(DMCProjectModel, $stateParams) {
                return DMCProjectModel.getModel($stateParams.projectId);
            }]
        }
    }).state('preview', {
        url: '/preview/:projectId',
        templateUrl: 'templates/project/project.html',
        controller: 'DMCPreviewProjectController as projectCtrl',
        resolve: {
            projectData: ['DMCProjectModel', '$stateParams',
                function(DMCProjectModel, $stateParams) {
                    return DMCProjectModel.getModel($stateParams.projectId);
                }]
        }
    }).state('submission', {
        url: '/submission/:projectId',
        templateUrl: 'templates/project/project.html',
        controller: 'DMCSubmissionProjectController as projectCtrl',
        resolve: {
            projectData: ['DMCProjectModel', '$stateParams',
                function(DMCProjectModel, $stateParams) {
                    return DMCProjectModel.getModel($stateParams.projectId);
                }]
        }
    }).state('submit', {
        url: '/submit/:projectId',
        templateUrl: 'templates/project/submit.html',
        controller: 'DMCSubmitProjectController as projectCtrl',
        resolve: {
            projectData: ['DMCProjectModel', '$stateParams',
                function(DMCProjectModel, $stateParams) {
                    return DMCProjectModel.getModel($stateParams.projectId);
                }]
        }
    });
    $urlRouterProvider.otherwise('/1');
})
    .controller('DMCProjectController', ['$stateParams', 'projectData', function ($stateParams, projectData) {
        var projectCtrl = this;
        projectCtrl.currentProjectId = angular.isDefined($stateParams.projectId) ? $stateParams.projectId : 1;
        projectCtrl.projectData = projectData;
    }])
    .controller('DMCPreviewProjectController', ['$scope','$stateParams', 'projectData', function ($scope, $stateParams, projectData) {
        var projectCtrl = this;
        projectCtrl.currentProjectId = angular.isDefined($stateParams.projectId) ? $stateParams.projectId : 1;
        projectCtrl.projectData = projectData;

        $scope.isPreview = true;
    }])
    .controller('DMCSubmissionProjectController', ['$scope','$stateParams', 'projectData', function ($scope, $stateParams, projectData) {
        var projectCtrl = this;
        projectCtrl.currentProjectId = angular.isDefined($stateParams.projectId) ? $stateParams.projectId : 1;
        projectCtrl.projectData = projectData;

        $scope.isSubmission = true;
    }])
    .controller('DMCSubmitProjectController', ['$scope','$stateParams', 'projectData', function ($scope, $stateParams, projectData) {
        var projectCtrl = this;
        projectCtrl.currentProjectId = angular.isDefined($stateParams.projectId) ? $stateParams.projectId : 1;
        projectCtrl.projectData = projectData;


    }]);