<?php
require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww . 'include/LayoutGen.php';
require_once $gfwww.'include/FormGen.php';
$LayoutGen=new LayoutGen;

site_header(array('title'=>'Search'));

$Search=getStringFromRequest('q');
$SearchProjects=isset($_REQUEST['projects']);
$SearchModels=isset($_REQUEST['models']);
$SearchUsers=isset($_REQUEST['users']);

$LayoutGen->col(2);
$HTML->boxTop()->heading(_('Search Options'),3);
echo '<form action="search.php" method="get">
<input type="text" name="q" value="'.$Search.'" placeholder="Search... autocomplete="off"  /><br />
<input type="checkbox" name="projects" autocomplete="off"';
if ($SearchProjects)echo 'checked=""';
echo '/> Projects<br />
<input type="checkbox" name="models" autocomplete="off"';
if($SearchModels)echo 'checked=""';
echo '/> Models<br />
<input type="checkbox" name="users" autocomplete="off"';
if($SearchUsers)echo ' checked=""';
echo' /> Users<br />
<input type="submit" value="Search" />
</form>';
$HTML->boxBottom();
$LayoutGen->endcol()->col(10);
$HTML->boxTop()->heading(_('Search Results'),3);
if ($Search){

}else{
	echo '<strong>No search string given</strong>';
}
$HTML->boxBottom();
$LayoutGen->endcol();
site_footer(array());
?>