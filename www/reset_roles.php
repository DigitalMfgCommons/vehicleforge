<?php
require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/Role.class.php';

$Result=db_query_params("SELECT * FROM groups where group_id>5 order by group_id asc");
$Groups=util_result_columns_to_array($Result);

foreach($Groups as $g){
	echo '<h1>'.$g['group_id'].'</h1>';
	$Roles=group_get_object($g['group_id'])->getRolesId();

	//Delete all roles except global ones
	foreach($Roles as $r){
		if ($r>2){
			$Role=new Role($g['group_id'],$r);
			echo 'Deleting role_id='.$r;

			if (!$Role->delete()){
				echo ': Cannot delete: '.$Role->getErrorMessage();

				if (count($Role->getUsers())){
					echo ': Attempting to remove users...';

					$Users=$Role->getUsers();
					foreach($Users as $u){
						echo ': Removing user_id='.$u->getID().': ';

						if ($Role->removeUser($u)){
							echo 'SUCCESS';
						}else{
							echo 'FAILURE:'.$Role->getErrorMessage();
						}
					}

					echo ': Trying to delete again...';
					if (!$Role->delete()){
						echo 'FAILURE: '.$Role->getErrorMessage();
					}else{
						echo 'SUCCESS';
					}
				}
			}

			echo '<br>';
		}
	}

	echo '. Creating roles: ';
	$Role=new Role($g);
	if (!$Role->createDefault('Project Member')){
		echo $Role->getErrorMessage();
	}
	if(!$Role->createDefault('Admin')){
		echo '...'.$Role->getErrorMessage();
	}
}
?>