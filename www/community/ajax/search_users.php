<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once '../community_util.php';

$Search=getStringFromRequest('q');
$Type=getStringFromRequest('type');

$HTML='';
$SearchResults=null;
$Error=false;

switch ($Type){
    case 'name':
        $SearchResults=SolrSearch::search('users',$Search,'realname');
        break;

    case 'skill':
        $SearchResults=SolrSearch::search('users',$Search,'skill_keyword');
        break;
}

/*if ($SearchResults){
    foreach($SearchResults as $i)*/
        $HTML.=formatMember(105, true);
/*}else{
    $Error=true;
}*/


echo json_encode(array(
    'error' => $Error,
    'html'  => $HTML
));

return true;
?>