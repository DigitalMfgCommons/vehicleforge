<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once '../community_util.php';


//$Search=getStringFromRequest('q');

$results = getStringFromRequest('results');
$HTML = "";
$Error=false;

if (!empty($results)){
    foreach($results as $i){
        // exclude admins    
        if($i['id'] > 102){
            $HTML .= formatMember($i['id'], true);
        }
    }
        
}else{
    $Error=true;
}

echo json_encode(array(
    'error' => $Error,
    'html'  => $HTML
));

return true;
?>