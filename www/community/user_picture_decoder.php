<?php

require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$user_id = getStringFromRequest('user_id');
$default = '/image_store/profile_images/default.png';

$userObj = user_get_object($user_id);
$image=false;
if($userObj) {
  if($userObj->getImageUrl()) {
    /* $image='/image_store/profile_images/'.$userObj->getUnixName().'/'.$userObj->getImage(); */
    $image=$userObj->getImageUrl();
  }
}

if ($image){
  $src = $image;
} else {
  $src = $default;
}

print "{ \"user_img_src\":\"".$src."\" }";

?>