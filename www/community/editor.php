<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once 'community_util.php';
// Check if the user is logged in
if(!session_get_user()){
    header( 'Location: community/resources.php' ) ;
}

// Handle the form posting back, either update or create page
if(getStringFromRequest('submit')){
    if(getStringFromRequest('pageTitle') && getStringFromRequest('pageContent')){
        $title = getStringFromRequest('pageTitle');
        $cat_id = getIntFromRequest('catID');
        // Clean up contents
        $search  = array('h1>', 'h2>', 'h3>');
        $replace = array('h4>', 'h5>', 'h6>');
        $contents = str_replace($search, $replace, trim(getStringFromRequest('pageContent')));
        // Update page
        if(getStringFromRequest('action') == 'edit' && getStringFromRequest('id')){
            $pageID = getStringFromRequest("id");
            $page=new WikiPage($pageID);
            $page->update(1,user_getid(), $title, $cat_id, $contents);
        }
        // Create new page
        elseif(getStringFromRequest('action') == 'create'){
            $pageID = WikiPage::create(1,user_getid(), $title, $cat_id, $contents);
        }

        // Jump to the newly created or updated page
        header( 'Location: community/resources.php?id='.$pageID ) ;
    }
}

// Handle setting up the page appropriately
// Action is what the user is trying to do to the wiki page (edit/create)
if (getStringFromRequest("action")) {
    $action = getStringFromRequest("action");
} else {
    // Set the edit mode to create if the action is not set
    $action = "create";
}

// Title of the page to edit
if (getStringFromRequest("id")){
    $pageID = getStringFromRequest("id");
    $page = new WikiPage($pageID);
    $pageOriginalTitle = $page->getPageName();
    $pageOriginalCatID = $page->getCatID();
    $pageOriginalCatName = $page->getCatName();
    $pageOriginalContent = $page->getPageContents();
    
    if(empty($pageOriginalCatName)){
        $pageOriginalCatID = 1;
        $pageOriginalCatName = "Getting Started";
    }
    
    // Delete page
    if($action == "delete"){
        $page->delete();
        // Jump back to resources page
        header( 'Location: community/resources.php' ) ;
    }
} else{
    // Set the edit mode to create if the page is not specified
    $pageID = -1;
    $action = "create";
    $pageOriginalTitle = "Untitled Page";
    $pageOriginalCatID = 1;
    $pageOriginalCatName = "Getting Started";
    $pageOriginalContent = "<p>Type this page's content here. You can add formatting, links or images using the toolbar above.</p>";
}


// Name the save button appropriately
if($action == "edit"){
    $saveButtonText = "Save Changes";
} else{
    $saveButtonText = "Create Page";
}


use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('community_style.css');
use_javascript('bootstrap/tabs.js');
use_javascript('nicEdit.js');
site_community_header(array('title'=>'Editing '.$pageOriginalTitle), 3);

$Layout->col(3,true);
?>
<style xmlns="http://www.w3.org/1999/html">
	.well hr {
		margin: 5px 0;
		border-top: 1px solid #DDD;
	}
	p small {
		font-size: 12px;
		color: #666;
	}
	.btn {
		margin-top: 5px;
		margin-left: 5px;
	}
    #titleText{
        font-style: italic;
    }
</style>
<script type="text/javascript">
    String.prototype.capitalize = function(){
        return this.replace( /(^|\s)([a-z])/g , function(m,p1,p2){ return p1+p2.toUpperCase(); } );
    };

    $(function(){
        <?php
            if($action == "create"){
        ?>
                $('#renameModal').modal('show');
        <?php
            }
        ?>
        $("#rename").click(function() {
            $('#renameName').val($('#titleText').text());
            var catName = $('#catText').text();
            if(catName == ""){
                catName = "<?php echo $pageOriginalCatName; ?>";
            }
            $("#renameCat").find("option:contains("+catName+")").attr("selected", "selected");
            $('#renameModal').modal('show');
        });
        
        $("#delete").click(function() {
            $('#deleteModal').modal('show');
        });

        $("#renameModalSave").click(function() {
            var newName = $.trim($("#renameName").val().capitalize());
            
            $('#titleText').text(newName);
            $('#pageTitle').val(newName);
            
            $('#catText').text($('#renameCat option:selected').html());
            $('#pageCat').val($('#renameCat').val());
            $('#renameModal').modal('hide');
        });
    });
    bkLib.onDomLoaded(function() {
        new nicEditor({iconsPath:'themes/css/img/nicEditorIcons.gif', maxHeight : 600, buttonList : ['fontFormat', 'bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript', 'ol', 'ul', 'indent', 'outdent', 'removeformat', 'hr', 'link', 'unlink', 'image', 'xhtml']}).panelInstance('editor');
    });
</script>

<div class="modal hide" id="renameModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3 id="renameModalTitle">Rename Page</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
          <fieldset>
            <div class="control-group">
              <label class="control-label" for="renameCat">Category</label>
              <div class="controls">
                <select name="renameCat" id="renameCat">
                    <?php
                        $Categories = getResourceCategories();
                        foreach($Categories as $c){
                           echo '<option value="'.$c["cat_id"].'">'.$c["name"].'</option>';
                        }
                    ?>
                </select>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="renameName">Page Name</label>
              <div class="controls">
                <input type="text" class="input-xlarge" name="renameName" id="renameName" value="<?php echo $pageOriginalTitle; ?>">
              </div>
            </div>
          </fieldset>
        </form>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
        <a href="#" id="renameModalSave" class="btn btn-primary">Save changes</a>
    </div>
</div>

<div class="modal hide" id="deleteModal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3 id="deleteModalTitle">Delete Page</h3>
    </div>
    <div class="modal-body">
        <p>Are you sure you want to delete <?php echo $pageOriginalTitle; ?>?</p>
    </div>
    <div class="modal-footer">
        <a href="#" class="btn" data-dismiss="modal">Cancel</a>
        <a href="editor.php?action=delete&id=<?php echo $pageID; ?>" id="deleteConfirm" class="btn btn-danger">Delete Page</a>
    </div>
</div>

<div class="container">
            <div class="row">
                <div class="span3">
					<div class="well">
						<p><small>
						Any member can add, modify, or delete the contents on this page. 
						Share your knowledge and make this page better!  
						All changes will go through the site admin before they are published to the site.
						</small>
						</p>
						<hr />
						<a href="resources.php?id=<?php echo $pageID; ?>">Return to article</a> | <a href="resources.php">Editing help</a>
						<hr />
						<a href="#" id="delete" class="btn" style="width: 156px;">Delete Page</a>
					</div>
                </div> <!-- end col-->
                <div class="span9">
					<h2 class="inline">
                        <?php
                            if($action == "edit"){
                                echo "Editing";
                            } else{
                                echo "Creating";
                            }
                        ?>
                        <span id="catText"><?php echo $pageOriginalCatName; ?></span> / 
                        <span id="titleText"><?php echo $pageOriginalTitle; ?></span>
                    </h2> 
                    <a href="#" id="rename" style="margin-left:10px;">[edit]</a>
					<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <input type="hidden" id="pageTitle" name="pageTitle" value="<?php echo $pageOriginalTitle; ?>"/>
                        <input type="hidden" id="pageCat" name="catID" value="<?php echo $pageOriginalCatID; ?>"/>
                        <input type="hidden" id="action" name="action" value="<?php echo $action; ?>"/>
                        <input type="hidden" id="id" name="id" value="<?php echo $pageID; ?>"/>
                        <textarea cols="50" id="editor" style="width:100%; height: 500px;" name="pageContent">
                            <?php echo $pageOriginalContent;?>
                        </textarea>
                        <input type="submit" name="submit" class="btn btn-primary btn-large pull-right" value="<? echo $saveButtonText;?>"/><a href="resources.php" class="btn btn-large pull-right">Cancel</a>
					</form>

                </div><!-- end col-->
            </div><!-- end row-->
    </div>

<?php
$Layout->endcol();
site_community_footer();
?>