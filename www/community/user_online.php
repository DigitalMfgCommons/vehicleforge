<?php

require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$user_id = getStringFromRequest('user_id');
$is_online = 'false';

$userObj = user_get_object($user_id);

if($userObj->getOnlineStatus()) {
  $is_online = 'true';
}

print "{ \"user_online\":\"".$is_online."\" }";

?>