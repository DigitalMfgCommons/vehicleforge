<?php
//require_once "PHPUnit/Extensions/Database/TestCase.php";
/*require_once WIN_APP_PATH.'www/unit_tests3/database-pgsql.php';
require_once WIN_APP_PATH.'www/unit_tests3/Assert.php';*/

$fusionforge_basedir = dirname(dirname(__DIR__)) ;
$gfcommon = $fusionforge_basedir.'/common/';
$gfwww = $fusionforge_basedir.'/www/';
$gfplugins = $fusionforge_basedir.'/plugins/';
$gfimagestore=$fusionforge_basedir.'/image_store/';

define('APP_PATH',dirname(dirname(__DIR__)).DIRECTORY_SEPARATOR);

spl_autoload_register(function($className){
    $Tried="";
    $Possibilities=array(
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'include'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'forum'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'Amazon'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'Amazon'.DIRECTORY_SEPARATOR.'sdk-1.5.2'.DIRECTORY_SEPARATOR.'services'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'Amazon'.DIRECTORY_SEPARATOR.'sdk-1.5.2'.DIRECTORY_SEPARATOR.$className.'.class.php',
        APP_PATH.'common'.DIRECTORY_SEPARATOR.'phpmailer'.DIRECTORY_SEPARATOR.$className.'.class.php'
    );

    foreach($Possibilities as $i){
        if (file_exists($i)){
            require_once $i;
            return true;
        }else{
            $Tried.=$i.'|';
        }
    }

    throw new Exception('Class not found: '.$className.': '.$Tried);
});

require_once 'database-pgsql.php';
require_once 'Assert.php';
//require_once 'utils.php';
require_once APP_PATH.'common/include/session.php';

if (!db_connect())
    exit;
?>
