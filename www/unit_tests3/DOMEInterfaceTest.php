<?php
require_once 'db_inc.inc';
//require_once 'DOMEInterface.class.php';
//require_once 'CEM.class.php';
use Assert as is;

class DOMEInterfaceTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var DOMEInterface
     */
    protected $object, $CEM, $CEMID, $Server, $InitObject;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        db_begin();
        $this->Server=Server::create("http://ec2-107-20-101-113.compute-1.amazonaws.com:7080/DOMEApiServicesV4/",105);

        $this->CEMID=CEM::create(28,"test");

        $ID=DOMEInterface::create($this->CEMID,'{"interFace":{"version":1,"modelId":"d5132080-d06b-1004-8706-a30ae6bad741","interfaceId":"d5132082-d06b-1004-8706-a30ae6bad741","type":"interface","name":"Area Interface","path":[29,30]},"inParams":{"width":{"name":"width","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb041-ce50-1004-8637-03affb87f791"},"length":{"name":"length","type":"Real","unit":"foot ( U.S.)","value":2,"id":"4fcdb040-ce50-1004-8637-03affb87f791"}},"outParams":{"area":{"name":"area","type":"Real","unit":"square foot ( Int\'l)","value":4.000016000047999,"id":"4fcdb042-ce50-1004-8637-03affb87f791"}}}', $this->Server);
        $this->object=new DOMEInterface($ID);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        db_rollback();
    }

    /**
     * @covers DOMEInterface::getID
     * @todo   Implement testGetID().
     */
    public function testGetID()
    {
        is::int($this->object->getID());
    }

    /**
     * @covers DOMEInterface::create
     * @todo   Implement testCreate().
     */
    public function testCreate()
    {
        //Positive
        is::int(DOMEInterface::create($this->CEMID,"data",2));

        //Negative - Wrong argument types
        is::false(DOMEInterface::create("1",213,"321"));
    }

    /**
     * @covers DOMEInterface::getData
     * @todo   Implement testGetData().
     */
    public function testGetData()
    {

        //Positive
        is::obj($this->object->getData());
    }

    /**
     * @covers DOMEInterface::getServerID
     * @todo   Implement testGetServerID().
     */
    public function testGetServerID()
    {
        is::int($this->object->getServerID());
    }

    /**
     * @covers DOMEInterface::getCEMID
     * @todo   Implement testGetCEMID().
     */
    public function testGetCEMID()
    {
        is::int($this->object->getCEMID());
    }

    /**
     * @covers DOMEInterface::getName
     * @todo   Implement testGetName().
     */
    public function testGetName()
    {
        is::string($this->object->getName());
    }
}
