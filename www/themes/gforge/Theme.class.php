<?php
/**
 * Default Theme
 *
 * Copyright 2010 (c) FusionForge Team
 * Copyright (C) 2010-2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once $gfwww.'include/Layout.class.php';

define('TOP_TAB_HEIGHT', 30);
define('BOTTOM_TAB_HEIGHT', 22);

class Theme extends Layout {
	var $header_displayed = false,
	$SubMenu=array(),
	$SubMenuSelected=null,
	$TertMenu=array(),
	$SubMenuLocation=null;

	function Theme() {
		// Ensure our stylesheets are loaded before the default one (reset first).
		/* 		$this->addStylesheet('/scripts/yui/reset-fonts-grids/reset-fonts-grids.css'); */
		/* 		$this->addStylesheet('/scripts/yui/base/base-min.css'); */
		$this->addStylesheet('/themes/css/bootstrap/bootstrap.min.css');
		/* 		$this->addStylesheet('/themes/css/fusionforge.css'); */
		/*  	$this->addStylesheet('/themes/gforge/css/theme-pages.css'); */

		$this->addStylesheet('/themes/css/fusionforge_new.css');

		// Parent constructor
		$this->Layout();
		//$this->doctype = 'strict';
		$this->themeurl = util_make_url('themes/gforge/');
		$this->imgbaseurl = $this->themeurl . 'images/';
		$this->imgroot = $this->imgbaseurl;
	}

	/**
	 * headerHTMLDeclaration() - generates the HTML declaration, i.e. the
	 * XML declaration, the doctype definition, and the opening <html>.
	 *
	 */
	function headerHTMLDeclaration() {
		global $sysXMLNSs;
		echo "<!DOCTYPE html>\n";
		echo '<html lang="en" ' . $sysXMLNSs . ">\n";
	}

	/**
	 * @param string|array $Header
	 * @param string|array $Body
	 * @return string
	 */
	function widget($Header, $Body, $ExtraClasses='', $attrs = array()){
		if (is_array($Header)){
			$Header=$Header[0].'<span class="label update pull-right">'.$Header[1].'</span>';
		}

		if (is_array($Body)){
			$_Body='<ul class="widget-list">';
			foreach($Body as $i)
				$_Body.='<li>'.$i.'</li>';

			$_Body.='</ul>';
		}else{
			$_Body=$Body;
		}

		$Return='<div class="cell '.$ExtraClasses.'" '.util_parse_attributes($attrs).'>
            <div class="widget">
            <div class="widget-header">
			<h5>'.$Header.'</h5>
            </div>
            <div class="widget-body">'.$_Body.'</div>
            </div>
            </div>';

		return $Return;
	}

	/**
	 * @param string|array $Header
	 * @param array $ListItems
	 * @return string
	 */
	function widget_list($Header,$ListItems=array(), $attrs=array()){
		return $this->widget($Header,$ListItems,'', $attrs);
	}

	function bodyHeader($params) {
		//global $user_guide;


		// Don't display the headers twice (when errors for example).
		if ($this->header_displayed)
			return;
		$this->header_displayed=true;

		// The root location for images
		if (!isset($params['h1'])) {
			$params['h1'] = util_ifsetor($params['title'], "!! title not set !!");
		}

		if (!util_ifsetor($params['title'])) {
			$params['title'] = forge_get_config('forge_name');
		} else {
			$params['title'] = $params['title'] . " – " . forge_get_config('forge_name');
		}
		//TODO: NAVBAR HEADER HERE////////////////////////////////////////
?>
<div id="content-wrapper">
<div class="navbar navbar-fixed-top navbar-inverse">
<div class="navbar-inner">
<div class="container" style="position:relative">
<div class="brand">
<?php echo util_make_link ('/', html_image('header/gelogo.png',145,22,array('alt'=>'VehicleForge')).html_image('header/logo_gear.png',17,17,array('id'=>'logo_gear','class'=>'rotate')),array('id'=>'logo'))?>
</div>

<div class="nav-collapse">
<ul class="nav">
<?php $this->outerTabs(); ?>
</ul>
</div>

<div class="nav-collapse pull-right">
<ul class="nav">
<form action="/search/" method="get" class="navbar-search pull-left">
<input type="text" class="search-query span2" name="q" placeholder="Search..." autocomplete="off" />
</form>
</ul>
</div>

<div class="nav-collapse pull-right">
<ul class="nav">
<?php
		if (session_loggedin()){
			$RunningModels=session_get_user()->getRunningModels();
			$FinishedModels=session_get_user()->getFinishedModels();

			$NotViewedModels=array_merge($RunningModels, $FinishedModels);
?>
<li class="dropdown">
<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
<i class="icon-wrench icon-white"></i>
<?php
			echo '<span id="running_model_count" class="badge badge-warning badge-notification'.(!$RunningModels?' hidden':'').'">'.count($RunningModels).'</span>';
			echo '<span id="finished_model_count" class="badge badge-success badge-notification'.(!$FinishedModels?' hidden':'').'">'.count($FinishedModels).'</span>';
?>
</a>
<ul class="dropdown-menu" id="interface_run_list">
<?php
			if ($NotViewedModels){
				foreach($NotViewedModels as $i){
					$Interface=new DOMEInterface($i->getInterfaceID());
					$CEM=new CEM($Interface->getCEMID());
					echo '<li data-run-id="'.$i->getID().'" class="interface_run">';
					if ($i->getStatus()){
						echo '<a href="/services/index.php?group_id='.$CEM->getGroupID().'&diid='.$Interface->getID().'&runid='.$i->getID().'">'.$Interface->getName();
					}else{
						echo '<a href="javascript:void(0)">'.$Interface->getName();
					}
					echo '<span class="run_status btn btn-mini disabled ';
					echo (!$i->getStatus()?'btn-warning">Running':'btn-success">Done');
					echo '</span></a></li>';
				}
?>
<li class="divider"></li>
<?php
			}
?>
<li><a href="javascript:void(0)" id="check_finished_models">Update</a></li>
<li><a href="/account/run_history.php">All History</a></li>
</ul>
</li>
<li class="dropdown">
<a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)">
<i class="icon-envelope icon-white"></i>
<?php
			if ($MessageCount=session_get_user()->getUnreadMessageCount())
				echo '<span id="message_count" class="badge badge-info badge-notification">'.$MessageCount.'</span>';
?>
</a>
<ul class="dropdown-menu">
<?php
			/*if ($MessageCount){
     $Messages=session_get_user()->getMessages(5,false);
     foreach($Messages as $i){
     echo '<li><a href="/account/messages.php?action=view&id='.$i->getID().'">'.$i->getSubject().'</a></li>';
     }
     }else{
     echo '<li><a href="javascript:void(0)">No New Messages :(</a></li>';
     }*/
?>
<li><a href="/account/messages.php?action=inbox">Inbox</a></li>
<li><a href="/account/messages.php?action=compose">Create Message</a></li>
</ul>
</li>
<li><a href="/my/"><i class="icon-home icon-white"></i></a></li>
<li class="dropdown">
<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
<?php echo session_get_user()->getRealName()?>
<b class="caret"></b>
</a>
<ul class="dropdown-menu">
<?php
			$items = $this->navigation->getUserLinks();
			for ($j = 0; $j < count($items['titles']); $j++) {
				if ($items['titles'][$j]==null)
					echo '<li class="divider"></li>';
				else
					echo '<li>'.util_make_link($items['urls'][$j], $items['titles'][$j],array(), true).'</li>';
			}
?>
</ul>
</li>
<?php
		}else{
?>
<li><a id="site_login" href="/account/login.php">Log In</a></li>
<li><a id="site_register" href="/account/register2.php">Register</a></li>
<?php
		}
?>
</ul>
</div>
</div>
</div>
</div>
<div id="body" style="padding-top:30px;">
<div class="container">
<?php
		if(isset($GLOBALS['error_msg']) && $GLOBALS['error_msg']) {
			echo $this->error_msg($GLOBALS['error_msg']);
		}
		if(isset($GLOBALS['warning_msg']) && $GLOBALS['warning_msg']) {
			echo $this->warning_msg($GLOBALS['warning_msg']);
		}
		if(isset($GLOBALS['feedback']) && $GLOBALS['feedback']) {
			echo $this->feedback($GLOBALS['feedback']);
		}

		/*if (isset($params['submenu']))
     echo $params['submenu'];*/

		$Notification="";
		if($Notification){
			echo '<div style="background: #F00;border:4px solid #000;padding:5px;font-size:20px;border-radius:5px;text-align:center">'.$Notification.'</div>';
		}
	}

	function bodyFooter($params) {
?>
</div><!--.container-->
</div>
<div id="push"></div>
</div>
<?php
	}

	function footer($params,$ShowTime=false) {
		$this->bodyFooter($params);
?>
<div id="dialog_feedback" title="Feedback Form" style="width:400px">Find a bug? Have a suggestion? Let me know!
<textarea cols="50" rows="3" id="feedback"></textarea></div>
<style>
footer{
    padding-top:30px;
    border-top: 1px solid #333;
background: rgb(45,45,45); /* Old browsers */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iIzJkMmQyZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjIyJSIgc3RvcC1jb2xvcj0iIzQ0NDQ0NCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
background: -moz-linear-gradient(top,  rgba(45,45,45,1) 0%, rgba(68,68,68,1) 22%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(45,45,45,1)), color-stop(22%,rgba(68,68,68,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(45,45,45,1) 0%,rgba(68,68,68,1) 22%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(45,45,45,1) 0%,rgba(68,68,68,1) 22%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(45,45,45,1) 0%,rgba(68,68,68,1) 22%); /* IE10+ */
background: linear-gradient(top,  rgba(45,45,45,1) 0%,rgba(68,68,68,1) 22%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2d2d2d', endColorstr='#444444',GradientType=0 ); /* IE6-8 */
}

nav ul {
    list-style: none;
}
footer nav ul li {
    list-style: none;
}
footer nav ul.mainNav {
    margin-bottom: 10px;
}
footer nav ul {
overflow: hidden;
padding: 0px;
}
footer nav ul.mainNav li, footer nav ul.secondaryNav li {
    float: left;
    margin-right: 12px;
}

footer nav ul.mainNav li a {
color: #999;
    font-size: 13px;
}

footer nav ul.mainNav li a:hover {
color: #EEE;
}

footer nav ul.secondaryNav li a:hover {
color: #777;
}
footer nav ul.secondaryNav li a {
color: #777;
    font-size: 12px;
}
.copyright a {
color: #777;
    font-weight: bold;
    padding-left: 4px;
}
footer .copyright {
    text-align: left;
color: #999;
    font-size: 11px;
    margin-top: 40px;
    margin-left: 15px;
}

</style>
<footer>
<div class="container">
<div class="row">
<div class="span12">
<nav>
<ul class="mainNav">
<li><a href="<?=util_make_url ('index2.php'); ?>"><?=_('About Us'); ?></a></li>
<li><a href="<?=util_make_url ('community/latest.php'); ?>"><?=_('Latest News'); ?></a></li>
<li><a href="<?=util_make_url ('community/members.php'); ?>"><?=_('Members'); ?></a></li>
<li><a href="<?=util_make_url ('community/boards.php'); ?>"><?=_('Discussion Board'); ?></a></li>
<li><a href="<?=util_make_url ('community/resources.php'); ?>"><?=_('Resources'); ?></a></li>
<li><a href="<?=util_make_url ('marketplace/components.php'); ?>"><?=_('Components'); ?></a></li>
<li><a href="<?=util_make_url ('marketplace/services.php'); ?>"><?=_('Services'); ?></a></li>
<li><a href="<?=util_make_url ('softwaremap/full_list.php'); ?>"><?=_('Projects'); ?></a></li>
</ul>
<ul class="secondaryNav">
<li><a href="http://dmdii.uilabs.org/membership/faq" target="_blank">FAQs</a></li>
<li><a href="http://dmdii.uilabs.org/connect" target="_blank">Contact Us</a></li>
<li><a href="http://www.facebook.com" target="_blank">Facebook</a></li>
<li><a href="http://www.twitter.com" target="_blank">Twitter</a></li>
</ul>
</nav>
<div class="copyright">
© 2011-2014 GE Global Research <a href="#">Terms of Service</a> <a href="#">Privacy Policy</a>
</div>
</div>
</div>
</div>
</footer>
<?php
		if ($ShowTime){
			$END_PAGE_TIME=explode(" ",microtime());
			$END_PAGE_TIME=$END_PAGE_TIME[1]+$END_PAGE_TIME[0];
			global $START_PAGE_TIME;

			$TOTAL_PAGE_TIME=$END_PAGE_TIME-$START_PAGE_TIME;
			echo 'Page loaded in '.round($TOTAL_PAGE_TIME,5).' seconds';
		}
		echo '</body>
    </html>';
	}

	function heading($Text,$Weight=1,$Return=false){
		$ReturnCode='<h'.$Weight.'>'.$Text.'</h'.$Weight.'>';

		if (!$Return){
			echo $ReturnCode;
			return $this;
		}else{
			return $ReturnCode;
		}
	}

	function alert($Text, $Type){
		$Code='<div class="alert alert-'.$Type.'">
        <a class="close" href="javascript:void(0)">&times;</a>
        <p>'.$Text.'</p>
		</div>';

		return $Code;
	}


	/**
	 * sectionStart() - Create Section
	 * @param string id
	 * @return \Theme
	 */
	function sectionStart($id='') {
		if ($id) {
			$id = $this->toSlug($id);
			echo '<section id="'.$id.'">';
		}
		else {
			echo '<section>';
		}

		return $this;
	}

	/**
	 * sectionEnd() - End Section
	 * @return \Theme
	 */
	function sectionEnd() {
		echo '</section>';

		return $this;
	}


	/**
	 * boxTop() - Top HTML box
	 * @return \Theme
	 */
	function boxTop($id='',$attribs=array()) {
		echo '<h1>INSTANCE OF BOXTOP</h1>';
		$Result='<section class="box"';

		if ($id) {
			$id = $this->toSlug($id);
			$Result.=' id="'.$id.'"';
		}

		while($i=pos($attribs)){
			$Result.=' '.key($attribs).'="'.$i.'"';
			next($attribs);
		}

		$Result.='/>';

		//$Result.='<div class="box">';
		echo $Result;

		return $this;
	}

	/**
	 * boxMiddle() - Middle HTML box
	 *
	 * @param   string  Box title
	 * @param   string  The box background color
	 */
	function boxMiddle($title, $id = '') {
		if ($id) {
			$id = $this->toSlug($id);
			$idtitle = ' id="' . $id . '-title"';
		} else {
			$idtitle = "";
		}

		$t_result ='
        </div> <!-- class="box-content" -->
        <h3' . $idtitle . ' class="box-middle">'.$title.'</h3>
        <div class="box-content">
        ';
		return $t_result;
	}

	/**
	 * boxBottom() - Bottom HTML box
	 *
	 * @return \Theme
	 */
	function boxBottom() {
		echo '<h1>INSTANCE OF BOXBOTTOM</h1>';
		echo '</section>';
		return $this;
	}

	function menu($params,$Selected=null){
		$request_uri = getStringFromServer('REQUEST_URI');

		$Count=sizeof($params);
		for($i=0;$i<$Count;$i++){
			// parse parameters
			$linkURL = $params[$i][1];
			$linkText = $params[$i][0];

			// (fallback) if a tab selection wasn't specified, try to infer it from the link url
			if ($Selected == null && util_make_uri($linkURL)===$request_uri)
				$Selected=$i;

			// create menu
			echo '<li' . (($Selected==$i) ? ' class="active"' : '') . '>';
			echo util_make_link($linkURL, $linkText);
			echo '</li>';
		}
	}

	function submenu($params=null,$Selected=null){
		if ($params!=null){
			echo '<h1>Convert the subnav for this page to use submenu_add</h1>';
			$this->setError("Convert the subnav for this page to use submenu_add");
		}

		global $Layout;
		$Layout->col(12);
		echo '<nav class="subnav"><ul class="tabs nav-tabs" style="margin-bottom:8px;margin-left:0">';

		$this->menu($this->SubMenu,$this->SubMenuSelected);
		echo '</ul></nav>';
		$Layout->endcol();
	}

	function submenu_init($LocationID){
		$this->SubMenuSelected=$LocationID;
	}

	function submenu_add($Text,$Link){
		$this->SubMenu[]=array($Text,$Link);
	}

	function tertiary_menu($Selected=null){
		echo '<nav>
		<ul class="nav nav-pills">';
		$this->menu($this->TertMenu, $Selected);
		echo '</ul></nav>';
	}

	function tertmenu_add($Text, $Link){
		$this->TertMenu[]=array($Text, $Link);
	}

	/**
	 * boxGetAltRowStyle() - Get an alternating row style for tables
	 *
	 * @param               int             Row number
	 */
	function boxGetAltRowStyle($i) {
		if ($i % 2 == 0) {
			return 'class="bgcolor-white"';
		} else {
			return 'class="bgcolor-grey"';
		}
	}

	function tabGenerator($TABS_DIRS, $TABS_TITLES, $nested=false, $selected=false, $sel_tab_bgcolor='WHITE', $total_width='100%') {
		$count=count($TABS_DIRS);
		if ($count < 1)
			return false;

		$return='<div class="tabGenerator">';

		$accumulated_width = 0;
		for ($i=0; $i<$count; $i++) {
			$tabwidth = intval(ceil(($i+1)*100/$count)) - $accumulated_width ;
			$accumulated_width += $tabwidth ;

			$return.='<div class="tab';
			if ($selected==$i)$return.=' selected';
			$return.='" style="width:'.$tabwidth.'%">
			<a href="'.$TABS_DIRS[$i].'">'.$TABS_TITLES[$i].'</a>
			</div>';
		}

		$return.='</div>';
		return $return;
	}


	/**
	 * beginSubMenu() - Opening a submenu.
	 *
	 * @return    string    Html to start a submenu.
	 */
	function beginSubMenu () {
		/*$return = '
         <p><strong>';*/
		$return='<div class="row"><div class="twelvecol">'.$this->boxTop('');
		return $return;
	}

	/**
	 * endSubMenu() - Closing a submenu.
	 *
	 * @return    string    Html to end a submenu.
	 */
	function endSubMenu () {
		//$return = '</strong></p>';
		$return =$GLOBALS['HTML']->boxBottom().'</div></div>';
		return $return;
	}

	/**
	 * printSubMenu() - Takes two array of titles and links and builds the contents of a menu.
	 *
	 * @param       array   The array of titles.
	 * @param       array   The array of title links.
	 * @return    string    Html to build a submenu.
	 */
	function printSubMenu ($title_arr,$links_arr) {
		echo '<h1>OLD printSubMenu</h1>';
		$count=count($title_arr);
		$count--;

		$return = '';

		for ($i=0; $i<$count; $i++) {
			$return .= util_make_link ($links_arr[$i], $title_arr[$i]) . ' | ';
		}
		$return .= util_make_link ($links_arr[$i], $title_arr[$i]);
		return $return;
	}

	/**
	 * subMenu() - Takes two array of titles and links and build a menu.
	 *
	 * @param       array   The array of titles.
	 * @param       array   The array of title links.
	 * @return    string    Html to build a submenu.
	 *
	 * @deprecated
	 */
	function _subMenu ($title_arr,$links_arr) {
		//$return  = $this->beginSubMenu () ;
		$return = $this->printSubMenu ($title_arr,$links_arr) ;
		//$return .= $this->endSubMenu () ;
		return $return;
	}

	/**
	 * multiTableRow() - create a mutlilevel row in a table
	 *
	 * @param    string    the row attributes
	 * @param    array    the array of cell data, each element is an array,
	 *                      the first item being the text,
	 *                    the subsequent items are attributes (dont include
	 *                    the bgcolor for the title here, that will be
	 *                    handled by $istitle
	 * @param    boolean is this row part of the title ?
	 *
	 */
	function multiTableRow($row_attr, $cell_data, $istitle) {
		$return= '
        <tr class="ff" '.$row_attr;
		if ( $istitle ) {
			$return .=' align="center"';
		}
		$return .= '>';
		for ( $c = 0; $c < count($cell_data); $c++ ) {
			$return .='<td class="ff" ';
			for ( $a=1; $a < count($cell_data[$c]); $a++) {
				$return .= $cell_data[$c][$a].' ';
			}
			$return .= '>';
			if ( $istitle ) {
				$return .='<strong>';
			}
			$return .= $cell_data[$c][0];
			if ( $istitle ) {
				$return .='</strong>';
			}
			$return .= '</td>';

		}
		$return .= '</tr>
        ';

		return $return;
	}

	/**
	 * getThemeIdFromName()
	 *
	 * @param    string  the dirname of the theme
	 * @return    integer the theme id
	 */
	function getThemeIdFromName($dirname) {
		$res=db_query_params ('SELECT theme_id FROM themes WHERE dirname=$1',array($dirname));
		return db_result($res,0,'theme_id');
	}
}

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
