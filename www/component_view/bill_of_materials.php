<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

// Include css/less for marketplace //TODO: include this in main CSS
$HTML->addStylesheet('/themes/gforge/css/marketplace.less');

// Create the site header
site_header(array('title'=>'Component View – Bill of Materials'));
?>

<div class="span3">

	<?php 
	$HTML->boxTop();
	$HTML->heading("Organizing Principle", 3); 
	?>
	<style type="text/css" style="display:none;">.selected {font-weight:bold;}</style>
	<ul style="list-style-type:disc; padding-left:10px;">
		<li><a href="./classic.php">Classic</a></li>
		<li><a href="./volume.php">Volume/3D</a></li>
		<li><a class="selected" href="./bill_of_materials.php">Bill of Materials</a></li>
		<li><a href="./services.php">Service Dependency</a></li>
		<li><a href="./personnel.php">Personnel</a></li>
		<li><a href="./supply_chain.php">Supply Chain</a></li>										
	</ul>
	
	<?php $HTML->boxBottom(); ?>

</div>

<div class="span9">

<style type="text/css" style="display:none;">
	ul[class|='l'] {margin-left:2em;}
	ul[class|='l'] li {list-style-image: url('/images/pointer_right.png');}
	ul[class|='l'] li.expanded {list-style-image: url('/images/pointer_down.png');}
	ul[class|='l'] li.i {list-style:disc;}
</style>
	
<?php 
$HTML->boxTop();
$HTML->heading("Powertrain System", 3);
echo '<ul class="l-0">';
listStart("l-1", "1 @ Engine", true);
	listItem("i", "4 @ .386 piston (#238203)");
	listItem("i", "1 @ 4-cylinder crankshaft (#293052)");
	listItem("i", "1 @ manifold (#293405)");	
	listItem("i", "1 @ GM throttle assy (#234405)");		
	listItem("i", "1 @ GM fuel injection assy (#74032)");		
listEnd();

listStart("l-1", "1 @ Transmission", false);
listEnd();

listStart("l-1", "1 @ Front Drive Axle", false);
listEnd();

listStart("l-1", "1 @ Rear Drive Axle", false);
listEnd();

listStart("l-1", "1 @ Cooling System", false);
listEnd();
	
echo '</ul>';

$HTML->boxBottom(); ?>
</div>

<?php
// Create site footer
site_footer(array());

function listStart($class, $root, $expanded) {
	if ($expanded) {
		echo '<li class="expanded">' . "<span style=\"font-weight:600;\">" . $root . "</span>" . '<ul class= "' . $class . '">';
	} else { 
		echo '<li>' . "<span style=\"font-weight:600;\">" . $root . "</span>" . '<ul class="' . $class . '">';
	}
}

function listEnd() {
	echo '</ul></li>';
}

function listItem($class, $root) {
	echo '<li class="' . $class . '">' . "<em>" . $root . "</em>" . '</li>';
}
?>