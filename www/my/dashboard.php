<?php
/**
 * FusionForge User's Personal Page
 *
 * Copyright 1999-2001, VA Linux Systems, Inc.
 * Copyright 2002-2004, GForge Team
 * Copyright 2009, Jean-Pierre Fortune/Spirtech
 * Copyright 2009-2010, Roland Mas
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'include/vote_function.php';
require_once $gfcommon.'tracker/ArtifactFactory.class.php';
require_once $gfcommon.'tracker/ArtifactsForUser.class.php';
require_once $gfcommon.'forum/ForumsForUser.class.php';
require_once $gfcommon.'pm/ProjectTasksForUser.class.php';

if (!session_loggedin()) {
	exit_not_logged_in();
}

use_javascript('bootstrap/tabs.js');
add_js('$(function(){
	$(".expand_button").click(function(){
			var $e=$("#"+$(this).data("elem"));
			if ($e.is(":hidden")){
				$(this).children("img").attr("src","'.$HTML->imgroot.'pointer_down.png");
			}else{
				$(this).children("img").attr("src","'.$HTML->imgroot.'pointer_right.png");
			}
			$e.slideToggle();
		});
});');

site_user_header(array('title'=>sprintf(_('Personal Page For %s'),session_get_user()->getRealName())));
$Layout->col(12,true);

$Groups=session_get_user()->getGroups();

echo '<ul class="nav nav-tabs">
<li class="active"><a href="#trackers" data-toggle="tab">Artifacts</a></li>
<li><a href="#tasks" data-toggle="tab">Tasks</a></li>
</ul>
<div class="tab-content">
<div class="tab-pane active" id="trackers">';
if (count($Groups)){
	echo '<style>
	td.group a{font-size:16px;font-weight:bold}
	</style>';
	
	$Columns=array(
		'Summary'=>true,
		'Last Updated'=>true,
		'Status'=>false,
		'Priority'=>true,
		'Assigned To'=>true,
		'Submitted By'=>true,
		'Associated Tasks'=>false
	);

    $TableHead=array();
    foreach($Columns as $key=>$val){
        if ($val){
            $TableHead[]=$key;
        }
    }

    $Table=new BsTable(array('class'=>'table table-striped'));
    $Table->head($TableHead);

	foreach($Groups as $g){
		$atf=new ArtifactTypeFactory($g);
		$atf_array=$atf->getArtifactTypes();
		$Artifacts=array();
		foreach($atf_array as $at){
			$ath=new ArtifactTypeHtml($g,$at->getID());
			$af=new ArtifactFactory($ath);
			$af->setup(0,"priority","DESC",0,"",session_get_user()->getID(),1);
			$Artifacts=$af->getArtifacts();
		}
		if (count($Artifacts)){
            $Table->col(util_make_link('/tracker/?group_id='.$g->getID(),$g->getPublicName()).' - '.count($Artifacts),array(
                'colspan'=>count($TableHead),
                'class'=>'group'
            ));

			foreach($Artifacts as $a){
				if ($a->getAssignedTo() == session_get_user()->getID()) {
	                $Table->col(util_make_link('/tracker/?func=detail&aid='.$a->getID().'&group_id='.$g->getID().'&atid='.$ath->getID(),$a->getSummary()))
	                    ->col(date('M d, Y',$a->getLastModifiedDate()).' at '.date('g:ia',$a->getLastModifiedDate()))
	                    ->col($a->getPriority())
	                    ->col($a->getAssignedRealName())
	                    ->col($a->getSubmittedRealName());
				}
            }
        }
    }


    echo $Table->render();
}else{
	echo '<strong>'._('You are not a member of any active projects').'</strong>';
}
echo '</div>
<div id="tasks" class="tab-pane">';
$sql = 'SELECT groups.group_id, groups.group_name, project_group_list.group_project_id, project_group_list.project_name '.
            'FROM groups,project_group_list,project_task,project_assigned_to '.
            'WHERE project_task.project_task_id=project_assigned_to.project_task_id '.
            'AND project_assigned_to.assigned_to_id=$1'.
            ' AND project_task.status_id=1 AND project_group_list.group_id=groups.group_id '.
            "AND project_group_list.is_public!='9' ".
          'AND project_group_list.group_project_id=project_task.group_project_id GROUP BY groups.group_id, groups.group_name, project_group_list.project_name, project_group_list.group_project_id';

$result=db_query_params($sql,array(user_getid()));
$rows=db_numrows($result);
if ($result && $rows) {
	$Content='<style>.done,.not_done{float:left;height:8px}.done{background:#0F0}.not_done{background:#F00}</style>';

	for ($j=0; $j<$rows; $j++) {
		$group_id = db_result($result,$j,'group_id');
		$group_project_id = db_result($result,$j,'group_project_id');

		$sql2 = 'SELECT project_task.project_task_id, project_task.priority, project_task.summary,project_task.percent_complete '.
			'FROM groups,project_group_list,project_task,project_assigned_to '.
			'WHERE project_task.project_task_id=project_assigned_to.project_task_id '.
			"AND project_assigned_to.assigned_to_id=$1 AND project_task.status_id='1'  ".
			'AND project_group_list.group_id=groups.group_id '.
			"AND groups.group_id=$2 ".
			'AND project_group_list.group_project_id=project_task.group_project_id '.
			"AND project_group_list.is_public!='9' ".
		   "AND project_group_list.group_project_id= $3 ORDER BY groups.group_id DESC LIMIT 100";

		$result2 = db_query_params($sql2,array(user_getid(),$group_id,$group_project_id));
		$rows2 = db_numrows($result2);

		$Content.='<div><a href="javascript:void(0)" class="expand_button" data-elem="tasks_'.$group_project_id.'"><img src="'.$HTML->imgroot.'pointer_right.png" /></a>  <a href="/pm/task.php?group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'">'.db_result($result,$j,'group_name').' - '.db_result($result,$j,'project_name').'</a> ['.$rows2.']
		<div id="tasks_'.$group_project_id.'" class="hide">';
		for ($i=0; $i<$rows2; $i++) {

			$Done=db_result($result2,$i,'percent_complete');
			//echo '<p><a href="/pm/task.php/?func=detailtask&amp;project_task_id='.db_result($result2, $i, 'project_task_id').'&amp;group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'">'.stripslashes(db_result($result2,$i,'summary')).'</a><span style="float:right">'.db_result($result2,$i,'percent_complete').'%</span></p>';
			$Content.='<a href="/pm/task.php/?func=detailtask&amp;project_task_id='.db_result($result2, $i, 'project_task_id').'&amp;group_id='.$group_id.'&amp;group_project_id='.$group_project_id.'">'.stripslashes(db_result($result2,$i,'summary')).'</a>
				<div class="completion_status">
					<div class="done" style="width:'.$Done.'%">&nbsp;</div>
					<div class="not_done" style="width:'.(100-$Done).'%">&nbsp;</div>
				</div><br />';
		}
		$Content.='</div></div>';
	}

	echo $Content;
}
echo '</div>
</div>';
/*
// Include both groups and foundries; developers should be similarly
// aware of membership in either.
$projects = session_get_user()->getGroups() ;
if (count ($projects) < 1) {
	echo '<strong>'._('You\'re not a member of any active projects').'</strong>';
} else {
	$display_col=array('summary'=>1,
			'changed'=>1,
			'status'=>0,
			'priority'=>1,
			'assigned_to'=>1,
			'submitted_by'=>1,
			'related_tasks'=>1);

	$title_arr=array();

	$title_arr[]=_('ID');
	if ($display_col['summary'])
		$title_arr[]=_('Summary');
	if ($display_col['changed'])
		$title_arr[]=_('Changed');
	if ($display_col['status'])
		$title_arr[]=_('Status');
	if ($display_col['priority'])
		$title_arr[]=_('Priority');
	if ($display_col['assigned_to'])
		$title_arr[]=_('Assigned to');
	if ($display_col['submitted_by'])
		$title_arr[]=_('Submitted by');
	if ($display_col['related_tasks'])
		$title_arr[]=_('Tasks');

	echo $GLOBALS['HTML']->listTableTop ($title_arr);

	foreach ($projects as $p) {
                    //$admin_flags = forge_check_perm ('project_admin',$p->getID()) ;

                    //  get the Project object
                    //
                    $atf = new ArtifactTypeFactory($p);
                    $at_arr =& $atf->getArtifactTypes();

		$art_found = 0;

		if(count($at_arr) > 0) {
                        echo '
                        <tr>
                        <td colspan="' . (array_sum($display_col)+1) . '" align="left" style="background-color: #CADACA; padding-top: 4px; border-top: 1px dotted darkgreen; border-bottom: 1px solid darkgreen; font-size: larger; color: darkgreen;"><strong> • ' .
				util_make_link ('/tracker/?group_id='.$p->getID(),
						$p->getPublicName())
				. '</strong></td></tr>';
                        foreach($at_arr as $at) {
				$art_found = 1;
                                //
                                //      Create the ArtifactType object
                                //
                                $ath = new ArtifactTypeHtml($p,$at->getID());
                                // create artifact object, setup object
                                $af = new ArtifactFactory($ath);
                                $af->setup(0,"priority","DESC",0,"",0,1,null);
                                // get artifacts from object
                                $art_arr = $af->getArtifacts();
                                if (count($art_arr) > 0) {
                                        echo '<tr><td colspan="' . (array_sum($display_col)+1) . '" align="left" style="color: darkred; border-bottom: 1px solid #A0A0C0; border-top: 1px dotted #A0A0C0; background-color: #CACADA;"><strong> · '.
						util_make_link ('/tracker/?group_id='.$at->Group->getID().'&amp;atid='.$at->getID(),
								$at->getName()) . '</strong></td></tr>';
                                        $toggle=0;
                                        foreach($art_arr as $art) {
                                                echo '<tr '. $HTML->boxGetAltRowStyle($toggle++) . ' valign="top"><td align="center">'. $art->getID() .'</td>';
                                                if ($display_col['summary'])
                                                echo '<td align="left"><a href="/tracker/?func=detail&amp;aid='.
                                                        $art->getID() .
                                                        '&amp;group_id='. $p->getID() .'&amp;atid='.
                                                        $ath->getID().'">'.
                                                        $art->getSummary().
                                                        '</a></td>';
                                                if ($display_col['changed'])
                                                        echo '<td width="150">'
                                                                .date(_('Y-m-d'),$art->getLastModifiedDate()) .'</td>';
                                                if ($display_col['status'])
                                                        echo '<td>'. $art->getStatusName() .'</td>';
                                                if ($display_col['priority'])
							echo '<td class="priority'.$art->getPriority() .'" align="center">'. $art->getPriority() .'</td>';
                                                if ($display_col['assigned_to'])
                                                        echo '<td>'. $art->getAssignedRealName() .'</td>';
                                                if ($display_col['submitted_by'])
                                                        echo '<td>'. $art->getSubmittedRealName() .'</td>';
                                                if ($display_col['related_tasks']) {
							$result_tasks = $art->getRelatedTasks();
							if($result_tasks) {
								$taskcount = db_numrows($art->relatedtasks);
                                                                echo '<td>';
							        if ($taskcount > 0) {
                                        for ($itask = 0; $itask < $taskcount; $itask++) {
										if($itask>0)
											echo '<br/>';
										$taskinfo = db_fetch_array($art->relatedtasks, $itask);
                                                $taskid = $taskinfo['project_task_id'];
							                    $projectid = $taskinfo['group_project_id'];
								            $groupid   = $taskinfo['group_id'];
										$g = group_get_object($groupid);
										$pg = new ProjectGroup($g, $projectid);
										echo $pg->getName().'<br/>';
                                                $summary   = util_unconvert_htmlspecialchars($taskinfo['summary']);
										echo '<a href="../pm/task.php?func=detailtask&amp;project_task_id='.$taskid.'&amp;group_id='.$groupid.'&amp;group_project_id='.$projectid.'">';
										echo $summary;
										echo '</a>';
									}
								}
                                                                echo '</td>';
							}
						}
                                                echo '</tr>';
                                        }
				}
			}
			if (!$art_found) {
				echo '
                        <tr>
                            <td colspan="' . (array_sum($display_col)+1) . '" align="left"><strong> --</strong></td></tr>';
			}
		}
	}
            echo $GLOBALS['HTML']->listTableBottom();
}

//second column of "my" page

echo '</td><td valign="top"></td></tr>


<!--  Bottom Row   -->
<!--
<tr><td colspan="2">';
echo show_priority_colors_key();
echo '
</td></tr>
-->
</table>';*/
$Layout->endcol();
site_user_footer(array());

db_display_queries();
?>
