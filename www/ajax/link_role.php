<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 1:19 PM 10/10/11
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';

$group_id = getIntFromRequest('group_id');
$group = group_get_object($group_id);
session_require_perm ('project_admin', $group_id) ;

function cache_external_roles () {
	global $used_external_roles, $unused_external_roles, $group, $group_id;

	if (USE_PFO_RBAC) {
		$unused_external_roles = array () ;
		foreach (RBACEngine::getInstance()->getPublicRoles() as $r) {
			$grs = $r->getLinkedProjects () ;
			$seen = false ;
			foreach ($grs as $g) {
				if ($g->getID() == $group_id) {
					$seen = true ;
					break ;
				}
			}
			if (!$seen) {
				$unused_external_roles[] = $r ;
			}
		}
		$used_external_roles = array () ;
		foreach ($group->getRoles() as $r) {
			if ($r->getHomeProject() == NULL
			    || $r->getHomeProject()->getID() != $group_id) {
				$used_external_roles[] = $r ;
			}
		}

		sortRoleList ($used_external_roles, $group, 'composite') ;
		sortRoleList ($unused_external_roles, $group, 'composite') ;

	}
}

cache_external_roles () ;

$Return=array();
if (USE_PFO_RBAC) {
	$role_id = getIntFromRequest('role_id');
	foreach ($unused_external_roles as $r) {
		if ($r->getID() == $role_id) {
			if (!$r->linkProject($group)) {
				$Return['status']=false;
			} else {
				$Return['status']=true;
				$Return['name']=$r->getDisplayableName($group);
			}
		}
	}
}

echo json_encode($Return);
?>