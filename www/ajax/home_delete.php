<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:29 PM 11/29/11
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';

$Return=array('error'=>false);

if (!HomeComments::delete(getIntFromRequest('aid'))){
	$Return['msg']='Could not delete message';
}

echo json_encode($Return);
?>