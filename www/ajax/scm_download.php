<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 4:04 PM 11/1/11
 */

require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfplugins.'scmsvn/common/SVNPlugin.class.php';

$SVNPlugin=new SVNPlugin;
$Repo=getStringFromRequest("repo");
$File=isset($_REQUEST['file'])?getStringFromRequest("file"):'/';

echo $SVNPlugin->download($Repo,$File);
?>