<?php
if (isset($_COOKIE['good'])){
	header("Location: index2.php");
}

if (isset($_POST['user'])){
	if ($_POST['user']=="GE" && $_POST['pass']=="geVehicle_Forge!"){
		setcookie('good',1);
		header("Location: index2.php");
	}else{
		$Msg="Wrong username and/or password";
	}
}
setcookie('good',1);
header("Location: index2.php");
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<title>VehicleForge</title>
	<style>
	#bg{background:url('images/trans_bg.png') repeat}
		body{padding:0;margin:0}
	</style>
	<script src="'.util_make_uri('/js/jquery-'.$GLOBALS['JQUERY_VERSION'].'.min.js').'"></script>
	<script>
$(function(){
	$('#bg').height($(window).height());
	function rotate(degree){
		$('.rotate').css({ '-moz-transform': 'rotate(' + degree + 'deg)','transform':'rotate(' + degree + 'deg)','-ms-transform':'rotate(' + degree + 'deg)','-webkit-transform':'rotate(' + degree + 'deg)','-o-transform':'rotate(' + degree + 'deg)'});
		setTimeout(function(){rotate(++degree)},80);
	}
	rotate(0);

	//if (!Modernizr.rgba)$('#bg').css({'background':"url('images/trans_bg.png') repeat"});
});
	</script>
</head>
<body style="background:url('themes/gforge/images/layout/gearbg.png') no-repeat center 15px">
	<div id="bg" style="position:absolute;width:100%;z-index:1"></div>
	<div class="container"  style="text-align: center; padding-top:200px;position:relative;z-index:2">
		<div class="row twelvecol">
				<span style="position:relative">
					<img src="images/LandingPage.png" />
					<img src="images/LandingPageGear.png" class="rotate" style="position:absolute;left:303px;bottom:12px" />
				</span>
		</div>
		<div class="row twelvecol" style="margin-top:20px">
			<form method="POST" action="index.php">
			    <input type="text" name="user" placeholder="username" />
			    <input type="password" name="pass" placeholder="password" />
			    <input type="submit" value="Log In" name="login" />
			</form>
			<?php 
			if (isset($Msg))echo '<div class="error" style="width:300px;margin:auto">'.$Msg.'</div>';
			?>
		</div>
	</div>
</body>
</html>
