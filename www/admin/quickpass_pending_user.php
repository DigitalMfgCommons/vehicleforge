<?php
/**
 *  quickpass_pending_user.php
 * 	author: Junrong Yan
 */

// Show no more pending projects per page than specified here
$LIMIT = 50;


require_once ('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once $gfwww . 'include/canned_responses.php';
require_once $gfwww . 'admin/admin_utils.php';


use_stylesheet ( '/admin/admin.css' );


session_require_global_perm ( 'forge_admin' );

//Purpose: approve user
//Given:   user_id
//Return:  if this user has been approved, then return true. Otherwise, return false.
function activate_user($user_id) {
	global $feedback;
	global $error_msg;
	$u = user_get_object ( $user_id );
	if (!$u || !is_object($u)) {
		$error_msg .= _ ( 'cant find this user' ).$user_id;
		
		return false;
	} else if ($u->isError ()) {
		$error_msg .= $u->getErrorMessage ();
		return false;
	}
	
	if ($u->setStatus('A')) {
		$feedback .= sprintf ( _ ( 'Approved User: %1$s' ), $u->getUnixName () );
	} else {
		$error_msg .= sprintf ( _ ( 'Error when approving User: %1$s' ), $u->getUnixName () ) . '<br />';
		$error_msg .= $u->getErrorMessage ();
		return false;
	}
	
	return true;
}



//Purpose: fetching form request determine whether to approve user
if (getStringFromRequest('approve', false )) {
	$user_id = getIntFromRequest('user_id');
	activate_user ($user_id);
} 
//Purpose: fetching form request to reject user
else if (getStringFromRequest ( 'reject', false )) {
	$user_id = getIntFromRequest ( 'user_id' );
	$response_text = getStringFromRequest ( 'response_text' );
	
	$u = user_get_object($user_id );
	if (!$u || !is_object ($u)) {
		$error_msg .= _ ( 'Could Not Get User' );
		//exit_no_user ();
		return false;
	} elseif ($u->isError ()) {
		exit_error ( $u->getErrorMessage (), 'home' );
	}
	
	$response_size= strlen($response_text);
	if ($response_size==0) {
		$error_msg .= sprintf ( _ ( 'Please input your reason to reject' ), $u->getUnixName() );	
	}else{
		
		if (! $u->setStatus ('D')) {
		exit_error ( _ ( 'Error during user rejection: ' ) . $u->getErrorMessage (), 'home' );
		
	    }
	    if(!$u->delete(true)){
	    	exit_error($u->getErrorMessage(),'home');
	    }
	 
	    $feedback .= sprintf ( _ ( 'Rejected User: %1$s' ), $u->getUnixName () );
	    $u->addHistory ( 'rejected', $response_text );
	
	}
} 
//Purpose: fetching form request to approve all users in one time.
elseif (getStringFromRequest ( 'approve-all', false )) {
	if ($users = getStringFromRequest ( 'list_of_users', false )) {
		$List = explode ( ',', $users );
		array_walk ( $List, 'activate_user' );
	}
}



//Purpose: once click reject button, show the hidden textarea 
//         & change button color to red.
$JS='$(function(){
    $("body").on("click","button",function(e){
		var btn_id=e.target.id;
        var me=document.getElementById(btn_id);
		var txt_id=me.value;
		var txtarea=document.getElementById(txt_id);

		if(me.className!= "btn btn btn-danger"){
		     txtarea.className="control-group";
		     me.className="btn btn btn-danger";
		     return false;
     }



})
});';

add_js ( $JS );

//Purpose: once change the task option of drop-list, update the page and
//         reload the content of the request.
$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})        
   
});';
add_js($JS_dropdown_list);

//Purpose: once change the sort option of drop-list, updage the page and 
//         reload the content of the request..
$JS_sort_by = '$(function(){
    $("#sort_by").change(function(){
		var $new_rank_page=$(this).val();
		location.href=$new_rank_page;
})
  
});';
add_js($JS_sort_by);

//<script type="text/javascript" src="popup_reject.js"></script>				
site_admin_header ( array ('title' => _ ( 'Quick Pass' )), 0 );
echo '<div class="page">
		<div class="row">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select id="pending_select" style="width: 100%;">
							<option value="index">Pending Projects</option>
							<option value="quickpass_pending_user" selected>Pending Users</option>
							<option value="quickpass_pending_news">Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users">Rejected Users</option>
							<option value="quickpass_reject_news">Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form action="quickpass_pending_user.php" method="post">
							<div class="input-append">
                				<input id="search_query" size="16" name="search_key" type="text" style="width: 108px;">
								<input type="hidden" name="groupsearch" value="1" />
								<button class="btn btn-primary" type="submit">Search</button>
            				</div>
						</form>
					</div>
				</div> 				
';

//Purpose: get the value of "search_key"
$search = trim(getStringFromRequest('search_key'));
//Purpose: get the value of "order_by"
$order_value=getStringFromRequest('order_by');
//Purpose: change the selected attr of option in sorting drop-list.
$optionstr1=$order_value == 1? "selected" : "";
$optionstr2=$order_value == 2? "selected" : "";
$optionstr3=$order_value == 3? "selected" : "";
$optionstr4=$order_value == 4? "selected" : "";

if (!$search) {
	if($order_value){
		//Purpose: sorting by time and user_name
		switch($order_value){
			case 1:
				$res_grp = db_query_params ( "SELECT * FROM users WHERE status='P' ORDER BY add_date DESC", array (), $LIMIT );
				break;
			case 2:
				$res_grp = db_query_params ( "SELECT * FROM users WHERE status='P' ORDER BY add_date ASC", array (), $LIMIT );
				break;
			case 3:
				$res_grp = db_query_params ( "SELECT * FROM users WHERE status='P' ORDER BY user_name", array (), $LIMIT );
				break;
			case 4:
				$res_grp = db_query_params ( "SELECT * FROM users WHERE status='P' ORDER BY user_name DESC", array (), $LIMIT );
				break;
		}
	}else{
		$res_grp = db_query_params ( "SELECT * FROM users WHERE status='P' ORDER BY user_name", array (), $LIMIT );
	}
	
	
	//$res_grp = db_query_params ( "SELECT * FROM users WHERE status='P' ORDER BY user_name", array (), $LIMIT );
	$rows = db_numrows( $res_grp );

if ($rows) {
echo '	
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
	if ($rows > $LIMIT) {
		print "<p>" . _ ( 'Pending users:' ) . "$LIMIT+ ($LIMIT shown)</p>";
	} else {
		print "<p>" . _ ( 'Pending users:' ) . "$rows</p>";
	}
	
	echo '</div>
			<div class="body-dropdownlist">
				<select id="sort_by" style="width: 100%;">
	               <option name="1" value="quickpass_pending_user?order_by=1" '.$optionstr1.'>Time:Newst</option>
	               <option name="2" value="quickpass_pending_user?order_by=2" '.$optionstr2.'>Time:Oldest</option>
			
					<option name="3" value="quickpass_pending_user?order_by=3" '.$optionstr3.'>Name:A-Z</option>
					<option name="4" value="quickpass_pending_user?order_by=4" '.$optionstr4.'>Name:Z-A</option>
				</select>
		
			</div>
	</div>';
	
    //Purpose: output the detail information of user
	pending_users_bsform($res_grp);
	
	//<input type="submit" name="submit" value="' . _ ( 'Approve All' ) . '" />
	
	// list of group_id's of pending projects
	$arr = util_result_column_to_array ( $res_grp, 0 );
	$user_list = implode ( $arr, ',' );
	
	echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_users" value="' . $user_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
        		</div>
        		</div>
        		</div>
        		</div>
        ';
} else {
	//echo _( 'No Pending Projects to Approve' );
	echo '
			<div class="widget-body">
   <div class="body_header">
            <div class="body-text">
                <p>No pending projects to approve</p>
            </div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
				</div>
				</div>
				</div>
				</div>';
}
			
	
}else{

	$crit_desc='';
	$status='P';
	
	//Purpose: get the value of "search_key"
  $search_crit=getStringFromRequest('search_key');
$qpa1_num="SELECT DISTINCT * FROM users WHERE status='P' AND (user_id = $1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY add_date DESC";
$qpa1_str="SELECT DISTINCT * FROM users WHERE status='P' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY add_date DESC";

$qpa2_num="SELECT DISTINCT * FROM users WHERE status='P' AND (user_id = $1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY add_date ASC";
$qpa2_str="SELECT DISTINCT * FROM users WHERE status='P' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY add_date ASC";

$qpa3_num="SELECT DISTINCT * FROM users WHERE status='P' AND (user_id = $1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY user_name ASC";
$qpa3_str="SELECT DISTINCT * FROM users WHERE status='P' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY user_name ASC";

$qpa4_num="SELECT DISTINCT * FROM users WHERE status='P' AND (user_id = $1 OR lower(user_name) LIKE $2 OR lower(email) LIKE $2 OR lower(realname) LIKE $2) ORDER BY user_name DESC";
$qpa4_str="SELECT DISTINCT * FROM users WHERE status='P' AND (lower(user_name) LIKE $1 OR lower(email) LIKE $1 OR lower(realname) LIKE $1) ORDER BY user_name DESC";
	
	if($order_value){
		//Purpose: sorting the searching results by time and user_name
		switch($order_value){
			case 1:
				//sorting based on the time from newest to oldest
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa1_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa1_str, array(strtolower("%$search_crit%")));
				}
				break;
			case 2:
				//sorting based on the time from oldest to newest
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa2_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa2_str, array(strtolower("%$search_crit%")));
				}
				break;
			case 3:
				//sorting based on the name from a to z
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa3_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa3_str, array(strtolower("%$search_crit%")));
				}
				break;
			case 4:
				//sorting based on the time from z to a
				if(is_numeric($search_crit)){
					$res_grp=db_query_params($qpa4_num, array($search, strtolower("%$search_crit%")));
				}else {
					$res_grp=db_query_params($qpa4_str, array(strtolower("%$search_crit%")));
				}
				break;
		
	}}else{
		if(is_numeric($search)) {
					
			$res_grp=db_query_params($qpa1_num, array($search, strtolower("%$search%")));
		} else {
					
			$res_grp=db_query_params($qpa1_str, array(strtolower("%$search%")));
		}
		}
			
		if($status){
			$crit_desc.=" status=$status";
		}
		if($crit_desc){
			$crit_desc="(".trim($crit_desc).")";
		}
	
		$rows=db_numrows($res_grp);
		if ($rows) {
			echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
			print '<p><strong>' .sprintf(ngettext('Found users matching <em>%s</em>: %d match', 'Found users matching <em>%s</em>: %d matches', $rows), $search.$crit_desc, $rows).'</strong></p>';
			echo '</div>
			<div class="body-dropdownlist">
			<select id="sort_by" style="width: 100%;">
			<option name="1" value="quickpass_pending_user?order_by=1&search=123&search_key='.$search_crit.'" '.$optionstr1.'>Time:Newst</option>
			<option name="2" value="quickpass_pending_user?order_by=2&search=123&search_key='.$search_crit.'" '.$optionstr2.'>Time:Oldest</option>
				
			<option name="3" value="quickpass_pending_user?order_by=3&search=123&search_key='.$search_crit.'" '.$optionstr3.'>Name:A-Z</option>
			<option name="4" value="quickpass_pending_user?order_by=4&search=123&search_key='.$search_crit.'" '.$optionstr4.'>Name:Z-A</option>
			</select>
			
			</div>
	</div>';
			
			pending_users_bsform($res_grp);
			
			//<input type="submit" name="submit" value="' . _ ( 'Approve All' ) . '" />
			
			// list of group_id's of pending projects
			$arr = util_result_column_to_array ( $result, 0 );
			$user_list = implode ( $arr, ',' );
			
			echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_users" value="' . $user_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
        		</div>
        		</div>
        		</div>
        		</div>
        ';
		}else {
			echo '
					
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
			print '<p><strong>' .sprintf(ngettext('Found users matching <em>%s</em>: %d match', 'Found users matching <em>%s</em>: %d matches', $rows), $search.$crit_desc, $rows).'</strong></p>';
		
 echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
	 </div>
				</div>
        		</div>
        		</div>
        		</div>
					';
		}
		
	
	
}

site_footer ( array () );
?>