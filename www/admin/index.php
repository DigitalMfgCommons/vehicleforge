<?php
/**
 *  index.php
 * 	author: Junrong Yan
 */

// Show no more pending projects per page than specified here
$LIMIT = 50;
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';
use_stylesheet ( '/admin/admin.css' );

//Purpose: approve project
//Given:   group_id
//Return:  if this project has been approved, then return true. Otherwise, return false.
function activate_group($group_id) {
	global $feedback;
	global $error_msg;

	$group = group_get_object ( $group_id );

	if (! $group || ! is_object ( $group )) {
		$error_msg .= _ ( 'Error creating group object' );
		return false;
	} else if ($group->isError ()) {
		$error_msg .= $group->getErrorMessage ();
		return false;
	}

	if ($group->approve ( session_get_user () )) {
		$feedback .= sprintf ( _ ( 'Approved Project: %1$s' ), $group->getPublicName() );
	} else {
		$error_msg .= sprintf ( _ ( 'Error when approving Project: %1$s' ), $group->getUnixName () ) . '<br />';
		$error_msg .= $group->getErrorMessage ();
		return false;
	}

	return true;
}
//Purpose: fetching form request determine whether to approve project
if (getStringFromRequest ( 'approve', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	activate_group ( $group_id );
}
//Purpose: fetching form request to reject project
 else if (getStringFromRequest ( 'reject', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	$response_id = getIntFromRequest ( 'response_id' );
	$add_to_can = getStringFromRequest ( 'add_to_can' );
	$response_text = getStringFromRequest ( 'response_text' );
	$response_title = getStringFromRequest ( 'response_title' );

	$group = group_get_object ( $group_id );
	if (! $group || ! is_object ( $group )) {
		exit_no_group ();
	} elseif ($group->isError ()) {
		exit_error ( $group->getErrorMessage (), 'admin' );
	}

	$response_size= strlen($response_text);
	if ($response_size==0) {
		//print "<p>" . _ ( 'Please input your reason to reject...' ) . "</p>";
		$error_msg .= sprintf ( _ ( 'Please input your reason to reject' ), $group->getPublicName() );
	}else{

		if (! $group->setStatus ( session_get_user (), 'D' )) {
			exit_error ( _ ( 'Error during group rejection: ' ) . $group->getErrorMessage (), 'admin' );
		}
		$feedback .= sprintf ( _ ( 'Rejected Project: %1$s' ), $group->getPublicName () );


		$group->addHistory ( 'rejected', $response_text );
		$group->sendRejectionEmail ( 0, $response_text );

	}

	/*
	 * // Determine whether to send a canned or custom rejection letter and send it if( $response_id == 100 ) { $group->sendRejectionEmail(0, $response_text); if( $add_to_can ) { add_canned_response($response_title, $response_text); } } else { $group->sendRejectionEmail($response_id); }
	*/
} 
//Purpose: fetching form request to approve all projects in one time.
elseif (getStringFromRequest ( 'approve-all', false )) {
	if ($Groups = getStringFromRequest ( 'list_of_groups', false )) {
		$List = explode ( ',', $Groups );
		array_walk ( $List, 'activate_group' );
	}
}




//Purpose: once click reject button, show the hidden textarea
//         & change button color to red.
$JS='$(function(){
    $("body").on("click","button",function(e){
		var btn_id=e.target.id;
        var me=document.getElementById(btn_id);
		var txt_id=me.value;
		var txtarea=document.getElementById(txt_id);

		if(me.className!= "btn btn btn-danger"){
		     txtarea.className="control-group";
		     me.className="btn btn btn-danger";
		     return false;
     }



})
});';
add_js ( $JS );

//Purpose: once change the task option of drop-list, update the page and
//         reload the content of the request.
$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})
  
});';
add_js($JS_dropdown_list);


//Purpose: once change the sort option of drop-list, updage the page and
//         reload the content of the request..
$JS_sort_by = '$(function(){
    $("#sort_by").change(function(){
		var $new_rank_page=$(this).val();
		location.href=$new_rank_page;
})

});';
add_js($JS_sort_by);


site_admin_header(array('title'=>_('Pending Projects')));
$Layout->col(12,true);
echo '		
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select name="pendingCategory" id="pending_select" style="width: 100%;">
							<option value="index" selected>Pending Projects</option>
							<option value="quickpass_pending_user">Pending Users</option>
							<option value="quickpass_pending_news">Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users">Rejected Users</option>
							<option value="quickpass_reject_news">Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form name="gpsrch" action="index.php" method="post" >
							<div class="input-append">
                				<input id="search_query" size="16" name="search_key" type="text" style="width: 108px;">
								<input type="hidden" name="groupsearch" value="1" />
								<button class="btn btn-primary" type="submit">Search</button>
            				</div>	
						</form>
					</div>
				</div> 
';
//Purpose: get the value of "search_key"
$search = trim(getStringFromRequest('search_key'));
//Purpose: get the value of "order_by"
$order_value=getStringFromRequest('order_by');
//Purpose: change the selected attr of option in sorting drop-list.
$optionstr1=$order_value == 1? "selected" : "";
$optionstr2=$order_value == 2? "selected" : "";
$optionstr3=$order_value == 3? "selected" : "";
$optionstr4=$order_value == 4? "selected" : "";

if (!$search) {
	if($order_value){
		//Purpose: sorting by time and user_name
		switch($order_value){
			case 1:
				$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P' ORDER BY register_time DESC", array (), $LIMIT );
				break;
			case 2:
				$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P' ORDER BY register_time ASC", array (), $LIMIT );
				break;
			case 3:
				$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P' ORDER BY group_name", array (), $LIMIT );
				break;
			case 4:
				$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P' ORDER BY group_name DESC", array (), $LIMIT );
				break;
		}
	}else{
		
		$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P' ORDER BY register_time DESC", array (), $LIMIT );
	}
	
	
	
	$rows = db_numrows( $res_grp );
	
	if ($rows) {
echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
		if ($rows > $LIMIT) {
			print "<p>" . _ ( 'Pending projects:' ) . "$LIMIT+ ($LIMIT shown)</p>";
		} else {
			print "<p>" . _ ( 'Pending projects:' ) . "$rows</p>";
		}
		
		echo '</div>
			<div class="body-dropdownlist">
				<select id="sort_by" style="width: 100%;">
	               <option name="1" value="index?order_by=1" '.$optionstr1.'>Time:Newst</option>
	               <option name="2" value="index?order_by=2" '.$optionstr2.'>Time:Oldest</option>
		
					<option name="3" value="index?order_by=3" '.$optionstr3.'>Name:A-Z</option>
					<option name="4" value="index?order_by=4" '.$optionstr4.'>Name:Z-A</option>
				</select>
		
			</div>
	</div>';
		//Purpose: output the detail information of projects
		pending_project_bsform($res_grp);//call this function from admin_utils.php
		
		// list of group_id's of pending projects
		$arr = util_result_column_to_array ( $res_grp, 0 );
		$group_list = implode ( $arr, ',' );
	
		echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_groups" value="' . $group_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
                
        		</div>
        		</div>';
	} else {
		//echo _( 'No Pending Projects to Approve' );
		echo '
<div class="widget-body">
   <div class="body_header">
            <div class="body-text">
                <p>No pending projects to approve</p>
            </div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
				
				</div>
				</div>';
	}
	
	
	              
     
}else {
//begin if($groupsearch)
	$status = 'P';
	$is_public = getIntFromRequest('is_public', -1);
	$crit_desc = '' ;
	$qpa = db_construct_qpa () ;
	
	//Purpose: get the value of "search_key"
	$search_crit=getStringFromRequest('search_key');
	
	$qpa1_num="SELECT DISTINCT * FROM groups WHERE status='P' AND (group_id=$1 OR lower (unix_group_name) LIKE $2 OR lower (group_name) LIKE $2) ORDER BY register_time DESC";
	$qpa1_str="SELECT DISTINCT * FROM groups WHERE status='P' AND (lower (unix_group_name) LIKE $1 OR lower (group_name) LIKE $1) ORDER BY register_time DESC";
	
	$qpa2_num="SELECT DISTINCT * FROM groups WHERE status='P' AND (group_id=$1 OR lower (unix_group_name) LIKE $2 OR lower (group_name) LIKE $2) ORDER BY register_time ASC";
	$qpa2_str="SELECT DISTINCT * FROM groups WHERE status='P' AND (lower (unix_group_name) LIKE $1 OR lower (group_name) LIKE $1) ORDER BY register_time ASC";
	
	$qpa3_num="SELECT DISTINCT * FROM groups WHERE status='P' AND (group_id=$1 OR lower (unix_group_name) LIKE $2 OR lower (group_name) LIKE $2) ORDER BY group_name";
	$qpa3_str="SELECT DISTINCT * FROM groups WHERE status='P' AND (lower (unix_group_name) LIKE $1 OR lower (group_name) LIKE $1) ORDER BY group_name";
	
	$qpa4_num="SELECT DISTINCT * FROM groups WHERE status='P' AND (group_id=$1 OR lower (unix_group_name) LIKE $2 OR lower (group_name) LIKE $2) ORDER BY group_name DESC";
	$qpa4_str="SELECT DISTINCT * FROM groups WHERE status='P' AND (lower (unix_group_name) LIKE $1 OR lower (group_name) LIKE $1) ORDER BY group_name DESC";
	
	if($order_value){
		//Purpose: sorting the searching results by time and group_name
		switch ($order_value){
			case 1:
				if(is_numeric($search_crit)) {
					$qpa = db_construct_qpa ($qpa, $qpa1_num, array ($search, strtolower ("%$search_crit%"))) ;
				} else {
					$qpa = db_construct_qpa ($qpa, $qpa1_str, array (strtolower ("%$search_crit%")));
				}
				break;
			case 2:
				if(is_numeric($search_crit)) {
					$qpa = db_construct_qpa ($qpa, $qpa2_num, array ($search, strtolower ("%$search_crit%"))) ;
				} else {
					$qpa = db_construct_qpa ($qpa, $qpa2_str, array (strtolower ("%$search_crit%")));
				}
				break;
			case 3:
				if(is_numeric($search_crit)) {
					$qpa = db_construct_qpa ($qpa, $qpa3_num, array ($search, strtolower ("%$search_crit%"))) ;
				} else {
					$qpa = db_construct_qpa ($qpa, $qpa3_str, array (strtolower ("%$search_crit%")));
				}
				break;
			case 4:
				if(is_numeric($search_crit)) {
					$qpa = db_construct_qpa ($qpa, $qpa4_num, array ($search, strtolower ("%$search_crit%"))) ;
				} else {
					$qpa = db_construct_qpa ($qpa, $qpa4_str, array (strtolower ("%$search_crit%")));
				}
				break;
		}
		
	}else{
	
	
	if(is_numeric($search)) {
		$qpa = db_construct_qpa ($qpa, $qpa1_num, array ($search, strtolower ("%$search_crit%"))) ;
	} else {
		$qpa = db_construct_qpa ($qpa, $qpa1_str, array (strtolower ("%$search_crit%")));
	}
	}
	if ($status) {
		//$qpa = db_construct_qpa ($qpa, ' AND status=$1', array ($status)) ;
		$crit_desc .= " status=$status";
	}

	if ($crit_desc) {
		$crit_desc = "(".trim($crit_desc).")";
	}

	$result = db_query_qpa ($qpa) ;
	$rows=db_numrows($result);
	if($rows){
echo '	
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
		print '<p><strong>'.sprintf(ngettext('Found projects matching <em>%s</em>: %d match', 'Found projects matching <em>%s</em>: %d matches', $rows), $search.$crit_desc, $rows).'</strong></p>';
		echo '</div>
			<div class="body-dropdownlist">
			<select id="sort_by" style="width: 100%;">
			<option name="1" value="index?order_by=1&search=123&search_key='.$search_crit.'" '.$optionstr1.'>Time:Newst</option>
			<option name="2" value="index?order_by=2&search=123&search_key='.$search_crit.'" '.$optionstr2.'>Time:Oldest</option>
		
			<option name="3" value="index?order_by=3&search=123&search_key='.$search_crit.'" '.$optionstr3.'>Name:A-Z</option>
			<option name="4" value="index?order_by=4&search=123&search_key='.$search_crit.'" '.$optionstr4.'>Name:Z-A</option>
			</select>
		
			</div>
	</div>';

		//Purpose: output the detail information of projects
		pending_project_bsform($result);//call this function from admin_utils.php
		
		
		$arr = util_result_column_to_array ( $result, 0 );
		$group_list = implode ( $arr, ',' );
		
		echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_groups" value="' . $group_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
        		
        		</div>
        		</div>';
		} else {
			//echo _( 'No Pending Projects to Approve' );
			echo '
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
			print '<p><strong>'.sprintf(ngettext('Found projects matching <em>%s</em>: %d match', 'Found projects matching <em>%s</em>: %d matches', $rows), $search.$crit_desc, $rows).'</strong></p>';
			echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
   </div>
					
					</div>
					</div>';
		}
	
}//end if(!$search)
$Layout->endcol();
site_admin_footer(array());
?>
