<?php
/**
 *  quickpass.php
 * 	author: Junrong Yan
 * 	date:11/5/2014
 */

// Show no more pending projects per page than specified here
$LIMIT = 50;


require_once ('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once $gfcommon . 'include/account.php';
require_once $gfwww . 'include/canned_responses.php';
require_once $gfwww . 'admin/admin_utils.php';
require_once $gfwww . 'project/admin/project_admin_utils.php';
require_once $gfcommon . 'tracker/ArtifactTypes.class.php';
require_once $gfcommon . 'forum/Forum.class.php';

use_stylesheet ( '/admin/admin.css' );


session_require_global_perm ( 'approve_projects' );
function activate_group($group_id) {
	global $feedback;
	global $error_msg;
	
	$group = group_get_object ( $group_id );
	
	if (! $group || ! is_object ( $group )) {
		$error_msg .= _ ( 'Error creating group object' );
		return false;
	} else if ($group->isError ()) {
		$error_msg .= $group->getErrorMessage ();
		return false;
	}
	
	if ($group->approve ( session_get_user () )) {
		$feedback .= sprintf ( _ ( 'Approving Project: %1$s' ), $group->getUnixName () );
	} else {
		$error_msg .= sprintf ( _ ( 'Error when approving Project: %1$s' ), $group->getUnixName () ) . '<br />';
		$error_msg .= $group->getErrorMessage ();
		return false;
	}
	
	return true;
}

if (getStringFromRequest ( 'approve', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	activate_group ( $group_id );
} else if (getStringFromRequest ( 'reject', false )) {
	$group_id = getIntFromRequest ( 'group_id' );
	$response_id = getIntFromRequest ( 'response_id' );
	$add_to_can = getStringFromRequest ( 'add_to_can' );
	$response_text = getStringFromRequest ( 'response_text' );
	$response_title = getStringFromRequest ( 'response_title' );
	
	$group = group_get_object ( $group_id );
	if (! $group || ! is_object ( $group )) {
		exit_no_group ();
	} elseif ($group->isError ()) {
		exit_error ( $group->getErrorMessage (), 'admin' );
	}
	
	$response_size= strlen($response_text);
	if ($response_size==0) {
		//print "<p>" . _ ( 'Please input your reason to reject...' ) . "</p>";
		$error_msg .= sprintf ( _ ( 'Please input your reason to reject' ), $group->getPublicName() );	
	}else{
		
		if (! $group->setStatus ( session_get_user (), 'D' )) {
		exit_error ( _ ( 'Error during group rejection: ' ) . $this->getErrorMessage (), 'admin' );
	}
	
	
	$group->addHistory ( 'rejected', 'x' );
	$group->sendRejectionEmail ( 0, $response_text );
	
	}
	
	/*
	 * // Determine whether to send a canned or custom rejection letter and send it if( $response_id == 100 ) { $group->sendRejectionEmail(0, $response_text); if( $add_to_can ) { add_canned_response($response_title, $response_text); } } else { $group->sendRejectionEmail($response_id); }
	 */
} elseif (getStringFromRequest ( 'approve-all', false )) {
	if ($Groups = getStringFromRequest ( 'list_of_groups', false )) {
		$List = explode ( ',', $Groups );
		/*echo  $List[0];
		foreach ($List as $peding_id){
			activate_group ( $peding_id );
		}*/
		array_walk ( $List, 'activate_group' );
	}
}


/*

$JS='$(function(){
    var $quote="\"";
	var $button_id=$quote.concat($group_id,$quote);
		document.write($button_id);
    $($quote.concat($group_id,$quote)).click(function(){
        var $me=$(this);
        var $ii=$(this).val();
		
        var text_id=$quote.concat($ii,$quote);
		
        if (!$me.hasClass("btn-danger")){
            $(text_id).slideDown();
            $(this).addClass("btn-danger");
            return false;   
        }
    });
});';
*/
/*
$JS='$(function(){
    $("#rejection_button").click(function(){
        var $me=$(this);
        
        if (!$me.hasClass("btn-danger")){
            $("#row_rejection_text").slideDown();
            $(this).addClass("btn-danger");
            return false;
        }
    });
});';
*/

$JS='$(function(){
    $("body").on("click","button",function(e){
		var btn_id=e.target.id;
        var me=document.getElementById(btn_id);
		var txt_id=me.value;
		var txtarea=document.getElementById(txt_id);
		
		if(me.className!= "btn btn btn-danger"){
		     txtarea.className="control-group";
		     me.className="btn btn btn-danger";
		     return false;
     }

		
		
})
});';

add_js ( $JS );

$JS_dropdown_list = '$(function(){
    $("#pending_select").change(function(){
		var $new_page=$(this).val();
		location.href=$new_page;
})        
   
});';
add_js($JS_dropdown_list);


				

site_admin_header ( array ('title' => _ ( 'Quick Pass' )), 0 );

$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P'", array (), $LIMIT );
$rows = db_numrows( $res_grp );
echo '<div class="page">';
if ($rows) {
echo '



	<div class="row">
		<div class="span12">
			<div class="widget">
				<div class="widget-header">
					<div class="widget-text">Select Pending Projects/Users/News</div>
					<div class="widget-dropdownlist" style="width: 47%;">
						<select name="pendingCategory" id="pending_select" style="width: 100%;">
							<option value="quickpass" selected>Pending Projects</option>
							<option value="quickpass_pending_user">Pending Users</option>
							<option value="quickpass_pending_news">Pending News</option>
							<option value="quickpass_reject_projects">Rejected Projects</option>
							<option value="quickpass_reject_users">Rejected Users</option>
							<option value="quickpass_reject_news">Rejected News</option>
						</select>
					</div>
					<div class="widget-search">
						<form name="gpsrch" action="search.php" method="post" >
		                    <input type="text" name="search"  placeholder="search..." />
		                   
		                    <input type="hidden" name="groupsearch" value="1" />
							<input type="submit" value="Search"/>
						</form>
					</div>
				</div> 		
		
<div class="widget-body">
   <div class="body_header">
       <div class="body-text">';
	if ($rows > $LIMIT) {
		print "<p>" . _ ( 'Pending projects:' ) . "$LIMIT+ ($LIMIT shown)</p>";
	} else {
		print "<p>" . _ ( 'Pending projects:' ) . "$rows</p>";
	}
	
	echo '</div>
			<div class="body-dropdownlist">
				<select name="sortby" style="width: 100%;">
					<option value="1">Time:Newest</option>
					<option value="2">Time:Oldest</option>
					<option value="3">Name:Asc</option>
					<option value="4">Name:Dsc</option>
					<option value="5">Best Match</option>
				</select>
			</div>
	</div>';





					
                      
                              
                                   

// get current information
//$res_grp = db_query_params ( "SELECT * FROM groups WHERE status='P'", array (), $LIMIT );

//$rows = db_numrows ( $res_grp );


	$BsForm = new BsForm;
	$i=1;
	while ( $row_grp = db_fetch_array ( $res_grp ))  {
		
		$u = user_get_object($row_grp['user_id'] );
		echo $BsForm->init ( getStringFromServer ( 'php_self' ), 'post', array ('class' => 'form-horizontal' ) )
		//->left ( new Checkbox ( '', array ('id' => 'selected' ) ) )
		->head ( $row_grp ['group_name'] )
		->group ( 'Project Admin',
				 new Custom ( util_make_link ( '/project/admin/?group_id=' . $row_grp ['group_id'],$u->getUnixName()
		         )) 
		)
		 ->group ( 'Project Members', 
		 		new Custom ( util_make_link ( '/admin/userlist.php?group_id=' . $row_grp ['group_id'], _ ( 'View/Edit Project Members' ) 
		        ) ) 
		)
		
		->group ( 'Purpose', 
				new Custom ( $row_grp ['register_purpose'] ) 
		)
		->group ( 'Register Time:',
				new Custom ( date('Y-m-d',$row_grp['register_time']))
		)
		->group ( 'Project Details', 
				new Custom ( util_make_link ( '/admin/groupedit.php?group_id=' . $row_grp ['group_id'], _ ( 'Edit Project Details' ) 
		       ) ) 
		)
		->group ( 'Project URL',
				 new Custom ( $row_grp ['unix_group_name'] ), 
				new Hidden ( array (
				'name' => 'group_id',
				'value' => $row_grp ['group_id'] 
				) ) 
		)
		->group ( 'Rejection Message', array ('class' => 'hidden','id' => 'row_rejection_text'.$i ), 
				new Textarea ( '', array (
						'name' => 'response_text'
				 ) )
		)
	    ->render_fieldset()
	    ->right ( 
	    		new Submit('Approve',array(
                    'class'=>'btn btn-success',   
                    'name'=>'approve',
                    'value'=>$row_grp ['group_id']
                )),
                new Submit('Reject',array(
                    'class'=>'btn',
                    //'id'=>'rejection_button',
                    'id'=>$row_grp ['group_id'],
                    'name'=>'reject',
                    'value'=>'row_rejection_text'.$i,
                ))
	     )
	    ->render ();
		echo "<hr />";
		$i=$i+1;
	}
	
	//<input type="submit" name="submit" value="' . _ ( 'Approve All' ) . '" />
	
	// list of group_id's of pending projects
	$arr = util_result_column_to_array ( $res_grp, 0 );
	$group_list = implode ( $arr, ',' );
	
	echo '
        <form action="' . getStringFromServer ( 'PHP_SELF' ) . '" method="post" >
        <p style="text-align: right;">
        <input type="hidden" name="action" value="activate" />
        <input type="hidden" name="list_of_groups" value="' . $group_list . '" />
        <input type="submit" name="approve-all" class="btn" value="' . _ ( 'Approve All' ) . '"/>
        </p>
        </form>
        ';
} else {
	echo _( 'No Pending Projects to Approve' );
}


?>
                      
                   
       </div>
	</div>
</div>
	
	
	
<div id="shortModal" class="modal modal-wide fade">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h4 class="modal-title">Reject Reason</h4>
					</div>
					<div class="modal-body">
						<textarea id="text_id" style="width: 96%; height: 100%;" rows="3">Please type in your reason to reject ...</textarea>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" data-dismiss="modal" class="btn btn-primary">Reject</button>
					</div>


				</div>
				<!-- /.modal -->
                       


	




<?php


$Layout->endcol ();
include $gfwww . 'help/help.php';
site_footer ( array () );
