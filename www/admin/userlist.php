<?php
/**
 *
 * Copyright 1999-2000 (c) The SourceForge Crew
 * Copyright 2010 (c) Franck Villaume - Capgemini
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'admin/admin_utils.php';

session_require_global_perm ('forge_admin');
 
$HTML->header(array('title'=>_('User List')));
$Layout->col(12,true);

/**
 * performAction() - Updates the indicated user status
 *
 * @param               string  $newStatus - the new user status
 * @param               string  $statusString - the status string to display
 * @param               string  $user_id - the user id to act upon
 */
function performAction($newStatus, $statusString, $user_id) {
	$u = user_get_object($user_id);
	if (!$u || !is_object($u)) {
		exit_error(_('Could Not Get User'),'home');
	} elseif ($u->isError()) {
		exit_error($u->getErrorMessage(),'home');
	}
	if($newStatus=="D") {
		if(!$u->setStatus($newStatus)) {
			exit_error($u->getErrorMessage(),'home');
		}
		if(!$u->delete(true)) {
			exit_error($u->getErrorMessage(),'home');
		}
	} else {
		if(!$u->setStatus($newStatus)) {
			exit_error($u->getErrorMessage(),'home');
		}
		if(!$u->setUnixStatus($newStatus)) {
			exit_error($u->getErrorMessage(),'home');
		}

	}
	echo '<div class="highlight">' .sprintf('<em>'.$u->getRealName().'</em>'._(' updated to %1$s status'), $statusString)."</div>";
}

function show_users_list ($users, $filter='') {
	if (!count($users)) {
		return;
	}

    echo '<table>
        <tr><th>Status</th><th>Username</th><th>Join Date</th><th>Actions</th></tr>';

    foreach ($users as $u) {
        echo '<tr><td>'.$u->getStatus().'</td>
        		<td><a href="useredit.php?user_id='.$u->getID().'">'.$u->getRealName().' ('.$u->getUnixName().')</a></td>
        				<td>'.($u->getAddDate() ? date(_('Y-m-d H:i'), $u->getAddDate()):'').'</td>';
        echo '<td>'.util_make_link ('/developer/?form_dev='.$u->getID(),_('DevProfile')).' '.util_make_link ('/admin/userlist.php?action=activate&amp;user_id='.$u->getID().$filter,_('Activate')).' '.util_make_link ('/admin/userlist.php?action=suspend&amp;user_id='.$u->getID().$filter,_('Suspend')).' '.util_make_link ('/admin/passedit.php?user_id='.$u->getID().$filter,_('Change PW')).'</td></tr>';
    }

    echo '</table>';
}

// Administrative functions

$group_id = getIntFromRequest('group_id');
$action = getStringFromRequest('action');
$user_id = getIntFromRequest('user_id');
$status = getStringFromRequest('status');

if ($action=='delete') {
	performAction('D', "DELETED", $user_id);
} else if ($action=='activate') {
	performAction('A', "ACTIVE", $user_id);
} else if ($action=='suspend') {
	performAction('S', "SUSPENDED", $user_id);
}

//	Show list of users
if (!$group_id) {
	$user_name_search = getStringFromRequest('user_name_search');


	if ($user_name_search) {
		$res = db_query_params ('SELECT user_id FROM users WHERE lower(user_name) LIKE $1 OR lower(lastname) LIKE $1 ORDER BY realname',
					   array (strtolower("$user_name_search%")));
	} elseif ($status) {
		$res = db_query_params ('SELECT user_id FROM users WHERE status = $1 ORDER BY realname',
					   array ($status));
	} else {
		$sortorder = getStringFromRequest('sortorder', 'realname');
		util_ensure_value_in_set ($sortorder,
					  array('realname','user_name','lastname','firstname','user_id','status','add_date')) ;
		$res = db_query_params('SELECT user_id FROM users ORDER BY '.$sortorder,
					  array ());
	}
	$filter='';
	if (in_array($status,array('D','A','S','P'))) {
		$filter = '&amp;status='.$status;
	}
	show_users_list (user_get_objects(util_result_column_to_array($res,0)),$filter);
} else {
	//For group
	$project = group_get_object($group_id) ;
	$HTML->heading(_('User list for project: '.$project->getPublicName()));
	show_users_list ($project->getUsers(false));
}

$Layout->endcol();
$HTML->footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
