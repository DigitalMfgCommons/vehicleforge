<?php
require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
/*require_once $gfcommon.'include/DOMEInterface.class.php';
require_once $gfcommon.'include/Runtimes.class.php';
require_once $gfcommon.'include/CEMRunning.class.php';*/

$InterfaceID=getIntFromRequest('diid');
$DOMEInterface=new DOMEInterface($InterfaceID);

function objectToArray( $object ){
	if( !is_object( $object ) && !is_array( $object ) ){
	    return $object;
	}
	if( is_object( $object ) ){
	    $object = get_object_vars( $object );
	}
	return array_map( 'objectToArray', $object );
}

function cmp($a, $b){
	$a=$a['occur'];
	$b=$b['occur'];

	if ($a==$b)
		return 0;

	return ($a<$b)?-1:1;
}

if (isset($_GET['complete'])){
	require_once $gfcommon.'include/CEMMessages.class.php';

	//This is a loaded model run
	$RunningID=getIntFromRequest('running_id');
	$CEMRunning=new CEMRunning($RunningID);
	$CEMRunning->setStatus(true);

	$Messages=array();
	foreach(CEMMessages::getByRunningID($RunningID) as $MessageObject){
		$Messages[]=objectToArray(json_decode($MessageObject->getMessage()));
	}

	usort($Messages, 'cmp');
}

function strToID($String){
	return str_replace(" ","_",$String);
}

//session_require_perm ('project_read', $group_id) ;

$ModelData=$DOMEInterface->getData();
$HasModels=false;

foreach ($ModelData->outParams as $i){
	if ($i->name=="VRML file"){
		$HasModels=true;
		break;
	}
}

$GroupID=getIntFromRequest("group_id")?getIntFromRequest("group_id"):1;

session_require_perm ('project_read', $GroupID) ;

site_project_header(array('title'=>_('Model: '.$ModelData->modelDef->name), 'group'=>$GroupID));

?>
<style>
#model_nav, #filler	{
	width: 150px;
	margin: -4px 0 -4px -4px;
	float: left;
}

#model_nav li	{
	list-style: none;
	height: 25px;
/*
	border-style: solid;
	border-color: #BEBEBE;
	border-width: 0 1px 1px 0;
*/
	font-size: 18px;
	cursor: pointer;
	padding: 4px;
	color: #999;
/* 	background: #FFF; */
}

#model_nav .current	{
	color: #309CD6;
	border-right: 0;
}

/*
#model_content  {
	margin-left: 155px;
}

#filler	{
	clear:both;
	margin: 4px 0 0 -5px;
	border-right: 1px solid #BEBEBE;
}
*/

.module .content {
	padding-bottom: 0;
}
</style>
<script>
var runTime=0,
		startTime=0,
		myRunTime=0,
		myTimer=null;

function strToID(Str){
	return Str.replace(/ /g,"_");
}

$(function(){
var h2_height=$('#content h2').height()+9;

function updateTimer(){
	myRunTime+=100;
	$("#my_runtime").text((myRunTime/1000)+"s");
}

function startTimer(){
	myRunTime=0;
	myTimer=setInterval(updateTimer, 100);
}

$('#filler').height($('#attr').height()-$('#model_nav').height()+h2_height);

$('#run').click(function(){
	startTime=new Date().getTime();
	startTimer();

	var Data={},
	$Inputs=$('.input');
	$Inputs.each(function(){
		$(this).prop('disabled',true);
		Data[$(this).data('name')]=$(this).val();
	});

	var $me=$(this);

	$me.prop('disabled',true);

	Data=JSON.stringify(Data);

	var pollTimer=null,
			queueURL="",
			totalMessages=0,
			received=0,
			runningID=0;
	$.post('ajax/model_run.php',{data:Data, server_id:<?=$DOMEInterface->getServerID()?>, interface_id:<?=$InterfaceID?>},function(msg){
		msg=$.parseJSON(msg);
		if (msg.error==false){
			queueURL=msg.queue_url;
			runningID=msg.running_id;
			poll();
		}else{
			alert(msg.msg);
		}
	});

	function poll(){
		if (pollTimer==null)
			pollTimer=setInterval(poll,2000);

		$.ajax({
			type: 'POST',
			url: 'ajax/poll_queue.php',
			dataType: 'json',
			data: {
				queue_url: queueURL,
				running_id:runningID
			},
			success: function(msg){
				if (msg.error==true){
					alert(msg.msg);
					clearInterval(pollTimer);
				}

				//Loop through all messages
				for(var i in msg.messages){
					received++;

					m=$.parseJSON(msg.messages[i]);

					//Check if message received is the message that specifies how many messages there should be
					if (m.event=="class mit.cadlab.dome3.api.ParameterStatusChangeEvent" && m.new_val=="SUCCESS"){
						totalMessages=m.param;
					}

					//If a value changed message is received, change it on the page
					if (m.event=="class mit.cadlab.dome3.api.ParameterValueChangeEvent"){
						$('#'+m.param).val(m.new_val);
					}

					//If messages received equals needed messages, stop polling
					if (totalMessages==received){
						//Get run time length
						runTime=new Date().getTime() - startTime;

						//Stop timers
						clearTimeout(myTimer);
						clearInterval(pollTimer);

						//Delete the queue
						$.ajax({
							type: 'POST',
							url: 'ajax/delete_queue.php',
							data: {
								queue_url: queueURL
							},
							dataType: 'json',
							success: function(msg){
								if (msg.error==true){
									alert(msg.msg);
								}
							}
						});

						//Send run results to a script to log them
	                    $.post('ajax/run_complete.php',{runningID:runningID, runtime:runTime, interface_id:<?=$InterfaceID?>});

						//Reset form
						$me.prop('disabled',false);
						$Inputs.each(function(){
							$(this).prop('disabled',false);
						});
					}
				}
			}
		});
	}
});

$('#model_nav li').click(function(){
	var $me=$(this),
	$cur=$('#model_nav li.current'),
	curview=$cur.data('view'),
	myview=$me.data('view');
	if (curview!=myview){
		$cur.removeClass('current');
		$me.addClass('current');
		$('#filler').animate({'height':$('#'+myview).height()-$('#model_nav').height()+h2_height});
		$('#'+curview).slideUp(function(){
			$('#'+myview).slideDown();
		});
	}
});
});
</script>
<?php
$Layout->col(3,true);
?>

<div id="model_nav">
	<ul>
	<li class="current" data-view="attr">Attributes</li>
	<li data-view="desc">Description</li>
	<!--<li data-view="graphs">Graphs</li>-->
	
<?php
if ($HasModels)
	echo '<li data-view="3d">3D Models</li>';
?>
	</ul>
	Average Run Time: <?=Runtimes::getAvg($InterfaceID)?>s
	Your Run Time: <span id="my_runtime">0s</span>
</div>
<?php
$Layout->endcol()->col(9);
?>
<!-- <div id="filler"></div> -->
<div id="model_content">
	<?='<h2>'.$ModelData->modelDef->name.'</h2>'; ?>
	<div id="attr" class="view">
		<div id="inputs">
			<h3>Inputs</h3>
<?php
foreach ($ModelData->inParams as $i){
	echo '<div><label>'.$i->name.'</label><input type="text" value="';
	if (isset($_GET['complete'])){
		foreach($Messages as $m){
			if ($m['event']=="class mit.cadlab.dome3.api.ParameterValueChangeEvent" && $m['param']==$i->name){
				echo $m['new_val'][0];
			}
		}
	}else{
		echo $i->value;
	}
	echo '" class="input" data-name="'.$i->name.'" id="'.$i->name.'" /><span class="xput_type">';
	if ($i->unit!='no unit')echo $i->unit;
	echo '  ('.$i->type.')</span></div>';
}
?>
		</div>
		<button id="run" class="btn primary" autocomplete="false">Run Model</button><br /><br />
		<div id="outputs">
			<h3>Outputs</h3>
<?php
foreach ($ModelData->outParams as $i){
	echo '<div';
	if ($i->type=="File")echo ' class="hide"';
	echo '><label>'.$i->name.'</label> ';
	echo '<input autocomplete="false" readonly="readonly" id="'.strToID($i->name).'" value="';

	if (isset($_GET['complete'])){
		foreach($Messages as $m){
			if ($m['event']=="class mit.cadlab.dome3.api.ParameterValueChangeEvent" && $m['param']==$i->name){
				echo $m['new_val'][0];
			}
		}
	}else{
		echo $i->value;
	}

	echo '" />';
	echo '<span class="xput_type">';
	if ($i->unit!='no unit')echo $i->unit;
	echo '  ('.$i->type.')</span></div>';
}
?>
		</div>
	</div>
	<div id="desc" class="view hidden"><?php //echo $ModelData->modelDef->desc;?></div>
	<div id="graphs"  class="view hidden">Graphs</div>
	<!--<div id="3d"  class="view hidden">
		<embed id="3dModel" src="plate.wrl" type="application/x-cortona" pluginspage="http://www.cortona3d.com/cortona" width="640" height="480"
		       ContextMenu="false"
		       vrml_background_color="#000077"
		       vrml_dashboard="true"
		       ShowFps="true"
		       AnimateViewports="true"
		       NavigationMode="3"
		       RendererMaxTextureSize="256" />
	</div>-->
</div><!--end content-->
<?php
$Layout->endcol();
site_footer();

//db_display_queries();
?>
