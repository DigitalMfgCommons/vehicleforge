<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$Value=getIntFromRequest('v')==1?true:false;
$CEMID=getIntFromRequest('cid');
$Return=array('error'=>false);

$CEM=new CEM($CEMID);
if (!$CEM->setExportControl($Value)){
    $Return['error']=true;
    $Return['msg']='Export control flag cannot be unset once set';
}

echo json_encode($Return);
?>