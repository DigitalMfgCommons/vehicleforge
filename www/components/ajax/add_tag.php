<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
/*require_once $gfcommon.'include/CEM.class.php';
require_once $gfcommon.'include/CEMTag.class.php';*/

$cem_id=getIntFromRequest('cem_id');
$group_id=getIntFromRequest('group_id');
$tag_name=getStringFromRequest('name');

$Return=array();
$Return['error'] = false;

$TagID=CEMTag::create($tag_name);
$CEM=new CEM($cem_id);
if (!$CEM->attachTag($TagID)){
    $Return['error']=true;
    $Return['msg']=$CEM->getErrorMessage();
}else{
    $Return['tag_name']=$tag_name;
    $Return['tag_id']=$TagID;
}

echo json_encode($Return);
?>