<?php

/*
 *	Show all components for a given project
 *	Jeff Mekler (2/11/2012)
 *
 *	local variables:
 *		$CEMs - array of CEM objects
 */

function renderSubComponents($cem,$group_id) {
	$subs = $cem->getSubComponents();
	if (count($subs) == 0) {
		echo '<li><a href="?group_id='.$group_id.'&cid='.$cem->getID().'">'.$cem->getName().'</a></li>';
				
	} else {
		echo '<li><b class="nav-toggle" style="display:inline;">&#9660;</b><a href="?group_id='.$group_id.'&cid='.$cem->getID().'" style="display:inline-block">'.$cem->getName().' ('.count($subs).')</a>';
		echo '<ul class="nav">';
		foreach ($subs as $sub) {
			renderSubComponents($sub,$group_id);
		}
		echo '</ul></li>';
	}
}

$Layout->col(4,true);

	if (count($CEMs) > 0) {
		echo '<ul class="nav">';
		foreach ($CEMs as $cem) {
			renderSubComponents($cem, $group_id);
		}
		echo '</ul>';
	}
$Layout->endcol();
?>

<script>
	$('.nav-toggle').bind('click', function(event) {		
		var li = $(this).closest('li');
		var ul = $(li).children('ul');
		
		// toggle list visibility
		ul.toggle();
		
		// change caret direction		
		if ($(ul).is(':visible')){
			$(li).children('b').html('&#9660;');
		} else {
			$(li).children('b').html('&#9654;');
		}
	});

</script>