 <?php
/*
 *  Show a specific component
 *  Jeff Mekler (2/11/2012)
 *
 *  local variables:
 *      $CEM - CEM object
 *      $group - Group object
 */

if ($CEM->getID() == 0) {
    echo '<h3>Select a component to browse files and models</h3>';
    return;
}

?>

<style>
    .action_bar .btn    {
        margin-left: 10px;
    }
</style>

<script src="/js/bootstrap/tabs.js"></script>

<ul class="nav nav-tabs">
	<li><h2 style="margin-right: 10px;"><?=$CEM->getName()?></h2></li>
  <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
  <li><a href="#files" data-toggle="tab">Files</a></li>
  <li><a href="#sub-components" data-toggle="tab">Sub-Components</a></li>
  <li><a href="#services" data-toggle="tab">Services</a></li>
</ul>

<div class="tab-content">

	<div class="tab-pane active" id="details">		

    <div class="action_bar">
        <a class="btn btn-small btn-danger pull-right" id="delete">Delete</a> <!-- href="javascript:void(0) -->        
<!--
        <a class="btn btn-small pull-right<?=$CEM->getExportControl()?' btn-warning disabled':''?>" href="javascript:void(0)" id="export_control_button" data-value="<?=$CEM->getExportControl()?1:0?>">Export Control: <?=$CEM->getExportControl()?'ON':'OFF'?></a>
		<a class="btn btn-small pull-right" href="javascript:void(0)" id="cem_edit_button" data-controls="edit_controls" data-target="cem_desc">Edit</a>
-->
        <a id="edit_details" data-target="#cem_desc" class="btn-small btn pull-right">Edit Details</a>
        <div class="hidden" id="edit_controls">
            <button class="btn btn-primary pull-right" id="cem_edit_save">Save</button>
            <button class="btn btn pull-right" id="cem_edit_cancel">Cancel</button>
        </div>
    </div>

		<div id="details">	  	
			<h5>Description</h5>
    	<p id="cem_desc"><?= $CEM->getDescription() ?></p>
		</div>
		
    <div id="tags">
      <h5 helptip-text="Add tags to a component to help others find it" helptip-position="right">Tags</h5>
            
      <form id="addTag" class="form-inline pull-left">
					<div class="input-append pull-left">
	          <input class="span2" id="addTagField" type="text">
	          <input class="btn btn-small" style="margin-left:-5px;" type="submit" value="Add">
          </div>
      </form>
      
      <ul class="unstyled" id="tag-list">
        <?php
        $Tags = $CEM->getTags();
        foreach($Tags as $tag) {
        	makeTag($tag->getName(), $tag->getID());
        }
        
        function makeTag($tag_name, $tag_id) {
        	echo '<li class="pull-left btn-group" style="margin-left:5px;">';
          	echo '<a class="btn btn-primary btn-small tag-name" href="#">' . $tag_name . '</a>';
	          echo '<a class="btn btn-primary btn-small removeTag" href="#" tag_id=' . $tag_id . '><span style="font-size:1.2em; line-height:12px;">&times</span></a>';
          echo '</li>';
        }
        ?>
      </ul>

		<script>
			$(document).ready( function() {
        
	        	$("#edit_details").click(function() {
		        	var target = $(this).data('target');
		        	var $target = $( target );
		        	var text = $target.val() || $target.html();
		        
					if ( $(this).data('action') == "save" ) {
						var self = this;
						$(self).html('Saving...');
				      	$.ajax({
	              			type: 'POST',
	              			url: '/components/ajax/set_description.php',
	              			dataType: 'json',
	              			data:   {
		              			cem: <?=$CEM->getID()?>,
		              			text: text
	              			},
	              			success: function(msg) {
								$target.replaceWith('<p id="' + target.replace(/#/, '') + '">' + text + '</p>');
					        	$(self).data('action', '').html("Edit Details").removeClass('btn-primary');
	              			}
		          		});			        
					} else {
			        	$target.replaceWith('<textarea id="' + target.replace(/#/, '') + '">' + text + '</textarea>');
			        	$(this).data('action', 'save').html("Save").addClass('btn-primary');

			        	$("#edit_details").display = 'block';
			        	
					}
	        	});
        
			    // create a new tag
			    $('#addTag').bind('submit', function(event) {
				    addTag( trim($('#addTagField').val()) );
				    $('#addTagField').val('');
				    return false;
			    });
			
			    // remove tag
			    $(document).on('click', '.removeTag', function(event) {
				    removeTag(this);
				    return false;
			    });

			    // delete component
			    $('#delete').click( function() {
			    	deleteComponent(<?= $CEM->getID() ?>);
			    });
			});
				
			function removeTag(handle) {
				$.ajax({
			    	type: 'POST',
			    	url: '/components/ajax/delete_tag.php',
			        dataType: 'json',
			        data: {
			          cem_id: <?php echo $CEM->getID()?>,
			          group_id: <?php echo $group->getID()?>,
			          tag_id: $(handle).attr('tag_id')
			        },			        
			        success: function(msg){
						if (!msg.error) {
							$(handle).closest('li').remove();
						}
					}
			    });
			}
		
		    function addTag(name) {
				$.ajax({
			    	type: 'POST',
			        url: '/components/ajax/add_tag.php',
			        dataType: 'json',
			        data: {
			        	cem_id: <?php echo $CEM->getID()?>,
			        	group_id: <?php echo $group->getID()?>,
			        	name: name
			        },			        
			        success: function(msg){
			        	if (msg.error) {
			            	alert(msg.msg);
			        	} else {
			          		var message = '<?php makeTag('tagname', 'tagid') ?>';
			        		$('#tag-list').append( message.replace(/tagname/, msg.tag_name).replace(/tagid/,msg.tag_id)) ;
			        	}
			    	}
			    });
		    }
		    
		    // delete component
			function deleteComponent(id) {
				var answer = confirm ("Are you sure you want to permanently delete this component?");
				if (answer) {		
			    	$.ajax({
			        	type: 'POST',
			        	url: '/components/ajax/delete_component.php',
			        	dataType: 'json',
			        	data: {
			          		cem_id: <?= $CEM->getID()?>,
			          		group_id: <?= $group->getID()?>
			        	},		        
			        	success: function(msg){
							if (!msg.error) {
								window.location.href = "/components/?group_id=<?= $group->getID() ?>";
							}
						}
			    	});		
		    	}
			}
		    
		    // remove multiple, leading or trailing spaces
		    // from: http://www.qodo.co.uk/blog/javascript-trim-leading-and-trailing-spaces
		    function trim(s) {
			    s = s.replace(/(^\s*)|(\s*$)/gi,"");
			    s = s.replace(/[ ]{2,}/gi," ");
			    s = s.replace(/\n /,"\n");
		    	return s;
		    }

      </script>
    </div>
	</div>
	
	<div class="tab-pane" id="files">      
	
        <div class="action_bar">
            <?php
            echo '<div style="margin-left:10px;" id="repo_status" class="btn btn-small pull-right disabled';
            switch ($CEM->getRepoStatus()){
                case 1:
                    echo ' btn-danger"><b>Status:</b> No repo';
                    break;

                case 2:
                    echo ' btn-warning"><b>Status:</b> Pending Creation';
                    break;

                case 3:
                    echo ' btn-info"><b>Status:</b> Pending Changes';
                    break;

                case 4:
                    echo ' btn-success"><b>Status:</b> OK';
                    break;
            }
            ?>
        </div>

        <a href="#accessControlModal" class="btn btn-small pull-right" data-toggle="modal" data-target="#accessControlModal" style="margin-left:10px;">Git Access Control</a>
        
				<?php if ($CEM->getRepoExists()){ ?>
				  <a href="#gitAccessModal" class="btn btn-small pull-right" data-toggle="modal" data-target="#gitAccessModal" style="margin-left:10px;">Git Access</a>
				<?php } ?>
				
      </div>  
		<?php if ($CEM->getRepoExists()){ ?>
	    <div style="margin: 5px 30px;" id="browser">
	    <?= file_get_contents("http://".$git_server."/git_browser/index.php?a=vf_main&p=".$CEM->getRepoName()); ?>
	    </div>			
		<?php } else { ?>
			<p>No git repository exists for this component yet</p>
		<?php } ?>
		
	</div>
	<div class="tab-pane" id="sub-components">
	
		<table class="table table-striped" style="margin-bottom:0">
      <thead style="font-size:11px">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th><a class="btn btn-small pull-right" href="#" data-toggle="modal" id="addSubComponent" data-target="#addSubComponentModal">Add Sub-Component</a></th>
        </tr>
        </thead>
        <tbody>
				<?php 
					$subs = $CEM->getSubComponents();
					if (count($subs) > 0) {
						foreach($subs as $sub) {					
				?>
							<tr>
								<td>
									<a href="<?= Router::route('/components/?group_id='.$group->getUnixName().'&cid='.$sub->getID()) ?>">
										<?= $sub->getName() ?>
									</a>
								</td>
								<td colspan=2><?= $sub->getDescription() ?></td>
							</tr>
				<?php					
						}
					} else {
						echo '<tr><td colspan=3>No sub-components added to ' . $CEM->getName() . ' yet.</td></tr>';
					}
					
				?>
        </tbody>
    </table>

	</div>

	<div class="tab-pane" id="services">
		<table class="table table-striped" style="margin-bottom:0">
	      <thead style="font-size:11px">
	        <tr>
	            <th>Name</th>
	            <th>Description</th>
	            <th><a href="/register/interface.php?<?='cem_id='.$CEM->getID().'&group_id='.$group_id?>" class="btn btn-small pull-right">Add Service</a></th>
	        </tr>
	        </thead>
	        <tbody>	        
		        <?php
			        require_once $gfcommon.'include/DOMEInterface.class.php';
			        $Interfaces = $CEM->getInterfaces();
			        if (count($Interfaces) > 0) {
				        foreach($Interfaces as $i){
				          echo '<tr><td><a href="/services/index.php?group_id='.$group_id.'&diid='.$i->getID().'">'.$i->getName().'</a></td><td colspan=2>'. $i->getDescription() . '</td>';
				        }
			        } else {
	   						echo '<tr><td colspan=3>No services added to ' . $CEM->getName() . ' yet.</td></tr>'; 
			        }
			        
		        ?>
	        </tbody>
	    </table>
    </div>
	
<!-- helptip-text="Attach services to a component to simulate behavior and provide verification" helptip-position="right" -->
<!--
    <div style="padding-top: 8px;" helptip-text="Browse files in a component's git repository" helptip-position="right">

    </div>
-->
    
    <script type="text/javascript">

    $(document).ready(function() {
        $("#cem_edit_button").click(function(){
            var $controls=$("#"+$(this).data('controls')),
            $target=$("#"+$(this).data('target'));
            var text=$target.text();
            $target.replaceWith('<textarea id="cem_desc">'+text+'</textarea>');
            $controls.show().data({
                'reset-control' :   $(this),
                'reset-target'  :   $("#cem_desc")
            });
            $(this).hide();
        });

        function editReset(NewText){
            $("#edit_controls").hide().data('reset-control').show();
            var text;
            if (!NewText){
                text=$("#edit_controls").data('reset-target').val()
            }else{
                text=NewText;
            }
            $("#edit_controls").data('reset-target').replaceWith('<div id="cem_desc">'+text+'</div>');
        }

        $("#cem_edit_cancel").click(function(){
            editReset();
        });

        $("#cem_edit_save").click(function(){
            var text=$(this).parent().data('reset-target').val();

            $.ajax({
                url         : '/components/ajax/set_description.php',
                dataType    : 'json',
                type        : 'post',
                data        :   {
                    cem     : <?=$CEM->getID()?>,
                    text    : text
                }
            });

            editReset(text);
        });

        $("#addSubComponent").click(function(){
            $("#sub_cname").focus();
        });

        $("#export_control_button").click(function(){
            var $me=$(this),
                v=$me.data('value');
            if (v==0){
                if (confirm("Are you sure you want to do this? You can't undo it!")){
                    $.getJSON('ajax/set_export_control.php',{v:1,cid:<?=$CEM->getID()?>},function(msg){
                        if (msg.error==false)
                            $me.data('value',1).addClass('btn-warning disabled').text('Export Control: ON');
                    });
                }
            }else{
                alert('Export control flag cannot be unset once set');
            }
        });

        $(document).on('click', ".folder", function(){
            var $me=$(this),
                obj={
                    h: $me.data('hash'),
                    hb: $me.data('hb'),
                    p: $me.data('project')
                };

            if($me.data('file')){
                obj.f=$me.data('file');
            }

            $("#browser").load("ajax/navigate_repo.php",obj);
        });
	    
    });

    </script>
    