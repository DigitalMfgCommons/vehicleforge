<?php
require_once('env.inc.php');
require_once $gfcommon . 'pre.php';
require_once 'community/community_util.php';

$HTML->addStylesheet('marketplace/marketplace.css');
site_header(array('title'=>'Whatchu lookin fer?'));
?>
<!--   
<ul id="tab" class="nav nav-tabs" style="margin:0;">
  <li class="active">
  	<a href="components.php">
  		<strong>Components</strong>
  	</a>
  </li>
  
	<li>
		<a href="services.php">
			<strong>Services</strong>
		</a>
	</li>
</ul>   
-->
<?php 
	$Layout->col(12,true,true,true);
  echo "<h2 style=color:#1589FF;>Jake's Amazing-Fantastic Solr Searcho</h2>"; 
  echo "<h4>Find Stuff!</h4>";        
	$Layout->endcol();
	
	$Layout->col(3);
?>

<!--
<br/>
<div class="row">
	<div class="span3">
-->
		<div class="well">
			<form id="search" method="get">
				<input type="text" name="q" style="width:80%; margin: 0 auto;" class="search-query" placeholder="Search..."/>
       </form>
		</div>
<!-- 	</div>     -->
<!-- 	<div class="span8">      -->
<?php $Layout->endcol()->col(9); ?>
		<div id="componentResultSummary"></div>    
		<br/><ul class="thumbnails" id="componentResultList"></ul>
		<div id="serviceResultSummary"></div>    
		<br/><ul class="thumbnails" id="serviceResultList"></ul>
		<div id="projectResultSummary"></div>    
		<br/><ul class="thumbnails" id="projectResultList"></ul>
		<div id="userResultSummary"></div>    
		<br/><ul class="thumbnails" id="userResultList"></ul>
<!-- 	</div> -->
<!-- </div>  -->

<?php $Layout->endcol(true); ?>



<?php 
global $url;
global $port;
global $path;
$solrComponentClient = new SolrComponentClient($url, $port, $path);
$solrComponentClient->writeJavaScriptInclude();
$solrComponentClient->writeModal();
$solrServiceClient = new SolrServiceClient($url, $port, $path);
$solrServiceClient->writeJavaScriptInclude();
$solrServiceClient->writeModal();
$solrUserClient = new SolrUserClient($url, $port, $path);
$solrUserClient->writeJavaScriptInclude();
$solrUserClient->writeModal();
$solrProjectClient = new SolrProjectClient($url, $port, $path);
$solrProjectClient->writeJavaScriptInclude();
$solrProjectClient->writeModal();
?>

<!--BEGIN script-->
<script>
  // initialization
  $(document).ready( function() {
      // return search results
      //searchComponents();
      searchAll();      
      // set up listener to add component to project	
      <?php if (session_loggedin()) { ?>
	
	$('.add_to_project').bind('click', function() {
	    $(this).closest('.dropdown-toggle').attr('disabled', 'disabled');
	    $('#add_to_project_status').html('Adding...');
	    
	    $.post('ajax/import_component_to_project.php',
		   {
		   cid: $('#componentModal').data('cid'),
		       to_gid: $(this).attr('group_id')
		       }
		   );
	    
	    $('#add_to_project_status').html('Added!');
	    window.setTimeout( function() {
		$(this).closest('.dropdown-toggle').removeAttr('disabled');
		$('#add_to_project_status').html('Add to Project');
	      }, 1000);
	  });
	<?php } ?>
	  
	  });


function searchAll() {
  searchComponents("componentResultList","componentResultSummary");
  searchServices("serviceResultList","serviceResultSummary");
  searchUsers("userResultList","userResultSummary");
  searchProjects("projectResultList","projectResultSummary");
  //searchUsers();
}	

/*
 * function searchServices()
 * parses GET parameters and queries Solr for search results
 */
function searchUsers1() {
	query = getParameterByName("q");
	$.ajax({
	  type: 'GET',
	  url: "/solrSearch/proxy.php/select",
	  data: {
	    q: "*" + query + "*",
	    defType: 'edismax',
	    qf: 'realname',
	    core: 'users',
	    start: 0,
	    rows: 50
	  },
	  dataType: "json",
	  success: function(json) {
	  	// remove old results
	  	$('#userResultList').children().remove();

	  	// add new results
	  	var response = json.response;
			for (var i=0; i<response.numFound; i++) {
				var doc = response.docs[i];
				makeUserThumb(doc);
			}
			
			if (query=='') {
				$('#userResultSummary').html('<h4>Users</h4>' + response.numFound + ' results found.');
			} else {
				$('#userResultSummary').html('<h4>Users</h4>' + response.numFound + ' results found for search: "' + query + '".');
			}
	  }   
	});
}

// builds a thumbnail view of a service
function makeUserThumb1(doc) {
	var group_id = -1;
	var user_img_src = '<img src="http://placehold.it/100x100&text=failure" alt="">';
	jQuery.ajax({
		  url: 'project/project_picture_decoder.php?group_id=' + group_id, 
		      success: function(data) {
		    	  group_img_src = data.group_img_src;
		        },
		      dataType: 'json',
		      async: false
		      });
	var html = $('<?= makeInterfaceThumb() ?>');
	// set attributes
	$(html).attr('interface_name', doc.interface_name);
	$(html).attr('interface_id', doc.id);
	$(html).attr('group_id', doc.group_id);
	$(html).data('interface_data', $.parseJSON(doc.interface_data)); 

	// set html
	$(html).find('.serviceImage').html('<img src="' + user_img_src + '" width=100 height=100 alt="">');
	$(html).find('.interface_name').html(doc.interface_name);
	
	// append results
  $('#serviceResultList').append(html);
  
  // add a listener
  $(html).on('click', showServiceModal);
}	

	function makeQueryData() {
		var data = {};
		data["defType"] = "edismax";
		data["core"] = "services";
		data["start"] = 0;
		data["rows"] = 50;
		return data;
	}
	
	function updateQueryData($queryData,$query) {
		$queryData["q"] = $query; 
		$queryData["qf"] = "interface_name";
	}
	
	/*
	 * function searchServices()
	 * parses GET parameters and queries Solr for search results
	 */
	function searchServices1() {
		jsondata = makeQueryData();
		query = getParameterByName("q");
		updateQueryData(jsondata,query);
		$.ajax({
		  type: 'GET',
		  url: "/solrSearch/proxy.php/select",
		  /*data: {
		    q: "*" + query + "*",
		    defType: 'edismax',
		    qf: 'interface_name',
		    core: 'services',
		    start: 0,
		    rows: 50
		  },*/
		  data: jsondata,
		  dataType: "json",
		  success: function(json) {
		  	// remove old results
		  	$('#serviceResultList').children().remove();

		  	// add new results
		  	var response = json.response;
				for (var i=0; i<response.numFound; i++) {
					var doc = response.docs[i];
					makeServiceThumb(doc);
				}
				
				if (query=='') {
					$('#serviceResultSummary').html('<h4>Services</h4>' + response.numFound + ' results found.');
				} else {
					$('#serviceResultSummary').html('<h4>Services</h4>' + response.numFound + ' results found for search: "' + query + '".');
				}
		  }   
		});
	}
        
	// builds a thumbnail view of a service
	function makeServiceThumb1(doc) {
		var group_id = -1;
		jQuery.ajax({
		  url: 'marketplace/service_group_id_decoder.php?interface_id=' + doc.id, 
		      success: function(data) {
		          group_id = data.group_id;
		        },
		      dataType: 'json',
		      async: false
		      });
		var interface_data = $(this).data('interface_data');
		var group_img_src = '<img src="http://placehold.it/100x100&text=failure" alt="">';
		jQuery.ajax({
			  url: 'project/project_picture_decoder.php?group_id=' + group_id, 
			      success: function(data) {
			    	  group_img_src = data.group_img_src;
			        },
			      dataType: 'json',
			      async: false
			      });
		var html = $('<?= makeInterfaceThumb() ?>');
		// set attributes
		$(html).attr('interface_name', doc.interface_name);
		$(html).attr('interface_id', doc.id);
		$(html).attr('group_id', doc.group_id);
		$(html).data('interface_data', $.parseJSON(doc.interface_data)); 

		// set html
		$(html).find('.serviceImage').html('<img src="' + group_img_src + '" width=100 height=100 alt="">');
		$(html).find('.interface_name').html(doc.interface_name);
		
		// append results
	  $('#serviceResultList').append(html);
	  
	  // add a listener
	  $(html).on('click', showServiceModal);
	}	

	/*
	 * function: showServiceModal
	 * parses information from the selected service and displays a detailed view in modal form
	 */ 
	function showServiceModal1(event) {

		var name = $(this).attr('interface_name');
		var id = $(this).attr('interface_id');
		var group_id = -1;
		/* var group_id = $(this).attr('group_id'); */
		jQuery.ajax({
		  url: 'marketplace/service_group_id_decoder.php?interface_id=' + id, 
		      success: function(data) {
		          group_id = data.group_id;
		        },
		      dataType: 'json',
		      async: false
		      });
		var interface_data = $(this).data('interface_data');

		// update modal values
		$('#service_name').html(name);
		$('#service_desc').html('Service description goes here...');
		$('#service_url').attr('href', '/services/?group_id=' + group_id + '&diid=' + id);
		$("#subscribe_to_service").data('interface_id',id);
		
		// write inputs
		var inputs = interface_data.inParams;
		$('#service_inputs').children().remove();
		for (i=0; i<inputs.length; i++) {
			var name = inputs[i].name;
			var type = inputs[i].type;
			$('#service_inputs').append('<li>' + name + ' [' + type + ']</li');
		}

		// write outputs
		var outputs = interface_data.outParams;
		$('#service_outputs').children().remove();
		for (i=0; i<outputs.length; i++) {
			var name = outputs[i].name;
			var type = outputs[i].type;
			$('#service_outputs').append('<li>' + name + ' [' + type + ']</li');
		}
		
		// show modal
		$('#serviceModal').modal();
	}

	// ajax query to return search results
	function searchComponents1() {
		query = getParameterByName("q");
		filter = "category_name:" + getParameterByName("category");
		
		// update page title
		if (query != "") {
			document.title = document.title + ' - search "' + query + '"';
		}
			
		// execute ajax request
		$.ajax({
		  type: 'GET',
		  url: "/solrSearch/proxy.php/select",
		  data: {
		    q: "*" + query + "*",
		    defType: 'edismax',
		    qf: 'component_name',
		    core: 'components',
/* 		    fq: filter, */
		    start: 0,
		    rows: 50
		  },
		  dataType: "json",
		  success: function(json) {
		  	// remove old results
		  	$('#componentResultList').children().remove();

		  	// add new results
		  	var response = json.response, doc;
				for (var i=0; i<response.numFound; i++) {
					doc = response.docs[i];
					makeComponentThumb(doc);
				}
				
				// update results summary
				if (query == '') {
					$('#componentResultSummary').html('<h4>Components</h4>' + response.numFound + ' results found.');
				} else {
					$('#componentResultSummary').html('<h4>Components</h4>' + response.numFound + ' results found for search: "' + query + '".');
				}
		  }   
		});
	}
	
	// helper method to parse query parameters
	function getParameterByName(name)
	{
	  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	  var regexS = "[\\?&]" + name + "=([^&#]*)";
	  var regex = new RegExp(regexS);
	  var results = regex.exec(window.location.search);
	  if(results == null)
	    return "";
	  else
	    return decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	// helper method to generate a component thumbnail view
function makeComponentThumb1(doc) {
  var group_img_src = '<img src="http://placehold.it/100x100&text=failure" alt="">';
  jQuery.ajax({
    url: 'project/project_picture_decoder.php?group_id=' + doc.group_id, 
	success: function(data) {
	group_img_src = data.group_img_src;
      },
	dataType: 'json',
	async: false
	});
  var html = $('<?= makeComponentThumb() ?>');
  // update the generic thumbnail
  $(html).data('component_name', doc.component_name);
  $(html).data('component_id', doc.id);
  $(html).data('group_id', doc.group_id);
  $(html).data('group_name', doc.group_name);
  $(html).find('.componentImage').html('<img src="' + group_img_src + '" width=100 height=100 alt="">');
  
  $(html).find('.component_name').html(doc.component_name);
  $(html).find('.group_name').html('<span style="font-weight: 200;">in </span>' + doc.group_name);
  
  // append component thumbnail to results lists
  $('#componentResultList').append(html);
  
  // add an action listener
  $(html).on('click', showComponentModal);
}	

	// helper method to display a detailed view of a component
	function showComponentModal1(event) {
		// update modal
		var name = $(this).data('component_name');
		var id = $(this).data('component_id');
		var group_id = $(this).data('group_id');
		var group_name = $(this).data('group_name');
		
		$('#component_name').html(name);
		$('#component_desc').html('This is a model for '+$(this).data('component_name'));
		$('#component_url').attr('href', '/components/?group_id=' + group_id + '&cid=' + id);
		$('#group_name').html(group_name);
		$('#group_name').attr('href', '/components/?group_id=' + group_id);
		$("#componentModal").data('cid',id);

		// show modal
		$('#componentModal').modal();
	}
	
</script>
<!--END script-->

<?php 
	function makeComponentThumb($imgSrc='http://placehold.it/160x120&text=Component') {
		return 
		'<li class="span2">' .
		'<div class="thumbnail">' .
		'<center><div class="componentImage">' .
		'<img src="'.$imgSrc.'" alt="">' .
		'</div></center>' .
		'<div class="caption" style="padding:5px;">' .
		'<p>WRONG FUNCTION</p>'.
		'<h4 class="component_name" style="text-align: center;"></h4>' .
		'<h5 class="group_name" style="font-weight:500; text-align: center;"></h5>' .
		'</div>' .
		'</div>' .
		'</li>';
	}
	function makeInterfaceThumb($imgSrc='http://placehold.it/160x120&text=Service') {
		return 
		'<li class="span2">' .
		'<div class="thumbnail">' .
		'<center><div class="serviceImage">' .
		'<img src="'.$imgSrc.'" alt="">' .
		'</div></center>' .
		'<div class="caption" style="padding:5px;">' .
		'<p>WRONG FUNCTION</p>'.
		'<h5 class="interface_name"></h5>' .
		'</div>' .
		'</div>' .
		'</li>';
	}

	function makeUserThumb($imgSrc='http://placehold.it/160x120&text=User', $data_uid=-1) {
		return
		'<li class="col4">'.
		'<a href="#" class="thumbnail user_thumb" data-uid="'.$data_uid.'">'.
		'<img src="'.$imgSrc.'" width="100" height="100" class="user_image" />'.
		'<h4 class="user_name"></h4>'.
		'<p><span class="label "></span></p>'.
		'</a>'.
		'</li>';
	}
	
	site_footer(array());
?>
