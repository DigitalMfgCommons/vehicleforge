<?php
require_once '../../env.inc.php';
require_once $gfcommon . 'include/pre.php';

$Return=array();

if ($_GET['type']=='project'){
	require_once $gfcommon . 'include/Group.class.php';
	$Group=new Group($_GET['id']);
	$Components=$Group->getComponents();

	foreach($Components as $c){
		$Return[]=array(
			'cid'   =>  $c->getID(),
			'name'  =>  $c->getName()
		);
	}
}else{
	require_once $gfcommon.'include/CEM.class.php';
	$CEM=new CEM($_GET['id']);
	$Subs=$CEM->getSubComponents();

	foreach($Subs as $s){
		$Return[]=array(
			'cid'   =>  $s->getID(),
			'name'  =>  $s->getName()
		);
	}
}

echo json_encode($Return);
?>