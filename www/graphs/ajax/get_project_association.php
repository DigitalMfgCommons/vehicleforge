<?php
require_once '../../env.inc.php';
require_once $gfcommon . 'include/pre.php';
require_once $gfcommon . 'include/Group.class.php';

$Parent=$_GET['pid'];

$Result=db_query_params("SELECT * FROM group_associations WHERE group_id=$1",array($Parent));
$Return=array();
while($Row=db_fetch_array($Result)){
	$Group=new Group($Row['associated_with']);
	$Return[]=array('name'=>$Group->getPublicName(),
		'group_id'=>$Group->getID(),
		'img'=>$Group->getImageHTML(60,60),
		'uname'=>$Group->getUnixName());
}

echo json_encode($Return);
?>