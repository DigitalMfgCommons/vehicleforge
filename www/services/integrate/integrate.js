!function($) {

	// BEGIN Resource class definition
	Resource = function(serviceID) {
		// increment resource counter
		this.id = Resource.prototype.count++;
		
		// add resource to resource list
		Resource.prototype.resources[this.id] = this;
			
		this.inputs = {};
		this.outputs = {};

		// query server for service data
		$.ajax({
			url: '/services/ajax/get_service.php',
			data: {
				diid: serviceID
			},
			context: this,
			success: function(json){
				var response = $.parseJSON(json);
				//console.log(response);
				if (!response.error) {
					this.data = response.data;	
					for (var i in this.data.inputs) {
						this.inputs[i] = new Parameter(this.getID(), this.data.inputs[i]);						
					}	
					for (var j in this.data.outputs) {
						this.outputs[j] = new Parameter(this.getID(), this.data.outputs[j]);						
					}			
					$('body').trigger('DOME_AddResource', this);
				}						
			}
		});
	};
	
	Resource.find = function(id) {
		return Resource.prototype.resources[id];	
	};
	
	Resource.prototype = {
		count: 1,			
		resources: new Object(),
		getID: function() {	return this.id; },			
		getInputs: function() {	return this.inputs; },
		getOutputs: function() { return this.outputs;	},			
		getName: function() {	return this.data.name; },
		getData: function() { 
			this.data['instanceName'] = this.getID();
			return this.data; 
		}
	};
	
	// BEGIN Parameter class definition
	Parameter = function(resourceID, paramData) {			
		this.id = Parameter.prototype.count++;
		Parameter.prototype.parameters[this.id] = this;
		this.resource_id = resourceID;
		this.paramData = paramData;
	};
	
	Parameter.find = function(id) {
		return Parameter.prototype.parameters[id];
	};
	
	Parameter.prototype = {
		count: 1,
		parameters: new Object(),
		getID: function() { return this.id; },
		getResourceID: function() { return this.resource_id; },
		getName: function() { return this.paramData.name || ''; },
		getUnit: function() { return this.paramData.unit || ''; },
		getType: function() { return this.paramData.type || '';},		
		getCategory: function() { return this.paramData.category || ''; },
		getDefaultValue: function() { return this.paramData.value || 0; },
		getDisplayName: function() { return this.getResourceID() + '.' + this.getName(); },
		getData: function() {
			return {
				id: this.getID(),
				name: this.getName(),
				type: this.getType(),
				category: this.getCategory(),
				value: this.getDefaultValue(),
				parameterid: this.paramData.id || '' 
			};		
		}
	};
	
	// BEGIN relation class definition
	Relation = function(fromParam, toParam ) {
		this.id = Relation.prototype.count++;
		this.from = fromParam;
		this.to = toParam;
	};
	
	Relation.prototype = {
		count: 1,
		getID: function() { return this.id; },
		containsResource: function(resourceID) { return this.from.getResourceID() == resourceID || this.to.getResourceID() == resourceID; },
		getData: function() {
			return {
				from: {
					instancename: this.from.getResourceID(),
					name: this.from.getName()
				},
				to: {
					instancename: this.to.getResourceID(),
					name: this.to.getName()
				}
			};
		}
	};
	
	IntegrationModel = function () {
		this.name = "";
		this.description = "";								
		this.resources = {};
		this.relations = {};
		this.inParams = {};
		this.outParams = {};
	};

	IntegrationModel.prototype = {
		addResource: function(interfaceID) {
			var resource = new Resource(interfaceID);
			this.resources[resource.getID()] = resource;
		},		
			
		removeResource: function(resourceID) {	
			var resource = this.resources[resourceID];
			
			if (resource != null) {					
				for (var i in this.relations) {
					var rel = this.relations[i];
					if (rel.containsResource(resourceID)) {					
						this.removeRelation(rel.getID());
					}
				}
				
				$('body').trigger('DOME_RemoveResource', resource);
				delete this.resources[resourceID];
			}
		},

		// relation methods
		addRelation: function(fromParam, toParam, triggerEvent) {
			var relation = new Relation(fromParam, toParam);
			this.relations[relation.getID()] = relation;
			if ( typeof triggerEvent == 'undefined')
				$('body').trigger('DOME_AddRelation', relation);
		},

		removeRelation: function(relationID) {		
			var relation = this.relations[relationID];
			if (relation != null) {
				$('body').trigger('DOME_RemoveRelation', relation);
				delete this.relations[relationID];
			}
		},

		// input/output parameter methods
		addParameter: function(parameter, type) {
			if (type == 'input')
				this.inParams[parameter.getName()] = parameter;
			else
				this.outParams[parameter.getName()] = parameter;
		},
		
		// getData method returns formatted data for integration model
		getData: function() {
			 
			return {
				name: this.name,
				description: this.description,
				resources: parseData(this.resources),
				relations: parseData(this.relations),
				inParams:  parseData(this.inParams),
				outParams: parseData(this.outParams)				
			}
		}
	};
	
	function parseData(list) {
		var data = {};
		for (var i in list) {
			data[i] = list[i].getData();
		}
		return data;
	}
	
}(window.jQuery);

var IM;
$(document).ready( function() {
	// instantiate an IntegrationModel
	IM = new IntegrationModel();	
});