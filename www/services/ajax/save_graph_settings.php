<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$ModelID=getIntFromRequest('model_id');
$GraphSettings=getStringFromRequest('graph_settings');

$Return=array('error'=>false);
//$Return['msg'] = ModelGraphSettings::entryExists($ModelID);
//$ModelGraphSettings = new ModelGraphSettings($ModelID);

if (ModelGraphSettings::entryExists($ModelID)) {
	$Return['msg'] = ModelGraphSettings::updateEntry($ModelID, $GraphSettings);
}
else {
	$Return['msg'] =  ModelGraphSettings::createEntry($ModelID, $GraphSettings);
}

//$Return['msg'] = array($ModelID, $GraphSettings);
echo json_encode($Return);
?>