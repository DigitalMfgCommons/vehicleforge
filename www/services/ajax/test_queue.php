<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'Amazon/SQS.class.php';
require_once $gfcommon.'include/CEMMessages.class.php';
require_once $gfcommon.'stomp-php-1.0.0/Stomp.php';

echo "Begin test_queue stuff: ";
$RunningID = 1;

$Return=array('error'=>false);

$con = new Stomp("tcp://forge2:61613");
try {
	$con->setReadTimeout(5, 0);
	// connect
	$con->connect();
	$con->subscribe("DOME_Model_Run_TestQueue");
		
	// receive all messages from the queue
	while ( $msg = $con->readFrame() ) {
		$Body=json_decode($msg->body);
		$Return['messages'][]=json_encode($Body);

		//Log the message body in the DB
		CEMMessages::create($msg->body,$RunningID);
		// mark the message as received in the queue
		$con->ack($msg);
	}
	
} catch (StompException $e) {
	$Return['msg']= $e->getMessage();
}
// disconnect
$con->unsubscribe("DOME_Model_Run_TestQueue");
$con->disconnect();
//Return all message data to page
echo "Return stuff: " . json_encode($Return);
?>
<h2 style="border-bottom: 1px solid #AAA;">Return this: <?=print_r($Return)?></h2><br/>