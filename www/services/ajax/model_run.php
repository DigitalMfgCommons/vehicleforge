<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
/*require_once $gfcommon.'include/DOMEApi.class.php';
require_once $gfcommon.'include/Server.class.php';
require_once $gfcommon.'include/CEMRunning.class.php';*/

try {
  if(isset($_FILES)) {

    $Return['error']=true;
    $Return['msg']='';
    foreach($_FILES as $file){
      $errorCode = $file['error'];

      if($errorCode != UPLOAD_ERR_OK) {
	$errorMessage = '';
	switch($errorCode) {
        case UPLOAD_ERR_INI_SIZE:
          $errorMessage = 'Uploaded file '.$file['name'].' exceeds maximum size of ' . ini_get(upload_max_filesize);
          break;
        case UPLOAD_ERR_FORM_SIZE:
          $errorMessage = 'Uploaded file '.$file['name'].' exeeds maximum form size.';
          break;
        case UPLOAD_ERR_PARTIAL:
          $errorMessage = 'File '.$file['name'].' was only partially uploaded.';
          break;
        case UPLOAD_ERR_NO_FILE:
          $errorMessage = 'No file was uploaded.';
          break;
        case UPLOAD_ERR_NO_TMP_DIR:
          $errorMessage = 'No temporary folder';
          break;
        case UPLOAD_ERR_CANT_WRITE:
          $errorMessage = 'Could not save file '.$file['name'].'.';
          break;
        case UPLOAD_ERR_EXTENSION:
          $errorMessage = 'Cannot upload this type of file.';
          break;
        default:
          $errorMessage = 'Error uploading file '.$file['name'].'.';
	}

	throw new RuntimeException($errorMessage);
      }  // else the upload is ok
    }
  }
} 
catch (RuntimeException $e) {
  $Return['error']=true;
  $Return['msg']=$e->getMessage();
  echo json_encode($Return);
  return;
}

//Get values from page
$DOMEServer=new Server(getIntFromRequest('server_id'));
$InterfaceID=getIntFromRequest('interface_id');
//$RunnableID=getIntFromRequest('rid');

//$CEMRunnable=new CEMRunnable($RunnableID);

$Return=array('error'=>false);
// Check for bulk data run information, if it exists the flow need to be altered.
$ModelInputData = getStringFromRequest('modelData');
$Inputs=json_decode($ModelInputData);
$NewInParams=array();

foreach($Inputs as $key=>$val){
  if ($key=="bulkRunner"){
    $uploadfolder = ini_get('upload_tmp_dir');

    $bulkRunnerFile=basename(str_replace( "\\", '/', $val));  //this removes the c:\\fakepath from the input file name.

    // THIS CAME FROM DOME API, NEED TO change how file(s) are moved from web server to DOME server
    foreach ($_FILES as $file) {
      if ($file["error"] == UPLOAD_ERR_OK) {
	$file["tmp_name"] = str_replace( "\\", '/', $file["tmp_name"]);
	$tmp_name = basename($file["tmp_name"]);
	$file["name"] = str_replace( "\\", '/', $file["name"]);
	$name = basename($file["name"]);
	if(!move_uploaded_file($tmp_name, $uploadfolder.'/'.$name)) {
          $Return['error']=true;
          $Return['msg']='Unable to move file';
        }
      } else { //signal error
	$Return['error']=true;
	$Return['msg']="Unable to upload file ".$$file["name"];
	echo json_encode($Return);
	return;
      }
    }

    $fileExtention=strrchr($bulkRunnerFile,'.');
    $inputDataValues = null;
    if(strcmp($fileExtention, ".csv") == 0) {
      //create input string pairs in json
      $inputDataValues = processCSV($bulkRunnerFile, $uploadfolder);  //inputDataValues contains array of JSON incoded input values, one per model invocation 
      $bulkRunCounter = 0;
      foreach($inputDataValues as $inputDataValue) {
	//This is where a broker is needed.

	$Return = createAndRunModel($DOMEServer, $InterfaceID, json_encode($inputDataValue));
	//	$Return[$bulkRunCounter] = createAndRunModel($DOMEServer, $InterfaceID, json_encode($inputDataValue));
	sleep(2);  
	$bulkRunCounter++;
      }
      //Return necessary data to page
      echo json_encode($Return);
      return;
    } else if(strcmp($fileExtention, ".zip") == 0) {
      // need to un ZIP
      $Return['error']=true;
      $Return['msg']="Received ZIP file ";
      echo json_encode($Return);
      return;
    } else {
      $Return['error']=true;
      $Return['msg']="Unable to handle file type ".$fileExtention;
      echo json_encode($Return);
      return;
    }

    //need to begin loop
    //$Return = createAndRunModel($DOMEServer, $InterfaceID, $ModelInputData);
    //end loop

    //TODO figure out how to move files to DOME server for both bulk and single model runs.
    $Return['error']=true;
    $Return['msg']="Run in bulk ".json_encode($inputDataValues);
    echo json_encode($Return);
    return;
  }
}
$Return = createAndRunModel($DOMEServer, $InterfaceID, $ModelInputData);

//Return necessary data to page
echo json_encode($Return);
return;


function createAndRunModel($DOMEServer, $InterfaceID, $ModelInputData) {
  $Return=array('error'=>false);
  //Create a new DOME API object with the specified URL                                                                                                                                                 
  $DOMEApi=new DOMEApi($DOMEServer->getURL(),false);

  //Create a new queue, tell DOME to run the model, and return the queue URL
  if ($Queue=$DOMEApi->runModel($InterfaceID, $ModelInputData)){
    //Create a new record to this specific run of this model                                                                                                                                           
    $Return['running_id']=CEMRunning::create($InterfaceID, user_getid(), $Queue);
    $Return['queue_url']=$Queue;
  }else{
    $Return['error']=true;
    $Return['msg']=$DOMEApi->getErrorMessage();
  }
  return $Return;
}


function processCSV($CSVFileName, $CSVFilePath) {
  $file = $CSVFilePath.$CSVFileName;

  ini_set("auto_detect_line_endings", true);  // allows php to detect both newlines (\n) and carrage returns (\r) as end of line.

  //read file.
  $handle = fopen($file, "r");
  $inputDataValues = array();
  if ($handle) {
    $columnNames = array();
    if(($line = trim(fgets($handle))) != false) {
      $columnNames = explode(',', $line);
      // also need to check that headers are all strings
    } else {
      // error no headers in CVS table
      $Return['error']=true;
      $Return['msg']=$CSVFileName." does not contain column header data";
      echo json_encode($Return);
      return;
    }
    
    $counter = 0;   
    while (($line = trim(fgets($handle))) != false) {
      $columnValues = explode(',', $line);
      $inputDataValues[$counter] = array();
      $i = 0;
      foreach($columnNames as $colName) {
	//foreach($columnValues as $colValue) {
	$inputDataValues[$counter][$colName] = (float)$columnValues[$i];
	$i++;
	  //	}
      }
      $counter++;
    }
  } else {
    // error opening the file.
    $Return['error']=true;
    $Return['msg']="Could not open file ".$file;
    echo json_encode($Return);
    return;
  }

  fclose($handle);
  unlink($file);
  return $inputDataValues;  // return somthing that will fit right in to the interface invocation
}


/*include($gfwww.'include/model.php');
$Model=new model(0);

function toArray($data) {
    if (is_object($data)) $data = get_object_vars($data);
    return is_array($data) ? array_map(__FUNCTION__, $data) : $data;
}

$ModelData=toArray($Model->get_def($_POST['modelID'])); //$_POST['modelDef'];
//print_r($ModelData);
$ModelData['modelDef']['dateModified']=$Model->format_value($ModelData['modelDef']['dateModified'],'Int');
$Data=json_decode($_POST['data'],true);
//print_r($Data);

$Size=sizeof($Data);
$InParams=$ModelData['inParams'];
for($i=0;$i<$Size;$i++){
	while ($DataParam=pos($Data)){
		//echo key($Data).'='.$InParams[$i]['name'];

		if (key($Data)==$InParams[$i]['name']){
			//If keys match, format values correctly and change to user-inputted value
			//Removes quotes from ints if necessary
			//Changes string vectors/matrices to arrays
			$InParams[$i]['value']=$Model->format_value($DataParam,$InParams[$i]['type']);
		}

		next($Data);
	}
	reset($Data);
}

//print_r($InParams);

//Go through all outParams to convert them to correct format
/*for($i=0;$i<count($ModelData['outParams']);$i++){
	if ($ModelData['outParams'][$i]['type']=="Vector"){
		$Vector=$ModelData['outParams'][$i]['value'];
		for($ii=0;$ii<count($Vector);$ii++){
			//Convert all values to floats
			//$ModelData['outParams'][$i]['value'][$ii]=floatval($Vector[$ii]);
		}
	}
}

//Replace old inParams with user-inputted inParams
$ModelData['inParams']=$InParams;

$PKGData=str_replace('\/','/',json_encode($ModelData));
//echo $PKGData;
//$PKGData=substr($PKGData,1,strlen($PKGData)-2);

//{"modelDef":{"name":"service resputation calculator","guid":"1","desc":"a model that combines a user community trust score with a validation score from the curator oft the model portal","dateShared":1311051600000},"inParams":[{"name":"community trust score","type":"real","unit":"none","val":"12"},{"name":"curator validation score","type":"real","unit":"none","val":"34"}],"outParams":[{"name":"service reputation score","type":"real","unit":"none","val":"1"}]}

echo $Model->run($PKGData);*/
?>
