<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';


$DIID=getIntFromRequest('diid');
$NewOrder=getStringFromRequest('inputs_array');

$Return=array('error'=>false);

$DOMEInterface=new DOMEInterface($DIID);
$Return['error']=!$DOMEInterface->updateInputsOrder($NewOrder);

echo json_encode($Return);
?>