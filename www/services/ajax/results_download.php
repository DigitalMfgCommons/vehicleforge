<?php
require_once '../../env.inc.php';
require_once $gfcommon . 'include/pre.php';

$RunID=getIntFromRequest('run_id');
$Run=new CEMRunning($RunID);
$DI=new DOMEInterface($Run->getInterfaceID());
$Messages=CEMMessages::getByRunningID($RunID);

$Data=null;

$Doc=new DOMDocument();
$ModelElement=$Doc->createElement('ModelResults');
$ModelName=$Doc->createAttribute('runID');
$ModelName->value=$RunID;
$ModelElement->appendChild($ModelName);

$ModelID=$Doc->createAttribute('modelID');
$ModelID->value=$Run->getInterfaceID();
$ModelElement->appendChild($ModelID);

$Root=$Doc->appendChild($ModelElement);

foreach($Messages as $i){
    $Data=$i->getMessage();
    $Data=json_decode($Data);

    //Don't add the end of run message, not necessary
    if ($Data->id->idString!="end_of_run"){
        $MessageElement=$Doc->createElement('Message'/*,json_encode($Data)*/);

        $MessageParam=$Doc->createAttribute('param');
        $MessageParam->value=$Data->param;
        $MessageElement->appendChild($MessageParam);

        $MessageOldVal=$Doc->createAttribute('oldVal');
        if (is_array($Data->old_val)){
            $MessageOldVal->value=$Data->old_val[0];
        }else{
            $MessageOldVal->value=$Data->old_val;
        }
        $MessageElement->appendChild($MessageOldVal);

        $MessageNewVal=$Doc->createAttribute('newVal');
        if (is_array($Data->new_val)){
            $MessageNewVal->value=$Data->old_val[0];
        }else{
            $MessageNewVal->value=$Data->old_val;
        }
        $MessageElement->appendChild($MessageNewVal);

        $MessageTime=$Doc->createAttribute('time');
        $MessageTime->value=$Data->occur;
        $MessageElement->appendChild($MessageTime);

        $Root->appendChild($MessageElement);
    }
}

$Doc->formatOutput=true;
$Name=str_replace(' ','',$DI->getName());

$Filename=$Name."_".$RunID.".xml";
if ($Doc->save(getStringFromServer('document_root')."/services/run_results/".$Filename)){
    echo json_encode(array('error'=>false,'file'=>"/run_results/".$Filename));
}

/*
$Filename="r".$RunID;
if ($F=fopen($_SERVER['DOCUMENT_ROOT']."/services/run_results/".$Filename,'w')){
    fwrite($F,$Data);
    fclose($F);
}*/
?>