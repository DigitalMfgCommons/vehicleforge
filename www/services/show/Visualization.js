//<script>
function runLineVisualization() {

var margin = {top: 20, right: 20, bottom: 30, left: 50},
    width = 480 - margin.left - margin.right,
    height = 250 - margin.top - margin.bottom;

var parseDate = d3.time.format("%d-%b-%y").parse;

var x = d3.time.scale()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var line = d3.svg.line()
    .x(function(d) { return x(d.date); })
    .y(function(d) { return y(d.close); });

    
var svg = d3.select("#visual").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

 
d3.csv("/services/show/dataLine.csv", function(error, data) {
	data.forEach( function(d) {
		d.date = parseDate(d.date);
		d.close = +d.close;
		});
	x.domain(d3.extent(data, function(d) { return d.date; }));
	y.domain(d3.extent(data, function(d) { return d.close; }));

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Price ($)");

  svg.append("path")
      .datum(data)
      .attr("class", "line")
      .attr("d", line);
});
}

function runBarVisualization () {
var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var formatPercent = d3.format(".0%");

var x = d3.scale.ordinal()
    .rangeRoundBands([0, width], .1);

var y = d3.scale.linear()
    .range([height, 0]);

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left")
    .tickFormat(formatPercent);

var svg = d3.select("#visual").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    var data = eval($('#outputs > .control-group').find('.output').val());
	alert(data);
    x.domain(data);
y.domain([0, 100]);

svg.append("g")
.attr("class", "x axis")
.attr("transform", "translate(0," + height + ")")
.call(xAxis);

svg.append("g")
.attr("class", "y axis")
.call(yAxis)
.append("text")
.attr("transform", "rotate(-90)")
.attr("y", 6)
.attr("dy", ".71em")
.style("text-anchor", "end")
.text("Frequency");

svg.selectAll(".bar")
.data(data)
.enter().append("rect")
.attr("class", "bar")
.attr("x", function(d) { return x(d); })
.attr("width", x.rangeBand())
.attr("y", function(d) { return y(d); })
.attr("height", function(d) { return height - y(d); });
    
    /* 
    This code was originally used to garner data to
    show the feasibility of plotting data.  It has now been
    updated with the new code above.
    
d3.csv("/services/show/dataBar.csv", function(error, data) {

  data.forEach(function(d) {
    d.frequency = +d.frequency;
  });

  x.domain(data.map(function(d) { return d.letter; }));
  y.domain([0, d3.max(data, function(d) { return d.frequency; })]);

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Frequency");

  svg.selectAll(".bar")
      .data(data)
    .enter().append("rect")
      .attr("class", "bar")
      .attr("x", function(d) { return x(d.letter); })
      .attr("width", x.rangeBand())
      .attr("y", function(d) { return y(d.frequency); })
      .attr("height", function(d) { return height - y(d.frequency); });

});
*/

}

function runScatterVisualization() {

	var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 960 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

var x = d3.scale.linear()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var color = d3.scale.category10();

var xAxis = d3.svg.axis()
    .scale(x)
    .orient("bottom");

var yAxis = d3.svg.axis()
    .scale(y)
    .orient("left");

var svg = d3.select("#visual").append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
  .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    
   
    // Find the activated radio button (x axis)
    var radioButtonVal = $("input[@name=radioX]:checked").val();
    // This will be either undefined or a string.  Use the string
    // to locate the text field to pull data
// Match the id with the data-name for the text fields
if (radioButtonVal && radioButtonVal != "") {
	var Keys = {};
	var Data = {};
	var test = $(":text").each(function(){
		if ($(this).attr("data-name") == radioButtonVal) {
			// alert('Found Matching X Value!');
			Data["x"] = eval($(this).val());
			Keys["x"] = $(this).attr("id");
		}
	});
}
else {
alert("Please select data for the x-axis");
	return;
}
//var data = $('#outputs > .control-group').find("#"+radioButtonVal);
    
    // Then find the activated checkboxes (y axis)
    // Then store the active values into a data array
var checkBoxes = $(":checkbox[name=checkboxY]:checked");
    if (checkBoxes.length > 0) {
        var index = 0;
    	checkBoxes.each(function() {
			yIndexName = $(this).val();
			var test = $(":text").each(function(){
				if ($(this).attr("data-name") == yIndexName) {
					// alert('Found Matching Y Value!');
					Data["y"+index] = eval($(this).val());
					Keys["y"+index] = $(this).attr("id");
					index++;
				}
			});
        });
    }
    else {
		alert("Please select at least one value for the y-axis");
    }

/*
d3.csv("/services/show/dataScatter.csv", function(error, data) {
  data.forEach(function(d) {
    d.sepalLength = +d.sepalLength;
    d.sepalWidth = +d.sepalWidth;
  });
*/

// Formats the data to output to D3
var plotData = [];
for (var i = 0; i < Data["x"].length; i++) {
	 plotData.push( { x: Data["x"][i], y: Data["y0"][i]})
	}

  x.domain(d3.extent(Data["x"], function(d) { return d; })).nice();
  y.domain(d3.extent(Data["y0"], function(d) { return d; })).nice();

  svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis)
    .append("text")
      .attr("class", "label")
      .attr("x", width)
      .attr("y", -6)
      .style("text-anchor", "end")
      .text(Keys["x"]);

  svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
    .append("text")
      .attr("class", "label")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text(Keys["y0"])
// Add the key name to the text
  svg.selectAll(".dot")
      .data(plotData)
    .enter().append("circle")
      .attr("class", "dot")
      .attr("r", 3.5)
      .attr("cx", function(d){return x(d.x);})
      .attr("cy", function(d){return y(d.y);})
      .style("fill", "blue");

  var legend = svg.selectAll(".legend")
      .data(color.domain())
    .enter().append("g")
      .attr("class", "legend")
      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });

  legend.append("rect")
      .attr("x", width - 18)
      .attr("width", 18)
      .attr("height", 18)
      .style("fill", "white");

  legend.append("text")
      .attr("x", width - 24)
      .attr("y", 9)
      .attr("dy", ".35em")
      .style("text-anchor", "end")
      .text(function(d) { return d; });

//});
	
}
//</script>
