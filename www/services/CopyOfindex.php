<?php
header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';

// get group
$group_id=getStringFromRequest('group_id');
$group = group_get_object($group_id);

session_require_perm ('project_read', $group_id) ;

if (!$group || !is_object($group)) {
    exit_no_group();
}

// get dome interface
$InterfaceID=getIntFromRequest('diid');
$RunID=getIntFromRequest('runid', false);
$DOMEInterface=new DOMEInterface($InterfaceID);

if ($DOMEInterface->getID() != 0){
  $Title=$DOMEInterface->getName();
} else {
	$Title='';
}

// add bootstrap tab plugin
use_javascript('bootstrap/tabs.js');

site_project_header(array('group'=>$group_id,'title'=>'Service View: '.$Title), 2);
if ($DOMEInterface->getID() != 0) {
    include 'CopyOfshow.php';
} else {

	/* this is a killer query... need to add group_id to the service_cem map */
	function findServices($componentArray) {
		$s = array();
		foreach ($componentArray as $c) {
			foreach($c->getInterfaces() as $i) {
				$s[]= array("component"=>$c, "interface"=>$i);
			}
			$s = array_merge($s, findServices($c->getSubComponents()));
		}
		return $s;
	}
	
	$services = findServices($group->getComponents());
	
	$Layout->col(3,true,true);
	include 'service_nav.php';
	$Layout->endcol();
	
	$Layout->col(9);
	?>
	<h2 style="border-bottom: 1px solid #AAA;">Services in <?=$group->getPublicName()?></h2><br/>
	
	<h3>Service Subscriptions</h3>
	<ul class="unstyled">
		<?php foreach($services as $s) { ?>
			<li>
				<a href="/services/?group_id=<?=$group->getID()?>&diid=<?=$s['interface']->getID()?>"><?=$s['interface']->getName()?></a>
				in
				<a href="/components/?group_id=<?=$group->getID()?>&cid=<?=$s['component']->getID()?>"><?=$s['component']->getName()?></a>
			</li>
		<?php } ?>
	</ul>
	
	<h3>Your Services</h3>
	No services registered yet.

	<?php
	$Layout->endcol();
}

site_project_footer();
?>
