<?php
/*
 * service_nav.php
 * navigation bar for the services tab of a project
 *
 * local variables:
 * 	$group
 */

?>

<div class="well">
	<ul id="service_nav" class="nav nav-list nav-stacked">
	  <li><a href="index.php?group_id=<?= $group->getID() ?>" ><i class="icon-th-list"></i> Browse Services</a></li>
	  <li><a href="register.php?group_id=<?= $group->getID() ?>" ><i class="icon-edit"></i> Register a Service</a></li>
	  <li><a href="publish.php?group_id=<?= $group->getID() ?>" ><i class="icon-share"></i> Share Service</a></li>
	  <li><a href="integrate.php?group_id=<?= $group->getID() ?>" ><i class="icon-random"></i> Integrate Services</a></li>
	</ul>
</div>

<script>
$(document).ready(function() {
	path = window.location.pathname;
	page = path.match(/\w+\.php/) || 'index.php';
	link = $('#service_nav a[href*="' + page + '"]');
	link.parent().addClass('active');
	link.children('i[class*="icon"]').addClass('icon-white');
});
</script>