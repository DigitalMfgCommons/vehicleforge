<?php

/*
 *	Show all services in a given project
 *	Jeff Mekler (2/29/2012 Leap Day!)
 */

	require_once '../env.inc.php';
	require_once $gfcommon . 'include/pre.php';
	
	
	// get group
	$group_id=getIntFromRequest('group_id');
	$group = group_get_object($group_id);

	session_require_perm ('project_read', $group_id) ;

	if (!$group || !is_object($group)) {
	    exit_no_group();
	}
	
	// recurse through all components looking for services //
	/*** this is a KILLER query... need to add group_id to the service_cem map */
	function findServices($componentArray) {
		$s = array();
		foreach ($componentArray as $c) {
			foreach($c->getInterfaces() as $i) {
				$s[]= array("component"=>$c, "interface"=>$i);
			}
			$s = array_merge($s, findServices($c->getSubComponents()));
		}
		return $s;
	}
	
	$services = findServices($group->getComponents());
	
	$HTML->addJavascript('bootstrap/tabs.js');
	
	site_project_header(array('group'=>$group_id,'title'=>'Service View:'),2);
	
	// BEGIN left hand column
	$Layout->col(3,true,true);
	include 'service_nav.php';
?>

<div class="sidebar-controls">
	<div data-tab="#connect">
		<div class="well" id="relation-pane">
			<h4 style="text-align: center;">Connection Builder</h4>
			<br/>
			<ul class="unstyled">
				<li><strong>From:</strong></li> 
				<li id="output-parameter"><input><i class="pull-right close">&times;</i></li>
				<li><strong>To:</strong></li>
				<li id="input-parameter"><input><i class="pull-right close">&times;</i></li>
				<li style="text-align: center;"><button id="save-relation" class="btn btn-primary" style="width: 70%;">Add Connection</button></li>
			</ul>
		</div>
	</div>
</div>

<?php
	$Layout->endcol(); // END left hand column
	$Layout->col(9); // BEGIN main content
?>

<ul class="nav nav-tabs">
  <li class="active"><a href="#connect" data-toggle="tab"><span style="font-size:1.3em; font-weight: bold;">1 </span>Connect Services</a></li>
  <li><a href="#build" data-toggle="tab"><span style="font-size:1.3em; font-weight: bold;">2 </span>Build Runtime Interface</a></li>
  <li><a href="#deploy" data-toggle="tab"><span style="font-size:1.3em; font-weight: bold;">3 </span>Test and Save</a></li>
</ul>

<script src="/js/jquery.json-2.4.min.js"></script>
<script src="integrate/integrate.js"></script>
<script>
		$('a[data-toggle="tab"]').on('shown', function (e) {
			activeID = $(e.target).attr('href'); // activated tab
			lastID = $(e.relatedTarget).attr('href'); // previous tab
			
			$('.sidebar-controls').children('div[data-tab="' + activeID + '"]').show();
			$('.sidebar-controls').children('div[data-tab="' + lastID + '"]').hide();
		})
</script>

<div class="tab-content">
	<div class="tab-pane active" id="connect">
		<?php include 'integrate/connect.php'; ?>
	</div>
  
	<div class="tab-pane" id="build">
		<?php include 'integrate/build.php'; ?>
	</div>
  
	<div class="tab-pane" id="deploy">
	  	<h4>Service Name</h4>
	  	<input id="service-name" placeholder="Name your integrated service">
  	
	  	<h4>Service Description</h4>
	  	<textarea id="service-description" class="span6" placeholder="Tell others what this model does (optional)"></textarea>
  	
	  	<br/><br/><button class="btn btn-primary" id="save-service">Save Integrated Service</button>
	  	<script>
	  		$('#save-service').click(saveService);
		  		
	  		function saveService() {	
	  			$('#save-service').prop('disabled',true);	  		
	  			// parse through build parameters and create them
	  			$('#build-inputs .build-parameter').each( function(index, el) {
	  				processIntegrationParameter(el, 'input');
	  			});
	  			
	  			$('#build-outputs .build-parameter').each( function(index, el) {
	  				processIntegrationParameter(el, 'output');
	  			});
	  			
	  			IM.name = $('#deploy #service-name').val();
	  			IM.description = $('#deploy #service-description').val();

		  		var jsonData = IM.getData();
		  		
					$.ajax({
						type: 'POST',
						url: 'ajax/put_integration.php',
						dataType: 'json',
						data: {
							'integrationModel': $.toJSON(jsonData),
							'group_id': <?= $group_id ?>
						},
						success: function(msg){
							$('#save-service').prop('disabled',false);
							alert("Integrated service created.");
							console.log($.toJSON(msg));
						},
						error: function(msg) {
							$('#save-service').prop('disabled',false);
							alert("Failure in creating service: " + JSON.stringify(msg));
						}
					});		 
  			}
  			
  			function processIntegrationParameter(el, type) {
	
  				var internalParam = Parameter.find($(el).attr('parameter-id'));
  				
  				if ( $(el).find('input[type="checkbox"]').is(':checked') ) {
		  			
		  			// create parameter data
		  			var paramData = {
			  			name: $(el).find('.integration-parameter > input').val(),
			  			unit: internalParam.getUnit(),
			  			type: internalParam.getType(),
			  			category: internalParam.getCategory(),
			  			value: $(el).find('.default-value > input').val() || linkeParam.getValue()
		  			}

		  			// build parameter
		  			var parameter = new Parameter(0, paramData);

		  			// set up links in integration model
		  			IM.addParameter(parameter, type);
		  			
		  			if (type == "input")
			  			IM.addRelation(parameter, internalParam, false);
			  		else 
			  			IM.addRelation(internalParam, parameter, false);
  				} 		
	  		}
	  	</script>
	</div>
</div>

<?php 
	$Layout->endcol();
	site_project_footer();
?>
