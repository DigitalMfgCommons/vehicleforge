<?php
require_once '../../env.inc.php';
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'include/DOMEApi.class.php';
require_once $gfcommon.'include/Server.class.php';
$Server=new Server(getIntFromRequest('server_id'));
$URL=$Server->getURL();

$DOMEApi=new DOMEApi($URL, false);

$Version=$_POST['version'];
$ModelID=$_POST['model_id'];
$Desc=$_POST['desc'];
$DateMod=$_POST['date_mod'];
$Type=$_POST['type'];
$Name=$_POST['name'];
//$FolderID=$_POST['folder_id'];
$Path=$_POST['path'];

$Return=array('error'=>false);
if ($Ints=$DOMEApi->getInterfaces($Version, $ModelID, $Desc, $DateMod, $Type, $Name, $Path)){
	$Return['data']=$Ints;
}else{
	$Return['error']=true;
	$Return['msg']=$DOMEApi->getErrorMessage();
}

echo json_encode($Return);
?>