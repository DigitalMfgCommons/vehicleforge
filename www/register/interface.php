<?php
require_once '../env.inc.php';
require_once $gfcommon.'include/pre.php';
//require_once $gfcommon.'include/DOMEApi.class.php';
//$DOMEApi=new DOMEApi;

$JS='';
add_js($JS);

$GroupID=getStringFromRequest('group_id');
$Group=group_get_object($GroupID);
$CEMID=getIntFromRequest('cem_id');
$CEM=new CEM($CEMID);

site_project_header(array('title'=>_('Register Interface'), 'group'=>$GroupID));
$Layout->col(12,true);
?>
<script type="text/javascript" src="/js/jquery.json-2.4.min.js"></script>
<script>
$(function(){
    var server_id=0;

    var $interface_data=$("#interface_data"),
        $register_button=$("#register"),
        $register_loader=$("#register_loader"),
        $tree=$("#tree"),
        $server=$("#server");

    $(document).on("click","a.folder, a.model, a.project", function(){
		var $me=$(this),
				data={},
				url='ajax/';

		if ($me.hasClass('folder')){
			data={
				type: $me.data("type"),
				name: $me.data("name"),
				path: $me.data("path")
			};
			url+="getChildren.php";
		}else if ($me.hasClass('model')){
			data={
				version:$me.data("version"),
				model_id:$me.data("model_id"),
				desc:$me.data("desc"),
				date_mod:$me.data("date"),
				type:$me.data("type"),
				name:$me.data("name"),
				path:$me.data("path")
			};
			url+="getInterfaces.php";
		}else if ($me.hasClass('project')){
			data={
					version:$me.data("version"),
					model_id:$me.data("model_id"),
					desc:$me.data("desc"),
					date_mod:$me.data("date"),
					type:$me.data("type"),
					name:$me.data("name"),
					path:$me.data("path")
			};
			url+="getInterfaces.php";
		}else{
			alert('need type');
		}

		data.server_id=server_id;

		$.ajax({
			url: url,
			dataType: "json",
			type: "post",
			data: data,
			success: function(msg){
				if (!msg.error){
					var $child,
						html="";
					for(var i in msg.data){
						html+="<div>"
						if (msg.data[i].type=="folder"){
                            $child=$("#c"+$me.data("name").replace(/\s+/g, "-"));
			    html+="<a href='javascript:void(0)' data-path='["+msg.data[i].path+"]' class='"+msg.data[i].type+"' data-type='"+msg.data[i].type+"' data-name='"+msg.data[i].name+"'>"+msg.data[i].name+"</a><div id='c"+msg.data[i].name.replace(/\s+/g, '-')+"' class='hidden child'></div>";
						}else if (msg.data[i].type=="model"){
                            $child=$("#c"+$me.data("name").replace(/\s+/g, "-"));
							html+="<a href='javascript:void(0)' data-path='["+msg.data[i].path+"]' data-date='"+msg.data[i].dateModified+"' data-desc='"+msg.data[i].description+"' data-version='"+msg.data[i].version+"' data-model_id='"+msg.data[i].modelId+"' class='"+msg.data[i].type+"' data-type='"+msg.data[i].type+"' data-name='"+msg.data[i].name+"'>"+msg.data[i].name+"</a><div id='m"+msg.data[i].modelId+"' class='hidden child'></div>";
						}else if (msg.data[i].type=="project"){
                            $child=$("#c"+$me.data("name").replace(/\s+/g, "-"));
							html+="<a href='javascript:void(0)' data-path='["+msg.data[i].path+"]' data-date='"+msg.data[i].dateModified+"' data-desc='"+msg.data[i].description+"' data-version='"+msg.data[i].version+"' data-model_id='"+msg.data[i].projectId+"' class='"+msg.data[i].type+"' data-type='"+msg.data[i].type+"' data-name='"+msg.data[i].name+"'>"+msg.data[i].name+"</a><div id='m"+msg.data[i].projectId+"' class='hidden child'></div>";
						}else if(msg.data[i].type=="interface"){
							var classType = (typeof msg.data[i].projectId === 'undefined'?"m":"p");
                            $child=$("#m"+$me.data("model_id"));
							html+="<a href='javascript:void(0)' class='"+classType+"interface' data-path='["+msg.data[i].path+"]' data-version='"+msg.data[i].version+"' data-name='"+msg.data[i].name+"' data-interface_id='"+msg.data[i].interfaceId+"' data-model_id='"+$me.data('model_id')+"' data-project_id='"+msg.data[i].projectId+"'>"+msg.data[i].name+"</a>";
						}else{
							html+="";
						}
						html+="</div>";
					}

					$child.html(html);
					$child.slideDown("fast");
				}else{
					alert(msg.msg);
				}
			}
		  });
	});

    $(document).on('click', ".minterface", function(){
        var $me=$(this);

        $.ajax({
            url: 'ajax/getModel.php',
            dataType: 'json',
            type: 'post',
            data: {
                version: $me.data('version'),
                model_id: $me.data('model_id'),
                name : $me.data('name'),
                path: $me.data('path'),
                interface_id: $me.data('interface_id'),
	            server_id:server_id
            },
            success: function(msg){
                if (!msg.error){
                    $interface_data.val($.toJSON(msg.data));
                    $register_button.prop('disabled',false);
                }else{
                    alert(msg.error);
                }
            }
        })
    });
    
    $(document).on('click', ".pinterface", function(){
        var $me=$(this);

        $.ajax({
            url: 'ajax/getModel.php',
            dataType: 'json',
            type: 'post',
            data: {
                version: $me.data('version'),
                project_id: $me.data('project_id'),
                name : $me.data('name'),
                path: $me.data('path'),
                interface_id: $me.data('interface_id'),
	            server_id:server_id
            },
            success: function(msg){
                if (!msg.error){
                    $interface_data.val($.toJSON(msg.data));
                    $register_button.prop('disabled',false);
                }else{
                    alert(msg.error);
                }
            }
        })
    });
    
    $register_button.click(function(){
        $(this).prop('disabled',true);
        $register_loader.fadeIn();
        $.post('ajax/registerInterface.php',{
            data: $interface_data.val(),
	        cem_id: <?=$CEMID?>,
	        server_id:server_id,
            alias: $("#alias").val()
        },function(msg){
            $register_loader.hide();
            clear();
        });
    });

	$server.change(function(){
		server_id=$(this).val();
        $("#tree_loader").fadeIn();
        $interface_data.val('');

		$tree.slideUp(function(){
			$.ajax({
				url:'ajax/getChildren.php',
				dataType:'json',
				type:'post',
				data:{
					server_id:server_id
				},
				success:function(msg){
					var html='',
							data=msg.data;

					for(var i in data){
						html+='<div><a href="javascript:void(0)" class="'+data[i].type+'" data-type="'+data[i].type+'" data-path="['+data[i].path+']" data-name="'+data[i].name+'">'+data[i].name+'</a>';
						if (data[i].type=="folder")
						  html+='<div id="c'+data[i].name.replace(/\s+/g, "-")+'" class="hidden child"></div>';
						html+='</div>';
					}
                    $("#tree_loader").hide();
					$tree.html(html).slideDown();
				}
			});
		});
	});

    $("#reset").click(function(){
        clear();
    });

    function clear(){
        $tree.html('');
        $interface_data.val('');
        $server.val(0);
        $register_button.prop('disabled',false);
    }
});
</script>
<style>
.child  {margin-left:10px}
.folder{color: green}
.model{color:red}
.interface{color:blue}
</style>
<?php
$Options=array(0=>'-Select a Server-');
$Servers=session_get_user()->getServers();
foreach($Servers as $i){
    if ($i->getAlias())
        $Options[$i->getID()]=$i->getAlias();
    else
        $Options[$i->getID()]=$i->getURL();
}

$Form=new BsForm;
echo $Form->init('javascript:void(0)','post',array('class'=>'form-horizontal'))
    ->head('Register a service in '.$Group->makeLink().' in component '.$CEM->makeLink())
    ->group('Server',
        new Dropdown($Options,0,array(
            'id'=>'server',
            'autocomplete'=>false
        ))
    )
    ->group('DOME Files',
        new Custom(util_ajax_loader(),array(
            'class'=>'hidden',
            'id'=>'tree_loader'
        )),
        new Custom('',array('id'=>'tree')),
        new Help('<b class="folder">Folders</b> <b class="model">Models</b> <b class="interface">Interfaces</b>')
    )
    ->group('Alias',
        new Text(array(
            'autocomplete'=>false,
            'id'=>'alias'
        ))
    )
    ->group('Service Data',
        new Textarea('',array(
            'id'=>'interface_data',
            'disabled'=>true,
            'autocomplete'=>false,
            'rows'=>5,
            'class'=>'input-xlarge'
        ))
    )
    ->actions(
        new Submit('Register',array(
            'id'=>'register',
            'disabled'=>true,
            'class'=>'btn btn-primary'
        )),
        new Reset('Reset',array('id'=>'reset')),
        new Custom(util_ajax_loader(),array(
            'class'=>'hidden',
            'id'=>'register_loader'
        ))
    )
    ->render();
?>
<!--<select id="server" autocomplete="off">
	<option value="0">-Select a Server-</option>
	<?php
/*	$Servers=session_get_user()->getServers();
	foreach($Servers as $i){
		echo '<option value="'.$i['server_id'].'">'.$i['url'].'</option>';
	}
	*/?>
</select>-->
<!--<div id="tree" class="hidden">
	<?php
/*	/*$Children=$DOMEApi->getChildren();
	foreach($Children as $i){
		echo '<div>
				<a href="javascript:void(0)" class="'.$i->type.'" data-type="'.$i->type.'" data-folder_id="'.$i->folderId.'" data-name="'.$i->name.'">'.$i->name.'</a>';
		if ($i->type=="folder")
			echo '<div id="c'.$i->folderId.'" class="hidden child"></div>';

		echo '</div>';
	}*/
	?>
</div>-->
<div id="reg_panel" class="hidden">
    <button class="btn success" id="register">Register</button>
</div>
<?php
$Layout->endcol();
site_footer();
?>