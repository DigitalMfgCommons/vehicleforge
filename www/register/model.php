<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
//require_once($gfwww.'include/model.php');
require_once($gfwww . 'include/LayoutGen.php');
require_once($gfwww . 'include/FormGen.php');
//$Model=new model;
$Layout=new LayoutGen;
$Form=new FormGen;

if (!session_loggedin()) {
	exit_not_logged_in();
}

use_javascript('/js/xbreadcrumbs.js');
use_stylesheet('/js/xbreadcrumbs/xbreadcrumbs.css');
use_javascript('jquery.bxSlider.min.js');
use_javascript('jquery.easing.1.3.js');
add_js('$(function(){
	$("#breadcrumbs").xBreadcrumbs();
	var $Slider=$("#content").bxSlider({controls:false});

	$("#breadcrumbs li a").click(function(e){
		var index=$("#breadcrumbs li a").index(this);
		//console.log(index);
		$Slider.goToSlide(index);

		$("#breadcrumbs li").removeClass("current");
		$(this).parent().addClass("current");
	});
});');

site_user_header(array('title'=>'Register Model'));

$Layout->col(12);
echo $HTML->boxTop(_('Register a New Model'),true);
echo '<ul id="breadcrumbs" class="xbreadcrumbs">
<li class="current"><a href="javascript:void(0)">Server Connection</a></li>
<li><a href="javascript:void(0)">Model Info</a></li>
</ul>';

echo '<div id="content"><div>
<style>.bx-wrapper{clear:both}</style>';
$Form->input('text','Server URL')
		//->input('text','Port')
		//->input('text','Username')
		//->input('password','Password')
		->input('button','',array('value'=>'Test Connection'))
		->input('button','',array('value'=>'Register','disabled'=>true))
		->display()
		->clear();
echo '</div><div>';
$Form//->input('select','DOME Server')
	->input('text','Model Name')
	->input('textarea','Description')
	->input('submit','',array('value'=>'Register'))
	->display();
echo '</div></div>';

echo $HTML->boxBottom();
$Layout->endcol();

site_project_footer(array());
?>