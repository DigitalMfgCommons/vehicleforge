<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:40 AM 10/13/11
 */

require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';

require_once 'include/FormGen.php';
require_once $gfcommon.'include/kadmin.class.php';
$FormGen=new FormGen;
$kadmin=new kadmin;

if (isset($_GET['add'])){
	$User=$_POST['user'];
	$Pass=$_POST['pass'];

	$kadmin->createPrincipal($User,$Pass);
}
echo '<br><br>';
echo 'Add User';
$FormGen->build('?add')
	->input('text','Username',array('name'=>'user'))
	->input('text','Password',array('name'=>'pass'))
	->input('submit','',array('value'=>'Submit'))
	->display()
	->clear();

echo 'Change pass';
$FormGen->build('?change')
	->input('text','Username')
	->input('text','Password')
	->input('submit','',array('value'=>'Submit'))
	->display();
?>