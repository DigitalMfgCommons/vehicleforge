<?php
/**
 * Developer Info Page
 *
 * Copyright 1999-2001 (c) VA Linux Systems 
 * Copyright 2010, FusionForge Team
 * Copyright (C) 2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * Assumes $user object for displayed user is present
 */

require_once $gfwww.'include/vote_function.php';

$ONUSERPAGE=true;//DO NOT REMOVE - Used in siteactivity.class
$UserActivity=new UserActivity($user_id);

$OnOwnProfile=(session_loggedin() && $user_id==session_get_user()->getID());

use_stylesheet('/js/jqplot/jquery.jqplot.min.css');
use_stylesheet('/themes/gforge/css/feed.css');
use_stylesheet('/themes/gforge/css/widget.css');

use_javascript('jqplot/jquery.jqplot.min.js');
use_javascript('activity_comments.js');
use_javascript('jqplot/jqplot.pieRenderer.min.js');
use_javascript('recommend.js');
use_javascript('pages/user_home.js');

$HTML->header(array('title'=>_('User Profile')));

$Layout->col(3,true);

echo '<div id="profile_image" style="position:relative;width:180px;height:180px">';
if ($OnOwnProfile)
    echo '<div id="profile_image_overlay" class="hidden" style="position:absolute;width:25px;height:25px;right:0;top:0;background:#2F9AD4"><i class="icon-pencil icon" style="margin:5px 0 0 5px"></i></div>';

echo $user->getImageHTML(180,180).'</div>';
echo '<p>Member Since: '.date(_('Y-m-d'), $user->getAddDate()).'</p>';

echo '<div class="cell"><i class="icon-envelope"></i><a href="/account/messages.php?action=compose&to='.$user->getID().'">Send a Message</a></div>';

if (!$OnOwnProfile && session_loggedin()){
    require_once APP_PATH.'common/include/RecommendationManager.class.php';
    $UR=new UserRecommendation;
    echo '<div class="clearfix cell" data-type="1" data-user="'.$user->getID().'">';
    $Rec=$UR->getRec($user->getID());
    if ($UR->hasRecommended(user_getid(),$user->getID())){
        $Val=$UR->get(user_getid(),$user->getID());

        echo'<button class="btn span1 recommend-yes';
        if ($Val==1)
            echo ' btn-success';
        echo '"><img src="/images/apps/thumb-up.png" alt="Thumb Up" /><span class="count">'.$Rec[0].'</span></button>
		    <button class="btn span1 recommend-no';
        if ($Val==0)
            echo ' error';
        echo '"><img src="/images/apps/thumb.png" alt="Thumb Down" /><span class="count">'.$Rec[1].'</span></button>';
    }else{
        $RecYes=new Button(util_make_image('/images/apps/thumb-up.png',array('alt'=>'Thumb Up')),array('class'=>'span1 recommend-yes'));
        $RecNo=new Button(util_make_image('/images/apps/thumb.png',array('alt'=>'Thumb Down')),array('class'=>'span1 recommend-no'));

        echo $RecYes->render().$RecNo->render();
    }
    echo '</div>';
}

if ($user->isSkillsPublic()){
    $Skills=$user->getSkills();
    if ($Skills){
        $SkillsHTML='';
        echo '<div id="skills" class="cell" style="position:relative">';
        if ($OnOwnProfile)
            echo '<div id="skills_overlay" class="hidden" style="position:absolute;width:25px;height:25px;right:0;top:0;background:#2F9AD4"><i class="icon-pencil icon" style="margin:5px 0 0 5px"></i></div>';

        echo '<style>.bullet{list-style-type:circle;margin-left:15px}</style>';

        foreach($Skills as $i)
            $SkillsHTML.='<li class="bullet">'.$i->getSkillName().'</li>';

        echo $HTML->widget('Skills', $SkillsHTML);

        echo '</div>';
    }
}


$Badges=$user->getBadges();
if ($Badges){
    $BadgeHTML='';
    foreach($Badges as $i)
        $BadgeHTML.=$i->getHTML();
    echo $HTML->widget('Badges', $BadgeHTML);
}

$Layout->endcol()->col(6);
?>
<h2><?=$user->getRealName()?></h2>
<p id="about_me"><?=$user->getAbout()?></p>
<h3><?=_('Recent Activity')?></h3>
<p>
<?=$UserActivity->getActivityContent(10)?>
</p>
<h3><?=_('What people are saying about ' . $user->getFirstName())?></h3>
<p>
<?php
    HomeComments::displayForm($user_id, 1);
    $UserActivity->getCommentContent();
?>
</p>
<?php
$Layout->endcol()->col(3);

if ($OnOwnProfile){
    echo '<div class="cell">';
    $B=new Button('<i class="icon-pencil icon-white"></i> Editing Mode',array(
        'class'=>'btn-primary pull-right',
        'id'=>'edit_mode',
        'data-uid'=>$user_id
    ));
    echo $B->render();
    $B=new Button('<i class="icon-remove icon-white"></i> Cancel',array(
        'class'=>'btn-danger pull-right hidden',
        'id'=>'edit_cancel'
    ));
    echo $B->render();
    echo '</div>';
}

if (!$OnOwnProfile && session_loggedin()){
    echo '<div class="cell">';
    $SubscriptionStatus=session_get_user()->getSubscriptionStatus($user->getID(),1);
    if ($SubscriptionStatus){
        $Button=new Button('<i class="icon-ok-sign icon-white"></i> <span>Subscribed</span>', array(
            'class'     => 'btn btn-success pull-right subscribed',
            'data-rid'  => $user->getID(),
            'id'        => 'subscribe_button'
        ));
    }else{
        $Button=new Button('<i class="icon-info-sign icon-white"></i> <span>Not Subscribed</span>', array(
            'class'     => 'btn btn-info pull-right',
            'data-rid'  => $user->getID(),
            'id'        => 'subscribe_button'
        ));
    }

    echo $Button->render().'</div>';

    /*$Head=_('Follow Me');
    $Me=session_get_user();
	$SubMsg=($Me->getSubscriptionStatus($user->getID(),1))?'- Unsubscribe':'+ Subscribe';
	$Head=array($Head,util_make_link('javascript:void(0)',$SubMsg,array('id'=>'subscribe_button','data-rid'=>$user->getID()),true));
    echo $HTML->widget_list($Head,array());*/
}

$Projects=$user->getGroups(true);
if ($Projects){
	$List=array();
	foreach($Projects as $i)
		$List[]=util_make_link_g($i->getUnixName(),$i->getID(),$i->getPublicName());

	echo $HTML->widget_list(_('Projects'),$List);
}

echo $HTML->widget(_('User Rating'),'<style>table td{padding:5px}div.jqplot-table-legend-swatch-outline{border:0}</style><div id="rating" style="width:190px"></div>');
$Layout->endcol();

if ($OnOwnProfile){
    echo '<div class="modal fade hide" id="modal_skills">
    <div class="modal-header">
    <a class="close" data-dismiss="modal">×</a>
    <h3>Modal header</h3>
    </div>
    <div class="modal-body">
    </div>
    <div class="modal-footer">
    <a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal">OK</a>
    </div>
    </div>';
}

$HTML->footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

db_display_queries();
?>
