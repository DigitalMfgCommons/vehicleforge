<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:58 AM 11/4/11
 */

class ProjectActivityOld{
	private $DateFormat='%b %e, %Y at %I:%M%P';

	/**
	 * formatDate($Date)
	 * Formats an integer date into a date string
	 *
	 * @param int $Date
	 * @return string
	 */
	private function formatDate($Date){
		return strftime($this->DateFormat,$Date);
	}

	/**
	 * getActivity($GroupID, $StartTime, $EndTime, $Limit, $Flags)
	 * Gets activity from single or multiple groups based on the arguments
	 *
	 * @param int|array $GroupID
	 * @param int $StartTime
	 * @param int $EndTime
	 * @param int $Limit
	 * @param array $Flags
	 * @return array
	 */
	function getActivity($GroupID,$StartTime=null,$EndTime=null,$Limit=null,$Flags=array('forumpost','trackeropen','trackerclose','news','commit','frsrelease')){
		$Results=array();
		if (!is_array($GroupID))
			$GroupID=array($GroupID);

		$Query='SELECT * FROM activity_vw WHERE';
		if ($StartTime!=null && $EndTime!=null)
			$Query.=' activity_date BETWEEN $1 AND $2 AND';

		$Query.=' (';
		for($i=0;$i<sizeof($GroupID);$i++){
			$Group=group_get_object($GroupID[$i]);

			if ($Group && is_object($Group)){
				if($i!=0)$Query.=' OR ';
				$Query.='group_id='.$GroupID[$i];
			}
		}

		if ($StartTime!=null && $EndTime!=null)
			$Query.=') AND section = ANY ($3) ORDER BY activity_date DESC';
		else
			$Query.=') AND section = ANY ($1) ORDER BY activity_date DESC';
		

		if ($Limit)$Query.=' LIMIT '.$Limit;

		//echo $Query;
		if ($StartTime!=null && $EndTime!=null)
			$Query=db_query_params($Query,array($StartTime,$EndTime,db_string_array_to_any_clause ($Flags)));
		else
			$Query=db_query_params($Query,array(db_string_array_to_any_clause ($Flags)));

		while ($res=db_fetch_array($Query)){
			$Results[]=$res;
		}

		return $Results;
	}

	/**
	 * userPrintActivity($GroupID, $StartTime, $EndTime, $Limit, $Flags)
	 * Prints the project activity that a user is a member of
	 *
	 * @param int|array $GroupID
	 * @param int $StartTime
	 * @param int $EndTime
	 * @param int $Limit
	 * @param array $Flags
	 * @return void
	 */
	function userPrintActivity($GroupID,$StartTime=null,$EndTime=null,$Limit=null,$Flags=array('forumpost','trackeropen','trackerclose','news','commit','frsrelease')){
		$Activity=$this->getActivity($GroupID,$StartTime,$EndTime,$Limit,$Flags);

		foreach($Activity as $i){
			$Group=group_get_object($i['group_id']);
			$Time=$this->formatDate($i['activity_date']);
			$Icon='';
			$Text='';
			switch ($i['section']){
				case 'forumpost':
					$Icon=html_image('/ic/forum20g.png',20,20,array('alt'=>'Forum','class'=>'icon'));
					$Text=util_make_link('/forum/forum.php?msg_id='.$i['subref_id'].'&amp;group_id='.$i['group_id'],$i['description']).' has been posted in the forums';
					break;

				case 'trackeropen':
					$Icon=html_image('/ic/tracker20g.png',20,20,array('alt'=>'Tracker','class'=>'icon'));
					$Text=util_make_link('/tracker/?func=detail&amp;aid='.$i['subref_id'].'&amp;group_id='.$i['group_id'].'&amp;atid='.$i['ref_id'],$i['description']).' has been added to the bug tracker';
					break;

				default:
					echo 'unfinished section: '.$i['section'];
			}

			echo '<div class="activity">'.$Icon.'<div class="time">'.$Time.' by '.util_make_link('/users/'.$i['user_name'],$i['realname']).'</div><div class="content">'.util_make_link('/projects/'.$Group->getUnixName(),$Group->getPublicName()).': '.$Text.'</div></div>';
		}
	}

	/**
	 * @param $GroupID
	 * @param null $StartTime
	 * @param null $EndTime
	 * @param null $Limit
	 * @param array $Flags
	 * @return void
	 */
	function projectPrintActivity($GroupID,$StartTime=null,$EndTime=null,$Limit=null,$Flags=array('forumpost','trackeropen','trackerclose','news','commit','frsrelease')){
		$Activity=$this->getActivity($GroupID,$StartTime,$EndTime,$Limit,$Flags);

		foreach($Activity as $i){
			$Group=group_get_object($i['group_id']);
			$Time=$this->formatDate($i['activity_date']);
			$Icon='';
			$Text='';
			switch ($i['section']){
				case 'forumpost':
					$Icon=html_image('/ic/forum20g.png',20,20,array('alt'=>'Forum','class'=>'icon'));
					$Text='A '.util_make_link('/forum/message.php?msg_id='.$i['subref_id'].'&amp;group_id='.$i['group_id'],'message').' has been posted by '.util_make_link('/users/'.$i['user_name'],$i['realname']);
					break;

				case 'trackeropen':
					$Icon=html_image('/ic/tracker20g.png',20,20,array('alt'=>'Tracker','class'=>'icon'));
					$Text=util_make_link('/tracker/?func=detail&amp;aid='.$i['subref_id'].'&amp;group_id='.$i['group_id'].'&amp;atid='.$i['ref_id'],$i['description']).' has been added to the bug tracker';
					break;

				case 'trackerclose':
					$Icon=html_image('/ic/tracker20g.png',20,20,array('alt'=>'Tracker','class'=>'icon'));
					$Text=util_make_link('/tracker/?func=detail&amp;aid='.$i['subref_id'].'&amp;group_id='.$i['group_id'].'&amp;atid='.$i['ref_id'],$i['description']).' has been closed';
					break;

				default:
					echo 'unfinished section '.$i['section'];
			}

			echo '<div class="activity">'.$Icon.'<div class="time">'.$Time.'</div><div class="content">'.$Text.'</div></div>';
		}
	}
}
?>