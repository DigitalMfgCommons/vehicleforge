function makeUserThumb(doc, resultList) {
    var user_img_src = '<img src="/image_store/profile_images/default.png" alt="">';
    var user_online = 'false';
    // jQuery.ajax({
    // 	    url: '/community/user_picture_decoder.php?user_id=' + doc.id, 
    // 		success: function(data) {
    // 		user_img_src = data.user_img_src;
    // 	    },
    // 		dataType: 'json',
    // 		async: false
    // 		});
    // jQuery.ajax({
    //         url: '/community/user_online.php?user_id=' + doc.id,
    //             success: function(data) {
    //             user_online = data.user_online;
    //         },
    //             dataType: 'json',
    //             async: false
    //             });

    jQuery.ajax({
	    url: '/community/user_picture_decoder.php?user_id=' + doc.id, 
		success: function(data) {
		user_img_src = data.user_img_src;
		$(html).find('#user_image').attr("src",user_img_src);
	    },
		dataType: 'json'
		});
    jQuery.ajax({
            url: '/community/user_online.php?user_id=' + doc.id,
                success: function(data) {
                user_online = data.user_online;
		if(user_online == 'true') {
		    $(html).find('#user_label').attr("class", "label label-success");
		    $(html).find('#user_label').html("Online");
		} else {
		    $(html).find('#user_label').attr("class", "label ");
		    $(html).find('#user_label').html("Offline");
		}
            },
                dataType: 'json'
                });
    var html = $(getUserThumbPlaceholder());
    // set attributes
    $(html).attr("data-uid", doc.id);
    $(html).find('#user_link').attr("data-uid", doc.id);
    $(html).find('#user_name').html(doc.realname);
    // $(html).find('#user_image').attr("src",user_img_src);
    
    // if(user_online == 'true') {
    // 	$(html).find('#user_label').attr("class", "label label-success");
    // 	$(html).find('#user_label').html("Online");
    // } else {
    // 	$(html).find('#user_label').attr("class", "label ");
    // 	$(html).find('#user_label').html("Offline");
    // }
    
    // append results
    $('#' + resultList).append(html);
    
    // add a listener
    $(html).on('click', showUserModal);

}	

function searchUsers(resultList, resultSummary) {
    query = getParameterByName("q");
    
    // execute ajax request
    $.ajax({
	    type: 'GET',
		url: "/solrSearch/searchUsers.php?q=" + query,
		dataType: "json",
		success: function(json) {
		// remove old results
		$('#' + resultList).children().remove();
		
		// add new results
		var response = json.response, doc;
		for (var i=0; i<response.numFound; i++) {
		    doc = response.docs[i];
		    makeUserThumb(doc, resultList);
		}

		// update results summary
		if (query == '') {
		    $('#' + resultSummary).html('<h4>Users</h4>' + response.numFound + ' results found.');
		} else {
		    $('#' + resultSummary).html('<h4>Users</h4>' + response.numFound + ' results found for search: "' + query + '".');
		}
	    }   
	});
}

function getUserThumbPlaceholder() {
    var thumb = '';
    thumb = thumb + '<li class="col4">';
    thumb = thumb + '<a id="user_link" href="#" class="thumbnail user_thumb" data-uid="-1">';
    thumb = thumb + '<img id="user_image" src="/image_store/profile_images/default.png" width="100" height="100" class="user_image" />';
    thumb = thumb + '<center><h4 id="user_name" class="user_name"></h4>';
    thumb = thumb + '<p><span id="user_label" class="label "></span></p></center>';
    thumb = thumb + '</a>';
    thumb = thumb + '</li>';
    return thumb;
}

function showUserModal(event) {
    var realname = "error, user not found";
    var image = "error, user not found";
    var link = "error, user not found";
    var about = "error, user not found";
    var skills = "";
    var uid = $(this).attr("data-uid");
    $.ajax({
	    url         : "/community/ajax/get_user_data.php?uid=" + uid,
		dataType    : "json",
		async: false,
		success: function(data) {
		realname = data.realname;
		image = data.image;
		link = data.link;
		about = data.about;
		for(var i = 0, l = data.skills.length, el; i < l; i ++){
		    el = data.skills[i];
		    skills += el[0]+"<br /><i>"+el[1].join(", ")+"</i><br />";
		}
	    }
	});
    $('#userModal').find("h3").text(realname);
    $('#userModal').find(".image").html(image);
    $('#userModal').find("#profile_link").attr("href", link);
    $('#userModal').find(".about_me").html(about);
    $('#userModal').find(".skills").html(skills);
    $('#userModal').modal();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if(results == null)
	return "";
    else
	return decodeURIComponent(results[1].replace(/\+/g, " "));
}
