function makeComponentThumb(doc, resultList) {
    var group_img_src = '<img src="http://placehold.it/100x100&text=GEForgeComponent" alt="">';
    jQuery.ajax({
	    url: '/project/project_picture_decoder.php?group_id=' + doc.group_id, 
		success: function(data) {
		group_img_src = data.group_img_src;
		$(html).find('.componentImage').html('<img src="' + group_img_src + '" width=100 height=100 alt="">');
	    },
		dataType: 'json'
		});
    var html = $(getComponentThumbPlaceholder());
    // update the generic thumbnail
    
    $(html).data('component_name', doc.component_name);
    $(html).data('component_id', doc.id);
    $(html).data('group_id', doc.group_id);
    $(html).data('group_name', doc.group_name);
		
    $(html).find('.component_name').html(doc.component_name);
    $(html).find('.group_name').html('<span style="font-weight: 200;">in </span>' + doc.group_name);
	  
    // append component thumbnail to results lists
    $('#' + resultList).append(html);
	  
    // add an action listener
    $(html).on('click', showComponentModal);
}	

function searchComponents(resultList, resultSummary) {
    query = getParameterByName("q");
    
    // execute ajax request
    $.ajax({
	    type: 'GET',
		url: "/solrSearch/searchComponents.php?q=" + query,
		dataType: "json",
		success: function(json) {
		// remove old results
		$('#' + resultList).children().remove();

		// add new results
		var response = json.response, doc;
		for (var i=0; i<response.numFound; i++) {
		    doc = response.docs[i];
		    makeComponentThumb(doc, resultList);
		}
				
		// update results summary
		if (query == '') {
		    $('#' + resultSummary).html('<h4>Components</h4>' + response.numFound + ' results found.');
		} else {
		    $('#' + resultSummary).html('<h4>Components</h4>' + response.numFound + ' results found for search: "' + query + '".');
		}
	    }   
	});
}

function getComponentThumbPlaceholder() {
    var thumb = '';
    thumb = thumb + '<li class="span2">';
    thumb = thumb + '    <div class="thumbnail">';
    thumb = thumb + '      <center>';
    thumb = thumb + '        <div class="componentImage">';
    thumb = thumb + '          <img src="http://placehold.it/100x100&text=GEForgeComponent" alt="">';
    thumb = thumb + '        </div>';
    thumb = thumb + '      </center>';
    thumb = thumb + '    <div class="caption" style="padding:5px;">';
    thumb = thumb + '      <h4 class="component_name" style="text-align: center;"></h4>';
    thumb = thumb + '      <h5 class="group_name" style="font-weight:500; text-align: center;"></h5>';
    thumb = thumb + '    </div>';
    thumb = thumb + '  </div>';
    thumb = thumb + '</li>';
    return thumb;
}

function showComponentModal(event) {
    // update modal
    var name = $(this).data('component_name');
    var id = $(this).data('component_id');
    var group_id = $(this).data('group_id');
    var group_name = $(this).data('group_name');
		
    $('#component_name').html(name);
    $('#component_desc').html('This is a model for '+$(this).data('component_name'));
    $('#component_url').attr('href', '/components/?group_id=' + group_id + '&cid=' + id);
    $('#group_name').html(group_name);
    $('#group_name').attr('href', '/components/?group_id=' + group_id);
    $("#componentModal").data('cid',id);

    // show modal
    $('#componentModal').modal();
}

function addComponentToProject() {
    $(this).closest('.dropdown-toggle').attr('disabled', 'disabled');
    $('#add_to_project_status').html('Adding...');
    
    $.post('/ajax/import_component_to_project.php',
	   {
	       cid: $('#modal').data('cid'),
		   to_gid: $(this).attr('group_id')
		   }
	   );

    $('#add_to_project_status').html('Added!');
    window.setTimeout( function() {
	    $(this).closest('.dropdown-toggle').removeAttr('disabled');
	    $('#add_to_project_status').html('Add to Project');
	}, 1000);
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(window.location.search);
    if(results == null)
	return "";
    else
	return decodeURIComponent(results[1].replace(/\+/g, " "));
}
