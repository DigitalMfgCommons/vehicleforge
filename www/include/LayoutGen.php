<?php
/**
 * Created By: Jason Kaczmarsky
 * Updated By: Ariadne Smith
 * Date: 2/26/2012
 */

class LayoutGen {
	private $ColsOpen=0, $RowsOpen=0, $Chain=true, $CurCols=0,
	$MaxCols=12, $PageOpen=false;

	function __construct($Chain=true){
		$this->Chain=$Chain;
	}

	function __destruct(){
		if ($this->ColsOpen!=0 || $this->RowsOpen!=0)
			echo '<strong>'.$this->RowsOpen.' endRow() missing, '.$this->ColsOpen.' endCol() missing</strong>';
	}

	function header($Text, $Weight=1){
		echo '<span style="color:red">REMOVE THIS ACCURANCE OF $Layout->header(), REPLACE WITH $HTML->heading()</span>';
		return $this;
	}

	function notification($Text){
		return '<span class="notification">'.$Text.'</span>';
	}
	
	/**
	 * @param int $Size
	 * @param bool $Page
	 * @param bool $startRow
	 * @param bool $Last
	 * @return LayoutGen|string
	 */
	function col($Size,$startPage=false,$startRow=false,$Last=false){

		// open a row
		if ($startRow || $this->CurCols==0) {
			$this->row($startPage);
		}
		
		// open a col
		$this->CurCols+=$Size;
		if ($Last) $this->CurCols=$this->MaxCols;
		if ($this->CurCols>$this->MaxCols) echo '<strong>More than '.$this->MaxCols.' columns specified in this row. Create a new row or reduce columns</strong>';

		echo '<div class="span'.$Size.'">';
		$this->ColsOpen++;
			
		// chain it
		if ($this->Chain){
			return $this;
		}
	}
	
	/**
	 * @param bool $endRow
	 * @param bool $endPage
	 * @return LayoutGen
	 */
	function endcol($endRow=false, $endPage=false){

		// close a col
		$this->ColsOpen--;
		echo '</div><!--end col-->';
		
		// close a row
		if ($this->CurCols==$this->MaxCols || $endRow){
			$this->endRow($endPage);
			$this->CurCols=0;
		}
		
		// chain it
		if ($this->Chain){
			return $this;
		}
	}
	
	/**
	 * @param $Page
	 * @return string
	 */
	private function row($startPage=false){
		// open a page
		if ($startPage==true) $this->page();

		// open a row
		$this->RowsOpen++;
		echo '<div class="row">';
		
		// chain it				
		if ($this->Chain) {
			return $this;
		}
	}

	private function endRow($endPage=false){
		// close the row
		echo '</div><!--end row-->';
		$this->RowsOpen--;
		
		// close the page
		if ($endPage) $this->endPage();
		
		// chain it
		if ($this->Chain){
			return $this;
		}
	}
	
	private function page() {
		// close existing open pages
		if ($this->PageOpen) $this->endPage();
		
		// open a page
		echo '<div class="page">';
		$this->PageOpen = true;
		
		// chain it
		if ($this->Chain){
			return $this;
		}
	}
	
	private function endPage() {
		$this->PageOpen = false;
		echo '</div><!--end page-->';
	}
}
?>