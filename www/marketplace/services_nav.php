<?php

/*
 *  services_nav.php
 *  Show a navigable tree of components
 *  Ariadne Smith (2/29/2012)
 *
 *  local variables:
 *      $group - Group object
 */


    echo '<a class="pull-right" href="#" data-toggle="modal" data-target="#addComponentModal"><i class="icon-plus"></i></a>';
    echo '<h3>Components</h3>';
    
        
    echo '<br/><a style="margin-left: 28%;" href="/graphs/components.php?group_id='.$group->getID().'" class="btn btn-small"><i class="icon-eye-open"></i> Graph</a></span>';
}


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN">

<html>
<head>

    <style type="text/css">
ul.nav-tree {
        margin-left: 15px;
    }

    ul.nav-tree li {
        list-style: none;   
        position: relative;
    }

    .tree-toggle {
        cursor: pointer;
        position: absolute;
        left: -15px;
        font-size: 0.8em;
    }
    </style>

    <title></title>
</head>

<body>
    <ul class="nav-tree">
        <li>
            <span class="tree-toggle">▶</span> <a href="">Physical Domains</a>

            <ul class="nav-tree hide">
                <li>
                    <span class="tree-toggle">▶</span> <a href="">Electrical</a>

                </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Hydraulic</a>
                 </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Mechanical</a>
                 </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Thermal</a>
                 </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Electromagnetic</a>
                 </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Aero</a>
                 </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Software</a>
                 </li>
            </ul>
       <li>
            <span class="tree-toggle">▶</span> <a href="">Functions</a>
            
            <ul class="nav-tree hide">
            	<li> <span class="tree-toggle">▶</span> <a href="">Rapid Prototyping</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">Statistics and Probability</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">Control Systems</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">Data Acquisition</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">Signal Processing</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">Test and Measurement</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">CAD Geometry</a>
            	</li>
            	<li> <span class="tree-toggle">▶</span> <a href="">Analytics</a>
            	</li>
            
            	<li> <span class="tree-toggle">▶</span> <a href="">META Models</a>
            	<ul class="nav-tree hide">
            		<li><a href="">Nonlinear PDE</a></li>
            		<li><a href="">Linear Differential Equations</a></li>
            		<li><a href="">Qualitative Relational Models</a></li>
            		</ul>
            	</li>
            <li> <span class="tree-toggle">▶</span> <a href="">CFD</a></li>
            <li> <span class="tree-toggle">▶</span> <a href="">FEA</a></li>
            <li> <span class="tree-toggle">▶</span> <a href="">iFAB Models</a>
            <ul class="nav-tree hide">
            		<li><a href="">Machining</a></li>
            		<li><a href="">Assembly</a></li>
            		<li><a href="">Process</a></li>
            		</ul>
            </li>
            <li> <span class="tree-toggle">▶</span> <a href="">Visualization</a>
            	</li>
            
  
            
            </ul>
      
        </li>
      
        </ul>

        <li><a href="?group_id=70&amp;cid=59">crankshaftbaby</a></li>
    </ul><script type="text/javascript">
$('.tree-toggle').bind('click', function(event) {       
        var li = $(this).closest('li');
        var ul = $(li).children('ul');
        
        if ($(ul).hasClass('hide')){
            $(li).children('span').html('&#9660;');
            $(ul).removeClass('hide');
        } else {
            $(li).children('span').html('&#9654;');
            $(ul).addClass('hide');
        }
    });
    // change caret direction
    </script>
</body>
</html>
