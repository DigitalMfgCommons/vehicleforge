<?php
require_once('../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once($gfwww.'include/model.php');
require_once($gfwww . 'include/TableGen.php');
//$Model=new model;
$Table=new TableGen;

$group_id = getIntFromRequest("group_id");

session_require_perm ('project_read', $group_id) ;
site_project_header(array('group'=>$group_id));

$Subnav=array();
if (forge_check_perm ('project_admin', $group_id))
	$Subnav[]=array(_('Model Admin'),util_make_uri('/project/admin/model_assoc.php?group_id='.$group_id));

$HTML->tertiary_menu($Subnav);

$HTML->boxTop();



$HTML->heading(_('Associated Models'),2);
$res=db_query_params("SELECT * FROM model_project_associations WHERE group_id=$1",array($group_id));

if (db_numrows($res)){
	$Table->top(array(array('Model Name',250),array('Date Shared',150)));

	while ($ModelRow=db_fetch_array($res)){
		$res2=db_query_params("SELECT * FROM models WHERE model_id=$1",array($ModelRow['model_id']));
		$ModelData=db_fetch_array($res2);
		$ModelDef=json_decode($ModelData['model_def']);
		$Table->col(util_make_link('models/model.php?group_id='.$group_id.'&mid='.$ModelData['model_id'],$ModelDef->name))->col(date('M j, Y',$ModelDef->dateModified/1000));
	}
	$Table->bottom();
}else{
	echo '<strong>No models have been associated with this project yet</strong>';
}
$HTML->boxBottom();

site_project_footer(array());
?>