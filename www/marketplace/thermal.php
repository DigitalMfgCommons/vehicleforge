<?php
require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';
require_once($gfwww . 'marketplace/model_utils.php');
require_once $gfwww.'include/model.php';
require_once $gfcommon.'include/ModelGroup.class.php';

// Include javascript for rating mechanism
use_javascript('raty/jquery.raty.min.js');
use_javascript('raty.js');

// Include css/less for marketplace //TODO: include this in main CSS
$HTML->addStylesheet('/themes/gforge/css/marketplace.css');
  

// Get the list of models to display
if (isset($_REQUEST['group_id'])){
    //Get an array of all model IDs associated with a project

}else{
    //Get array of all model projects
    $ModelClass=new model;
    $ModelGroupList = $ModelClass->getall();
}

// Create the site header
if (isset($GroupID)){
    site_project_header(array('title'=>'Marketplace','group'=>$GroupID));
}else{
    site_header(array('title'=>'Marketplace'));
    

    
    ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
    <title></title>
</head>

<body>
    <ul id="tab" class="nav nav-tabs" style="margin:0;">
        <li><a href="#components" data-toggle="tab"><strong>Components</strong></a></li><!-- <li><a href="#search" data-toggle="tab"><strong>Search</strong></a></li> -->

        <li class="active"><a href="#services" data-toggle="tab"><strong>Services</strong></a></li>
    </ul><?
        
        
        
        $Layout->col(12,true,true,true);
        $HTML->heading("<h2 style=color:#1589FF;>Marketplace</h2>", 2); 
        $HTML->heading("<h4>Find, run, integrate, and add services and components.</h4>"); /* $Layout-&gt;endcol() */; } /* $Layout-&gt;col(12,true); */ 
        
        $Layout->endcol();
        
        
        ?>
        
        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;

    <div id="myTabContent" class="tab-content">
        <div class="tab-pane" id="components">
            <?php // Create Category List
                                       




            if (!isset($GroupID)) {
	            $HTML->heading("Popular categories", 3);
	            makeCategoryView("Powertrain", "Simulations of engines, transmissions, radiators, drive axles, and other powertrain components.", "/mock_images/engine-drive-line.jpg", ".?group_id=24");
	            makeCategoryView("Suspension", "Simulations that aid in the determination of vehicle ride dynamics and suspension selection.", "/mock_images/generic-suspension.jpg", ".?group_id=24");
	            makeCategoryView("Electrical", "Simulations of vehicle electrical systems.", "/mock_images/electrical-system.jpg", ".?group_id=24");    
            }
                
	        $Layout->col(6,false,true,true);
            
                $HTML->heading("Browse Models and Services", 3);
            echo '<form class="span6" action="'.util_make_uri('/search/').'" method="get">
                <div class="float-left">
                    <input style="width:200px;" type="text" name="q" data-provide="typeahead" data-items="4" data-source="[&quot;Electrical&quot;,&quot;Hydraulic&quot;,&quot;Mechanical&quot;,&quot;Thermal&quot;,&quot;Electromagnetic&quot;,&quot;Aero&quot;,&quot;Software&quot;]"/>
                    
                </div>
                </form>';
                
              
              $Layout->endcol();
              
           
              

            echo "<ul class=\"modelList\">
            \n"; foreach ($ModelGroupList as $ModelGroupID) { makeModelView($ModelGroupID); } echo "\t\t\n";
            
            
            
            ?>
            
        
        
        
       <!--  </div> -->
    </div>

    <div class="tab-pane fade active in" id="services">
        
       
       
       
       
       
       
       
     <!--Importing Jeff's Component Nav Tree-->  
       
       
       <?php>
       $Layout->col(3);
echo '<div class="well">';
include 'component_nav.php';


/* $Layout->col(9); */
/* echo '<div class="well">'; */
include 'show.php';
/* echo '</div>'; */
/* $Layout->endcol(); */
        
         
         ?>
         
       <h3>Services</h3>  
<ul class="nav-tree">
        <li>
            <span class="tree-toggle">▶</span> <a href="">Physical Domains</a>

            <ul class="nav-tree">
                <li>
                    <span class="tree-toggle"></span> <a href="">Electrical</a>

                </li>
                 <li>
                    <span class="tree-toggle"></span> <a href="">Hydraulic</a>
                 </li>
                 <li>
                    <span class="tree-toggle"></span> <a href="">Mechanical</a>
                 </li>
                 <li>
                    <span class="tree-toggle">▶</span> <a href="">Thermal</a>
                 </li>
                 <li>
                    <span class="tree-toggle"></span> <a href="">Electromagnetic</a>
                 </li>
                 <li>
                    <span class="tree-toggle"></span> <a href="">Aero</a>
                 </li>
                 <li>
                    <span class="tree-toggle"></span> <a href="">Software</a>
                 </li>
            </ul>
       <li>
            <span class="tree-toggle">▶</span> <a href="">Functions</a>
            
            <ul class="nav-tree hide">
            	<li> <span class="tree-toggle"></span> <a href="">Rapid Prototyping</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">Statistics and Probability</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">Control Systems</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">Data Acquisition</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">Signal Processing</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">Test and Measurement</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">CAD Geometry</a>
            	</li>
            	<li> <span class="tree-toggle"></span> <a href="">Analytics</a>
            	</li>
            
            	<li> <span class="tree-toggle">▶</span> <a href="">META Models</a>
            	<ul class="nav-tree hide">
            		<li><a href="">Nonlinear PDE</a></li>
            		<li><a href="">Linear Differential Equations</a></li>
            		<li><a href="">Qualitative Relational Models</a></li>
            		</ul>
            	</li>
            <li> <span class="tree-toggle"></span> <a href="">CFD</a></li>
            <li> <span class="tree-toggle"></span> <a href="">FEA</a></li>
            <li> <span class="tree-toggle">▶</span> <a href="">iFAB Models</a>
            <ul class="nav-tree hide">
            		<li><a href="">Machining</a></li>
            		<li><a href="">Assembly</a></li>
            		<li><a href="">Process</a></li>
            		</ul>
            </li>
            <li> <span class="tree-toggle"></span> <a href="">Visualization</a>
            	</li>
            
  
            
            </ul>
      
        </li>
      
        </ul>  
        
        </div>
        
        <? $Layout->endcol(); ?>
        
         <div class="span9">    

    <h2 style="border-bottom: 1px solid #AAA;">Thermal</h2><br>

    <div class="active" id="files">
        <h3>Heat Transfer</h3>

        <div style="margin: 0 30px;">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>name</th>

                        <th>inputs</th>

                        <th>outputs</th>

                        <th>subscribe</th>
                    </tr>
                </thead>

                <tbody>
                    <tr><td><i class="icon-play-circle"></i>&nbsp;<a href="#">Heat Transfer in Solids</a></td><td>T</td><td>Q</td><td><input type="checkbox" id="optionsCheckbox" value="option1"></td>
                    </tr>
                    
                    <tr><td><i class="icon-play-circle"></i>&nbsp;<a href="#">Heat Transfer in Fluids</a></td><td>T</td><td>Q</td><td><input type="checkbox" id="optionsCheckbox" value="option1"></td></tr>
                    
                    <tr><td><i class="icon-play-circle"></i>&nbsp;<a href="#">Heat Transfer in Porous Media</a></td><td>T</td><td>Q</td><td><input type="checkbox" id="optionsCheckbox" value="option1"></td></tr>
                    
                    <tr><td><i class="icon-play-circle"></i>&nbsp;<a href="#">Laminar Conjugate Heat Transfer</a></td><td>T</td><td>Q</td><td><input type="checkbox" id="optionsCheckbox" value="option1"></td></tr>
                    
                    <tr><td><i class="icon-play-circle"></i>&nbsp;<a href="#">Turbulent Conjugate Heat Transfer</a></td><td>T</td><td>Q</td><td><input type="checkbox" id="optionsCheckbox" value="option1"></td></tr>                    </tbody>
            </table>
        </div>
    </div>

    <div id="services" style="border-top: 1px solid #000; padding-top: 8px;">
        <a href="/register/interface.php?cem_id=&lt;?php echo $CEM-&gt;getID(); ?&gt;&amp;group_id=&lt;?php echo $group_id?&gt;" class="btn btn-small pull-right" style="margin-left:30px;">Subscribed to Selected Services</a>

        
        <ul class="unstyled" style="margin-left:0px;">
                    </ul>
    </div>

        </div>
    
    </div>
        
        
        
        
        
        
         
       
         
         <script>
	$('.tree-toggle').bind('click', function(event) {		
		var li = $(this).closest('li');
		var ul = $(li).children('ul');
		
		if ($(ul).hasClass('hide')){
			$(li).children('span').html('&#9660;');
			$(ul).removeClass('hide');
		} else {
			$(li).children('span').html('&#9654;');
			$(ul).addClass('hide');
		}
	});
	// change caret direction
</script>

<style>
	ul.nav-tree {
		margin-left: 15px;
	}

	ul.nav-tree li {
		list-style: none;	
		position: relative;
	}
	
	.tree-toggle {
		cursor: pointer;
		position: absolute;
		left: -15px;
		font-size: 0.8em;
	}
</style>
         
         
         
         
         
         
         
         </div>
         </div>
         
         
        
        <?php



        $Layout->endpage();

        // Create site footer
        site_footer(array());

        /*
         * Functions used in /marketplace/index.php
         */
        function makeModelView($ModelGroupID) {
            if (forge_check_perm('project_read',$ModelGroupID)){
                $ModelGroup=new ModelGroup($ModelGroupID);

                global $Layout;

                //Get the model ID linked to the project
                $model=$ModelGroup->getModel();

                //Get the model definition
                $model=getModel($model);

				$Layout->col(4);
                echo '<li class="box modelThumb">';

                // Start: first row
                echo '<div style="width:100%; height:110px;">';

                // Model image
                echo '<a class="noStyle" href="/projects/'.$ModelGroup->getUnixName().'"><img class="modelImage" src="http://placehold.it/100&text=img"/></a>';

                // Model name
                echo '<div class="modelText">';
                    echo '<h3 class="modelTitle"><a class="noStyle" href="/projects/'.$ModelGroup->getUnixName().'">'. $model['name'] . '</a></h3>';


                    // Model description
                    echo '<div class="modelDesc">' . $model['desc'] . '</div>';

                    // Model author;
                    echo '<div class="modelDesc">by: ' . $model['author'] . '</div>';
                echo '</div>';

                // End: first row
                // Begin: second row
                echo '</div>';
                echo '<div style="width:100%;">';

                // Model Rating, Runs, Reviews
                echo '<div class="raty" style="float:left;" data-value="3.5"></div>';
                echo '<div class="modelText">';
                    echo '<div class="stat" style="float:right">';
                        echo '<div class="number">' . rand(0,30) . '</div>';
                        echo '<div class="quantity">runs</div>';
                    echo '</div>'; // end of stat

                    echo '<div class="stat" style="float:right">';
                        echo '<div class="number">' . rand(0,30) . '</div>';
                        echo '<div class="quantity">users</div>';
                    echo '</div>'; // end of stats

                    echo '<div class="stat" style="float:right">';
                        echo '<div class="number">0</div>';
                        echo '<div class="quantity">projects</div>';
                    echo '</div>'; // end of stat
                echo '</div>'; // end of modelText
                echo '</div>'; // end of second row;

                echo "</li>";
                $Layout->endcol();
            }
        } 

        function makeCategoryView($cat, $desc, $image, $url) {
            global $HTML, $Layout;
            
            $Layout->col(4);
            echo '<div class="box category" style="background-image: url(' . $image . ')">';
            echo '<a href="' . $url .'" style="text-decoration:none; color:inherit;"><div class="title">'.$cat.'</div></a>';        
            echo '<a href="' . $url .'" style="text-decoration:none; color:inherit;"><div class="desc">'.$desc.'</div></a>';        
            echo "</div>";
            $Layout->endcol();
        } 

        ?>
   
</body>
</html>
