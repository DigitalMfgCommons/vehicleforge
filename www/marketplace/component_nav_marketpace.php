<?php

/*
 *	component_nav.php
 *	Show a navigable tree of components
 *	Jeff Mekler (2/11/2012)
 *
 *	local variables:
 *		$group - Group object
 */

function getComponents($components) {
	$out = array();
	foreach($components as $c) {
		$out[]= $c;
		$out = array_merge($out, getComponents($c->getSubComponents()));
	}
	return $out;
}

function makeComponentNav($group) {
	$CEMs = $group->getComponents();
	
	// iterate through all CEMs
	if (count($CEMs) > 0) {
		echo '<ul class="nav-tree">';
		foreach ($CEMs as $cem) {
			renderSubComponents($group, $cem);
		}
		echo '</ul>';
	}
}

// recursive method to generate subcomponent navigation
function renderSubComponents($group, $cem) {
	
	$id = $cem->getID();
	$name = $cem->getName();

	$subs = $cem->getSubComponents();
	if (count($subs) == 0) {
		echo '<li class="component" cid='.$id.'>';
			echo '<a href="?group_id='.$group->getID().'&cid='.$id.'">'.$name.'</a>';
		echo '</li>';
				
	} else {
	
		echo '<li class="component hasSubList" cid=' . $id . '>';
			echo '<span class="tree-toggle"> &#9654; </span>';
			echo '<a href="?group_id='.$group->getID().'&cid='.$id.'">'.$name.'</a>';

			echo '<ul class="nav-tree hide">';
			foreach ($subs as $sub) {
				renderSubComponents($group, $sub);
			}
			echo '</ul>';
		echo '</li>';
	}
	

}

/* 	$selected_CEM = $CEM->getID(); */
	$components = getComponents($group->getComponents());
	$names = function($component) {return $component->getName();};
	$ids = function($component) {return $component->getID();};
	
?>

<div class="well" id="component-nav">
	<form id="component-search">
		<input type="text" class="search-query" placeholder="Find Components" data-provide="typeahead" data-source='<?= json_encode(array_map($names,$components)) ?>'/>
	</form>
	
	<?php makeComponentNav($group); ?>
	
	<a class="btn btn-small center" href="/graphs/components.php?group_id=<?=$group->getID()?>"><i class="icon-eye-open"></i> Graph View</a>
	<a class="btn btn-small center" href="#" data-toggle="modal" data-target="#addComponentModal"><i class="icon-plus"></i> Component</a>
<!-- 	<a class="btn btn-small center" href="#" id="edit-component"><i class="icon-move"></i> edit</a> -->
</div>

<script>
	$(document).ready( function() {
		
		$('.component[cid="<?=$CEM->getID()?>"]').addClass('active');
		
		var listsToOpen = $('.component.active').parentsUntil('#component-nav', '.component');
		listsToOpen.each( function() {
			$(this).children('ul').removeClass('hide');
			$(this).children('span').html('&#9660;');
		});
		
	});

	$('.tree-toggle').bind('click', function(event) {		
		var li = $(this).closest('li');
		var ul = $(li).children('ul');
		
		if ($(ul).hasClass('hide')){
			$(li).children('span').html('&#9660;');
			$(ul).removeClass('hide');
		} else {
			$(li).children('span').html('&#9654;');
			$(ul).addClass('hide');
		}
	});
	
	$('#component-search').bind('submit', function() {
		// implement search
		return false;
	});
	
</script>

<style>
	
	#component-search > input {
		width: 85%;
	}
	
	ul.nav-tree {
		margin-left: 15px;
	}

	.component {
		position: relative;
		line-height:1.6em;
	}

	.component.hasSubList {
		list-style: none;	
	}
	
	.component.active > a{
		font-weight: bold;
	}
	
	.tree-toggle {
		cursor: pointer;
		position: absolute;
		left: -15px;
		font-size: 0.8em;
	}
	
	.center {
		margin-left: 20%;
		width: 50%;
		margin-bottom: 10px;
	}
</style>