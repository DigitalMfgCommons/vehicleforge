<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

use_stylesheet('/themes/gforge/css/widget.css');
use_stylesheet('../new-marketplace_style.css');
//use_javascript('bootstrap/transition.js');
use_javascript('bootstrap/dropdown.js');
use_javascript('bootstrap/button.js');
use_javascript('bootstrap/carousel.js');
use_javascript('bootstrap/modal.js');
use_javascript('bootstrap/tabs.js');
site_header(array('title'=>'Component Name | Vehicle Forge Marketplace'));

?>
<style type="text/css">
    td p {
        margin-bottom: 0;
    }
</style>
<script>
    $(function(){
        $('#preview-modal').modal({show: false});
        $('.carousel').carousel();
        $('.dropdown-toggle').dropdown();
        $('.reply').click(function(e){
            e.preventDefault();
            $('#reply-input').remove();
            $('.reply').show();
            $(this).hide();
            $(this).parents('.comment').append('<div id="reply-input"><textarea class="span6" style="margin-bottom:2px;" id="reply-content"></textarea><button class="btn pull-right btn-info post-reply" style="margin:5px">Post</button><button class="btn pull-right cancel-reply" style="margin:5px">Cancel</button></div>');
        });
        $(".comment").on("click", ".cancel-reply", function(){
            $('.reply').show();
            $('#reply-input').remove();
        });
        $(".comment").on("click", ".post-reply", function(){
            /* TODO Add ajax display of the post */
            $('.reply').show();
            <?php
                if(user_getid()){
                    $me = user_get_object(user_getid());
                    $meURL = $me->getURL();
                    $meIMG = $me->getImage();
                    $meName = $me->getRealName();
                } else{
                    $meURL = 'javascript:void(0)';
                    $meIMG = 'image_store/profile_images/default.png';
                    $meName = 'Guest';
                }
            ?>
            var newComment   =  '<div class="sub-comment">';
            newComment      +=      '<a href="<?php echo $meURL;?>" class="pull-left comment-image"><img src="<?php echo  $meIMG;?>" alt="" height="50" width="50"/></a>';
            newComment      +=      '<div class="pull-left comment-content">';
            newComment      +=          '<p class="comment-header">';
            newComment      +=          '<a href="<?php echo $meURL;?>" class="author"><?php echo  $meName;?></a>';
            newComment      +=          ' <span class="date"><?php echo date('M d, Y g:ia'); ?></span>';
            newComment      +=          '</p>';
            newComment      +=          '<p class="message">'+ $('#reply-content').val() +'</p>';
            newComment      +=      '</div>';
            newComment      +=  '</div>';
            $(this).parents('.comment').append(newComment);
            $('#reply-input').remove();

        });

        $('.preview-thumbs a').not('#preview-more').hover(function(){
            $('.preview-thumbs a').removeClass('active');
            $(this).addClass('active');
            $('#preview-large').attr('src', $('img',this).attr('src'));
            $('#hint').addClass('active');
        }, function(){
            $('#hint').removeClass('active');
        });

        $('.preview-main').hover(function(){
            $('#hint').addClass('active');
        }, function(){
            $('#hint').removeClass('active');
        });

        $('.preview-main, .preview-thumbs img, #preview-more').click(function(){
            $('#preview-modal').modal('show');
        });

    });
</script>
<div class="modal hide" id="preview-modal">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h3>Powertrain Simulator</h3>
    </div>
    <div class="modal-body">
        <div id="preview-carousel" class="carousel slide">
            <!-- Carousel items -->
            <div class="carousel-inner">
                <div class="active item"><img src="http://placehold.it/250x250" alt="" /></div>
                <div class="item"><img src="http://placehold.it/150x150" alt="" /></div>
                <div class="item"><img src="http://placehold.it/350x350" alt="" /></div>
            </div>
            <!-- Carousel nav -->
            <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
        </div>
    </div>
</div>
    <div class="container">
        <a href="../new-components.php" class="btn" style="margin-bottom:5px;"><i class="icon-arrow-left"></i> Back to Marketplace</a>
        <div class="page">
            <div class="row">
                <div class="span3">
                    <div class="preview-main thumbnail">
                        <img src="http://placehold.it/250x250" id="preview-large" alt="" />
                        <span id="hint"><i class="icon-resize-full icon-white"></i> Click to expand</span>
                    </div>
                    <div class="preview-thumbs">
                        <a href="#" class="thumbnail active"><img src="http://placehold.it/250x250" alt="" /></a>
                        <a href="#" class="thumbnail"><img src="http://placehold.it/150x150" alt="" /></a>
                        <a href="#" class="thumbnail"><img src="http://placehold.it/50x50" alt="" /></a>
                        <a href="#" class="btn btn-mini pull-right" id="preview-more" style="margin-top:20px;">More</a>
                    </div>

                </div> <!-- end col-->
                <div class="span6">
                    <div id="component-header">
                        <h3>Powertrain Simulator</h3>
                        <p>by <a href="">Joe Smith</a>, January 4, 2012</p>
                    </div>
                    <div class="tabbable">
                        <ul class="nav nav-tabs" style="margin-bottom:5px">
                            <li class="active"><a href="#details" data-toggle="tab">Details</a></li>
                            <li><a href="#files" data-toggle="tab">Files</a></li>
                            <li><a href="#services" data-toggle="tab">Services</a></li>
                            <li><a href="#subcomponents" data-toggle="tab">Sub-components</a></li>
                            <li><a href="#projects" data-toggle="tab">Projects</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="details">
                                <table>
                                    <tr>
                                        <td width="100px"><h5>Last Updated</h5></td>
                                        <td><a href="#">Le Zhao</a>, June 8, 2012</td>
                                    </tr>
                                    <tr>
                                        <td><h5>Domain Area</h5></td>
                                        <td><a href="#">Automotive</a></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Category</h5></td>
                                        <td><a href="#">Simulation</a></td>
                                    </tr>
                                    <tr>
                                        <td><h5>Function</h5></td>
                                        <td><a href="#">Rapid Prototyping</a></td>
                                    </tr>
                                </table>
                                <p>
                                <h5>Description</h5>
                                This is a model for Powertrain Simulator.  It is very sophisticated, demonstrating the full capabilities of a power train.
                                </p>
                                <p>
                                <h5>Tags</h5>
                                <a href="">3D Model</a>, <a href="">simulator</a>, <a href="">powertrain</a>
                                </p>
                                <a class="btn-small btn pull-right">Edit Details</a>
                            </div>
                            <div class="tab-pane" id="files">
                                    <table class="table table-striped" style="margin-bottom:0">
                                        <thead style="font-size:11px">
                                            <tr>
                                                <th>Name</th>
                                                <th>Description</th>
                                                <th>Last Updated</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="list fileName">
                                                    
                                                    <a href="javascript:void(0)" class="file">Concept</a>
                                                </td>
                                                <td>
                                                    Describes the powertrain simulator
                                                </td>

                                                <td>
                                                    <p><small>Jan 4, 2012</small></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="list fileName">
                                                    
                                                    <a href="javascript:void(0)" class="file">Guide</a>
                                                </td>
                                                <td>
                                                    Guide to use the simulator
                                                </td>
                                                <td>
                                                    <p><small>Jan 4, 2012</small></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="list fileName">
                                                    
                                                    <a href="javascript:void(0)" class="file">3D Model</a>
                                                </td>
                                                <td>
                                                    Model you can use in the simulator
                                                </td>
                                                <td>
                                                    <p><small>Jan 4, 2012</small></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="list fileName">
                                                    
                                                    <a href="javascript:void(0)" class="file">MathLab Script</a>
                                                </td>
                                                <td>
                                                    Simulation script
                                                </td>
                                                <td>
                                                    <p><small>Jan 4, 2012</small></p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <a class="btn-small btn btn-success pull-right"><i class="icon-folder-open icon-white pull-left"></i> Upload Files</a>
                            </div>
                            <div class="tab-pane" id="services">
                                <table class="table table-striped" style="margin-bottom:0">
                                    <thead style="font-size:11px">
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Last Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="list fileName">
                                                
                                                <a href="javascript:void(0)" class="file">Torque</a>
                                            </td>
                                            <td>
                                                Simulates the torque on the powertrain
                                            </td>
                                            <td>
                                                <p><small>Jan 4, 2012</small></p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <a class="btn-small btn btn-success pull-right"><i class="icon-plus icon-white pull-left"></i> Create Service</a>
                            </div>
                            <div class="tab-pane" id="subcomponents">
                                <table class="table table-striped" style="margin-bottom:0">
                                    <thead style="font-size:11px">
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Last Updated</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="list fileName">
                                            
                                            <a href="javascript:void(0)" class="file">Powertrain Model</a>
                                        </td>
                                        <td>
                                            Model of the powertrain
                                        </td>

                                        <td>
                                            <p><small>Jan 4, 2012</small></p>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <a class="btn-small btn btn-success pull-right"><i class="icon-plus icon-white pull-left"></i> Create Subcomponents</a>
                            </div>
                            <div class="tab-pane" id="projects">
                               <table class="table table-striped" style="margin-bottom:0">
                                    <thead style="font-size:11px">
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="list fileName">
                                            <a href="javascript:void(0)" class="file">Aardvark8</a>
                                        </td>
                                        <td>
                                            This is a demo project to show the capabilities of the system.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="list fileName">
                                            <a href="javascript:void(0)" class="file">GE Vehicleforge Development</a>
                                        </td>
                                        <td>
                                            This project will track the work being done by the development group to build this site. Watch this site to see development efforts, tasks, and progress in competing the work.
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div id="component-details">



                    </div>
                    <hr />
                    <div id="component-comments">
                        <p>Comment on this component:</p>
                        <textarea class="span6" id="appendedInputButton" style="margin-bottom:2px;"></textarea>
                        <button class="btn pull-right span2 btn-info" style="margin-bottom:5px">Post</button>
                        <div id="activity">
                            <div class="comment">
                                <a href="/users/ben" class="pull-left comment-image"><img src="image_store/profile_images/ben/profile_image.jpg" width="50" height="50"></a>
                                <div class="pull-left comment-content">
                                    <a class="btn-mini btn pull-right reply">Reply</a>
                                    <p class="comment-header">
                                        <a href="/users/ben" class="author">Benjamin Beckmann</a>
                                        <span class="date">Apr 23, 2012 at 2:39pm</span>
                                    </p>
                                    <p class="message">This is pretty epic! I want to use this in all my projects! Are there any further developments coming up?</p>
                                </div>
                            </div>
                            <div class="comment">
                                <a class="pull-left comment-image" href="/users/jasonk"><img src="image_store/profile_images/jasonk/profile_image-1337020855.jpg" width="50" height="50"></a>
                                <div class="pull-left comment-content">
                                    <a class="btn-mini btn pull-right reply">Reply</a>
                                    <p class="comment-header">
                                        <a href="/users/jasonk" class="author">Jason Kaczmarsky</a>
                                        <span class="date">Mar 18, 2012 at 2:39pm</span>
                                    </p>
                                    <p class="message">Could you upload the source code?</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end col-->
                <div class="span3" id="highlights">
                    <button class="btn-success btn btn-large expand"><i class="icon-download-alt icon-white pull-left"></i> Download</button>
                    <div class="btn-group">
                        <button class="btn btn-large btn-primary dropdown-toggle expand" data-toggle="dropdown">Copy to Project <span class="caret pull-right"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Project 1</a></li>
                            <li><a href="#">Project 2</a></li>
                            <li><a href="#">Project 3</a></li>
                        </ul>
                    </div>
                    <button class="btn btn-large expand"><i class="icon-star pull-left"></i> Subscribe</button>
                    <div class="well" id="component-overview">
                        <h6>Component Overview</h6>
                        <ul>
                            <li>
                                <i class="icon-file"></i> <span>4 files</span>
                            </li>
                            <li>
                                <i class="icon-download"></i> <span>5 downloads</span>
                            </li>
                            <li>
                                <i class="icon-star"></i> <span>333 subscribers</span>
                            </li>
                            <li>
                                <i class="icon-eye-open"></i> <span>1,400 views</span>
                            </li>
                            <li>
                                <i class="icon-user"></i> <span>2 comments</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- end row-->
    </div>

<?php
$Layout->endcol();
site_community_footer();
?>
