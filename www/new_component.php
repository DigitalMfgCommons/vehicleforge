<?php

require_once '../env.inc.php';
require_once $gfcommon . 'include/pre.php';
require_once $gfcommon.'include/CEM.class.php';


/*
$group_id=getIntFromRequest('group_id');
$group = group_get_object($group_id);
$CemID=isset($_GET['cid'])?getIntFromRequest('cid'):null;

if (!$group || !is_object($group)) {
    exit_no_group();
}
*/

if (isset($_POST['cname'])) {
	
	$CEM = new CEM;
	$CName = getStringFromRequest('cname');
	echo $CName;
	$group_id = isset($_POST['group_id']) ? getIntFromRequest('group_id') : null;
	echo (string)$group_id;
	$CemID = isset($_POST['cid']) ? getIntFromRequest('cid') : null;
	echo (string)$CemID;
	
	if ($CEM->create($group_id, $CName, $CemID)) {
		header( 'Location: /components/?group_id='.$group_id.'&cid='.$CEM->getID());
	} else {
		$error_msg=_('Error creating new component: '.$CEM->getErrorMessage());
	}
}

/*

site_project_header(array('group'=>$group_id, 'title'=>'Add new component'));
$Layout->col(12,true);
?>
<form class="form-horizontal" action="new_component.php?group_id=<?=$group_id?><?php if ($CemID)echo'&cid='.$CemID ?>" method="post">
	<fieldset>
		<legend>Add a Component</legend>
		<div class="control-group">
			<label class="control-label" for="">Component Name</label>
			<div class="controls">
				<input type="text" autocomplete="off" name="cname" />
			</div>
		</div>
		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Add</button>
			<button class="btn" type="reset">Cancel</button>
		</div>
	</fieldset>
</form>
<?php
$Layout->endcol();
site_project_footer();
*/
?>