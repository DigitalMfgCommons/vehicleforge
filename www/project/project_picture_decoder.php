<?php

require_once('../env.inc.php');
require_once $gfcommon . 'include/pre.php';

$group_id = getStringFromRequest('group_id');
$default = 'http://placehold.it/160x120&text=GEForge';

$groupObj = group_get_object($group_id);
if($groupObj) {
	$image=$groupObj->getImage();
} else {
	$image=false;
}

if ($image){
	$doc=new Document($image);
	$src=$doc->getPublicPath();
}else{
	$src=$default;
}

print "{ \"group_img_src\":\"".$src."\" }";

?>