<?php
/**
 * Project Admin Main Page
 *
 * This page contains administrative information for the project as well
 * as allows to manage it. This page should be accessible to all project
 * members, but only admins may perform most functions.
 *
 * Copyright 2004 GForge, LLC - Tim Perdue
 * Copyright 2010 (c), Franck Villaume
 * Copyright (C) 2010-2011 Alain Peyrat - Alcatel-Lucent
 * http://fusionforge.org
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'include/role_utils.php';
require_once $gfwww.'project/admin/project_admin_utils.php';
require_once $gfcommon.'include/GroupJoinRequest.class.php';


$group_id = getStringFromRequest('group_id');
$feedback = htmlspecialchars(getStringFromRequest('feedback'));

session_require_perm ('project_admin', $group_id) ;

// get current information
$group = group_get_object($group_id);
if (!$group || !is_object($group)) {
    exit_no_group();
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(),'admin');
}

$group->clearError();

// If this was a submission, make updates
if (getStringFromRequest('submit')) {
	$form_group_name = getStringFromRequest('form_group_name');
	$form_shortdesc = getStringFromRequest('form_shortdesc');
	$form_homepage = getStringFromRequest('form_homepage');
	$logo_image_id = getIntFromRequest('logo_image_id');
	$use_mail = getStringFromRequest('use_mail');
	$use_survey = getStringFromRequest('use_survey');
	$use_forum = getStringFromRequest('use_forum');
	$use_pm = getStringFromRequest('use_pm');
	$use_scm = getStringFromRequest('use_scm');
	$use_news = getStringFromRequest('use_news');
	$use_docman = getStringFromRequest('use_docman');
	$use_ftp = getStringFromRequest('use_ftp');
	$use_tracker = getStringFromRequest('use_tracker');
	$use_frs = getStringFromRequest('use_frs');
	$use_stats = getStringFromRequest('use_stats');
	$tags = getStringFromRequest('form_tags');
	$addTags = getArrayFromRequest('addTags');
	$new_doc_address = getStringFromRequest('new_doc_address');
	$send_all_docs = getStringFromRequest('send_all_docs');
	$GalleryGroupID=getIntFromRequest('gallery_folder');
	$ProfileImage=getIntFromRequest('profile_image');
    $ExportControlled=util_convert_bool(util_parse_checkbox(getStringFromRequest('export_control')));
	
	if (trim($tags) != "") {
		$tags .= ",";
	}
	$tags .= implode(",", $addTags);

	$res = $group->update(
		session_get_user(),
		$form_group_name,
		$form_homepage,
		$form_shortdesc,
		$use_mail,
		$use_survey,
		$use_forum,
		$use_pm,
		1,
		$use_scm,
		$use_news,
		$use_docman,
		$new_doc_address,
		$send_all_docs,
		100,
		$use_ftp,
		$use_tracker,
		$use_frs,
		$use_stats,
		$tags,
		0,
		$GalleryGroupID,
		$ProfileImage,
        $ExportControlled
	);
	
	//100 $logo_image_id

	if (!$res) {
		$error_msg .= $group->getErrorMessage();
	} else {
		$feedback .= _('Project information updated');
	}
}

use_javascript('file_tree/jqueryFileTree.js');
use_stylesheet('/js/file_tree/jqueryFileTree.css');

$JS='$(function(){
	$("#profile_image").fileTree({
		script: "/js/file_tree/jqueryFileTree.php",
		root: "project_files/'.$group->getUnixName().'/"
	},function(file){
		console.log(file);
	});
});';
add_js($JS);

project_admin_header(array('title'=>_('Project Information'), 'group'=>$group_id));
$Layout->col(12,true);
$HTML->tertiary_menu(0);
?>
<form action="<?=getStringFromServer('PHP_SELF')?>" method="post">
    <input type="hidden" name="group_id" value="<?=$group->getID()?>" />
    <table class="zebra-striped">
	<tr>
		<td><?=_('Project Name')?></td>
		<td>
			<input type="text" name="form_group_name" value="<?=$group->getPublicName()?>" size="40" maxlength="40" />
		</td>
	</tr>
	<tr>
		<td><?=_('Project Description')?></td>
		<td>
			<textarea name="form_shortdesc" cols="90" rows="5"><?=$group->getDescription()?></textarea>
		</td>
	</tr>
	<tr>
		<td><?=_('Project Tags')?></td>
		<td>
			<input type="text" name="form_tags" size="100" value="<?=$group->getTags()?>" /><span class="help-inline"><?=_('Use commas to separate')?></span>
		</td>
	</tr>
	<tr>
		<td><?=_('Categorization')?></td>
		<td>
			<a href="/project/admin/group_trove.php?group_id=<?=$group->getID()?>">[<?=_('Edit Trove')?>]</a>
		</td>
	</tr>
	<tr>
		<td><?=_('Homepage Link')?></td>
		<td>
			<input type="text" name="form_homepage" size="100" value="<?=$group->getHomePage()?>" />
		</td>
	</tr>
	<tr>
		<td><?=_('Gallery Folder')?></td>
		<td>
			<select name="gallery_folder" autocomplete="off">
<?php
$Result=db_query_params("SELECT * FROM doc2_groups WHERE group_id=$1",array($group->getID()));
$SelectOptions='<option value="0" selected>No gallery</option>';
while ($DocGroup=db_fetch_array($Result)){
	$SelectOptions.='<option value="'.$DocGroup['doc_group_id'].'"';
	if ($DocGroup['doc_group_id']==$group->getGalleryGroupID()){
		$SelectOptions.=' selected';
	}
	$SelectOptions.='>'.$DocGroup['group_name'].'</option>';
}
echo $SelectOptions;
?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?=_('Profile Image')?></td>
		<td>
			<select name="profile_image">
<?php
$Result=db_query_params("SELECT * FROM doc2_files WHERE group_id=$1",array($group->getID()));
$SelectOptions='<option value="0" selected>Default Image</option>';
while($Doc=db_fetch_array($Result)){
	$SelectOptions.='<option value="'.$Doc['file_id'].'"';

	if ($Doc['file_id']==$group->getImage()){
		$SelectOptions.=' selected';
	}

	$SelectOptions.='>'.$Doc['filename'].'</option>';
}
echo $SelectOptions;
?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?=_('Visibility')?></td>
		<td>
<?php
switch($group->getVisibility()){
	case 1:
		echo 'Private';
		break;

	case 2:
		echo 'Non-Private';
		break;
}
?>
			<br /><a href="/project/admin/users.php?group_id=<?=$group_id?>">Edit Roles</a>
		</td>
	</tr>
    <tr>
        <td><?=_('Export Controlled')?></td>
        <td>
            <input type="checkbox" name="export_control" autocomplete="off" <?=$group->getExportControl()?' checked="checked"':''?>/>
        </td>
    </tr>
	<tr>
		<td></td>
		<td>
<?php
if(forge_get_config('use_mail'))
	echo '<input type="hidden" name="use_mail" value="'.($group->usesMail() ? '1' : '0').'" />';
if(forge_get_config('use_survey'))
	echo '<input type="hidden" name="use_survey" value="'.($group->usesSurvey() ? '1' : '0').'" />';
if(forge_get_config('use_forum'))
	echo '<input type="hidden" name="use_forum" value="'.($group->usesForum() ? '1' : '0').'" />';
if(forge_get_config('use_pm'))
	echo '<input type="hidden" name="use_pm" value="'.($group->usesPM() ? '1' : '0').'" />';
if(forge_get_config('use_scm'))
	echo '<input type="hidden" name="use_scm" value="'.($group->usesSCM() ? '1' : '0').'" />';
if(forge_get_config('use_news'))
	echo '<input type="hidden" name="use_news" value="'.($group->usesNews() ? '1' : '0').'" />';
if(forge_get_config('use_docman'))
	echo '<input type="hidden" name="use_docman" value="'.($group->usesDocman() ? '1' : '0').'" />';
if(forge_get_config('use_ftp'))
	echo '<input type="hidden" name="use_ftp" value="'.($group->usesFTP() ? '1' : '0').'" />';
if(forge_get_config('use_tracker'))
	echo '<input type="hidden" name="use_tracker" value="'.($group->usesTracker() ? '1' : '0').'" />';
if(forge_get_config('use_frs'))
	echo '<input type="hidden" name="use_frs" value="'.($group->usesFRS() ? '1' : '0').'" />';
?>
			<input type="hidden" name="use_stats" value="<?=($group->usesStats() ? '1' : '0')?>" />
			<input type="hidden" name="new_doc_address" value="<?=$group->getDocEmailAddress()?>" size="40" maxlength="250" />
			<input type="checkbox" class="hidden" name="send_all_docs" value="1" <?=($group->docEmailAll())?'checked="checked"':''?> />
			<input type="submit" name="submit" class="btn primary" value="Update">
		</td>
	</tr>
</table>
</form>


<!--
<h2><?php echo _('Descriptive Project Name'); ?></h2>
<p>
<input type="text" name="form_group_name" value="<?php echo $group->getPublicName(); ?>" size="40" maxlength="40" />
</p>

<h2><?php echo _('Short Description'); ?></h2>
<p>
<?php echo _('Maximum 255 characters, HTML will be stripped from this description'); ?>
</p>
<p>
<textarea cols="80" rows="3" name="form_shortdesc">
<?php echo $group->getDescription(); ?>
</textarea>
</p>

<?php if (forge_get_config('use_project_tags')) { ?>
<h2><?php echo _('Project tags'); ?></h2>
<p>
<?php echo _('Add tags (use comma as separator): ') ?><br />
<input type="text" name="form_tags" size="100" value="<?php echo $group->getTags(); ?>" />
</p>
<?php
	//TODO: add this back in
	/*$infos = getAllProjectTags();
	if ($infos) {
		echo '<br />';
		echo _('Or pick a tag from those used by other projects: ');
		echo '<br />';
		echo '<table width="100%"><thead><tr>';
		echo '<th>'._('Tags').'</th>';
		echo '<th>'._('Projects').'</th>';
		echo '</tr></thead><tbody>';

		$unix_name = $group->getUnixName();
		foreach ($infos as $tag => $plist) {
			$disabled = '';
			$links = array();
			foreach($plist as $project) {
				$links[] = util_make_link('/projects/'.$project['unix_group_name'].'/',$project['unix_group_name']);
				if ($project['group_id'] == $group_id) {
					$disabled = ' disabled="disabled"';
				}
			}

			echo '<tr>';
			echo '<td><input type="checkbox" name="addTags[]" value="'.$tag.'"'.$disabled.' /> ';
			if ($disabled) {
				echo '<s>'.$tag.'</s>';
			} else {
				echo $tag;
			}
			echo '</td>';
			echo '<td>'.implode(' ', $links).'</td>' ;
			echo '</tr>' ;
		}
		echo '</tbody></table>' ;
	}*/
} ?>

<h2><?php echo _('Trove Categorization'); ?></h2>
<p>
<a href="/www/project/admin/group_trove.php?group_id=<?php echo $group->getID(); ?>">[<?php echo _('Edit Trove'); ?>]</a>
</p>

<h2><?php echo _('Homepage Link') ?></h2>
<p>
<input type="text" name="form_homepage" size="100" value="<?php echo $group->getHomePage(); ?>" />
</p>

<?php
// This function is used to render checkboxes below
function c($v) {
        if ($v) {
                return 'checked="checked"';
        } else {
                return '';
        }
}
?>

<?php
if(forge_get_config('use_mail')) {
?>
<input type="hidden" name="use_mail" value="<?php echo ($group->usesMail() ? '1' : '0'); ?>" />
<?php
} 

if(forge_get_config('use_survey')) {
?>
<input type="hidden" name="use_survey" value="<?php echo ($group->usesSurvey() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_forum')) {
?>
<input type="hidden" name="use_forum" value="<?php echo ($group->usesForum() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_pm')) {
?>
<input type="hidden" name="use_pm" value="<?php echo ($group->usesPM() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_scm')) {
?>
<input type="hidden" name="use_scm" value="<?php echo ($group->usesSCM() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_news')) {
?>
<input type="hidden" name="use_news" value="<?php echo ($group->usesNews() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_docman')) {
?>
<input type="hidden" name="use_docman" value="<?php echo ($group->usesDocman() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_ftp')) {
?>
<input type="hidden" name="use_ftp" value="<?php echo ($group->usesFTP() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_tracker')) {
?>
<input type="hidden" name="use_tracker" value="<?php echo ($group->usesTracker() ? '1' : '0'); ?>" />
<?php
}

if(forge_get_config('use_frs')) {
?>
<input type="hidden" name="use_frs" value="<?php echo ($group->usesFRS() ? '1' : '0'); ?>" />
<?php } ?>

<input type="hidden" name="use_stats" value="<?php echo ($group->usesStats() ? '1' : '0'); ?>" />

<p>
<?php echo _('If you wish, you can provide default email addresses to which new submissions will be sent') ?>.<br />
<strong><?php echo _('New Document Submissions') ?>:</strong><br />
<input type="text" name="new_doc_address" value="<?php echo $group->getDocEmailAddress(); ?>" size="40" maxlength="250" />
<?php echo _('(send on all updates)') ?>
<input type="checkbox" name="send_all_docs" value="1" <?php echo c($group->docEmailAll()); ?> />
</p>
	
<p>
	<strong><?=_('Gallery Folder')?>:</strong><br />
	<select name="gallery_folder" autocomplete="off">
<?php
$Result=db_query_params("SELECT * FROM doc2_groups WHERE group_id=$1",array($group->getID()));
$SelectOptions='<option value="0" selected>No gallery</option>';
while ($DocGroup=db_fetch_array($Result)){
	$SelectOptions.='<option value="'.$DocGroup['doc_group_id'].'"';
	if ($DocGroup['doc_group_id']==$group->getGalleryGroupID()){
		$SelectOptions.=' selected';
	}
	$SelectOptions.='>'.$DocGroup['group_name'].'</option>';
}
echo $SelectOptions;
?>
	</select>
</p>
<p>
	<strong>Profile Image:</strong><br />
	<select name="profile_image">
<?php
$Result=db_query_params("SELECT * FROM doc2_files WHERE group_id=$1",array($group->getID()));
$SelectOptions='<option value="0" selected>Default Image</option>';
while($Doc=db_fetch_array($Result)){
	$SelectOptions.='<option value="'.$Doc['file_id'].'"';

	if ($Doc['file_id']==$group->getImage()){
		$SelectOptions.=' selected';
	}

	$SelectOptions.='>'.$Doc['filename'].'</option>';
}
echo $SelectOptions;
?>
	</select>
</p>

<p>
<input type="submit" name="submit" value="<?php echo _('Update') ?>" />
</p>

</form>
-->

<?php
plugin_hook('admin_project_link',$group_id) ;

$Layout->endcol();
project_admin_footer(array());

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
