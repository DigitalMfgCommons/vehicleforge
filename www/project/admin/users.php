<?php
/*-
 * Project Admin Main Page
 *
 * Copyright 2004 GForge, LLC
 * Copyright 2006 federicot
 * Copyright © 2011
 *	Thorsten Glaser <t.glaser@tarent.de>
 * All rights reserved.
 *
 * This file is part of FusionForge. FusionForge is free software;
 * you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the Licence, or (at your option)
 * any later version.
 *
 * FusionForge is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with FusionForge; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *-
 * This page contains administrative information for the project as well
 * as allows to manage it. This page should be accessible to all project
 * members, but only admins may perform most functions.
 */

require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfwww.'project/admin/project_admin_utils.php';
require_once $gfwww.'include/role_utils.php';
require_once $gfcommon.'include/account.php';
require_once $gfcommon.'include/GroupJoinRequest.class.php';
require_once $gfwww.'include/FormGen.php';
$FormGen=new FormGen;

$group_id = getStringFromRequest('group_id');
$group=group_get_object($group_id);

session_require_perm ('project_read', $group_id) ;

//TODO: fix the AJAX functionality
$JS='$(function(){
	var group_id='.$group->getID().';

	$(document).on("click", ".unlinkrole", function(){
		var $me=$(this);
		var role=$me.data("role_id"),
		role_desc=$me.parent().parent().children("td:first").text().trim();

		$.post("/ajax/unlink_role.php",{"role_id":role, "group_id":group_id},function(m){
			if (m==true){
				$("#cur_role_"+role).parent().slideUp(function(){$(this).remove();});
				$me.parent().parent().slideUp(function(){
					$(this).remove();

					$("#link_role_id").append("<option value=\""+role+"\">"+role_desc+"</option>");
				});
			}
		});
	});

	$(document).on("click", "#linkrole", function(){
		var $me=$(this);
		var role=$("#link_role_id").val(),
		role_desc=$("#link_role_id option:selected").text().trim();
		
		$.getJSON("/ajax/link_role.php",{"role_id":role, "group_id":group_id},function(m){
			if (m.status==true){
			    var html="<tr><td>"+role_desc+"</td><td><input class=\"unlinkrole btn danger small\" type=\"button\" data-role_id=\""+role+"\" value=\"Unlink\"></td></tr>";
				$("#used_roles tbody").append(html);

                html="<tr><td>"+m.name+"</td><td><a href=\"roleedit.php?group_id="+group_id+"&role_id="+role+"\">Edit Permissions</a></td></tr>";
				$("#current_roles tbody").append(html);

				$("#link_role_id option").each(function(){
					if ($(this).val()==role){
						$(this).remove();
					}
				});
			}
		});
	});

	$(document).on("click", ".grant_role", function(){
		var $me=$(this);
		var unix_name=$me.data("unix_name"),
		role=$me.siblings("select").val(),
		user_id=$me.data("user_id"),
		role_name=$me.siblings("select").children("option:selected").text().trim();

		if (role!=0){
			$.post("ajax/add_mem_to_project.php",{"role_id":role, "group_id":group_id, "form_unix_name":unix_name},function(m){
				if (m==true){
					$me.parent().siblings(":nth-child(2)").append("<div class=\"role_"+role+"\">"+role_desc+"</div>");
					$me.siblings("div").append("<input type=\"submit\" class=\"remove_role\" data-user_id=\""+user_id+"\" data-role_id=\""+role+"\" name=\"rmuser\" value=\"Remove "+role_name+"\" /><br />")
				}
			});
		}else{
			$me.siblings("select").focusChange();
		}
document.location.reload(true);

	});

	$(document).on("click", ".remove_role", function(){
		var $me=$(this);
		var user=$me.data("user_id"),
		role=$me.data("role_id");

		$.post("ajax/remove_mem_from_project.php",{"role_id":role, "group_id":group_id, "user_id":user},function(m){
			if (m==true){
				$me.parent().parent().siblings(":nth-child(2)").children(".role_"+role).remove();
				$me.remove();
			}
		});
document.location.reload(true);
	});

	$("#new_member_button").click(function(){
		var $me=$(this),
		$name=$("#new_member_name"),
		$role=$("#add_member_role_id");

		var unix_name=$name.val(),
		role_id=$role.val();

		if (!unix_name){
			$name.focusChange();
		}else if (role_id==-1){
			$role.focusChange();
		}else{
			$.post("/ajax/add_mem_to_project",{"role_id":role_id, "group_id":group_id, "form_unix_name":unix_name},function(m){
				m=$.parseJSON(m);

				var $msg=$("#add_member_msg");
				//$msg.removeClass("hightlight error");

				if (m.status==true){
					//$msg.addClass("hightlight");
					$name.val("");
					$role.val(-1);
				}else{
					//$msg.addClass("error");
				}

				$msg.text(m.msg).slideDown().delay(2000).slideUp();
			});
		}
document.location.reload(true);
	});
});';

add_js($JS);

$feedback = htmlspecialchars(getStringFromRequest('feedback'));
$warnig_msg = htmlspecialchars(getStringFromRequest('warnig_msg'));
$error_msg = htmlspecialchars(getStringFromRequest('error_msg'));


if (!$group || !is_object($group)) {
    exit_no_group();
} elseif ($group->isError()) {
	exit_error($group->getErrorMessage(),'admin');
}

// Add hook to replace users managements by a plugin.
$html_code = array();
if (plugin_hook_listeners("project_admin_users") > 0) {
	$hook_params = array () ;
	$hook_params['group_id'] = $group_id ;
	plugin_hook ("project_admin_users", $hook_params);
}

function cache_external_roles () {
	global $used_external_roles, $unused_external_roles, $group, $group_id;

	if (USE_PFO_RBAC) {
		$unused_external_roles = array () ;
		foreach (RBACEngine::getInstance()->getPublicRoles() as $r) {
			$grs = $r->getLinkedProjects () ;
			$seen = false ;
			foreach ($grs as $g) {
				if ($g->getID() == $group_id) {
					$seen = true ;
					break ;
				}
			}
			if (!$seen) {
				$unused_external_roles[] = $r ;
			}
		}
		$used_external_roles = array () ;
		foreach ($group->getRoles() as $r) {
			if ($r->getHomeProject() == NULL
			    || $r->getHomeProject()->getID() != $group_id) {
				$used_external_roles[] = $r ;
			}
		}

		sortRoleList ($used_external_roles, $group, 'composite') ;
		sortRoleList ($unused_external_roles, $group, 'composite') ;

	}
}

cache_external_roles () ;

if (getStringFromRequest('submit')) {
	if (getStringFromRequest('adduser')) {
		/*
			add user to this project
			*/
		$form_unix_name = getStringFromRequest('form_unix_name');
		$user_object = &user_get_object_by_name($form_unix_name);
		if ($user_object === false) {
			$warning_msg .= _('No Matching Users Found');
		} else {
			$role_id = getIntFromRequest('role_id');
			if (!$role_id) {
				$warning_msg .= _('Role not selected');
			} else {
				$user_id = $user_object->getID();
				if (!$group->addUser($form_unix_name,$role_id)) {
					$error_msg = $group->getErrorMessage();
				} else {
					$feedback = _("Member Added Successfully");
					//if the user have requested to join this group
					//we should remove him from the request list
					//since it has already been added
					$gjr=new GroupJoinRequest($group,$user_id);
					if ($gjr || is_object($gjr) || !$gjr->isError()) {
						$gjr->delete(true);
					}
				}
			}
		}
	} else if (getStringFromRequest('rmuser')) {
		/* remove a member from this project */
		$user_id = getIntFromRequest('user_id');
		$role_id = getIntFromRequest('role_id');
		$role = RBACEngine::getInstance()->getRoleById($role_id) ;
		if ($role->getHomeProject() == NULL) {
			session_require_global_perm ('forge_admin') ;
		} else {
			session_require_perm ('project_admin', $role->getHomeProject()->getID()) ;
		}
		if (!$role->removeUser (user_get_object ($user_id))) {
			$error_msg = $role->getErrorMessage() ;
		} else {
			$feedback = _("Member Removed Successfully");
		}
	} else if (getStringFromRequest('updateuser')) {
		/* Adjust Member Role */
		$user_id = getIntFromRequest('user_id');
		$role_id = getIntFromRequest('role_id');
		if (! $role_id) {
			$error_msg = _("Role not selected");
		}
		else {
			if (!$group->updateUser($user_id,$role_id)) {
				$error_msg = $group->getErrorMessage();
			} else {
				$feedback = _("Member Updated Successfully");
			}
		}
	} elseif (getStringFromRequest('acceptpending')) {
		/*
			add user to this project
			*/
		$role_id = getIntFromRequest('role_id');
		if (!$role_id) {
			$warning_msg .= _("Role not selected");
		} else {
			$form_userid = getIntFromRequest('form_userid');
			$form_unix_name = getStringFromRequest('form_unix_name');
			if (!$group->addUser($form_unix_name,$role_id)) {
				$error_msg = $group->getErrorMessage();
			} else {
				$gjr=new GroupJoinRequest($group,$form_userid);
				if (!$gjr || !is_object($gjr) || $gjr->isError()) {
					$error_msg = _('Error Getting GroupJoinRequest');
				} else {
					$gjr->delete(true);
				}
				$feedback = _("Member Added Successfully");
			}
		}
	} elseif (getStringFromRequest('rejectpending')) {
		/*
			reject adding user to this project
			*/
		$form_userid = getIntFromRequest('form_userid');
		$gjr=new GroupJoinRequest($group,$form_userid);
		if (!$gjr || !is_object($gjr) || $gjr->isError()) {
			$error_msg .= _('Error Getting GroupJoinRequest');
		} else {
			if (!$gjr->reject()) {
				$error_msg = $gjr->getErrorMessage();
			} else {
				$feedback .= 'Rejected';
			}
		}
	} else if (getStringFromRequest('linkrole')) {
		/* link a role to this project */
		if (USE_PFO_RBAC) {
			$role_id = getIntFromRequest('role_id');
			foreach ($unused_external_roles as $r) {
				if ($r->getID() == $role_id) {
					if (!$r->linkProject($group)) {
						$error_msg = $r->getErrorMessage();
					} else {
						$feedback = _("Role linked successfully");
						cache_external_roles () ;
					}
				}
			}
		}
	} else if (getStringFromRequest('unlinkrole')) {
		/* unlink a role from this project */
		if (USE_PFO_RBAC) {
			$role_id = getIntFromRequest('role_id');
			foreach ($used_external_roles as $r) {
				if ($r->getID() == $role_id) {
					if (!$r->unLinkProject($group)) {
						$error_msg = $r->getErrorMessage();
					} else {
						$feedback = _("Role unlinked successfully");
						cache_external_roles () ;
					}
				}
			}
		}
	}
}

$group->clearError();

project_admin_header(array('title'=>'Users & Permissions'));
$Layout->col(12,true);
$HTML->tertiary_menu(1);
$Layout->endcol()->col(6);

?>
<h2><?=_('Add Members')?></h2>
<?php
//Pending join requests
$reqs =& get_group_join_requests($group);
if (count($reqs) > 0) {
	?>
	<h3>Member Requests</h3>
	<table class="zebra-striped">
		<tr>
			<th>Member</th>
			<th>Actions</th>
		</tr>
		<tr>
<?php
for ($i=0; $i<count($reqs); $i++) {
	$user =& user_get_object($reqs[$i]->getUserId());
	if (!$user || !is_object($user)) {
		echo "Invalid User";
	}
//TODO:make this an AJAX form
?>
		<td><a href="/users/<?=$user->getUnixName()?>"><?=$user->getRealName()?></a></td>
		<td>
		<form action="<?=getStringFromServer('PHP_SELF').'?group_id='.$group_id; ?>" method="post">
			<input type="hidden" name="submit" value="y" />
			<input type="hidden" name="form_userid" value="<?=$user->getId(); ?>" />
			<input type="hidden" name="form_unix_name" value="<?=$user->getUnixName(); ?>" />
			<?=role_box($group_id,'role_id','xzxzxz',false,'','span3')?>
			<input type="submit" name="acceptpending" class="btn success" value="<?=_("Accept")?>" />
			<input type="submit" name="rejectpending" class="btn danger" value="<?=_("Reject")?>" />
		</form>
	</td></tr>
<?php
	}
	echo '</table>';
}

if (isset($html_code['add_user'])) {
	echo $html_code['add_user'];
} else {
	?>
	<h3>Add New Member</h3>
	<div class="pull-left"><input id="new_member_name" type="text" class="span3" /></div>
	<div class="pull-right">
        <?php
            echo role_box($group_id,'role_id','xzxzxz',true,'add_member_role_id','span3')
		//	echo role_box($group->getId(), "adduser_dropdown", 0);
        ?><br />
        <input class="btn small primary pull-right" value="<?=_('Add Member')?>" id="new_member_button" type="button" />
    </div>
	<div class="hide" style="clear:both" id="add_member_msg"></div>
	<div class="pull-left"><a href="massadd.php?group_id=<?=$group_id?>"><?=_("Add Users From List")?></a></div>
	<?php
}
$Layout->endcol()->col(6);
?>
	<h2><?=_("Edit Roles")?></h2>
	<table class="zebra-striped" id="current_roles">
		<tr>
			<th>Role Name</th>
			<th>Actions</th>
		</tr>
<?php
$roles = $group->getRoles() ;
sortRoleList ($roles, $group, 'composite') ;

foreach ($roles as $r) {
	?>
	<tr>
		<td id="cur_role_<?=$r->getID()?>"><?=$r->getDisplayableName($group)?></td>
		<td>
			<a href="roleedit.php?group_id=<?=$group_id?>&role_id=<?=$r->getID()?>"><?=_('Edit Permissions')?></a><br />
			<!--<a href="roledelete.php?group_id=<?=$group_id?>&role_id=<?=$r->getID()?>">Delete Role</a>-->
		</td>
	</tr>
	<?php
}
?>
</table>
<!--<form action="roleedit.php?group_id=<?/*=$group_id*/?>" method="post">
<input type="text" name="role_name" size="10" autocomplete="off" />
	<input type="submit" class="btn primary" name="add" value="<?/*=_('Create Role')*/?>" />
</form>-->
<?php
if (!USE_PFO_RBAC) {
		echo '<form action="roleedit.php?group_id='. $group_id .'&amp;role_id=observer" method="post">
        <p><input type="submit" name="edit" value="'._("Edit Observer").'" /></p>
        </form>';
}
$Layout->endcol()->col(6,false,true);
?>
	<h2><?=_("Current Project Members")?></h2>
	<table class="table table-striped">
		<tr>
			<th>Member</th>
			<th>Role</th>
			<th>Actions</th>
		</tr>
<?php
$members = $group->getUsers() ;

foreach ($members as $user) {
?>
    <tr>
        <td><a href="/users/<?=$user->getUnixName()?>"><?=$user->getRealName()?></td>
        <td>
<?php

	 
	/* $roles = $user->getRoles($group->getID()); */
		$roles = array();
	foreach (RBACEngine::getInstance()->getAvailableRolesForUser ($user) as $role) {
		if ($role->getHomeProject() && $role->getHomeProject()->getID() == $group->getID()) {
			$roles[] = $role ;
		}
	}

	sortRoleList ($roles) ;

    foreach ($roles as $role){
?>
			<div class="role_<?=$role->getID()?>"><?=$role->getName()?></div>
			<input type="submit" class="remove_role btn btn-danger btn-mini" data-user_id="<?=$user->getID()?>" data-role_id="<?=$role->getID()?>" name="rmuser" value="<?=_("Remove ".$role->getName())?>" />
<?php
    }
?>
		</td>
		<td>
			  <?=role_box($group->getId(), "user".$user->getID()."dropdown", 0)?><br />
			<input type="submit" name="adduser" class="grant_role btn btn-success btn-small" data-user_id="<?=$user->getID()?>" data-unix_name="<?=$user->getUnixName()?>" value="<?=_("Grant Role")?>" />
		</td>
	</tr>
<?php
}
?>
</table>
<?php
$Layout->endcol()->col(6);
?>
	<h2><?=_('External Roles')?></h2>
<?php
if (USE_PFO_RBAC) {
	//Show used roles
	?>
	<table class="zebra-striped" id="used_roles">
		<tr>
			<th>Role Name</th>
			<th>Actions</th>
		</tr>
	<?php
	if (count ($used_external_roles)) {
		foreach ($used_external_roles as $r){
	?>
			<tr>
				<td><?= $r->getDisplayableName($group) ?></td>
				<td><input type="button" class="unlinkrole btn danger small" value="<?=_("Unlink")?>" data-role_id="<?=$r->getID()?>" /></td>
			</tr>
	<?php
		}
	}

	$ids = array () ;
	$names = array () ;
	foreach ($unused_external_roles as $r) {
		$ids[] = $r->getID() ;
		$names[] = $r->getDisplayableName($group) ;
	}
	?>
	</table>
    <?=html_build_select_box_from_arrays($ids,$names,'role_id','',false,'',false,'',false,'link_role_id')?>
    <input type="submit" id="linkrole" class="btn success small" name="linkrole" value="<?=_("Link")?>" />

<?php
}
$Layout->endcol();

project_admin_footer(array());

db_display_queries();

// Local Variables:
// mode: php
// c-file-style: "bsd"
// End:

?>
