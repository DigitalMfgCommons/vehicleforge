<?php
require_once '../../env.inc.php';
require_once $gfcommon . 'include/pre.php';

$ActivityID=getIntFromRequest('aid');
$SiteActivity=new SiteActivity;

echo json_encode($SiteActivity->formatData($SiteActivity->getSingle($ActivityID)));
?>