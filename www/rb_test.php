<?php
require_once 'env.inc.php';
require_once $gfcommon.'include/rb.php';
R::setup('pgsql:host=ip-10-12-158-132.ec2.internal;dbname=gforgedev','gforge','');

class BeanObject    {
	protected $Bean;
    static protected $BeanName;

	function __construct($ID){
		if ($ID){
			$this->Bean=R::load(static::$BeanName, $ID);
		}
	}

    function getBeanName(){
        return static::$BeanName;
    }

	function __destruct(){
		if ($this->Bean->getMeta('tainted')){
			echo 'Bean ('.static::$BeanName.') has been changed, but not saved';
		}
	}
}

class rbDOMEInterface extends BeanObject   {
    static protected $BeanName='rbDOMEInterface';

	function getID(){
		return $this->Bean->id;
	}

	function getServerID(){
		return $this->Bean->server_id;
	}

	static function create($InterfaceData, $ServerID){
		$rbDI=R::dispense(self::$BeanName);
		$rbDI->interface_data=$InterfaceData;
		$rbDI->server_id=$ServerID;

		return R::store($rbDI);
	}

	function setServerID($ServerID){
		$this->Bean->server_id=$ServerID;
		return $this;
	}

	function setInterfaceData($InterfaceData){
		$this->Bean->interface_data=$InterfaceData;
		return $this;
	}

	function update(){
		R::store($this->Bean);
	}
}

$DI=new rbDOMEInterface(4);
echo $DI->getBeanName();
//$DI->setInterfaceData('new data')->setServerID(3)->update();

/*$Book=R::dispense('book');
$Book->title='Test Title';
$Book->author='Jason Kaczmarsky';
$Book->price=9.98;
$ID=R::store($Book);
echo 'ID: '.$ID;*/

/*$Needles=R::find('book','author=?',array('Jason Kaczmarsky'));
foreach($Needles as $i){
	echo $i->title;
}*/

R::close();
?>