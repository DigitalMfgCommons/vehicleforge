<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:03 AM 11/28/11
 */

require_once('env.inc.php');
require_once $gfcommon.'include/pre.php';
require_once $gfcommon.'forum/ForumHTML.class.php';
require_once $gfcommon.'forum/Forum.class.php';
require_once $gfcommon.'forum/ForumAdmin.class.php';
require_once $gfcommon.'forum/ForumFactory.class.php';
require_once $gfcommon.'forum/ForumMessageFactory.class.php';
require_once $gfcommon.'forum/ForumMessage.class.php';
require_once $gfcommon.'include/TextSanitizer.class.php';

$_REQUEST['forum_name']="GlobalForum";
$_REQUEST['description']="Testing if I can make a global forum using the default forum tools";
$_REQUEST['send_all_posts_to']='';
$_REQUEST['allow_anonymous']=1;
$_REQUEST['is_public']=1;
$_REQUEST['moderation_level']=0;
$_REQUEST['group_forum_id']=1;

$FA=new ForumAdmin(1);
$FA->ExecuteAction("add_forum");
?>