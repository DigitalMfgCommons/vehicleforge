<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once APP_PATH.'common/include/account.php';
require_once APP_PATH.'common/include/timezones.php';

$Submit=getStringFromRequest('submit',false);

if ($Submit){
    $unix_name = getStringFromRequest('unix_name');
    $firstname = getStringFromRequest('firstname');
    $lastname = getStringFromRequest('lastname');
    $password1 = getStringFromRequest('password1');
    $password2 = getStringFromRequest('password2');
    $email = getStringFromRequest('email');
    $mail_site = getIntFromRequest('mail_site');
    $mail_va = getIntFromRequest('mail_va');

    $language_id = getIntFromRequest('language_id');
    $timezone = getStringFromRequest('timezone');
    $jabber_address = getStringFromRequest('jabber_address');
    $jabber_only = getStringFromRequest('jabber_only');
    $theme_id = 1;
    $address = '';
    $address2 = '';
    $phone = getStringFromRequest('phone');
    $fax = getStringFromRequest('fax');
    $title = getStringFromRequest('title');
    $ccode = getStringFromRequest('ccode');
    $accept_conditions = getIntFromRequest ('accept_conditions');

    if (!form_key_is_valid(getStringFromRequest('form_key'))) {
        exit_form_double_submit('my');
    }

	if (forge_get_config('user_registration_accept_conditions') && ! $accept_conditions) {
        $warning_msg = _("You can't register an account unless you accept the terms of use.") ;
    } else {
        //$activate_immediately = getIntFromRequest('activate_immediately');
        $activate_immediately=1;
        if (($activate_immediately == 1) && forge_check_global_perm ('forge_admin')) {
            //if (($activate_immediately == 1)) {
            $send_mail = false;
            $activate_immediately = true;
        } else {
            $send_mail = true;
            $activate_immediately = false;
        }
	
	$send_mail = false;
	$activate_immediately = false;

        $new_user = new GFUser();
        $register = $new_user->create($unix_name,$firstname,$lastname,$password1,$password2,
            $email,$mail_site,$mail_va,$language_id,$timezone,$jabber_address,$jabber_only,$theme_id,'',
            $address,$address2,$phone,$fax,$title,$ccode,$send_mail);
        if ($register) {
            site_header(array('title'=>'Register Confirmation'));

            if ($activate_immediately) {
                if (!$new_user->setStatus('A')) {
                    print '<span class="error">' .
                        _('Error during user activation but after user registration (user is now in pending state and will not get a notification eMail!)') .
                        '</span>' ;
                    print '<p>' . sprintf(_("Could not activate newly registered user's forge account: %s"), htmlspecialchars($new_user->getErrorMessage())) . '</p>';
                    $HTML->footer(array());
                    exit;
                }
            }

            if ($send_mail) {
                echo '<p>';
                printf(_('You have registered the %1$s account on %2$s.'),
                    $new_user->getUnixName(),
                    forge_get_config ('forge_name'));
                echo '</p>';
                print '<p>' . _('A confirmation email is being sent to verify the submitted email address. Visiting the link sent in this email will activate the account.') . '</p>';
            } else {
                print '<p>' ;
                printf (_('You have registered and activated user %1$s on %2$s. They will not receive an eMail about this fact.'), $unix_name, forge_get_config('forge_name'));
                print '</p>' ;
            }
            site_footer();
            exit;
        }else{
            $error_msg = $new_user->getErrorMessage();
            if (isset($register_error)) {
                $error_msg .= ' '.$register_error;
            }
        }
    }
}

if (!isset($ccode) || empty($ccode) || !preg_match('/^[a-zA-Z]{2}$/', $ccode)) {
    $ccode = forge_get_config('default_country_code');
}

use_javascript('jquery.validate.min.js');
$JS='$(function(){
    $.validator.addMethod("no-caps", function(value){
        return (value.match(/[A-Z]/)===null);
    }, "The username cannot have capital letters");

    $.validator.addMethod("password-match", function(){
        if ($("#password1").val()===$("#password2").val()){
            $(".error[for=password2]").remove();
            $(".error[for=password1]").remove();
            return true;
        }else{
            return false;
        }
    }, "The passwords must match");

    $("#form").validate({
        rules:{
            email:{
                email:true
            },
            unix_name:{
                "no-caps":true
            },
            password1:{
                minlength: 6,
                "password-match":true
            },
            password2:{
                minlength:6,
                "password-match":true
            }
        }
    });
});';

add_js($JS);

site_header(array('title'=>'Register a New Account'));
$Layout->col(12,true);
?>

<h1>You're one step away from joining the <?php echo forge_get_config('forge_name'); ?> Community!</h1>

<br/><form action="<?php echo util_make_url('/account/register2.php'); ?>" method="post" id="form" class="form-horizontal">
    <input type="hidden" name="form_key" value="<?php echo form_generate_key(); ?>"/>
    <input type="hidden" name="activate_immediately" value="1" />

    <fieldset>
        <legend>Account Settings</legend>

        <div class="control-group">
            <label class="control-label" for="email">Email Address <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="email" class="input-xlarge required" name="email" id="email" style="display: inline;" autocomplete="off" <?=($Submit)?'value="'.$email.'"':''?> />
                <p style="display: inline-block;" class="help-block">so we can verify your account and help you reset your password if you forget it</p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="unix_name">Username <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="text" class="input-xlarge required" name="unix_name" id="unix_name" style="display: inline;"autocomplete="off" <?=($Submit)?'value="'.$unix_name.'"':''?> />
                <p style="display: inline-block;" class="help-block"> do not use uppercase letters</p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password1">Password <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="password" class="input-xlarge required" name="password1" id="password1" style="display: inline;" autocomplete="off" />
                <p style="display: inline-block;" class="help-block"> min length: 6 characters</p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="password2">Verify Password <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="password" class="input-xlarge required" name="password2" id="password2" style="display: inline;" autocomplete="off" />
                <p style="display: inline-block;" class="help-block"> min length: 6 characters</p>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>About You</legend>
        <div class="control-group">
            <label class="control-label" for="firstname">First Name <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="text" class="input-xlarge required" name="firstname" id="firstname" style="display: inline;" autocomplete="off" <?=($Submit)?'value="'.$firstname.'"':''?> />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="lastname">Last Name <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="text" class="input-xlarge required" name="lastname" id="lastname" style="display: inline;" autocomplete="off" <?=($Submit)?'value="'.$lastname.'"':''?> />
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="ccode">Country</label>
            <div class="controls">
                <?= html_get_ccode_popup('ccode', $ccode) ?>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="language_id">Language</label>
            <div class="controls">
                <?= html_get_language_popup ('language_id', language_name_to_lang_id (choose_language_from_context ())) ?>
            </div>
        </div>
    </fieldset>

    <fieldset>
        <legend>Email Preferences</legend>
        <div class="control-group">
            <label class="control-label" for="mail_site">Site Updates</label>
            <div class="controls">
                <input type="checkbox" name="mail_site" id="mail_site" value="0" autocomplete="off" />
                <p style="display: inline-block;" class="help-block">get notified about site updates <i>(low traffic and includes security notices)</i></p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="mail_va">Community Mailing</label>
            <div class="controls">
                <input type="checkbox" name="mail_va" id="mail_va" value="0" autocomplete="off" />
                <p style="display: inline-block;" class="help-block">receive additional community mailings <i>low traffic)</i></i></p>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="accept_conditions">Accept Terms <?= utils_requiredField() ?></label>
            <div class="controls">
                <input type="checkbox" name="accept_conditions" id="accept_conditions" value="1" class="required" autocomplete="off"  />
                <p style="display: inline-block;" class="help-block">do you accept the <a href="/terms.php">terms of use</a> for this site?</p>
            </div>
        </div>
    </fieldset>

    <div class="form-actions">
        <input type="submit" name="submit" class="btn btn-primary" value="Register Now"> fields marked with an <?= utils_requiredField() ?> are required

        <br/><br/><h4>Still waiting for your email confirmation?</h4>
        <a href="pending-resend.php">Resend confirmation email to a pending account</a>
    </div>
</form>

<?php
$Layout->endcol();
site_footer();
?>