<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once APP_PATH.'common/include/BsForm.class.php';

$JS='$(function(){
    var $toggle_add_button=$("#toggle_add_button"),
        $add_area=$("#section_add_key"),
        $add_new=$("#add_new_button"),
        $cancel=$("#cancel"),
        $title=$("#title"),
        $key=$("#key"),
        $delete_button=$(".delete");

    $toggle_add_button.click(function(){
        $add_area.slideToggle();
    });

    $cancel.click(function(){
        $add_area.slideUp();
    });

    $add_new.click(function(){
        $.post("ajax/add_publickey.php",{title:$title.val(),key:$key.val()});
    });

    $(".delete").click(function(){
        var $me=$(this);
        $.ajax({
            url         : "ajax/remove_publickey.php",
            dataType    : "json",
            type        : "post",
            data        : {
                keyid   : $me.data("keyid")
            }
        }).done(function(msg){
            if (msg.error==false){
                $me.parent().parent().slideUp().remove();
            }
        });
    });
});';
add_js($JS);

site_user_header(array('title'=>_('Edit Public Keys')));
$Layout->col(12,true);

$HTML->tertmenu_add('Account Settings','/account/');
$HTML->tertmenu_add('Public Profile',session_get_user()->getURL());
if(forge_get_config('use_people'))
    $HTML->tertmenu_add('Edit Skills','/people/editprofile.php');
$HTML->tertmenu_add('Edit Servers','/account/servers.php');
$HTML->tertmenu_add('Edit Public Keys','/account/publickeys.php');
echo $HTML->tertiary_menu(4);

$Keys=session_get_user()->getPublicKeys();

//if (!$Keys){
    $AddKey=new Button('Add New',array('id'=>'toggle_add_button'));
    echo $AddKey->render();
//}

echo '<div id="section_add_key" class="hidden">';
$Form=new BsForm;
echo $Form->init('javascript:void(0)','post',array('class'=>'form form-horizontal'))
    ->group('Title',new Text(array(
        'id'=>'title'
    )))
    ->group('Key',new Textarea('',array(
        'id'=>'key'
    )))
    ->actions(
        new Submit('Add Key',array(
            'id'=>'add_new_button',
            'class'=>'btn-primary'
        )),
        new Reset('Cancel',array(
            'id'=>'cancel'
        ))
    )
    ->render();
echo '</div>';

$Table=new BsTable(array('class'=>'table table-striped'));
$Table->head(array('Title','Actions'));

if ($Keys){
    foreach($Keys as $i)
        $Table->col($i->getTitle())
            ->col('<button class="btn btn-primary delete" data-keyid="'.$i->getID().'">Delete</button>');
}else{
    $Table->col('No keys found')->col('');
}
echo $Table->render();

$Layout->endcol();
site_user_footer();
?>