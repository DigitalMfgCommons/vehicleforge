<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$Return=array('error'=>false);
$ParentMessageID=getIntFromRequest('mid');
$Message=new UserMessage($ParentMessageID);
if ($NewMessageID=$Message->reply(getStringFromRequest('message'), session_get_user()->getID())){
    $NewMessage=new UserMessage($NewMessageID);
    $Return['html']=$NewMessage->formatReply(session_get_user()->getID());
}

echo json_encode($Return);
?>
