<?php
require_once '../../env.inc.php';
require_once APP_PATH.'common/include/pre.php';

$Servers=getStringFromRequest('servers');
$Servers=json_decode($Servers);

foreach($Servers as $i){
    $S=new Server($i->id);
    $S->update(user_getid(),$i->alias,$i->url);
}
?>