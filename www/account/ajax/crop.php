<?php
require_once('../../env.inc.php');
require_once $gfcommon.'include/pre.php';

$File=$_POST['file'];
$Me=session_get_user()->getUnixName();
$DestFile='profile_image-'.time().'.jpg';
if (!file_exists('/usr/share/gforge/image_store/profile_images/'.$Me)) {
	mkdir('/usr/share/gforge/image_store/profile_images/'.$Me, 0777, true);
}
$Destination='/usr/share/gforge/image_store/profile_images/'.$Me.'/'.$DestFile;
$targ_w = $targ_h = 100;
$jpeg_quality = 95;

$src = '/usr/share/gforge/www'.$File;
$img_r = imagecreatefromjpeg($src);
$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);

$Return=array('error'=>false);

if (!imagejpeg($dst_r, $Destination, $jpeg_quality)){
	$Return['error']=true;
}else{
	$Return['image']=$DestFile;
}

//Remove temporary file
unlink($src);

echo json_encode($Return);
return true;
?>