<?php
require_once '../env.inc.php';
require_once APP_PATH.'common/include/pre.php';
require_once EMAIL_CONFIG_INC;

if (getStringFromRequest('forgot')){
    $Login=getStringFromRequest('login');
    $User=null;

    if (user_get_object_by_name($Login)){
        $User=user_get_object_by_name($Login);
    }elseif(user_get_object_by_email($Login)){
        $User=user_get_object_by_email($Login);
    }

    if ($User){
        $RandPass=util_generate_password();
        $User->setPasswd($RandPass);
        $User->sendEmail($site_admin_address, "VehicleForge Password Reset", "Your password has been reset to: ".$RandPass);
        $feedback=_('Your password has been reset. Check the email linked to this account to access your account.');
    }
}

site_header(array('title'=>'Forgot Password'));
$Layout->col(12,true);

$Form=new BsForm();
echo $Form->init(getStringFromServer('php_self'),'post',array('class'=>'form form-horizontal'))
    ->head(_('Password Retrieval'))
    ->group(_('Username or Email'),
        new Text(array(
            'name'=>'login'
        ))
    )
    ->actions(
        new Submit('Submit',array(
            'name'=>'forgot',
            'value'=>1
        )),
        new Reset()
    )
    ->render();

$Layout->endcol();
site_footer();
?>