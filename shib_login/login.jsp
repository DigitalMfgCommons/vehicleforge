<html>
<head>
    <title>VehicleForge: Login</title>
    <link rel="stylesheet" type="text/css" href="http://vfdev.geinspire.com/themes/css/bootstrap/bootstrap.css">
    <style>
        .page {
            background-color: #FFF;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            -o-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 2px 8px 2px #BBB;
            -moz-box-shadow: 0 2px 8px 2px #BBB;
            -o-box-shadow: 0 2px 8px 2px #BBB;
            box-shadow: 0 2px 8px 2px #BBB;
            padding: 15px 20px;
            text-align: left;
        }

        .container{
            width: 500px;
        }

        body    {
            background:url(http://vfdev.geinspire.com/themes/gforge/images/layout/gearbg.png) no-repeat scroll center 15px #808080;
            padding-top: 200px;
            text-align: center;
        }

        input   {
            height: 26px;
        }

        .form-actions   {
            background: none;
            margin: 0;
            border: 0;
            padding: 0 0 0 160px;
        }
    </style>
</head>
<body id="homepage">
<div class="container">
    <div class="page">
        <% if(request.getAttribute("actionUrl") != null){ %>
        <form action="<%=request.getAttribute("actionUrl")%>" method="post" class="form form-horizontal">
        <% }else{ %>
        <form action="j_security_check" method="post" class="form form-horizontal">
            <% } %>
            <fieldset><legend>VehicleForge Login</legend>
                <% if ("true".equals(request.getAttribute("loginFailed"))) { %>
                <div class="alert alert-error">
                    Credentials not recognized
                </div>
                <% } %>
                <div class="control-group">
                    <label class="control-label" for="username">Username</label>
                    <div class="controls">
                        <input type="text" autocapitalize="off" id="username" name="j_username" />
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="username">Password</label>
                    <div class="controls">
                        <input type="password" id="password" name="j_password" />
                    </div>
                </div>
                <div class="form-actions">
                    <input type="submit" value="Log In" class="btn btn-primary" />
                </div>
            </fieldset>
        </form>
        <div>
            <a href="http://vfdev.geinspire.com/account/forgot_password.php">Forgot your password?</a><br />
            <a href="http://vfdev.geinspire.com">Back to VehicleForge</a>
        </div>
    </div>
</div>
</body>
</html>