<?php
require_once CONFIG_PATH.'db.inc';
require_once CONFIG_PATH.'git.inc';

$fusionforge_basedir = dirname(__DIR__);
define('GF_IMAGE_STORE', APP_PATH.'image_store/');

$gfcgfile = '/etc/gforge/local.inc';
$gfconfig = '/etc/gforge/';
$gf_db_config='/etc/gforge/db.inc';
$gf_git_config='/etc/gforge/git.inc';

$gfcommon = $fusionforge_basedir.'/common/';
$gfwww = $fusionforge_basedir.'/www/';
$gfplugins = $fusionforge_basedir.'/plugins/';
$gfimagestore=$fusionforge_basedir.'/image_store/';
?>