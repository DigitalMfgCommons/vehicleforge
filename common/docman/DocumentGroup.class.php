<?php
require_once $gfcommon.'include/Error.class.php';

class DocumentGroup extends Error {
	private $DocGroupID, $Data, $Debug;

	/**
	 * @param null|int $DocGroupID
	 * @param bool $Debug
	 */
	function __construct($DocGroupID=null, $Debug=false){
		if ($DocGroupID){
			$this->DocGroupID=$DocGroupID;

			$Result=db_query_params("SELECT * FROM doc2_groups WHERE doc_group_id=$1",array($DocGroupID));
			$this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
		}

		$this->Debug=$Debug;
	}

	function getID(){
		return $this->Data['doc_group_id'];
	}

	/**
	 * @return string
	 */
	function getName(){
		return $this->Data['group_name'];
	}

	/**
	 * @return int
	 */
	function getOwnerID(){
		return $this->Data['owner_id'];
	}

	function getOwnerGroupID(){
		return $this->Data['group_id'];
	}

	/**
	 * @return string
	 */
	function getOwnerName(){

	}

	function getOwnerGroupName(){
		$Group=group_get_object($this->getOwnerGroupID());
		return $Group->getUnixName();
	}

	/**
	 * @return string
	 */
	function getDescription(){
		return $this->Data['description'];
	}

	/**
	 * create() - Creates a folder in a given group folder and a row in the DB
	 * @param string $DocGroupName
	 * @param int $GroupID
	 * @param string $Description
	 * @param int $OwnerID
	 * @param null|int $ParentGroupID
	 * @return bool
	 */
	function create($DocGroupName, $GroupID, $Description, $OwnerID, $ParentGroupID=null){
		$Group=group_get_object($GroupID);

		$ParentDirectoryPath=GF_IMAGE_STORE.'project_files/'.$Group->getUnixName();

		if (!is_dir($ParentDirectoryPath.'/'.$DocGroupName)){
			if (mkdir($ParentDirectoryPath.'/'.$DocGroupName)){
				if ($Result=db_query_params("INSERT INTO doc2_groups(owner_id,group_id,description,parent_group_id,group_name) VALUES($1,$2,$3,$4,$5)",array($OwnerID,$GroupID,$Description,$ParentGroupID,$DocGroupName))){
					$Newest=db_insertid($Result,"doc2_groups","doc_group_id");
					return new DocumentGroup($Newest);
				}else{
					rmdir($ParentDirectoryPath.'/'.$DocGroupName);
					$this->setError("Unable to create DocGroup in database");
				}
			}else{
				$Error="Unable to create DocGroup directory";
				if ($this->Debug)
					$Error.=" (".$ParentDirectoryPath.'/'.$DocGroupName.")";
				$this->setError($Error);
			}
		}else{
			$this->setError("DocGroup already exists");
		}

		return false;
	}

	function getContents($GroupID,$DocGroupID,$Limit=null){
		$QueryString="SELECT * FROM doc2_files WHERE group_id=$1 AND doc_group_id=$2";
		if ($Limit)
			$QueryString.=" LIMIT ".$Limit;

		$Result=db_query_params($QueryString,array($GroupID,$DocGroupID));
		return util_result_columns_to_array($Result);
	}
}
?>
