<?php
class Server extends Error  {
	private $ID, $Data=null;

	function __construct($ID=null){
        $this->ID=$ID;
	}

	function fetch_data(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM servers WHERE server_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
	}

	function getID(){
		return $this->ID;
	}

	function getURL(){
        $this->fetch_data();

		return $this->Data['url'];
	}

    function getAlias(){
        $this->fetch_data();

        return $this->Data['alias'];
    }

    /**
     * Creates a new server for a user
     *
     * @static
     * @param $URL
     * @param $UserID
     * @param string $Alias
     * @return bool|int
     */
	static function create($URL, $UserID, $Alias=''){
		if ($Result=db_query_params("INSERT INTO servers(url, user_id, alias) VALUES($1,$2,$3)", array($URL, $UserID, $Alias))){
            return (int)db_insertid($Result,'servers','server_id');
		}else{
			return false;
		}
	}

	static function checkOK($URL){
		//require_once $GLOBALS['gfcommon'].'include/DOMEApi.class.php';
		$DOMEApi=new DOMEApi($URL,false);
		if ($DOMEApi->getChildren()){
			return true;
		}else{
			return false;
		}
	}

    function update($UserID, $Alias, $URL){
        if ($this->ID){
            return db_query_params('UPDATE servers SET user_id=$1, url=$2, alias=$3 WHERE server_id=$4',array($UserID,$URL,$Alias,$this->getID()));
        }
    }
}
?>
