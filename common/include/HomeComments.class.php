<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:15 AM 11/29/11
 */

class HomeComments extends Error  {
	private $RefID, $TypeID, $Ref;

	function __construct($RefID,$Type){
		$this->RefID=$RefID;
		switch($Type){
			case 'user':
			case 1:
				$this->TypeID=1;
				$this->Ref=user_get_object($RefID);
				break;

			case 'project':
			case 2:
				$this->TypeID=2;
				$this->Ref=group_get_object($RefID);
				break;

			default:
				$this->setError(_("Unknown type in HomeComments"));
				return false;
		}

		return true;
	}

    /**
     * displayForm - Displays the form necessary for posting on user/project home pages
     * TypeID of 1 is for users
     * TypeID of 2 is for projects
     *
     * @param int $ObjectID
     * @param int $TypeID
     * @return void
     */
    static function displayForm($ObjectID,$TypeID){
        if (session_loggedin()){
            if ($ObjectID && $TypeID && $ObjectID!=user_getid()){
                echo '<div class="clearfix">
						<div class="feed-content">
							<div class="message">
								<textarea rows="3" cols="50" id="home_comment_text" placeholder="';

                switch($TypeID){
                    case 1:
                        $User=user_get_object($ObjectID);
                        echo 'Share your thoughts with '.$User->getRealName();
                        break;

                    case 2:
                        echo 'Share your thoughts on this project';
                        break;
                }

                echo '" autocomplete="off"></textarea>
								</div>
							</div>
							<div class="feed-comment clearfix">
								<input type="button" id="home_comment_submit" class="btn primary pull-right" value="Send" data-rid="'.$ObjectID.'" data-tid="'.$TypeID.'" />
							</div>
					</div><hr />';
            }
        }
    }

    /**
     * edit - Edits a single comment
     *
     * @static
     * @param int $ActivityID
     * @param string $Content
     * @return bool
     */
    static function edit($ActivityID, $Content){
        if ($Result=db_query_params("SELECT * FROM site_activity WHERE activity_id=$1",array($ActivityID))){
            $Activity=db_fetch_array($Result,null,PGSQL_ASSOC);
            if ($Activity['user_id']==user_getid()){
                if ($Activity['type_id']==1 || $Activity['type_id']==2){
                    return db_query_params("UPDATE home_comments SET comment=$1 WHERE comment_id=$2",array($Content,$Activity['ref_id']));
                }
            }
        }

        return false;
    }

    /**
     * delete - Deletes the post, the subcomments, and the activity associated with it
     *
     * @static
     * @param int $ActivityID
     * @return bool
     */
    static function delete($ActivityID){
        db_begin();
        if ($Result=db_query_params("SELECT * FROM site_activity WHERE activity_id=$1",array($ActivityID))){
            $Activity=db_fetch_array($Result);
            if ($Activity['user_id']==user_getid()){
                if (db_query_params("DELETE FROM home_comments WHERE comment_id=$1",array($Activity['ref_id']))){
                    if (db_query_params("DELETE FROM site_activity WHERE activity_id=$1",array($ActivityID))){
                        if (db_query_params("DELETE FROM site_activity_comments WHERE site_activity_id=$1",array($ActivityID))){
                            db_commit();
                            return true;
                        }
                    }
                }
            }
        }

        db_rollback();
        return false;
    }

	/**
     * post - Creates a new comment
     *
     * @param int $UserID
     * @param string $Comment
     * @return array
     */
	function post($UserID,$Comment){
        $Return=array('error'=>false);

        if (!is_int($UserID) || !is_string($Comment)){
            $Return['error']=true;
            $Return['msg']='Wrong argument types when posting. '.var_dump($UserID).'|'.var_dump($Comment);
            return $Return;
        }

		if ($Res=db_query_params("INSERT INTO home_comments(ref_id,type_id,time_posted,user_id,comment) VALUES($1,$2,$3,$4,$5)",array($this->RefID,$this->TypeID,time(),$UserID,$Comment))){
			$Return['last_id']=db_insertid($Res,'home_comments','comment_id');

			//$Return['activity_id']=$SiteActivityObject->create($UserID,$this->TypeID,$this->RefID,$Return['last_id']);
			$Return['activity_id']=SiteActivity::create($UserID,$this->TypeID,$this->RefID,$Return['last_id']);
		}else{
			$Return['error']=true;
            $Return['msg']='Unable to create row in comments table';
		}

		return $Return;
	}
}
?>