<?php

    function returnByteValue($value) {
        $unit = substr($value, -1);
        $value = substr($value, 0, -1);
        switch($unit) {
            case 'G':
	      return $value*pow(1024, 3);
            case 'M':
	      return $value*pow(1024, 2);
            case 'K':
                return $value*1024;
            default:
                return $value;

        }
    }
    
?>