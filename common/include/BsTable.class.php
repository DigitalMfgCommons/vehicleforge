<?php
class BsTable   {
    private $Code='', $Cols=0, $CurCol=0, $Rows=0;

    private function parseAttribs($Attribs){
        $C='';
        foreach($Attribs as $key=>$val)
            $C.=' '.$key.'="'.$val.'"';

        return $C;
    }

    function __construct($Attribs=array()){
        $this->Code.='<table'.$this->parseAttribs($Attribs).'>';
    }

    function head($Headings=array()){
        $this->Code.='<thead>';
        foreach($Headings as $key=>$val){
            $this->Cols++;
            $this->Code.='<th';
            if (is_array($val)){
                $this->Code.=$this->parseAttribs($val).'>'.$key.'</th>';
            }else{
                $this->Code.='>'.$val.'</th>';
            }
        }

        $this->Code.='</thead><tbody>';

        return $this;
    }

    function col($Content='', $Attribs=array()){
        if (isset($Attribs['colspan'])){
            $this->CurCol+=$Attribs['colspan'];
        }else{
            $this->CurCol++;
        }

        if ($this->CurCol>$this->Cols)
            $this->CurCol=1;

        if ($this->CurCol==1){
            $this->Code.='<tr>';
            $this->Rows++;
        }

        $this->Code.='<td'.$this->parseAttribs($Attribs).'>'.$Content.'</td>';

        if ($this->CurCol==$this->Cols)
            $this->Code.='</tr>';

        return $this;
    }

    function render(){
        $this->Code.='</tbody></table>';

        if ($this->CurCol<$this->Cols && $this->Rows!=0)
            echo '<b style="color:red">Missing column in table</b>';

        return $this->Code;
    }
}
?>