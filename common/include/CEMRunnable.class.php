<?php
/**
 * @deprecated
 */
class CEMRunnable extends Error   {
	private $Types=array(
		1   =>  'dome_interfaces'
	), $Indexes=array(
		1   =>  'interface_id'
	), $ID, $Data;

	function __construct($ID=null){
		if ($ID){
			$this->ID=$ID;
			$this->fetch_data();
		}
	}

	function fetch_data(){
		$Result=db_query_params("SELECT * FROM cem_runnables WHERE runnable_id=$1",array($this->getID()));
		$this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);

		$Result=db_query_params("SELECT * FROM ".$this->Types[$this->getTypeID()]." WHERE ".$this->Indexes[$this->getTypeID()]."=$1", array($this->getRefID()));
		$Row=db_fetch_array($Result,null,PGSQL_ASSOC);
		switch($this->getTypeID()){
			case 1:
				$_name=json_decode($Row['interface_data']);
				$this->Data['name']=$_name->modelDef->name;
				break;
		}
	}

	function getID(){
		return $this->ID;
	}

	/**
	 * @return mixed
	 *
	 * @deprecated Not necessary
	 */
	function getTypeID(){
		return $this->Data['type_id'];
	}

	function getRefID(){
		return $this->Data['ref_id'];
	}

	function getName(){
		return $this->Data['name'];
	}

	function getCEMID(){
		return $this->Data['cem_id'];
	}

	protected function create($CemID, $TypeID, $RefID){
		if ($Result=db_query_params("INSERT INTO cem_runnables(cem_id, type_id, ref_id) VALUES($1,$2,$3)", array($CemID, $TypeID, $RefID))){
			return db_insertid($Result,'cem_runnables','runnable_id');
		}else{
			$this->setError('Unable to create cem_runnable');
			return false;
		}
	}
}
?>