
<?php
class SystemCategory extends Error   {
	private $ID, $Data;

	function __construct($ID=null) {
		if ($ID){
			$this->ID=$ID;
			$this->fetchData();
		}
	}

	private function fetchData(){
		$Result=db_query_params("SELECT * FROM service_categories WHERE cat_id=$1", array($this->getID()));
		$this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
	}

	function getID(){
		return $this->ID;
	}

	function getName(){
		return $this->Data['name'];
	}

	function getParentID(){
		return $this->Data['parent_id'];
	}
	
	static function find_or_create($Name, $ParentID=0) {
		$Result = db_query_params("SELECT * FROM service_categories WHERE name = $1", array($Name));
		//if $Result
	}
	
	static function create($Name, $ParentID=0){
		db_query_params("INSERT INTO service_categories(name,parent_id) VALUES($1,$2)", array($Name, $ParentID));
	}

	/**
	 * getAll - Gets all service categories
	 * Table columns:
	 *      cat_id
	 *      name
	 *      parent_id   -   If 0, category is a top-level category
	 *
	 * @static
	 * @return array
	 */
	static function getAll(){
		$Result=db_query_params("SELECT * FROM service_categories");
		return util_result_columns_to_array($Result);
	}
}
?>