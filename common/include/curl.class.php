<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:48 AM 8/30/11
 */

class curl extends Error{
    private $Curl, $Files=array(), $Data=array();

    /**
     * @param string $URL
     * @return curl
     */
    function __construct($URL=null){
        $this->Curl=curl_init($URL);
        return $this->Curl;
    }

    /**
     * curlCheck - Checks if the cURL extension is loaded into Apache
     *
     * @return bool
     */
    function curlCheck(){
        if (extension_loaded('curl')){
            return true;
        }else{
            $this->setError('cURL extension not loaded');
            return false;
        }
    }

    /**
     * format_data - Formats the data to be passed in CURLOPT_POSTFIELDS
     *
     * @param array $DataArray
     * @return bool|string
     */
    public function format_data($DataArray=array('key'=>'value')){
        if (is_array($DataArray)){
            if (array_keys($DataArray) !== range(0, count($DataArray) - 1)){
                $Data="";
                foreach($DataArray as $key=>$value)
                    $Data.=$key.'='.$value.'&';

                $Data=substr($Data,0,strlen($Data)-1);

                return $Data;
            }else{
                $this->setError('DataArray is not an associative array');
                return false;
            }
        }else{
            $this->setError('DataArray is not an array');
            return false;
        }
    }

    /**
     * @param $OptionsArray
     * @return bool
     */
    public function opts($OptionsArray){
        if (is_array($OptionsArray)){
            if (array_keys($OptionsArray) !== range(0, count($OptionsArray) - 1)){
                return curl_setopt_array($this->Curl,$OptionsArray);
            }else{
                $this->setError('DataArray is not an associative array');
                return false;
            }
        }else{
            $this->setError('DataArray is not an array');
            return false;
        }
    }

    // sends message using get
    public function exec($Return=true){
        if ($this->Data){
            if ($this->Files){
                foreach($this->Files as $f){
                    $this->Data[ $f['name']]='@'.$f['tmp_name'];
                }
            }
            curl_setopt($this->Curl, CURLOPT_POSTFIELDS, $this->format_data($this->Data));
        }

        curl_setopt($this->Curl, CURLOPT_RETURNTRANSFER,$Return);
        curl_setopt($this->Curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($this->Curl, CURLOPT_POST, false);
	//		curl_setopt($this->Curl, CURLOPT_HTTPHEADER, array('Content-Type: text/html; charset=utf-8'));
        curl_setopt($this->Curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)");
        return curl_exec($this->Curl);
    }

    // sends message using post
    public function execPost($Return=true){
        if ($this->Data){
            if ($this->Files){
                foreach($this->Files as $f){
                    $this->Data[ $f['name']]='@'.$f['tmp_name'];
                }
            }
            curl_setopt($this->Curl, CURLOPT_POSTFIELDS, $this->format_data($this->Data));
        }

        curl_setopt($this->Curl, CURLOPT_RETURNTRANSFER,$Return);
        curl_setopt($this->Curl, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($this->Curl, CURLOPT_POST, true);
	//		curl_setopt($this->Curl, CURLOPT_HTTPHEADER, array('Content-Type: text/html; charset=utf-8'));
        curl_setopt($this->Curl, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)");
        return curl_exec($this->Curl);
    }
    
    /**
     * setURL - Override the URL for this call and set to CURLOPT_URL
     *
     * @param string $url
     * @return bool
     */
    public function setURL($url) {
    	curl_setopt($this->Curl,CURLOPT_URL,$url);
    	return true;
    }

    public function info(){
        $Data=curl_getinfo($this->Curl);
        foreach ($Data as $key=>$value){
            echo $key.'='.$value.'<br />';
        }
    }

    public function addFile($FilePath){
        $this->Files[]=$FilePath;
        return count($this->Files)-1;
    }

    public function data($Data){
        $this->Data=array_merge($Data, $this->Data);
    }

    public function error(){
        echo '<b>CURL Error (#'.curl_errno($this->Curl).'): </b>'.curl_error($this->Curl);
    }

    function __destruct(){
        curl_close($this->Curl);
    }
}
?>
