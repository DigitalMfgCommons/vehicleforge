<?php

class TextMessage extends Error   {
	private $Gateways=array();

	function __construct(){
		if (!$this->Gateways){
			$this->fetchGateways();
		}
	}

	private function fetchGateways(){
		$Result=db_query_params("SELECT * FROM cell_gateways");
		$this->Gateways=util_result_columns_to_array($Result, PGSQL_ASSOC);
		return true;
	}

	function getGateways(){
		return $this->Gateways;
	}
	
	function getGatewayByID($GatewayID){
		foreach($this->Gateways as $i){
			if ($i['gateway_id']==$GatewayID){
				return $i;
			}
		}

		return false;
	}

	function send($Number, $Carrier, $Message, $From, $FromName){
        //require_once $GLOBALS['gfcommon'].'phpmailer/phpmailer.inc.php';

        $Mail=new phpmailer();
        $Mail->IsSMTP();
        $Mail->AddAddress($Number.'@'.$Carrier);
        $Mail->From=$From;
        $Mail->FromName=$FromName;
        $Mail->Body=$Message;
        $Mail->IsHTML(false);
        return $Mail->Send();

		/*$To=$Number.'@'.$Carrier;
		$Headers = 'From: VehicleForge.com <VehicleForge.com>' . "\r\n" .
			"Content-type: text/plain; charset=utf-8\r\n".
		    'X-Mailer: PHP/' . phpversion();
		$Additional='-f VehicleForge.com';

		if (mail($To,"", $Message, $Headers, $Additional)){
			return true;
		}else{
			$this->setError('Unable to send mail');
			return false;
		}*/
	}
}
?>