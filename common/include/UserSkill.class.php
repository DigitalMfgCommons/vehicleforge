<?php
class UserSkill    {
    private $ID, $Data=null;

    static private $ExperienceLevels=array(
        1=>'Novice',
        2=>'Beginner',
        3=>'Intermediate',
        4=>'Advanced',
        5=>'Expert'
    );

    public function __construct($ID=null){
        $this->ID=$ID;
    }

    private function fetchData(){
        if ($this->ID!==null && $this->Data===null){
            $Result=db_query_params("SELECT * FROM user_skills WHERE user_skill_id=$1",array($this->getID()));
            $this->Data=db_fetch_array($Result,null,PGSQL_ASSOC);
        }
    }

    public function getID(){
        return $this->ID;
    }

    /**
     * Adds a skill to a user
     *
     * @static
     * @param int $UserID
     * @param string $SkillName
     * @param array $Keywords
     * @param int $ExperienceLevel
     * @return bool|int
     */
    static function create($UserID, $SkillName, $Keywords, $ExperienceLevel){
        if ($Check=self::getBySkillName($SkillName, $UserID)){
            //Already exists
            return true;
        }else{
            db_begin();
            if($Result=db_query_params("INSERT INTO user_skills(user_id, user_skill_name, experience_level) VALUES($1, $2, $3)",array($UserID, $SkillName, $ExperienceLevel))){
                $SkillID=(int)db_insertid($Result,'user_skills','user_skill_id');
                if (!self::setKeywords($SkillID, $Keywords)){
                    db_rollback();
                    return false;
                }

                db_commit();
                return $SkillID;
            }

            db_rollback();
            return false;
        }
    }

    /**
     * Gets data based on a skill name
     * Returns:
     *      If nobody has the $SkillName:           false
     *      If one person has the $SkillName:       (int) user_skill_id
     *      If multiple people have the $SkillName: (array) user_skill_id
     *
     * @static
     * @param string $SkillName
     * @param null|int $UserID
     * @return array|bool|int
     */
    static function getBySkillName($SkillName, $UserID=null){
        $QB=new QueryBuilder;
        $QB->addSql("SELECT user_skill_id FROM user_skills WHERE ")
            ->addParam("user_skill_name", $SkillName);

        if ($UserID){
            $QB->addSql(" AND ")
                ->addParam("user_id", $UserID);
        }

        if($Result=db_query_params($QB->getQuery(), $QB->getParams())){
            if ($Count=db_numrows($Result)){
                if ($Count>1){
                    $Return=array();
                    while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                        $Return[]=$Row['user_skill_id'];

                    return $Return;
                }else{
                    return (int)db_result($Result,0,'user_skill_id');
                }
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Gets a list of user skills for a particular user
     * Returns an array of UserSkill objects
     *
     * @static
     * @param int $UserID
     * @return array|bool
     */
    static function getByUserID($UserID){
        if($Result=db_query_params("SELECT user_skill_id FROM user_skills WHERE user_id=$1",array($UserID))){
            $Return=array();
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=new UserSkill($Row['user_skill_id']);

            return $Return;
        }else{
            return false;
        }
    }

    /**
     * Gets the skill name of this skill
     *
     * @return string
     */
    public function getSkillName(){
        $this->fetchData();
        return $this->Data['user_skill_name'];
    }

    /**
     * Gets the user ID this skill is linked to
     *
     * @return int
     */
    public function getUserID(){
        $this->fetchData();
        return (int)$this->Data['user_id'];
    }

    /**
     * Gets the experience level of this skill
     * if $Text is true, it returns the text value for a experience level
     *
     * @param bool $Text
     * @return bool
     */
    public function getExpLevel($Text=true){
        $this->fetchData();
        if ($Text){
            return (isset(self::$ExperienceLevels[$this->Data['experience_level']]))?
                self::$ExperienceLevels[$this->Data['experience_level']]:
                false;
        }else{
            return $this->Data['experience_level'];
        }
    }

    /**
     * Updates a user skill with new data
     *
     * @param $SkillName
     * @param $Keywords
     * @param $ExperienceLevel
     * @return bool
     */
    public function update($SkillName, $Keywords, $ExperienceLevel){
        db_begin();
        if(db_query_params("UPDATE user_skills SET user_skill_name=$1, experience_level=$2 WHERE user_skill_id=$3",array($SkillName, $ExperienceLevel, $this->getID()))){
            if(!$this->setKeywords($this->getID(), $Keywords)){
                db_rollback();
                return false;
            }
        }

        db_commit();
        return true;
    }

    /**
     * Set the keywords for a skill
     *
     * @static
     * @param $SkillID
     * @param $Keywords
     * @return bool
     */
    static private function setKeywords($SkillID, $Keywords){
        //Remove all links to keywords
        if (db_query_params("DELETE FROM user_skills_keywords_join WHERE user_skill_id=$1",array($SkillID))){
            //Replace all keywords
            $KeywordIDs=array();
            foreach($Keywords as $i)
                $KeywordIDs[]=SkillKeyword::create($i);

            foreach($KeywordIDs as $i){
                if (!db_query_params("INSERT INTO user_skills_keywords_join(user_skill_id, skill_keyword_id) VALUES($1, $2)",array($SkillID, $i))){
                    return false;
                }
            }

            return true;
        }else{
            return false;
        }
    }

    /**
     * Gets all the keywords associated with this user skill
     * Returns an array of strings if successful
     *
     * @return array|bool
     */
    public function getKeywords(){
        if($Result=db_query_params("SELECT * FROM user_skills_keywords_join, skill_keywords WHERE user_skills_keywords_join.user_skill_id=$1 AND skill_keywords.skill_keyword_id=user_skills_keywords_join.skill_keyword_id",array($this->getID()))){
            $Return=array();
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=$Row['skill_keyword_name'];

            return $Return;
        }else{
            return false;
        }
    }

    /**
     * Get the experience levels in an associative array
     *
     * @static
     * @return array
     */
    static function getExperienceLevels(){
        return self::$ExperienceLevels;
    }

    /**
     * Removes a skill form a user
     *
     * @static
     * @param $SkillID
     * @return bool
     */
    static public function remove($SkillID){
        db_begin();
        if (db_query_params("DELETE FROM user_skills_keywords_join WHERE user_skill_id=$1",array($SkillID))){
            if (db_query_params("DELETE FROM user_skills WHERE user_skill_id=$1",array($SkillID))){
                db_commit();
                return true;
            }
        }

        db_rollback();
        return false;
    }
}
?>