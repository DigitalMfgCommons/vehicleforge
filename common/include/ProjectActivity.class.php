<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 9:24 AM 12/6/11
 */

class ProjectActivity extends SiteActivity  {
	private $ProjectID;

	function __construct($ProjectID){
		$this->ProjectID=$ProjectID;
	}

    /**
     * getData() - Gets all activity data associated with a project
     * @param bool $AJAX
     * @param int $Limit
     * @return array
     */
	function getData($AJAX=false, $Limit=20){
        if ($AJAX){
            $Return=array();
            $Result=db_query_params("SELECT DISTINCT ON(time_posted,type_id,object_id,ref_id) activity_id FROM site_activity WHERE object_id=$1 AND type_id!=$2 ORDER BY time_posted DESC LIMIT ".$Limit,array($this->ProjectID,1));
            while($Row=db_fetch_array($Result,null,PGSQL_ASSOC))
                $Return[]=(int)$Row['activity_id'];

            return $Return;
        }else{
            $Result=db_query_params("SELECT DISTINCT ON(time_posted,type_id,object_id,ref_id) * FROM site_activity WHERE object_id=$1 AND type_id!=$2 ORDER BY time_posted DESC LIMIT ".$Limit,array($this->ProjectID,1));
            return util_result_columns_to_array($Result);
        }
    }

    /**
     * getContents() - Displays all activity associated with a project
     * @param int $Limit
     * @return void
     */
	function getContent($Limit=20){
		$Data=$this->getData(false,$Limit);

		echo '<div id="activity">';
		foreach($Data as $i){
			echo $this->formatData($i);
		}
		echo '</div>';
	}

    function generateActivityUpdateJS($ObjectID, $TargetElementID, $Timing=10000){
        return parent::generateActivityJS('group', $ObjectID, $TargetElementID, $Timing);
    }
}
?>
