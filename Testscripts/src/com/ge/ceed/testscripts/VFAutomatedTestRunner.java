package com.ge.ceed.testscripts;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.lang.reflect.Array;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Properties;

import org.junit.BeforeClass;
import org.junit.Test;

public class VFAutomatedTestRunner {

	@BeforeClass
	public static void SetupTests() throws IOException {
		Properties props = new Properties();
		props.load(VFAutomatedTestRunner.class.getClassLoader().getResourceAsStream("config/config.properties"));
		
		System.getProperties().put( "proxySet", "true" );
		System.getProperties().put( "proxyHost", props.getProperty("proxy.host") );
		System.getProperties().put( "proxyPort", props.getProperty("proxy.port") );
	}

	@Test
	public void test() throws IOException {
		System.out.println("Starting test...");
		Properties props = new Properties();
		props.load(VFAutomatedTestRunner.class.getClassLoader().getResourceAsStream("config/config.properties"));
		System.out.println("Running test...");
		runTests(props.getProperty("testURL"));
		System.out.println("Getting URLs test...");
		String [] urls = getResults(props.getProperty("resURL"));
		System.out.println("Writing out files test...");
		writeResultFiles(urls, props.getProperty("resURL"));
		System.out.println("End test...");
	}

	private void writeResultFiles(String[] urls, String rURL) {
		
		for (String url : urls){
			try {
				URL urlConn = new URL(rURL + url);

				URLConnection uc = urlConn.openConnection();
				uc.setUseCaches(false);
				BufferedReader in = new BufferedReader(
						new InputStreamReader(
								uc.getInputStream()));
				String inputLine;
				File file = new File("test-reports/" + url);
				Writer output = new BufferedWriter(new FileWriter(file));
				while ((inputLine = in.readLine()) != null) {
					output.write(inputLine);
				}
				output.close();
				in.close();
			}
			catch (Exception ex) {
				dbgMessage(ex.toString());
			}
		}
	}


	private String[] getResults(String resURL2) {
		
		ArrayList<String> ret = new ArrayList<String>();
		
		try {
			URL urlConn = new URL(resURL2);

			URLConnection uc = urlConn.openConnection();
			uc.setUseCaches(false);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							uc.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				if (inputLine.contains("php-results")){	
					for (String ii : inputLine.split("\"")) {
						if (ii.contains("php-results")){
							ret.add(ii);
							break;
						}
					}
				}
			}
			in.close();
		}
		catch (Exception ex) {
			dbgMessage(ex.toString());
		}
		
		return ret.toArray(new String[0]);
	}


	private void runTests(String testURL2) {
		try {
			URL urlConn = new URL(testURL2);

			URLConnection uc = urlConn.openConnection();
			uc.setUseCaches(false);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(
							uc.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) 
				System.out.println(inputLine);
			in.close();
		}
		catch (Exception ex) {
			dbgMessage(ex.toString());
		}
	}
	
	private void dbgMessage(String msg) {
		System.out.println(msg);
		assertTrue(msg, false);
	}

}
