<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 10:58 AM 10/31/11
 */

//Gets the last revision and date of revision of a particular file

require_once 'config.php';

$Repo=$_REQUEST['repo'];
$File=$_REQUEST['file'];

$CMD="svn info file://".$SVNRoot."/".$Repo.$File." | grep 'Last Changed'";
$Return=array();
$Output=array();
exec($CMD,$Output);

$Revision=explode(":",$Output[0]);
$Return['rev']=$Revision[1];

$Date=explode(" ",$Output[1]);
$Return['date']=$Date[3]." ".$Date[4];

echo json_encode($Return);
?>