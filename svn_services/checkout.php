<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 9:55 AM 10/28/11
 */

require_once 'config.php';

/*
 * Zip(string $Source, string $Destination)
 * Zips a folder and its contents to a location
 *
 * @return bool
 */
function Zip($source, $destination){
    if (extension_loaded('zip') === true){
        if (file_exists($source) === true){
            $zip = new ZipArchive();

            if ($zip->open($destination, ZIPARCHIVE::CREATE) === true){
                $source = str_replace('\\', '/', realpath($source));

                if (is_dir($source) === true){
                    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

                    foreach ($files as $file){
                        $file = str_replace('\\', '/', realpath($file));

                        if (is_dir($file) === true){
                            $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
                        }else if (is_file($file) === true){
                            $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
                        }
                    }
                }else if (is_file($source) === true){
                    $zip->addFromString(basename($source), file_get_contents($source));
                }
            }

            return $zip->close();
        }
    }

    return false;
}

$RepoName=$_REQUEST['repo'];

$Repo=$SVNRoot."/".$RepoName;

//Generate a time hash - Prevents duplicate file overwriting
$Date=md5(time());
$FolderName=$RepoName."-".$Date;

$CMD="svn checkout file://".$Repo." ".$SVNCheckouts."/".$FolderName;
//echo $CMD;
$Lines=array();
exec($CMD, $Lines,$Ret);

$Return=array('error'=>true);

if ($Ret!=1){
	//No error on checkout
	//Zip checked out folder to downloadable location
	if (Zip($SVNCheckouts."/".$FolderName,"/var/www/html/svn_checkouts/".$FolderName.".zip")){
		//No error zipping folder
		$Return['error']=false;
		$Return['msg']="http://".$_SERVER['HTTP_HOST']."/svn_checkouts/".$FolderName.".zip";
	}else{
		//Error zipping folder
		$Return['msg']="Error in zipping folder";
	}

	//Remove checked out folder from /svn_checkouts/
	exec("rm -rf ".$SVNCheckouts."/".$FolderName);
}else{
	//Error on checkout
	$Return['msg']="Error in checkout";
}

echo json_encode($Return);
?>