<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 11:29 AM 10/27/11
 */

//Creates a repository given a repo name

require_once 'config.php';

$RepoName=$_REQUEST['repo'];
$Path=$SVNRoot."/".$RepoName;

//Create the repo using the svnadmin command
passthru("$SVNAdminLocation create $Path",$Ret);
//echo "Creating repo...<br />$SVNAdminLocation create $SVNRoot/$RepoName : $Ret";

if ($Ret==0){
	//No errors
	//Create the primary subfolders in the repository (branch, trunk, tags)
	passthru("$SVNLocation mkdir -m'Init' file://$Path/trunk file://$Path/tags file://$Path/branches >/dev/null",$Ret);
	//echo "<br />Creating subfolders...<br />$SVNLocation mkdir -m'Init' file://$Path/trunk file://$Path/tags file://$Path/branches >/dev/null : $Ret";
	if ($Ret!=0){
		echo 'Error in creating repository folders';
	}else{
        echo 1;
    }
}else{
	echo 'Error in creating repository';
}
?>