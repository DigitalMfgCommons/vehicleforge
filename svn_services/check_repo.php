<?php
/**
 * Created By: Jason Kaczmarsky
 * Date: 2:48 PM 10/27/11
 */

//Checks if a repository exists by checking if the folder exists in /svn_root/

require_once 'config.php';

$RepoName=$_REQUEST['repo'];

$Repo=$SVNRoot."/".$RepoName;

//returns true if repo folder and format file exists
echo (is_dir($Repo) && is_file($Repo."/format"))?true:false;
?>